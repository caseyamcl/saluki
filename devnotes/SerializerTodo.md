IMHO this is the trickiest part of the entire application.  There are a number of trade-offs to the two
major approaches:

1. Declarative (via annotations or YAML/XML files)
2. Programmatic (via special classes or some-such)

Making serialization declarative means that we lose the ability to easily add complex logic to the serialization.
For example, a user might be allowed to see a lot of information related to their own records, but very little
information related to other user records.

Making serialization declarative is easier, but then adds a ton of annotations to models, which themselves end
up being cluttered and difficult to read.  Also, you end up getting a lot of out-of-band logic for each thing
that needs to be serialized, which is difficult to test.  Finally, out-of-band semantics require some contortions
for access control.  Who can access which field is often entity-context-specific, and thus requires a bit of
conditional logic.  This logic is difficult to model in a purely declarative form.

All of this came home to me in rcc.wsddd, when I attempted to use a custom annotation-based serializer to allow
entities to specify serialization behavior in comments.  The concept of one data model for both storage and
API quickly broke down under the weight of the details.

I ended up having to create custom serialization access roles for each entity and then try to remember what
those were when writing serialization code.  They were identified in one class (the service class) and implemented
in annotations for the entity!  No good.

It occurred to me that modeling the entity for storage and representing it in the API are two separate concerns.
The latter is still an ongoing challenge to find a way to most efficiently handle.

## Trying to re-invent wheels..

I need to do another survey of available serialization tools.  JMSSerializer (out of band) contains too few
features, and declarative serialization rules don't seem to work in practice.

## Things that the serializer needs to be aware of

Mainly, serializers for a single entity should know:

* A slug (configured at a high level of the app) for the type of entity being serialized
* The user accessing the data

## Things I haven't seen in existing serializers yet (or at least not well)

1. Customization of which related entities or attributes to include based on user/auth context
2. other stuff, I'm sure...

## Projects to pull from

The only project I've worked on that attempts to intelligently tackle the serialization problem is the
`rcc.wsddd` project.

## Laravel model?

Laravel has an interesting approach to this problem.  Entities normalize themselves with a built-in `toArray()`
function. By default, this serializes everything (does it handle recursive relationships well?), but a custom
protected property `make_visible` or `make_invisble` will turn on/off properties.

I'm thinking of taking the idea and implementing a `Serializable` interface, that would implement a method:

```php
function normalize(UserInterface $user): NormalizedItem;
```

`NormalizedItem` could behave pretty much the same way that the `rcc.wsddd` does.

We could provide a default implementation: `DoctrineEntityNormalizer` that derives serialized identifier,
attributes, and related items from Doctrine metadata.  Considering that we'll need to make behavior modifications
quite frequently, we'll need to add some semantics that make it easy to discern we're making changes to an
existing model.  Something like this:

```php
class UserNormalizer extends DoctrineEntityNormalizer implements NormalizerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function normalize(UserInterfacce $user): NormalizedItem
    {
        // this would be the roughly the same logic for every single Doctrine entity.
        // automatically derive ID from doctrne metadata
        // automatically determine attributes from doctrine metadata
        // automtaically determine relationships from doctrine metadata
    }
    
    /**
     * Return array of strings
     */
    protected function hideFields(): array
    {
       // default for this would be none.
    }
    
    /**
     * Return array of strings
     */
    protected function showFields(): array
    {
        // default for this would be all.
    }
} 
```

We would need to also implement smarter recursion detection when traversing relationships.  
We can keep an array of object hashes that have already been visited, and if an object has been visited,
then we don't include it in the output.

## Web-specific output additions

Finally, there's the subject of adding `_href` to the output (HAL) or `links` (JSON API).  This would be
done in an event listener that is web-aware.  Perhaps a request middleware can add the event listener before
the request (once we know which resource types map to which URI endpoints), and then add the links
during normalization.