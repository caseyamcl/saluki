Add entities and services from the Bleiholder project.
Add serialization stuff...

Provide captcha support with built-in reCaptcha implementation.

Also implement a random `usleep()` when checking passwords (always less than 1sec).  This can slow down
attackers.

We'll implement the capability for an email verification-based workflow, but there needs to be some flexibility
here.  In some cases, we will want to allow the user to use the system for a period of time between creation
and verification.

In other cases, we'll want to make sure the user is verified only for certain endpoints.

In other cases, the user should not be able to login until verification is finished.  In either case, the
user should be able to submit an unauthenticated API request to receive a verification token 
again (each time changing the token).  The endpoint for this should be rate-limited like the others (reset pw, login),
and it should send a new verification email.

For security purposes, it should always send an email even if the user does not exist.  If the user does not exist,
then the email sent should invite the user to join.  If the user does exist, it should be the standard email.  This
will prevent the endpoint from being abused to deduce valid user names.

Recommended Auth Endpoint request handlers:

| endpoint    |    captcha    | rate-limit       | backoff | notes | 
| ----------- | ------------- | ---------------- | ------- | ----- |
| login       | after x tries | yes (x req/time) |   yes   | usleep() when checking passwords to prevent timing attacks and slow-down attackers
| register    |      yes      | yes (x req/time) |   yes   |
| forgot      |      yes      | yes (x req/time) |   yes   | always send email even if account does not exist (separate template for not exist)
| verify      |      yes      | yes (x req/time) |   yes   | always send email even if account does not exist (separate template for not exist)

OWASP security recommends that reset/verification tokens expire after eight hours if not used.

We should also implement an `isApproved()` in the user interface, but make the implementing app decide how to implement
it.  For example, in some apps without an approval process, then we just always return TRUE.  In other apps, we store
the value in the database and add an approval endpoint for an authorized approver to approve accounts.

Logging - Saluki should provide a mechanism for logging failures and/or successes.  Configure PSR-3 support but have a default
database implementation (and perhaps also log to the console when verbosity is increased)

Allow an optional temporary lockout policy, implemented as middleware, that uses the cache interface to 
determine how many failed requests have been submitted for a given user in a given time period, and if it exceeds a set number (say, 20), consider adding a 
temporary lockout (in cache).  The server would respond in this case with a 429 and a Retry-After value.  the message
should be something like 'login attempts banned for username ""' and it should respond as such regardless of whether the
username actually exists or not.

* Lockout threshold is configurable
* lockout time length is configurable
* Message is configurable

## Access Control
Consider a centralized access control system based on roles and/or permissions.  (is_role('admin))  If the access
check fails, then we offer the option to log that as well (console/STDOUT/database).

What I've used in rcc.wsddd is a built-in hard-coded role system.
For bleiholder, ditto (just one flag (is admin: true/false))
For Neighborhood CRM, it maybe more complex, and require the ability to pass custom callbacks to the access checker.

Either way, its clear the access checker will need to be stateful.

## Documentation

Add best-practices to documentation..

1. Combined backoff/rate-limiter algorithm on login, pwreset, and register endpoints (if enabled).
2. Rate-limiter on all three should be site-wide based on a prediction of the maximum number of attempts per hour
   on each (for example, we don't expect more than 1000 people to signup in a given 30 minute period).
3. Be sure that error messages are sufficiently vague.  For example, on pw reset and registration forms, an attacker
   might use error messages to deduce usernames or registered accounts.  Perhaps on password reset, send an email
   to the address, but the email says that username is not registered ("do you want to register?")
4. Mention that using emails is preferable to using usernames (for a number of reasons), but emails should be protected
   (error messages should not disclose whether an email address is registered or not)

## Passwords

Per NIST guidelines, mandatory timed password resets are a bad idea, so lets avoid coding anything for those as part
of Saluki.  But... the ability for an administrator to reset all passwords to random, strong values is important so that
an admin can respond to being hacked quickly.  This would force every user (including admins) into the 'reset password' 
workflow.

Don't forget: An empty password in the database should not be evaluated as valid for an empty password request attempt.

### Password reset

The password reset workflow should allow users to reset their password via email.

## Built-in login strategies to support for the /auth endpoint

This is pulled heavily from `rcc.wsddd` codebase, because a lot of that is reusable.

* username/password
* token

Username/password works the way that you'd expect it to.  Matching credentials mean that authentication succeeded.  The
default handler simply generates an access token and returns a response.

Token-based login is a bit more complex.

Tokens contain a use-count, and a maximum number of uses allowed (default = 1, 0 = infinite).
Tokens can have an expiration date/time.
Tokens can be tagged with certain programmatic behaviors, which would generate different behaviors for the server:

Some token params would modify the behavior of the access token:
- A token with 'allowed-endpoints' would limit any requests used with that token to specific endpoint(s).  Use OAuth
  scopes for this.
- A token with 'allowed-num-requests' would limit the number of requests that can be called before the access token
  expires.  Increment the number of times the access token was used as middleware (barring a 500 error) to handle
  this feature.  In the implementing middleware, fail if the access token has been used too many times. 

Some token params would trigger additional logic before completely validating the auth request:
- A 2FA token would verify a security code passed with the request against a 2FA service to ensure it is valid.

## Username/password logins

If using username passwords, Saluki will provide pluggable backends for authenticating the user.  The user can can be
limited to a specific backend as a property of the User object (e.g. 'auth=local,ldap').  No value would either mean
"use the default" or "use any" depending on the app implementing the auth.  There should be a `CredentialsChecker`
interface that can be implemented with multiple backends (e.g. local database, LDAP, etc.)

## Token-based login

Send `X-Auth-Token` HTTP header.

## Two-factor authentication

Some apps may take advantage of Google 2FA.  All security-minded actions (register, login, forgot, verify) should be
able to take advantage of 2FA or any other middleware before doing the action.  Examples:

By default, sending a valid username/password combo to the OAUTH `/auth` endpoint will generate an access token and 
respond with the token.

Using 2FA, the endpoint would do the following:
1. If username/password are good, then trigger 2FA logic:
2. If login timestamp older than certain date, or some other criteria, or just for every request, then:
3. Generate a 2FA token by sending a request to a 2FA service, getting a token, and then respond with a JSON object
   containing 2FA token to be submitted once the user has interacted with the 2FA app.
4. The client would presumably 

## Recommended rate limiting strategies

For login w/password, employ a back-off strategy per IP, per username, and per password.
For login w/token, employ a back-off strategy per IP and employ a rate-limit of 4/req/sec site-wide
For register, forgot, and verify, employ back-off strategy per IP and per username

## References
* https://www.owasp.org/index.php/Blocking_Brute_Force_Attacks