We should add setup commands here, and what-not.  

## Notes on CLI commands for web apps

Web apps should not go crazy with CLI commands, because so much of the scaffolding work is handled in the HTTP request-response lifecycle.  We don't want to re-create
a parallel version of that for CLI commands if we can avoid it.

Thus, non-development CLI commands (used by sysadmins) for web apps should be limited to the bare minimum
necessary.  All other actions should use the --yknow-- web platform. 

## Developer vs deployment commands

Some commands will be used by developers (`orm` commands), and some might be exposed to sysadmins for apps
running in deployment (such as `setup`).

There should be two seperate console apps, one for each.  The developer commands should be available only if
the app is running in devmode.

## Some common commands to consider bundling...

* `setup`          - Aggregates all of the setup commands below. Relies on no external services
* `setup:check`    - Checks config and runtime environment to ensure that the app can run
* `setup:db`       - Migrates/builds the database (if not done automatically in HTTP request lifecycle)
* `setup:access`   - Configures initial users and/or oauth apps.
* `recover`        - Recover access to the system if all other modes have failed.  Need to think about the
                     security implications of this one.
* `check:services` - Perform a check of external services and report on statuses.  This requires each external
                     service to conform to a common interface.
* `web:serve`      - Start a React web server
* `worker:run`     - Run a queue listener/worker process (maybe option to list specific queues if defined)

Again, keep it simple.  Most of the time, we're building a web app, not a console app.  

## Passing bootstrap parameters in the console

Because of the nature of CLI commands, it makes sense to make some configuration values available as
runtime parameters for all CLI commands. See `modemob` for a working prototype of this idea.

Things like `log-level`, `config` (file path), and `dev-mode` ought to be available as CLI options (with
sensible defaults).

If we create a new interface for commands, its important to also make it easy to register built-in Symfony
commands as well, to bypass this behavior.
 
## Console commands and transactions:

There are three types of console commands:

1. Non-state changing commands that output information only.
    a. These need no transaction management, but it wouldn't hurt to have it.
2. Single-action state-changing commands that do a thing and report on success.  These act kinda like a
   single HTTP request
    a. These need implicit transaction management (perhaps through console events)
    b. Upon failure, default behavior (exit with error)
3. Batch commands that run multiple #2's, each in their own transactional boundary
    a. These commands need access to a transaction manager that they can call explicitly
    b. they must not be wrapped in a transaction.
    c. Upon non-bootstrap failure, catch the exception, log it, and attempt to continue running.