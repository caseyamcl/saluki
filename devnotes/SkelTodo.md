Put project skeleton in here.  Includes:

1. Sample source code and README template (based on League skeleton)
2. Docker stuff with good comments and sample implementations of Redis, Postgres, MySQL, etc..
   Mailhog, PHP, and NGINX should be enabled by default
3. Good build stuff.  Opinionated build process should be:
   a. Create Docker container with PHAR for running in production.
   b. Configuration should be able to be exported in a number of different ways.

TODO: Read up on Kubernetes configuration management, so we can ensure our PHP can read from that.  In fact,
that should be added to Configula as a loader!
