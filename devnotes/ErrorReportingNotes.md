Default Logging:

- By default, all log messages are written to error_log (usually: STDERR).
- Add additional loggers by registering any service that implements Monolog `HandlerInterface`

Error Handling:

- In console apps, errors are handled by the Symfony Console Error Handler
- In the web handler, errors are handled by the ErrorMiddleware
   - If ErrorMiddleware is disabled or not registered, the exception will turn
     into a fatal error.
- If Web SAPI, then bootstrap errors are caught manually handled and generate
  a 500 Response.
  


    
