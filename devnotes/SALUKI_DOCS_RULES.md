When writing documentation, consider the following:

## When coding setup steps...

1. When run in `non-interactive` mode (`$io->isInteractive() === false`), automatically run any non-risky
   steps.  This includes database migrations, auto setup, and generally anything that doesn't clobber data.
2. When run in `non-interactive` mode, if there is a risky step, there should be a way to do the step in the case
   that we are in non-interactive mode.  In these cases, there is a `-y`/`--yes` option that you can reference:
   
```php
    if ($io->getOption('yes')) {
        // do risky thing automatically
    } else {
        // prompt user to do risky thing
    }
``` 

## When coding REST Resource Endpoints

1. You *at-least* need something that extends the RestRetrieveEndpoint.  This endpoint tells the system how
   to normalize the resource and generate URIs.  Other types of endpoints are optional (index, create, update, delete).
2. Choose `snake_case` or `camelCase` for API parameters and stick with it.  Built-in endpoints use `camelCase`

## On validation of entities

* Entity validation (whether through Symfony Validator or regular domain rules that throw exceptions) should always
  throw an internal error that translates into a 500 response.
* This means that 422s are generated only from controllers/service layer, and that some rules may have to be duplicated
  into the service layer in order to catch all validation exceptions appropriately.  This was an explicit decision.
