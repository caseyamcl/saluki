Provide configurable user-definable settings that are stored in the database.
This is a registry.  Each setting should have the following:
- name
- description (to present to an administrator in a UI)
- validation rules (optionally using validation library); think about what state we can pass in to each rule
- default value

When storing, refer to bleiholder.  We store as a JSON column in the database in order to have types preserved
in the PHP code.  This works quite well.

Consider also allowing admins to set settings in the config and disable user override.  So, when we present the
setting to a client app, we present it as an object:

```json
{
    "name": "my_setting_alnum",
    "value": "some_value",
    "default": "some_default",
    "description": "This is the value of my setting",
    "mutable": true,
    "provider": "config" 
}
```

Let's also document a settings provider interface, so we know exactly what provider provided this setting value.
It can be "default", "config" (loaded from config file), "database", or whatever.

Settings should be loaded in a cascading way.  Order matters in the SettingsLoader. 

The "mutable" property indicates if the setting can be overridden by the next settings loader in the chain.  If
the value is `true` (default true), then the value can be changed by the next loader.



## Some thoughts

This is actually more complicated than it at first seems.  The issue is that if something (e.g. an email server setting)
is set in the database and not in the hard-coded configuration, then if the database fails, then we cannot retrieve
the setting.

This has implications for apps that have a bootstrap phase and a request handler phase.  What should be loaded/tested
during bootstrap, and what do we allow to be deferred to fail until requests are processed?

Here is how it ought to be done, I think....

1. The database should not *have* to be accessed at all during bootstrap.  In fact, connecting should be deferred until
   the first request is made.  If lazy-loading works correctly, then the DI container should facilitate this.
2. What happens if something goes wrong during bootstrapping?  Easy, simply output the error directly to the STDERR
   stream (error_log()) and then exit with non-zero error code. No emails, or other tom-foolery.  If the PHP_SAPI is
   web, then also write a 500 HTTP error to STDOUT as well.
3. One good reason to not rely on database access during setup is that the database is an external service, and it
   may come or go during the application runtime.  The app should be able to handle that.
4. For things like sending emails (during runtime after bootstrap), if sending emails failed, it should be *logged* 
   and the app should be able to run.  In fact, it is best to send emails in an async task queue, in which case, we 
   treat a failed email sending as a failed job, and re-queue it.
5. The request/response cycle should *not* be dependent upon successful execution of anything related to external
   services.  The app should be able to intelligently know when an external service has failed and generate an
   appropriate error message and response.  
   
Using this model, the following should definitely *NOT* be classified as 'settings':

1. `dev_mode` - Config only.  This should only be mutable to sysadmins, and it should not be a setting
2. `db` - Obviously database connection parameters cannot be in the db
3. `log_level` - This is a system setting that affects bootstrap phase and runtime phase.
4. `encryption_key` - If two-way encryption is used, this should not be set in the database.

Things that can be in the database:

1. Other external service parameters, so long as they are not needed during bootstrap.  For example...
    1. email
    2. other databases
    3. ldap servers
2. Session length or default token expiration
3. email templates
4. site name, etc.
5. administrator email

## External services and app failures.

External services, including the database, should only cause the app to fail if the request depends on the service.
For example, LDAP failures should only fail for auth requests. Since the primary database is used for most requests,
then most requests will fail if the database fails.

## React-style process, external service failures, and persistent connections

We should consider long-running HTTP servers (bootstrap once; handle many requests; e.g. ReactHTTP).   In some cases,
it makes sense to maintain a persistent connection in-memory across requests in order to achieve best performance.  For
example, the database, or a queueing service.

In other cases, it makes sense to establish a connection during a request and disconnect after the request completes.
E.g. LDAP or email servers.

The latter are easier to manage, since a temporary connection or service failure will be automatically re-tried
for each request.

For the former, we must take care in handling connection or service failures.  If a service fails during handling
of a request, that request should fail, but the next request should attempt to reconnect.  Code might look like this
for the service:

```php
public function doSomethingOnService($data) {
   $this->ensureConnection(); // attempts to reconnect if not already connected (throws exception on failure)
   // do stuff with data in service.
```

For the latter, we need to close the connection after a request cycle.  This might involve yet another very outer-layer
request middleware.

## What about those apps that auto-build their own schema or auto-migrate?

In this case, we intercept requests by checking if the database is up-to-date, possibly as an HTTP middleware, and if
we are supposed to auto-migrate/build, then do so, else we have an error!

This should come after bootstrapping, of course.