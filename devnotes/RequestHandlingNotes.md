Notes about Saluki App Runtime:

1. We need to facilitate either shared-nothing request handling processes (default PHP), or React-style
   persistent servers.
2. App has two main states:
   a. bootstrap
      * (parse errors caught here)
      * (all exceptions or errors that occur are output directly to stderr here and program exits)
          * if PHP_SAPI is determined to be web, then also send appropriate headers and show generic message.
      1. start timer (and log bootstrap started to std error log)
      2. read configuration (exceptions may happen here)
      3. build object graph
      4. register routes, setting definitions, and other registry things   
   b. ready/listening
      * (errors are handled by error handler stack, which should include logging to stdout, but also have things
        like email/slack/etc. options + finally returning a 500 error to the client)
      * External services are initialized separately for each request (in case they come back online); this
        includes settings
        

## Some thoughts on database transactions:

Database transaction middleware should wrap the entire request in an explicit transaction (same as rcc.wsddd).

1. Entity state should always be the responsibility of the app.  The database should never 'decide' on any
   value for an entity.  This way, we don't have to wait on a database transaction for any entity state (such
   as ID)
2. Sometimes, a user needs to be notified when something occurs (such as an email dispatched, or queue job
   submitted).  We need to know that the database transaction succeeded before we dispatch the email or queue
   the job.
   a. If the task is important, such as sending/queuing an email that is required, or adding an audit log message,
      then it should be done as part of the database transaction. (flush, then send email, then finish transaction)
   b. If the task is not important, such as sending/queuing a slack alert, then it should be done after the database
      transaction completes.
