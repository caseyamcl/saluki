By default, Saluki doesn't provide any mechanism for generating application configuration programmatically.

If we want to provide this in the future, then the best way to do it is to setup a completely separate
entry-point that guides the user (sysadmin) through generating the configuration.  For now, this is expected
to be in the README.
