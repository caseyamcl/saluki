<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 10/3/18
 * Time: 3:02 PM
 */

namespace Saluki\Validation;

use PHPUnit\Framework\TestCase;

class ValidatorFieldSetTest extends TestCase
{
    /**
     * Test that, when referencing other fields in the
     */
    public function testContextHasAccessToPreparedValues()
    {
        $test = null;

        $values = [
            'a' => 'yes',
            'b' => 100
        ];

        $fieldset = new ValidatorFieldSet();
        $fieldset->field('a')->transform(function () {
            return true;
        });

        $fieldset->field('b')->integer()->callback(function ($value, ValidatorContext $context) use (&$test) {
            $test = $context->getFieldValue('a');
        });

        $fieldset->prepare($values);
        $this->assertTrue($test === true);
    }

    /**
     * Test that default values skip the transformations step.
     */
    public function testDefaultValuesAreNotPrepared()
    {
        $fieldset = new ValidatorFieldSet();
        $fieldset->field('a')->boolean()->setDefault('yes')->transform(function ($value) {
            return 'no';
        }, false);

        $values = $fieldset->prepare([]);
        $this->assertEquals('yes', $values['a']);
    }
}
