<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Validation;

use PHPUnit\Framework\TestCase;
use Saluki\Validation\Rule\Boolean;

/**
 * Class ValidatorRuleSetTest
 * @package Saluki\Validation
 */
class ValidatorRuleSetTest extends TestCase
{
    /**
     * The getErrorMessages() method should modify the value by reference
     */
    public function testGetErrorMessagesReturnsPreparedValue()
    {
        $value = 'yes';
        $obj = new ValidatorRuleSet([new Boolean()]);
        $obj->getErrorMessages($value);
        $this->assertTrue($value === true);
    }

    /**
     * The prepare() method should return the prepared value
     */
    public function testPrepareReturnsPreparedValue()
    {
        $value = 'yes';
        $obj = new ValidatorRuleSet([new Boolean()]);
        $prepared = $obj->prepare($value);
        $this->assertTrue($prepared === true);
    }
}
