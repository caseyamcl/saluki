<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use PHPUnit\Framework\TestCase;
use Saluki\Validation\ValidatorContext;

class EmailAddressTest extends TestCase
{
    public function testInstantiation()
    {
        $obj = new EmailAddress();
        $this->assertInstanceOf(EmailAddress::class, $obj);
    }

    public function testValidateValidEmail()
    {
        $obj = new EmailAddress();
        $result = $obj->__invoke('something@fsu.edu', new ValidatorContext([]));
        $this->assertEquals('something@fsu.edu', $result);
    }
}
