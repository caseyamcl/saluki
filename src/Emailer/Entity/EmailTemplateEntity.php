<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 11:48 AM
 */

namespace Saluki\Emailer\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EmailTemplateEntity
 * @package Saluki\Emailer\Entity
 */
#[ORM\Table(name: 'email_template')]
#[ORM\Entity(repositoryClass: EmailTemplateRepository::class)]
class EmailTemplateEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private string $name;

    #[ORM\Column(type: 'boolean', nullable: false)]
    private bool $isHtml;

    #[ORM\Column(name: 'from_address', type: 'string', length: 255, nullable: true)]
    private ?string $from = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $replyTo = null;

    #[ORM\Column(type: 'string', length: 1024, nullable: false)]
    private string $subject;

    #[ORM\Column(type: 'text', nullable: false)]
    private string $body;

    /**
     * EmailTemplateEntity constructor.
     * @param string $templateName
     * @param string $subject
     * @param string $body
     * @param bool $isHtml
     */
    public function __construct(string $templateName, string $subject, string $body, bool $isHtml = true)
    {
        $this->subject = $subject;
        $this->body = $body;
        $this->name = $templateName;
        $this->isHtml = $isHtml;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFrom(): ?string
    {
        return $this->from;
    }

    public function getReplyTo(): ?string
    {
        return $this->replyTo;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function setFrom(?string $from): void
    {
        $this->from = $from;
    }

    public function setReplyTo(?string $replyTo): void
    {
        $this->replyTo = $replyTo;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    public function isHtml(): bool
    {
        return $this->isHtml;
    }

    public function setIsHtml(bool $isHtml): void
    {
        $this->isHtml = $isHtml;
    }
}
