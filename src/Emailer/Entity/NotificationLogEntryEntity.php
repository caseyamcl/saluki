<?php

/*
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Emailer\Entity;

use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * EmailTemplateEntity
 *
 * Represents an entry in the notification_log table
 *
 * @package Saluki\Emailer\Entity
 */
#[ORM\Table(name: 'notification_log')]
#[ORM\Entity(repositoryClass: NotificationLogEntryRepository::class)]
class NotificationLogEntryEntity
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private UuidInterface $id;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    private ChronosInterface $timestamp;

    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    private string $channel;

    #[ORM\Column(type: 'string', length: 1024, nullable: false)]
    private string $recipient;

    #[ORM\Column(type: 'string', length: 100, nullable: false)]
    private string $reason;

    #[ORM\Column(type: 'string', length: 2048, nullable: true)]
    private string $subject;

    #[ORM\Column(type: 'json', nullable: true)]
    private array $details;

    /**
     * NotificationLogEntryEntity constructor.
     * @param string $recipient
     * @param string $channel
     * @param string $reason
     * @param DateTimeInterface|null $timestamp
     * @param string|null $subject
     * @param array $details
     */
    public function __construct(
        string $recipient,
        string $channel,
        string $reason,
        DateTimeInterface $timestamp = null,
        ?string $subject = null,
        array $details = []
    ) {
        $this->id = Uuid::uuid4();
        $this->timestamp = $timestamp ? Chronos::instance($timestamp) : Chronos::now();

        $this->recipient = $recipient;
        $this->channel = $channel;
        $this->reason = $reason;
        $this->subject = $subject;
        $this->details = $details;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getChannel(): string
    {
        return $this->channel;
    }

    public function getRecipient(): string
    {
        return $this->recipient;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function getTimestamp(): ChronosInterface
    {
        return $this->timestamp;
    }

    public function getDetails(): array
    {
        return $this->details;
    }
}
