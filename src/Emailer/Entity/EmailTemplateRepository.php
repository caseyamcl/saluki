<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 11:54 AM
 */

namespace Saluki\Emailer\Entity;

use Saluki\Persistence\Model\AbstractEntityRepository;

/**
 * Class EmailTemplateRepository
 * @method EmailTemplateEntity find($id, $lockMode = null, $lockVersion = null)
 * @package Saluki\Emailer\Entity
 */
class EmailTemplateRepository extends AbstractEntityRepository
{
    const ENTITY_CLASS = EmailTemplateEntity::class;
}
