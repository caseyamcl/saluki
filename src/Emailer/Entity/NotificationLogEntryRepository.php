<?php

/*
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Emailer\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Saluki\Persistence\Model\AbstractEntityRepository;

/**
 * Class NotificationLogEntryRepository
 * @package Saluki\Emailer\Entity
 */
class NotificationLogEntryRepository extends AbstractEntityRepository
{
    public const ENTITY_CLASS = NotificationLogEntryEntity::class;

    /**
     * Find notifications for a particular recipient
     *
     * @param string $to
     * @param string|null $reason
     * @param DateTimeInterface|null $since
     * @return Collection
     */
    public function findNotificationsSentTo(
        string $to,
        ?string $reason = null,
        ?DateTimeInterface $since = null
    ): Collection {
        $qb = $this->createQueryBuilder('n');

        $qb->andWhere($qb->expr()->eq('n.recipient', ':recipient'));
        $qb->setParameter(':recipient', $to);

        if ($reason) {
            $qb->andWhere($qb->expr()->eq('n.reason', ':reason'));
            $qb->setParameter(':reason', $reason);
        }

        if ($since) {
            $qb->andWhere($qb->expr()->gte('n.timestamp', ':dt'));
            $qb->setParameter(':dt', $since);
        }

        return new ArrayCollection($qb->getQuery()->getResult());
    }

    /**
     * @param NotificationLogEntryEntity $entry
     */
    public function addLogEntry(NotificationLogEntryEntity $entry): void
    {
        $this->getEntityManager()->persist($entry);
    }
}
