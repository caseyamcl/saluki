<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 1:08 PM
 */

namespace Saluki\Emailer;

use Psr\Container\ContainerInterface;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Emailer\Factory\TwigFactory;
use Saluki\Emailer\Service\Emailer;
use Saluki\Emailer\Service\EmailerTransactionSubscriber;
use Saluki\Emailer\Service\EmailService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Twig\Environment;

/**
 * Class EmailerProvider
 * @package Saluki\Emailer
 */
class EmailerProvider implements SalukiProviderInterface
{
    private bool $buildTwig;

    /**
     * EmailerProvider constructor.
     *
     * @param bool $buildTwig  If FALSE, the app must provide another instance of \Twig\Environment
     */
    public function __construct(bool $buildTwig = true)
    {
        $this->buildTwig = $buildTwig;
    }

    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        if ($this->buildTwig) {
            $builder->addEntityDirectory(__DIR__ . '/Entity');
            $builder->addServiceDefinition(Environment::class, \DI\factory(new TwigFactory()));
        }

        $builder->addFromDirectory(__DIR__ . '/ConsoleCommand');
        $builder->addFromDirectory(__DIR__ . '/Setting');
        $builder->addServiceDefinition(EmailService::class);

        $builder->addBuildCallback(function (ContainerInterface $container) {
            $subscriber = new EmailerTransactionSubscriber($container->get(Emailer::class));
            $container->get(EventDispatcherInterface::class)->addSubscriber($subscriber);
        });
    }
}
