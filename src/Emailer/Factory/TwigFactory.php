<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 1:14 PM
 */

namespace Saluki\Emailer\Factory;

use Saluki\Container\Model\SalukiParams;
use Saluki\Emailer\Entity\EmailTemplateRepository;
use Saluki\Emailer\Twig\EmailEntityLoader;
use Twig\Environment;
use Twig\Loader\ChainLoader;
use Twig\Loader\FilesystemLoader;

/**
 * Class TwigFactory
 * @package Saluki\Emailer\Factory
 */
class TwigFactory
{
    /**
     * @param SalukiParams $params
     * @param EmailTemplateRepository $emailTemplateRepository
     * @return Environment
     */
    public function __invoke(SalukiParams $params, EmailTemplateRepository $emailTemplateRepository)
    {
        $loader = new ChainLoader([
            new EmailEntityLoader($emailTemplateRepository),
            new FilesystemLoader($params->getEmailTemplateDirectories()->getArrayCopy())
        ]);

        return new Environment($loader);
    }
}
