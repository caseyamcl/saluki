<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 12:13 PM
 */

namespace Saluki\Emailer\Model;

/**
 * Class EmailMessageReport
 * @package Saluki\Emailer\Model
 */
class EmailMessageReport
{
    /**
     * @var EmailMessage
     *
     */
    private $emailMessage;

    /**
     * @var bool
     */
    private $succeeded;

    /**
     * @var string
     */
    private $message;

    /**
     * EmailMessageReportInterface constructor.
     * @param EmailMessage
     * $emailMessage
     * @param bool $succeeded
     * @param string $message
     */
    public function __construct(EmailMessage $emailMessage, bool $succeeded, string $message = '')
    {
        $this->emailMessage = $emailMessage;
        $this->succeeded = $succeeded;
        $this->message = $message;
    }

    /**
     * @return EmailMessage
     *
     */
    public function getEmailMessage(): EmailMessage
    {
        return $this->emailMessage;
    }

    /**
     * @return bool
     */
    public function isSucceeded(): bool
    {
        return $this->succeeded;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
