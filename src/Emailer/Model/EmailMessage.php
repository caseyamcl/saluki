<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 12:30 PM
 */

namespace Saluki\Emailer\Model;

/**
 * Class EmailMessage
 * @package Saluki\Emailer\Model
 */
class EmailMessage
{
    /**
     * @var bool
     */
    private $isHtml;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $body;

    /**
     * @var string
     */
    private $to;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var string|null
     */
    private $from;

    /**
     * @var string|null
     */
    private $replyTo;
    /**
     * @var string
     */
    private $reason;

    /**
     * Create HTML message
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param array $headers
     * @param string $reason
     * @return EmailMessage
     */
    public static function htmlMessage(
        string $to,
        string $subject,
        string $body,
        array $headers = [],
        string $reason = ''
    ) {
        return new static($to, $subject, $body, true, $headers, $reason);
    }

    /**
     * Create plain text message
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param array $headers
     * @param string $reason
     * @return EmailMessage
     */
    public static function plaintextMessage(
        string $to,
        string $subject,
        string $body,
        array $headers = [],
        string $reason = ''
    ) {
        return new static($to, $subject, $body, false, $headers, $reason);
    }

    /**
     * EmailMessage constructor.
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param bool $isHtml
     * @param array $headers
     * @param string $reason
     */
    public function __construct(
        string $to,
        string $subject,
        string $body,
        bool $isHtml = true,
        array $headers = [],
        string $reason = ''
    ) {
        $this->isHtml = $isHtml;
        $this->subject = $subject;
        $this->body = $body;
        $this->to = $to;
        $this->headers = $headers;
        $this->reason = $reason;
    }

    /**
     * Is this an HTML message or plaintext email?
     *
     * @return bool
     */
    public function isHtml(): bool
    {
        return $this->isHtml;
    }

    /**
     * @return null|string
     */
    public function getReplyTo(): ?string
    {
        return $this->replyTo;
    }

    /**
     * @return null|string
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param bool $isHtml
     */
    public function setIsHtml(bool $isHtml): void
    {
        $this->isHtml = $isHtml;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to): void
    {
        $this->to = $to;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    /**
     * @param string $header
     */
    public function addHeader(string $header): void
    {
        $this->headers[] = $header;
    }

    /**
     * @param null|string $from
     */
    public function setFrom(?string $from): void
    {
        $this->from = $from;
    }

    /**
     * @param null|string $replyTo
     */
    public function setReplyTo(?string $replyTo): void
    {
        $this->replyTo = $replyTo;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }
}
