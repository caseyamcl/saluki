<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Emailer\Contract;

use Saluki\Emailer\Model\EmailMessage;
use Saluki\Emailer\Model\EmailMessageReport;

/**
 * Class Emailer
 * @package Saluki\Emailer\Service
 */
interface EmailerInterface
{
    /**
     * Enqueue a message to be sent after current transaction completes
     *
     * @param EmailMessage $emailMessage
     */
    public function enqueue(EmailMessage $emailMessage);

    /**
     * Flush message queue
     *
     * @return iterable|\Generator|EmailMessage[]
     */
    public function flushQueue(): iterable;

    /**
     * Send message
     *
     * @param EmailMessage $emailMessage
     * @return EmailMessageReport
     */
    public function send(EmailMessage $emailMessage): EmailMessageReport;
}
