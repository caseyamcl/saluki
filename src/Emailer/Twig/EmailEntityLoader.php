<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 12:41 PM
 */

namespace Saluki\Emailer\Twig;

use Saluki\Emailer\Entity\EmailTemplateEntity;
use Saluki\Emailer\Entity\EmailTemplateRepository;
use Twig\Loader\LoaderInterface;
use Twig\Source;
use Twig\Error\LoaderError;
use Webuni\FrontMatter\Document;
use Webuni\FrontMatter\FrontMatter;

/**
 * Class EmailEntityLoader
 * @package Saluki\Emailer\Twig
 */
class EmailEntityLoader implements LoaderInterface
{
    private EmailTemplateRepository $repository;
    private FrontMatter $frontMatter;

    /**
     * EmailEntityLoader constructor.
     * @param EmailTemplateRepository $repository
     * @param FrontMatter|null $frontMatter
     */
    public function __construct(EmailTemplateRepository $repository, ?FrontMatter $frontMatter = null)
    {
        $this->repository = $repository;
        $this->frontMatter = $frontMatter ?: FrontMatter::createYaml();
    }

    public function getSourceContext($name): Source
    {
        $name = (string) $name;
        $entity = $this->retrieveTemplate($name);
        return new Source($this->prepareTemplate($entity), $name);
    }

    public function exists($name): bool
    {
        return (bool) $this->retrieveTemplate($name, false);
    }

    public function getCacheKey($name): string
    {
        $entity = $this->retrieveTemplate($name);
        return $name . ':' . spl_object_hash($entity);
    }

    public function isFresh($name, $time): bool
    {
        return (bool) $this->retrieveTemplate($name);
    }

    /**
     * @param string $name
     * @param bool $throw
     * @return EmailTemplateEntity|null
     * @throws LoaderError()
     */
    private function retrieveTemplate(string $name, bool $throw = true): ?EmailTemplateEntity
    {
        if ($template = $this->repository->find($name)) {
            return $template;
        } elseif ($throw) {
            throw new LoaderError(sprintf('Template "%s" is not defined.', $name));
        } else {
            return null;
        }
    }

    /**
     * Prepare template as front-matter document so that we can send it to the EmailTemplateBuilder
     *
     * @param EmailTemplateEntity $template
     * @return string
     */
    private function prepareTemplate(EmailTemplateEntity $template): string
    {
        $frontMatter = array_filter([
            'from'     => $template->getFrom(),
            'subject'  => $template->getSubject(),
            'reply-to' => $template->getReplyTo(),
            'is-html'  => $template->isHtml()
        ]);

        return $this->frontMatter->dump(new Document($template->getBody(), $frontMatter));
    }
}
