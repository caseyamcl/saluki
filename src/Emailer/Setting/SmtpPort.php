<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 4:47 PM
 */

namespace Saluki\Emailer\Setting;


use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\ValidatorRuleSet;
use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

class SmtpPort extends AbstractSettingDefinition
{
    const NAME         = 'smtp_port';
    const DISPLAY_NAME = 'SMTP Port';
    const NOTES        = 'The SMTP TCP Port';
    const DEFAULT      = 25;
    const SENSITIVE    = true;

    /**
     * Process, validate, and store a new value
     *
     * @param mixed $value The raw value
     * @return mixed  The processed value to store
     */
    public function processValue($value)
    {
        $validator = (new ValidatorRuleSet())->naturalNumber();

        try {
            return (int) $validator->prepare($value);
        } catch (ValidationRuleException $e) {
            throw new InvalidSettingValueException($e->getMessage());
        }
    }
}
