<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Emailer\Setting;

use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\ValidatorRuleSet;
use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

/**
 * Class SmtpEncryption
 * @package Saluki\Emailer\Setting
 */
class SmtpEncryption extends AbstractSettingDefinition
{
    const NAME         = 'smtp_encryption';
    const DISPLAY_NAME = 'SMTP Encryption Mode';
    const NOTES        = 'The authentication mode used on the SMTP server ("tls", "ssl", or nothing)';
    const DEFAULT      = '';
    const SENSITIVE    = true;

    /**
     * Process, validate, and store a new value
     *
     * @param mixed $value The raw value
     * @return mixed  The processed value to store
     */
    public function processValue($value)
    {
        $validator = (new ValidatorRuleSet())->transform('strtolower')->inList(['tls', 'ssl', '']);

        try {
            return $validator->prepare($value);
        } catch (ValidationRuleException $e) {
            throw new InvalidSettingValueException($e->getMessage());
        }
    }
}
