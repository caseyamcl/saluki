<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 4:47 PM
 */

namespace Saluki\Emailer\Setting;

use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\ValidatorRuleSet;
use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

class EmailDefaultLocalPart extends AbstractSettingDefinition
{
    const NAME         = 'email_default_local_part';
    const DISPLAY_NAME = 'Email default local part';
    const NOTES        = 'The part of the email address before the "@"';
    const DEFAULT      = 'system';
    const SENSITIVE    = true;

    /**
     * Process, validate, and store a new value
     *
     * @param mixed $value The raw value
     * @return mixed  The processed value to store
     */
    public function processValue($value)
    {
        $validator = (new ValidatorRuleSet())->emailLocal()->length(2, 255)->notBlank();
        try {
            return $validator->prepare($value);
        } catch (ValidationRuleException $e) {
            throw new InvalidSettingValueException($e->getMessage());
        }
    }
}
