<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 12:56 PM
 */

namespace Saluki\Emailer\Service;

use Saluki\Emailer\Model\EmailMessage;
use Saluki\Emailer\Model\EmailMessageReport;

class EmailService
{
    private Emailer $emailer;
    private EmailTemplateBuilder $builder;

    /**
     * EmailService constructor.
     * @param Emailer $emailer
     * @param EmailTemplateBuilder $builder
     */
    public function __construct(Emailer $emailer, EmailTemplateBuilder $builder)
    {
        $this->emailer = $emailer;
        $this->builder = $builder;
    }

    /**
     * Enqueue a message from a template
     *
     * @param string $to
     * @param string $template
     * @param array $templateData
     * @param string $reason
     * @return EmailMessage
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function enqueue(string $to, string $template, array $templateData = [], string $reason = ''): EmailMessage
    {
        $message = $this->builder->build($to, $template, $templateData, $reason);
        $this->emailer->enqueue($message);
        return $message;
    }

    /**
     * Send a message from a template immediately
     *
     * @param string $to
     * @param string $template
     * @param array $templateData
     * @param string $reason
     * @return EmailMessageReport
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function send(
        string $to,
        string $template,
        array $templateData = [],
        string $reason = ''
    ): EmailMessageReport {
        $message = $this->builder->build($to, $template, $templateData, $reason);
        return $this->sendMessage($message);
    }

    /**
     * Enqueue an already-prepared message
     */
    public function enqueueMessage(EmailMessage $message): void
    {
        $this->emailer->enqueue($message);
    }

    /**
     * Send an already-prepared message
     *
     * @param EmailMessage $emailMessage
     * @return EmailMessageReport
     */
    public function sendMessage(EmailMessage $emailMessage): EmailMessageReport
    {
        return $this->emailer->send($emailMessage);
    }
}
