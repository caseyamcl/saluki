<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Emailer\Service;

use Psr\Log\LoggerInterface;
use Saluki\Emailer\Contract\EmailerInterface;
use Saluki\Emailer\Entity\NotificationLogEntryEntity;
use Saluki\Emailer\Entity\NotificationLogEntryRepository;
use Saluki\Emailer\Model\EmailMessage;
use Saluki\Emailer\Model\EmailMessageReport;
use Saluki\Emailer\Setting as EmailSetting;
use SettingsManager\Contract\SettingProvider;
use SplQueue;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Transport\Smtp\Auth\CramMd5Authenticator;
use Symfony\Component\Mailer\Transport\Smtp\Auth\LoginAuthenticator;
use Symfony\Component\Mailer\Transport\Smtp\Auth\PlainAuthenticator;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mime\Email;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class Emailer
 * @package Saluki\Emailer\Service
 */
class Emailer implements EmailerInterface
{
    public const CHANNEL_NAME = 'email';

    private SettingProvider $settings;
    private SplQueue $messageQueue;
    private NotificationLogEntryRepository $notifyLog;
    private LoggerInterface $logger;

    /**
     * Emailer constructor.
     *
     * @param SettingProvider $settingsProvider
     * @param NotificationLogEntryRepository $notifyLog
     */
    public function __construct(
        SettingProvider $settingsProvider,
        NotificationLogEntryRepository $notifyLog,
        LoggerInterface $logger
    ) {
        $this->settings = $settingsProvider;
        $this->notifyLog = $notifyLog;
        $this->logger = $logger;

        $this->messageQueue = new SplQueue();
    }

    /**
     * Enqueue a message to be sent after current transaction completes
     *
     * @param EmailMessage $emailMessage
     */
    public function enqueue(EmailMessage $emailMessage)
    {
        $this->messageQueue->push($emailMessage);
    }

    /**
     * Flush message queue
     *
     * @return iterable|EmailMessage[]
     */
    public function flushQueue(): iterable
    {
        $mailer = $this->buildMailer();

        /** @var EmailMessage $message */
        while ($this->messageQueue->count() > 0) {
            $message = $this->messageQueue->dequeue();
            $out[] = $this->send($message, $mailer);
        }

        return $out ?? [];
    }

    /**
     * Send message
     *
     * @param EmailMessage $emailMessage
     * @param Mailer|null $mailer
     * @return EmailMessageReport
     */
    public function send(EmailMessage $emailMessage, Mailer $mailer = null): EmailMessageReport
    {
        $lineBreak = $emailMessage->isHtml() ? '<br/>' : PHP_EOL;

        $body = trim(
            $emailMessage->getBody() . $lineBreak . $lineBreak .
            str_replace(PHP_EOL, $lineBreak, $this->getSettingValue(EmailSetting\EmailFooter::NAME))
        );

        // Set email metadata
        $email = (new Email())
            ->to($emailMessage->getTo())
            ->subject($emailMessage->getSubject())
            ->from($emailMessage->getFrom() ?: $this->getDefaultFrom());

        ($emailMessage->isHtml()) ? $email->html($body) : $email->text($body);

        if ($emailMessage->getReplyTo() or $this->getSettingValue(EmailSetting\EmailDefaultReplyToAddress::NAME)) {
            $email->replyTo($emailMessage->getReplyTo()
                ?: $this->getSettingValue(EmailSetting\EmailDefaultReplyToAddress::NAME));
        }

        try {
            ($mailer ?: $this->buildMailer())->send($email);

            $this->notifyLog->addLogEntry(new NotificationLogEntryEntity(
                $emailMessage->getTo(),
                static::CHANNEL_NAME,
                $emailMessage->getReason() ?: 'unknown',
                null,
                $emailMessage->getSubject()
            ));

            return new EmailMessageReport($emailMessage, true);
        } catch (TransportExceptionInterface $e) {
            return new EmailMessageReport($emailMessage, false, $e->getMessage());
        }
    }

    /**
     * @return string
     */
    private function getDefaultFrom(): string
    {
        return sprintf(
            '%s@%s',
            $this->getSettingValue(EmailSetting\EmailDefaultLocalPart::NAME),
            $this->getSettingValue(EmailSetting\EmailDomain::NAME)
        );
    }

    /**
     * @param string $settingName
     * @return mixed
     */
    private function getSettingValue(string $settingName)
    {
        return $this->settings->findValue($settingName);
    }

    /**
     * @return Mailer
     */
    public function buildMailer(): Mailer
    {
        $transport = new EsmtpTransport(
            $this->getSettingValue(EmailSetting\SmtpHost::NAME),
            $this->getSettingValue(EmailSetting\SmtpPort::NAME),
            $this->getSettingValue(EmailSetting\SmtpEncryption::NAME) ?: null,
            null,
            $this->logger
        );

        switch ($this->getSettingValue(EmailSetting\SmtpAuthMode::NAME)) {
            case 'plain':
                $authenticator = new PlainAuthenticator();
                break;
            case 'login':
                $authenticator = new LoginAuthenticator();
                break;
            case 'cram-md5':
                $authenticator = new CramMd5Authenticator();
                break;
            default:
                $authenticator = null;
        }

        if ($username = $this->getSettingValue(EmailSetting\SmtpUsername::NAME)) {
            $transport->setUsername($username);
        }
        if ($password = $this->getSettingValue(EmailSetting\SmtpPassword::NAME)) {
            $transport->setPassword($password);
        }
        if ($authMode = $this->getSettingValue(EmailSetting\SmtpAuthMode::NAME)) {
            $transport->addAuthenticator($authenticator);
        }

        return new Mailer($transport);
    }
}
