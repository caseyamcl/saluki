<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 1:23 PM
 */

namespace Saluki\Emailer\Service;

use Saluki\Events\SalukiEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EmailerTransactionSubscriber
 * @package Saluki\Emailer\Service
 */
class EmailerTransactionSubscriber implements EventSubscriberInterface
{
    private Emailer $emailer;

    public function __construct(Emailer $emailer)
    {
        $this->emailer = $emailer;
    }

    /**
     * Flush the email queue
     */
    public function flushEmailQueue()
    {
        $this->emailer->flushQueue();
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [SalukiEvents::TRANSACTION_COMPLETE => 'flushEmailQueue'];
    }
}
