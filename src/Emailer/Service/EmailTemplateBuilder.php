<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 12:23 PM
 */

namespace Saluki\Emailer\Service;

use League\CommonMark\CommonMarkConverter;
use Saluki\Emailer\Contract\EmailTemplateBuilderInterface;
use Saluki\Emailer\Model\EmailMessage;
use Twig\Environment as TwigEnvironment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Webuni\FrontMatter\FrontMatter;
use Webuni\FrontMatter\FrontMatterInterface;

/**
 * Class EmailTemplateBuilder
 *
 * @package Saluki\Emailer\Service
 */
class EmailTemplateBuilder implements EmailTemplateBuilderInterface
{
    private TwigEnvironment $twig;
    private CommonMarkConverter $markdown;
    private FrontMatter $frontMatter;

    /**
     * EmailTemplateBuilder constructor.
     * @param TwigEnvironment $twig
     * @param FrontMatterInterface|null $frontMatterLoader
     * @param CommonMarkConverter|null $markdown
     */
    public function __construct(
        TwigEnvironment $twig,
        FrontMatterInterface $frontMatterLoader = null,
        CommonMarkConverter $markdown = null
    ) {
        $this->twig = $twig;
        $this->frontMatter = $frontMatterLoader ?: FrontMatter::createYaml();
        $this->markdown = $markdown ?: new CommonMarkConverter();
    }

    /**
     * @param string $to
     * @param string $templateName
     * @param array $templateData
     * @param string $reason
     * @return EmailMessage
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function build(string $to, string $templateName, array $templateData = [], string $reason = ''): EmailMessage
    {
        $rendered = trim($this->twig->render($templateName, $templateData));
        $parsed = $this->frontMatter->parse($rendered);
        $metadata = $parsed->getData();
        $body = $this->markdown->convert($parsed->getContent());

        $message = new EmailMessage(
            $to,
            $metadata['subject'] ?? '',
            $body,
            $metdata['is-html'] ?? true,
            $metadata['headers'] ?? [],
            $reason
        );

        if (isset($metadata['from'])) {
            $message->setFrom($message['from']);
        }
        if (isset($metadata['reply-to'])) {
            $message->setReplyTo($metadata['reply-to']);
        }

        return $message;
    }
}
