<?php

/*
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Emailer\ConsoleCommand;

use Saluki\Console\ConsoleIO;
use Saluki\Emailer\Model\EmailMessage;
use Saluki\Emailer\Service\EmailService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailTestCommand extends Command
{
    private EmailService $emailService;

    public function __construct(EmailService $emailService, string $name = null)
    {
        $this->emailService = $emailService;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('email:test');
        $this->setDescription('Send a test message to an email address');
        $this->addArgument('to', InputArgument::REQUIRED, 'Email address to send test message');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new ConsoleIO($input, $output);

        $emailMessage = new EmailMessage(
            $io->getArgument('to'),
            'Test Message',
            'Test Body'
        );
        $sendReport = $this->emailService->sendMessage($emailMessage);

        if ($sendReport->isSucceeded()) {
            $io->success('Message sent successfully to: ' . $io->getArgument('to'));
        } else {
            $io->error('Message not sent to: ' . $io->getArgument('to'));
            $io->error($sendReport->getMessage());
        }

        return 0;
    }
}
