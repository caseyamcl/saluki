<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserTokenEntity;

use Cake\Chronos\ChronosInterface;
use DateInterval;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Cake\Chronos\Chronos;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Saluki\Authentication\Contract\UserTokenInterface;

/**
 * Class UserToken
 * @package Saluki\Authentication\Entity
 */
#[ORM\Entity(repositoryClass: UserTokenRepository::class)]
class UserToken implements UserTokenInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    protected UuidInterface $id;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    protected ChronosInterface $createdTimestamp;

    #[ORM\Column(type: 'chronos_datetime', nullable: true)]
    protected ?ChronosInterface $revokedTimestamp = null;

    #[ORM\Column(type: 'chronos_datetime', nullable: true)]
    protected ?ChronosInterface $expiresTimestamp = null;

    #[ORM\Column(type: 'string', nullable: false)]
    protected string $userId;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private string $type;

    /**
     * @param string $userId
     * @param string $type
     * @param DateInterval $interval
     * @return UserToken
     */
    public static function buildFromExpirationInterval(string $userId, string $type, DateInterval $interval): UserToken
    {
        return new static($userId, $type, Chronos::now()->add($interval));
    }

    /**
     * PersonSingleUseToken constructor.
     * @param string $userId
     * @param string $type
     * @param DateTimeInterface|null $expiresTimestamp
     * @throws Exception
     */
    public function __construct(string $userId, string $type, DateTimeInterface $expiresTimestamp = null)
    {
        $this->id = Uuid::uuid4();
        $this->userId = $userId;
        $this->createdTimestamp = Chronos::now();
        $this->expiresTimestamp = $expiresTimestamp;
        $this->type = $type;
    }

    /**
     * Revoke this token (can only be done once)
     */
    public function revoke(): void
    {
        if ($this->revokedTimestamp) {
            throw new \RuntimeException('Cannot revoke a single-use token more than once');
        }

        $this->revokedTimestamp = Chronos::now();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getCreatedTimestamp(): ChronosInterface
    {
        return $this->createdTimestamp;
    }

    public function getRevokedTimestamp(): ?ChronosInterface
    {
        return $this->revokedTimestamp;
    }

    public function getExpiresTimestamp(): ?ChronosInterface
    {
        return $this->expiresTimestamp;
    }

    public function isRevoked(): bool
    {
        return (bool) $this->revokedTimestamp;
    }

    public function isExpired(): bool
    {
        return Chronos::now() <= $this->expiresTimestamp;
    }

    public function getToken(): string
    {
        return $this->id->__toString();
    }

    public function __toString(): string
    {
        return $this->getToken();
    }


    public function isValid(): bool
    {
        return (! $this->isExpired() && ! $this->isRevoked());
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
