<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserTokenEntity;

use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;
use Saluki\Authentication\Contract\UserRepositoryInterface;
use Saluki\Authentication\Contract\UserTokenInterface;
use Saluki\Authentication\Contract\UserTokenRepositoryInterface;
use Saluki\Authentication\UserEntity\User;
use Saluki\Authentication\UserTokenEntity\UserToken;
use Saluki\Persistence\Model\AbstractEntityRepository;

/**
 * Class UserTokenRepository
 * @package Saluki\Authentication\Entity
 * @method UserToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserToken|null findOneBy(array $criteria, array $orderBy = null)
 */
class UserTokenRepository extends AbstractEntityRepository implements UserTokenRepositoryInterface
{
    /**
     * UserTokenRepository constructor.
     * @param EntityManagerInterface $em
     * @throws ReflectionException
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @param UserToken|UserTokenInterface $token
     */
    public function add(UserTokenInterface|UserToken $token): void
    {
        $this->getEntityManager()->persist($token);
    }
}
