<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Helper;

use DangerousUserNames\DangerousUserNames;
use Saluki\Access\Contract\UserRolesInterface;
use Saluki\Authentication\Contract\UserRepositoryInterface;
use Saluki\Authentication\Service\PasswordService;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\Validation\ValueTransformer;

/**
 * Class UserValidators
 * @package Saluki\Authentication\Helper
 */
class UserValidators
{
    /**
     * @var UserRolesInterface
     */
    private $userRoles;

    /**
     * @var PasswordService
     */
    private $passwordService;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserValidators constructor.
     *
     * @param UserRolesInterface $userRoles
     * @param PasswordService $passwordService
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRolesInterface $userRoles,
        PasswordService $passwordService,
        UserRepositoryInterface $userRepository
    ) {
        $this->userRoles = $userRoles;
        $this->passwordService = $passwordService;
        $this->userRepository = $userRepository;
    }

    /**
     * @return ValidatorRuleSet
     */
    public function usernameRules(): ValidatorRuleSet
    {
        return (new ValidatorRuleSet())
            ->notBlank()
            ->unique('username', $this->userRepository)
            ->transform('strtolower')
            ->alphanumeric('_')
            ->length(2, 255)
            ->notInList((new DangerousUserNames())->getArrayCopy());
    }

    /**
     * @return ValidatorRuleSet
     */
    public function emailRules(): ValidatorRuleSet
    {
        return (new ValidatorRuleSet())
            ->notBlank()
            ->unique('email', $this->userRepository)
            ->emailAddress();
    }

    /**
     * @return ValidatorRuleSet
     */
    public function passwordRules(): ValidatorRuleSet
    {
        return (new ValidatorRuleSet())
            ->length(8, 512)
            ->zxcvbn(2)
            ->notBlank()
            ->transform(function (string $plainTextPassword) {
                return $this->passwordService->hash($plainTextPassword);
            }, ValueTransformerInterface::AFTER_VALIDATION);
    }

    /**
     * @return ValidatorRuleSet
     */
    public function displayNameRules(): ValidatorRuleSet
    {
        return (new ValidatorRuleSet())
            ->notBlank()
            ->sanitize()
            ->length(2, 512);
    }

    /**
     * @return ValidatorRuleSet
     */
    public function rolesRules(): ValidatorRuleSet
    {
        return (new ValidatorRuleSet())
            ->isList()
            ->inList($this->userRoles->getUserRoles());
    }
}
