<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Factory;

use Saluki\Authentication\Contract\UserResolverInterface;
use Saluki\Authentication\UserResolver\ChainUserResolver;
use Saluki\Authentication\UserResolver\UserEntityResolver;
use Saluki\Authentication\UserResolver\UserTokenResolver;
use Psr\Container\ContainerInterface;

/**
 * Class UserResolverFactory
 * @package Saluki\Authentication\Factory
 */
class UserResolverFactory
{
    const USE_DEFAULT = null;

    /**
     * @var UserResolverInterface|string[]
     */
    private $resolverClassName;

    /**
     * UserResolverFactory constructor.
     * @param string|null $resolverClassName
     */
    public function __construct(?string $resolverClassName = self::USE_DEFAULT)
    {
        $this->resolverClassName = $resolverClassName;
    }

    /**
     * @param ContainerInterface $container
     * @return UserResolverInterface
     */
    public function __invoke(ContainerInterface $container): UserResolverInterface
    {
        if ($this->resolverClassName == self::USE_DEFAULT) {
            return new ChainUserResolver([
                $container->get(UserTokenResolver::class),  // This one goes fast...
                $container->get(UserEntityResolver::class), // Then we check person account (slower),
            ]);
        } elseif (is_string($this->resolverClassName)) {
            return $container->get($this->resolverClassName);
        } else {
            return $this->resolverClassName;
        }
    }
}
