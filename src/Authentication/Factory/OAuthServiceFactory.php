<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 7:36 PM
 */

namespace Saluki\Authentication\Factory;


use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use Saluki\Authentication\Contract\OAuthServiceInterface;
use Saluki\Authentication\Contract\OAuthUserLookupInterface;
use Saluki\Authentication\OAuthEntity\OAuthRefreshToken;
use Saluki\Authentication\Service\OAuthService;
use Saluki\Config\Model\Config;
use SettingsManager\Contract\SettingProvider;

class OAuthServiceFactory
{
    public function __invoke(
        EntityManagerInterface $em,
        Config $config,
        SettingProvider $settingsProvider,
        OAuthUserLookupInterface $authUserLookup
    ): OAuthServiceInterface {

        $oAuthService = new OAuthService($em, $config, $settingsProvider);

        $oAuthService->addGrantType(new PasswordGrant(
            $authUserLookup,
            $em->getRepository(OAuthRefreshToken::class)
        ));

        $oAuthService->addGrantType(new RefreshTokenGrant($em->getRepository(OAuthRefreshToken::class)));

        return $oAuthService;
    }
}
