<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication;

use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Saluki\Authentication\Contract\FindUserByIdentifier;
use Saluki\Authentication\Contract\OAuthServiceInterface;
use Saluki\Authentication\Contract\OAuthUserLookupInterface;
use Saluki\Authentication\Contract\PasswordServiceInterface;
use Saluki\Authentication\Contract\UserRepositoryInterface;
use Saluki\Authentication\Contract\UserResolverInterface;
use Saluki\Access\Contract\UserRolesInterface;
use Saluki\Authentication\Contract\UserTokenRepositoryInterface;
use Saluki\Authentication\Factory\OAuthServiceFactory;
use Saluki\Authentication\Helper\UserValidators;
use Saluki\Authentication\OAuthEntity\OAuthAccessTokenRepository;
use Saluki\Authentication\OAuthEntity\OAuthClientRepository;
use Saluki\Authentication\OAuthEntity\OAuthScopeRepository;
use Saluki\Authentication\Permission\ManageUsersPermission;
use Saluki\Authentication\Service\OAuthUserRepository;
use Saluki\Authentication\UserEntity\UserRepository;
use Saluki\Authentication\Factory\UserResolverFactory;
use Saluki\Access\Model\UserRoles;
use Saluki\Authentication\Service\PasswordService;
use Saluki\Authentication\UserTokenEntity\UserTokenRepository;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;

/**
 * Class AuthenticationProvider
 * @package Saluki\Authentication
 */
class AuthenticationProvider implements SalukiProviderInterface
{
    private bool $registerUserEntity;
    private bool $registerOAuthEntities;
    private bool $registerTokenEntity;

    /**
     * AuthenticationProvider constructor.
     * @param bool $registerUserEntity
     * @param bool $registerOAuthEntities
     * @param bool $registerTokenEntity
     */
    public function __construct(
        bool $registerUserEntity = true,
        bool $registerOAuthEntities = true,
        bool $registerTokenEntity = true
    ) {
        $this->registerUserEntity = $registerUserEntity;
        $this->registerOAuthEntities = $registerOAuthEntities;
        $this->registerTokenEntity = $registerTokenEntity;
    }

    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        // Setup steps
        $builder->addSetupStep(Setup\SetupOAuthKeysStep::class);
        $builder->addSetupStep(Setup\SetupOAuthClientStep::class);

        // Console Commands
        $builder->addServiceDefinition(ConsoleCommand\SetupOAuthClient::class);
        $builder->addServiceDefinition(ConsoleCommand\ListOAuthClients::class);

        // Settings
        $builder->addFromDirectory(__DIR__ . '/Setting');

        // Register default entities (also registers default user endpoint helper)
        // If implementing custom user entities and logic, all of these need to be overridden
        if ($this->registerUserEntity) {
            // Setup entity and repositories
            $builder->addEntityDirectory(__DIR__ . '/UserEntity');
            $builder->addServiceDefinition(UserRepositoryInterface::class, \DI\autowire(UserRepository::class));
            $builder->addServiceDefinition(FindUserByIdentifier::class, \DI\get(UserRepositoryInterface::class));

            // Setup endpoint helpers
            $builder->addServiceDefinition(UserValidators::class);

            // User Resource endpoints
            $builder->addServiceDefinition(RestResource\UserResource::class);

            // Setup admin user step
            $builder->addSetupStep(Setup\SetupAdminUserStep::class);
        }

        // Token entities
        if ($this->registerTokenEntity) {
            $builder->addEntityDirectory(__DIR__ . '/UserTokenEntity');

            $builder->addServiceDefinition(
                UserTokenRepositoryInterface::class,
                \DI\autowire(UserTokenRepository::class)
            );

            // Event Listeners
            $builder->addServiceDefinition(EventListener\CreateVerificationTokenOnNewUnverifiedUser::class);

            // Email templates
            $builder->addEmailTemplateDirectory(__DIR__ . '/EmailTemplates');
        }

        // OAuth Entities
        if ($this->registerOAuthEntities) {
            $builder->addEntityDirectory(__DIR__ . '/OAuthEntity');
            $builder->addServiceDefinitions([
                AccessTokenRepositoryInterface::class => \DI\autowire(OAuthAccessTokenRepository::class),
                ClientRepositoryInterface::class      => \DI\autowire(OAuthClientRepository::class),
                OAuthServiceInterface::class          => \DI\factory(new OAuthServiceFactory()),
                ScopeRepositoryInterface::class       => \DI\autowire(OAuthScopeRepository::class),
                OAuthUserLookupInterface::class       => \DI\get(OAuthUserRepository::class),
            ]);
        }

        // Permissions
        $builder->registerPermission(ManageUsersPermission::class, [UserRoles::ADMIN]);

        // Service definitions
        $builder->addServiceDefinitions([
            UserRolesInterface::class       => \DI\autowire(UserRoles::class),
            PasswordServiceInterface::class => \DI\autowire(PasswordService::class),
            UserResolverInterface::class    => \DI\factory(new UserResolverFactory()),
        ]);
    }
}
