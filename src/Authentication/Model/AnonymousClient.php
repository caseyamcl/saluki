<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/17/18
 * Time: 8:11 PM
 */

namespace Saluki\Authentication\Model;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use Saluki\Authentication\Contract\ClientAppInterface;

class AnonymousClient implements ClientAppInterface, ClientEntityInterface
{
    const ANONYMOUS_ID = 'anonymous';

    /**
     * @return string
     */
    public function getId(): string
    {
        return static::ANONYMOUS_ID;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'anonymous (unregistered) client';
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getName();
    }

    /**
     * Get the unique key that allows a non-person request to map to the system account
     *
     * @return string|null
     */
    public function getSystemUserKey(): ?string
    {
        return null;
    }

    /**
     * Does this client allow system account access?
     *
     * @return bool
     */
    public function allowsSystemAccount(): bool
    {
        return false;
    }

    /**
     * Get IP addresses or IP address ranges (IPv4 and/or IPv6) that this client allows
     *
     * Return ['*'] for no IP address restrictions (should be used rarely)
     *
     * @return array|string[]
     */
    public function getAllowedIps(): array
    {
        return self::ALL;
    }

    /**
     * Is this client app enabled?
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return true;
    }

    /**
     * @return array|string[]
     */
    public function getDomainNames()
    {
        return self::ALL;
    }

    /**
     * @param string $domain
     * @return bool
     */
    public function isAllowedDomain(string $domain): bool
    {
        return true;
    }

    /**
     * Get the client's identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getId();
    }

    /**
     * Returns the registered redirect URI (as a string).
     *
     * Alternatively return an indexed array of redirect URIs.
     *
     * @return string|string[]
     */
    public function getRedirectUri()
    {
        return [];
    }

    public function isConfidential()
    {
        return false;
    }
}
