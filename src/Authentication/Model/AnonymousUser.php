<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Model;

use Saluki\Authentication\Contract\UserInterface;

/**
 * Class UnauthenticatedUser
 * @package Saluki\Authorization\AuthInfoModel
 */
class AnonymousUser implements UserInterface
{
    const ANONYMOUS_USER_ID = 'ANONYMOUS';

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return static::ANONYMOUS_USER_ID;
    }

    /**
     * @return bool
     */
    public function isAnonymous(): bool
    {
        return true;
    }

    /**
     * @return array|string[]
     */
    public function getRoles(): array
    {
        return [];
    }
}
