<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/6/18
 * Time: 1:53 PM
 */

namespace Saluki\Authentication\Model;

use Saluki\Access\Contract\PermissionInterface;
use Saluki\Utility\RequireConstantTrait;

/**
 * Class AbstactPermission
 * @package Saluki\Authentication\Model
 */
class AbstractPermission implements PermissionInterface
{
    use RequireConstantTrait;

    public const DESCRIPTION = '';
    public const IS_CONFIGURABLE = true;

    /**
     * @var array
     */
    private $roles;

    /**
     * AbstractPermission constructor.
     * @param array $roles
     */
    public function __construct(array $roles = [])
    {
        $this->roles = $roles;
    }

    /**
     * List roles that have this permission
     *
     * @return array|string[]
     */
    public function listRoles(): array
    {
        return $this->roles;
    }

    /**
     * Get human-friendly description of permission
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->requireConstant('DESCRIPTION');
    }

    /**
     * Is this permission configurable by administrators?
     *
     * @return bool
     */
    public function isConfigurable(): bool
    {
        return (bool) $this->requireConstant('IS_CONFIGURABLE');
    }
}
