<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserEntity;

use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Access\Model\UserRoles;
use Webmozart\Assert\Assert;

/**
 * Class User
 * @package Saluki\Authentication\Entity
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface
{
    const NOW = null;

    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private UuidInterface $id;

    #[ORM\Column(type: 'string', length: 255, unique: true, nullable: false)]
    private string $username;

    #[ORM\Column(type: 'string', length: 512, nullable: false)]
    private string $displayName;

    #[ORM\Column(type: 'string', length: 512, unique: true, nullable: false)]
    private string $email;

    #[ORM\Column(type: 'string', length: 1024, nullable: true)]
    private string $password;

    #[ORM\Column(type: 'chronos_datetime', nullable: true)]
    private ChronosInterface $passwordTimestamp;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    private ChronosInterface $createdTimestamp;

    #[ORM\Column(type: 'chronos_datetime', nullable: true)]
    private ?ChronosInterface $verifiedTimestamp;

    #[ORM\Column(type: 'chronos_datetime', nullable: true)]
    private ?ChronosInterface $approvedTimestamp;

    /**
     * @var array<int,string>
     */
    #[ORM\Column(type: 'json', nullable: false)]
    private array $roles = [];

    /**
     * User constructor.
     *
     * @param string $email
     * @param string $username
     * @param string $password
     * @param string $displayName
     * @param bool $isApproved
     * @param bool $isVerified
     * @param array $roles
     * @throws Exception
     */
    final public function __construct(
        string $email,
        string $username,
        string $password,
        string $displayName,
        bool $isApproved = false,
        bool $isVerified = false,
        array $roles = []
    ) {
        $this->id = Uuid::uuid4();
        $this->email = $email;
        $this->username = $username;
        $this->displayName = $displayName;
        $this->createdTimestamp = Chronos::now();
        $this->approvedTimestamp = $isApproved ? Chronos::now() : null;
        $this->verifiedTimestamp = $isVerified ? Chronos::now() : null;
        $this->setPassword($password);
        $this->roles = $roles;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getCreatedTimestamp(): ChronosInterface
    {
        return $this->createdTimestamp;
    }

    public function isVerified(): bool
    {
        return (bool) $this->verifiedTimestamp;
    }

    public function isApproved(): bool
    {
        return (bool) $this->approvedTimestamp;
    }

    public function getEmailObfuscated(): string
    {
        return (strlen($this->email) > 4)
            ? '********' . substr($this->email, -4)
            : '*' . explode('@', $this->email, 2)[1];
    }

    public function getVerifiedTimestamp(): ?ChronosInterface
    {
        return $this->verifiedTimestamp;
    }

    public function getApprovedTimestamp(): ?ChronosInterface
    {
        return $this->approvedTimestamp;
    }

    /**
     * Return TRUE if the user can log in (is verified and approved)
     */
    public function canLogin(): bool
    {
        return $this->isVerified() && $this->isApproved();
    }

    public function getIdentifier(): string
    {
        return $this->getId()->__toString();
    }

    /**
     * @return array<int,string>
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getPasswordTimestamp(): ChronosInterface
    {
        return $this->passwordTimestamp;
    }

    public function isAnonymous(): bool
    {
        return false;
    }

    // ---------------------------------------------------------------
    // Mutations

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @param string $displayName
     */
    public function setDisplayName(string $displayName): void
    {
        $this->displayName = $displayName;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @param string $hashedPassword
     */
    public function setPassword(string $hashedPassword): void
    {
        $this->password = $hashedPassword;
        $this->passwordTimestamp = Chronos::now();
    }

    /**
     * Set all roles (clobbers existing)
     *
     * @param array<int,string> $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = [];
        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    /**
     * Add a role if it is not already added
     *
     * @param string $role
     */
    public function addRole(string $role): void
    {
        Assert::oneOf($role, UserRoles::list());

        if (! in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }
    }

    public function verify(?DateTimeInterface $timestamp = self::NOW)
    {
        if (! $this->verifiedTimestamp) {
            $this->verifiedTimestamp = $timestamp ? Chronos::instance($timestamp) : Chronos::now();
        }
    }

    /**
     * Remove verification
     */
    public function removeVerification(): void
    {
        $this->verifiedTimestamp = null;
    }

    public function approve(?DateTimeInterface $timestamp = self::NOW): void
    {
        if (! $this->approvedTimestamp) {
            $this->approvedTimestamp = $timestamp ? Chronos::instance($timestamp) : Chronos::now();
        }
    }

    /**
     * Remove approval
     */
    public function rescindApproval(): void
    {
        $this->approvedTimestamp = null;
    }
}
