<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserEntity;

use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Contract\UserRepositoryInterface;
use Saluki\Authentication\UserTokenEntity\UserToken;
use Saluki\Persistence\Model\AbstractEntityRepository;

/**
 * Class UserRepository
 *
 * @package Saluki\Authentication\Entity
 * @method UserInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserInterface|null findOneBy(array $criteria, array $orderBy = null)
 */
class UserRepository extends AbstractEntityRepository implements UserRepositoryInterface
{
    /**
     * @param UserInterface|User $user
     * @throws \Doctrine\ORM\ORMException
     */
    public function add(UserInterface $user): void
    {
        $this->getEntityManager()->persist($user);
    }

    /**
     * @param UserInterface $user
     * @throws \Doctrine\ORM\ORMException
     */
    public function remove(UserInterface $user): void
    {
        $this->getEntityManager()->remove($user);

        // Because we cannot use database relational integrity constraints,
        // we must manually delete all of the related UserTokens
        try {
            $userTokenRepo = $this->getEntityManager()->getRepository(UserToken::class);
            foreach ($userTokenRepo->findBy(['userId' => $user->getIdentifier()]) as $token) {
                $this->getEntityManager()->remove($token);
            }
        } catch (\Exception $e) {
            // pass..
        }
    }


    /**
     * @param string $identifier
     * @return null|UserInterface
     */
    public function findByIdentifier(string $identifier): ?UserInterface
    {
        $qb = $this->createQueryBuilder('u');
        $qb->orWhere($qb->expr()->eq('u.id', ':id'));
        $qb->orWhere($qb->expr()->eq('u.username', ':id'));
        $qb->setParameter(':id', $identifier);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
