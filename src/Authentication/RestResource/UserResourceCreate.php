<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/1/18
 * Time: 4:03 PM
 */

namespace Saluki\Authentication\RestResource;


use Psr\Http\Message\ResponseInterface;
use Saluki\RestResource\Endpoint\CreateEndpoint;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\SalukiRequest;
use Saluki\WebInterface\RequestParam\ParameterList;

/**
 * Class UserResourceCreate
 *
 * @package Saluki\Authentication\RestResource
 */
class UserResourceCreate extends CreateEndpoint
{
    public function listQueryParameters(): ParameterList
    {
        $params = parent::listQueryParameters();

        $params->optional('token_url')->url()->contains('{token}')->setDescription(
            'Verification URL in client App (include "{token}" placeholder in URL'
        );

        return $params;
    }

    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        // Ensure this client can use this verification_url
        if ($verifyUrl = $request->findBodyParam('token_url')) {
            $verifyHost = parse_url($verifyUrl, PHP_URL_HOST);
            if (! $request->getClientApp()->isAllowedDomain($verifyHost)) {
                return JsonApiErrorResponse::forbidden();
            }
        }

        return parent::__invoke($request);
    }
}
