<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/1/18
 * Time: 3:30 PM
 */

namespace Saluki\Authentication\RestResource;

use Doctrine\ORM\QueryBuilder;
use Saluki\Authentication\Helper\UserValidators;
use Saluki\Authentication\Permission\ManageUsersPermission;
use Saluki\Authentication\Service\PasswordService;
use Saluki\Authentication\UserEntity\User;
use Saluki\RestResource\Field\FieldListBuilder;
use Saluki\RestResource\Model\AbstractRestResource;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\RestResource\Model\RestResourceAccess;
use Saluki\RestResource\Service\RestResourceHelper;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\ValueTransformer;
use Saluki\WebInterface\Contract\EndpointInterface;
use Saluki\WebInterface\Model\SalukiRequest;

/**
 * Class UserResource
 * @package Saluki\Authentication\RestResource
 */
class UserResource extends AbstractRestResource
{
    public const ENTITY_CLASS = User::class;
    public const NAME = 'users';

    public const PAGINATION_DEFAULT_LIMIT = 100;
    public const PAGINATION_MAX_LIMIT = null;
    public const DEFAULT_SORT = ['username' => 'asc'];
    /**
     * @var UserValidators
     */
    private $validators;
    /**
     * @var PasswordService
     */
    private $passwordService;

    /**
     * UserResource constructor.
     * @param RestResourceHelper $helper
     * @param UserValidators $validators
     * @param PasswordService $passwordService
     */
    public function __construct(
        RestResourceHelper $helper,
        UserValidators $validators,
        PasswordService $passwordService
    ) {
        $this->validators = $validators;
        $this->passwordService = $passwordService;

        parent::__construct($helper);
    }

    /**
     * Configure fields
     *
     * @param FieldListBuilder $fields
     */
    protected function configureFields(FieldListBuilder $fields): void
    {
        $fields->string('username')
            ->readable()
            ->writable()
                ->requiredOnCreate()
                ->notOnUpdate()
                ->addRuleSet($this->validators->usernameRules());

        $fields->string('email')
            ->readable()
            ->writable()
                ->requiredOnCreate()
                ->addRuleSet($this->validators->emailRules());

        $fields->string('password')
            ->readable()
                ->getter(function (User $user) {
                    return (! empty($user->getPassword())) ?  '**********' : null;
                })
            ->writable()
                ->addRuleSet($this->validators->passwordRules())
                ->transform(function ($value) {
                    return $this->passwordService->hash($value);
                }, ValueTransformerInterface::AFTER_VALIDATION);

        $fields->string('displayName')
            ->readable()
            ->writable()
                ->requiredOnCreate()
                ->addRuleSet($this->validators->displayNameRules());

        $fields->boolean('approved')
            ->readable()
            ->writable()
                ->optionalOnCreate()
                ->default(false)
                ->constructorArgName('isApproved')
                ->setter(function (User $user, $value) {
                    ($value) ? $user->approve() : $user->rescindApproval();
                })
                ->onlyIf(function (User $user) {
                    return $this->accessChecker->isAllowed(ManageUsersPermission::class, $user);
                });

        $fields->boolean('verified')
            ->readable()
            ->writable()
                ->optionalOnCreate()
                ->default(false)
            ->setter(function (User $user, $value) {
                ($value) ? $user->verify() : $user->removeVerification();
            })
            ->onlyIf(function (User $user) {
                return $this->accessChecker->isAllowed(ManageUsersPermission::class, $user);
            });

        $fields->list('roles', 'string')
            ->readable()
            ->writable()
                ->addRuleSet($this->validators->rolesRules())
                ->onlyIf(function (User $user) {
                    return $this->accessChecker->isAllowed(ManageUsersPermission::class, $user);
                });
    }

    /**
     * Configure access
     *
     * @param RestResourceAccess $access
     */
    protected function setAccess(RestResourceAccess $access): void
    {
        // Either the requester is allowed to manage users or the requester matches the user ID
        $access->default = function (RestRequestContext $request, ?User $user = null) {
            return $this->accessChecker->isAllowed(ManageUsersPermission::class, $request->getUser())
                or ($user && $request->getUser()->getIdentifier() == $user->getIdentifier());
        };

        // Only admins are allowed to see the index
        $access->index = function (RestRequestContext $request) {
            return $this->accessChecker->isAllowed(ManageUsersPermission::class, $request->getUser());
        };
    }

    /**
     * Add any additional rules for queries here (specifically, user or role-based access)
     *
     * @param SalukiRequest $request
     * @param QueryBuilder $qb
     */
    protected function prepareIndexQuery(SalukiRequest $request, QueryBuilder $qb): void
    {
        // PASS...
        // Since only admins are allowed to see the index, we don't have any special restrictions
    }

    /**
     * @return EndpointInterface
     */
    protected function buildCreateEndpoint(): EndpointInterface
    {
        return new UserResourceCreate($this);
    }
}
