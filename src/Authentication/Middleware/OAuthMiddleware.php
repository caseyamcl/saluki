<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Middleware;

use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\Authentication\Exception\InvalidAccessTokenException;
use Saluki\Authentication\Exception\InvalidAuthException;
use Saluki\Authentication\Model\AnonymousClient;
use Saluki\Authentication\Model\AnonymousUser;
use Saluki\Authentication\Service\AuthVerifier;
use Saluki\Authentication\Service\OAuthService;
use Saluki\Authentication\Service\OAuthUserRepository;
use Laminas\Diactoros\Response;

/**
 * Class OAuthMiddleware
 * @package Saluki\Authentication\Middleware
 */
class OAuthMiddleware implements MiddlewareInterface
{
    private OAuthUserRepository $authUserRepository;
    private AuthVerifier $authVerifier;
    private OAuthService $oAuthService;

    /**
     * OAuthMiddleware constructor.
     *
     * @param OAuthService $oAuthService
     * @param OAuthUserRepository $authUserRepository
     * @param AuthVerifier $authVerifier
     */
    public function __construct(
        OAuthService $oAuthService,
        OAuthUserRepository $authUserRepository,
        AuthVerifier $authVerifier
    ) {
        $this->authUserRepository = $authUserRepository;
        $this->authVerifier = $authVerifier;
        $this->oAuthService = $oAuthService;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * to the next middleware component to create the response.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // If no auth header present, assume anonymous user (OAuth library only checks 'Authorization' header
        // with bearer token)
        if (! $this->shouldAuthorizeOAuth($request)) {
            $request = $request
                ->withAttribute('user', new AnonymousUser())
                ->withAttribute('client_app', new AnonymousClient());
            return $handler->handle($request);
        }

        // If auth header present, resolve to current user using OAuth libraries
        try {
            $request = $this->oAuthService->getResourceServer()->validateAuthenticatedRequest($request);

            // Request object now has the following additional attributes:
            //
            // - oauth_access_token_id - the access token identifier
            // - oauth_client_id       - the client identifier
            // - oauth_user_id         - the user identifier represented by the access token
            // - oauth_scopes          - an array of string scope identifiers (currently unused)

            // Ensure client and user can be used
            try {
                // Add the user to the request object her as an attribute.
                $request = $request->withAttribute('user', $this->authUserRepository->resolveAccessTokenToUser(
                    $request->getAttribute('oauth_access_token_id')
                ));

                $this->authVerifier->ensureUserIsValid($request->getAttribute('user'));
                $client = $this->authVerifier->ensureClientIsValid($request, $request->getAttribute('oauth_client_id'));
                $request = $request->withAttribute('client_app', $client);
                return $handler->handle($request);
            } catch (InvalidAuthException | InvalidAccessTokenException $e) {
                throw OAuthServerException::accessDenied($e->getMessage());
            }
        } catch (OAuthServerException $e) { // Something went wrong.
            return $e->generateHttpResponse(new Response());
        }
    }

    /**
     * Should this server authorize the request as an OAuth request?
     *
     * @param ServerRequestInterface $request
     * @return bool
     */
    private function shouldAuthorizeOAuth(ServerRequestInterface $request): bool
    {
        if ($request->hasHeader('authorization')) {
            $authLine = strtolower($request->getHeaderLine('authorization'));
            return strcmp(substr($authLine, 0, strlen('bearer ')), 'bearer ') === 0;
        } else {
            return false;
        }
    }
}
