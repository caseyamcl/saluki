<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/20/18
 * Time: 2:42 PM
 */

namespace Saluki\Authentication\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\Authentication\Model\AnonymousClient;
use Saluki\WebInterface\Behavior\RequestHelper;
use Saluki\WebInterface\Model\JsonApiErrorResponse;

/**
 * Require that OAuth has been processed middleware
 *
 * @package Saluki\Authentication\Middleware
 */
class RequireOAuthMiddleware implements MiddlewareInterface
{
    use RequestHelper;

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $client = $this->getClientFromRequest($request, false);
        if ((! $client) or $client instanceof AnonymousClient) {
            return JsonApiErrorResponse::forbidden();
        }

        return $handler->handle($request);
    }
}
