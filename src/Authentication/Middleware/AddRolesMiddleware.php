<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/27/18
 * Time: 9:17 PM
 */

namespace Saluki\Authentication\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\WebInterface\Behavior\RequestHelper;

/**
 * Class AddRolesMiddleware
 * @package Saluki\Authentication\Middleware
 */
class AddRolesMiddleware implements MiddlewareInterface
{
    const ROLES_HEADER_NAME = 'X-User-Roles';

    use RequestHelper;

    /**
     * @param ResponseInterface $response
     * @param array|string[] $roles
     * @return ResponseInterface
     */
    public static function addRolesToResponse(ResponseInterface $response, array $roles): ResponseInterface
    {
        return $response->withAddedHeader(static::ROLES_HEADER_NAME, implode(',', $roles));
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);

        // If not already added, add the roles now.
        if (! $response->hasHeader(static::ROLES_HEADER_NAME)) {
            $response = static::addRolesToResponse($response, $this->getUserFromRequest($request)->getRoles());
        }

        return $response;
    }
}
