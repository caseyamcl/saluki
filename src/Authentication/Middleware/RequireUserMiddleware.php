<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/5/18
 * Time: 2:47 PM
 */

namespace Saluki\Authentication\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\WebInterface\Behavior\RequestHelper;
use Saluki\WebInterface\Model\JsonApiErrorResponse;

/**
 * Class RequireAuthMiddleware
 * @package Saluki\Authentication\Middleware
 */
class RequireUserMiddleware implements MiddlewareInterface
{
    use RequestHelper;

    protected const ALL = ['*'];

    /**
     * @var array
     */
    private $roles;

    /**
     * Allow only authenticated users with specific roles
     *
     * @param array|string[] $roles
     * @return RequireUserMiddleware
     */
    public static function roles(array $roles): self
    {
        return new static($roles);
    }

    /**
     * Allow any authenticated, non-anonymous user
     *
     * @return RequireUserMiddleware
     */
    public static function auth(): self
    {
        return new static();
    }

    /**
     * RequireAuthMiddleware constructor.
     * @param array $onlyRoles
     */
    public function __construct(array $onlyRoles = self::ALL)
    {
        $this->roles = $onlyRoles;
    }


    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $user = $this->getUserFromRequest($request);

        if ($user->isAnonymous()) {
            return JsonApiErrorResponse::forbidden();
        }

        if ($this->roles !== self::ALL && count(array_intersect($user->getRoles(), $this->roles)) == 0) {
            return JsonApiErrorResponse::forbidden();
        }

        return $handler->handle($request);
    }
}
