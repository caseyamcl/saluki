<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 1:19 PM
 */

namespace Saluki\Authentication\Exception;

use Throwable;

/**
 * Class InvalidAuthException
 * @package Saluki\Authentication\Exception
 */
class InvalidAuthException extends \RuntimeException
{
    /**
     * InvalidAuthException constructor.
     * @param string $message
     * @param Throwable|null $previous
     * @param int $code
     */
    public function __construct(string $message = "", Throwable $previous = null, int $code = 401)
    {
        parent::__construct($message, $code, $previous);
    }
}
