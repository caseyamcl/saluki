<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Exception;

/**
 * Invalid Access Token Exception
 *
 * This is thrown when a valid access token exists, but the user for the access token cannot be found.
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class InvalidAccessTokenException extends \RuntimeException
{
    // pass..
}
