<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 4:16 PM
 */

namespace Saluki\Authentication\ConsoleCommand;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\OAuthEntity\OAuthClient;
use Saluki\Authentication\OAuthEntity\OAuthClientRepository;
use Saluki\Authentication\Setup\SetupOAuthClientStep;
use Saluki\Console\ConsoleIO;
use Saluki\Persistence\Service\TransactionManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SetupOAuthClient
 *
 * @package Saluki\Authentication\ConsoleCommand
 */
class SetupOAuthClient extends Command
{
    /**
     * @var SetupOAuthClientStep
     */
    private $setupClient;

    /**
     * @var ClientRepositoryInterface
     */
    private $clientRepository;

    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * SetupOAuthClient constructor.
     * @param SetupOAuthClientStep $setupClient
     * @param OAuthClientRepository $clientRepository
     * @param TransactionManager $transactionManager
     */
    public function __construct(
        SetupOAuthClientStep $setupClient,
        OAuthClientRepository $clientRepository,
        TransactionManager $transactionManager
    ) {
        parent::__construct();
        $this->setupClient = $setupClient;
        $this->clientRepository = $clientRepository;
        $this->transactionManager = $transactionManager;
    }

    protected function configure()
    {
        $this->setName('auth:client');
        $this->setDescription('Add (or remove) an OAuth client');

        $this->addOption(
            'ip',
            '',
            InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
            'Specify valid IP range(s) for this client (default is to allow all IPs)',
            null
        );

        $this->addOption(
            'domains',
            '',
            InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
            'Specify valid callback domain(s) for this client (default is none)',
            null
        );

        $this->addOption(
            'enable-system-acct',
            's',
            InputOption::VALUE_NONE,
            'Enable the anonymous "system" account for this client (this is dangerous; see --help for details)'
        );

        $this->addOption(
            'description',
            'd',
            InputOption::VALUE_REQUIRED,
            'A descriptive name for the client'
        );

        // Delete mode...
        $this->addOption(
            'delete',
            '',
            InputOption::VALUE_REQUIRED,
            'Delete an auth client with the given client key'
        );

        // Disable mode...
        $this->addOption(
            'disable',
            '',
            InputOption::VALUE_REQUIRED,
            'Disable an auth client with the given key'
        );

        // Enable mode...
        $this->addOption(
            'enable',
            '',
            InputOption::VALUE_REQUIRED,
            'Disable an auth client with the given key'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new ConsoleIO($input, $output);

        switch (true) {
            case $input->getOption('disable'):
                $client = $this->getClientEntity($input->getOption('disable'));
                $result = $this->transactionManager->execute(function () use ($client) {
                    if ($client->isEnabled()) {
                        $client->disable();
                        return true;
                    } else {
                        return false;
                    }
                });
                $result
                    ? $io->success(sprintf('Client disabled: <info>%s</info>', $client->getId()))
                    : $io->note('Client was already disabled');
                break;
            case $input->getOption('enable'):
                $client = $this->getClientEntity($input->getOption('enable'));
                $result = $this->transactionManager->execute(function () use ($client) {
                    if (! $client->isEnabled()) {
                        $client->enable();
                        return true;
                    } else {
                        return false;
                    }
                });
                $result
                    ? $io->success(sprintf('Client enabled: <info>%s</info>', $client->getId()))
                    : $io->note('Client was already enabled');
                break;
            case $input->getOption('delete'):
                $client = $this->getClientEntity($input->getOption('delete'));
                $this->transactionManager->execute(function () use ($client) {
                    $this->clientRepository->remove($client);
                });
                $io->success(sprintf('Deleted client <info>%s</info>', $client->getId()));
                break;
            default: // create new client
                return $this->setupClient->__invoke($io, true)->succeeded() ? 0 : 1;
        }

        return 0;
    }

    /**
     * @param string $clientId
     * @return OAuthClient|ClientEntityInterface
     */
    private function getClientEntity(string $clientId): OAuthClient
    {
        if ($client = $this->clientRepository->getClientEntity($clientId)) {
            return $client;
        } else {
            throw new \RuntimeException('Invalid or non-existent client ID: ' . $clientId);
        }
    }
}
