<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/5/18
 * Time: 4:14 PM
 */

namespace Saluki\Authentication\ConsoleCommand;

use Saluki\Authentication\OAuthEntity\OAuthClient;
use Saluki\Authentication\OAuthEntity\OAuthClientRepository;
use Saluki\Console\ConsoleIO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ListOAuthClients
 * @package Saluki\Authentication\ConsoleCommand
 */
class ListOAuthClients extends Command
{
    private OAuthClientRepository $repository;

    /**
     * ListOAuthClients constructor.
     * @param OAuthClientRepository $repository
     */
    public function __construct(OAuthClientRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    protected function configure()
    {
        $this->setName('auth:clients');
        $this->setDescription('List OAuth client apps');
        $this->addOption(
            'show-disabled',
            'd',
            InputOption::VALUE_NONE,
            'Include disabled clients'
        );

        $this->addOption(
            'filter',
            'f',
            InputOption::VALUE_REQUIRED,
            'Filter by description'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new ConsoleIO($input, $output);

        $qb = $this->repository->createQueryBuilder('t');
        if (! $io->getOption('show-disabled')) {
            $qb->andWhere($qb->expr()->eq('t.enabled', ':enabled'));
            $qb->setParameter(':enabled', true);
        }
        if ($io->getOption('filter')) {
            $qb->andWhere($qb->expr()->like('t.description', ':desc'));
            $qb->setParameter(':desc', '%' . $io->getOption('filter') . "%");
        }
        $iterator = $qb->getQuery()->getResult();

        /** @var OAuthClient $item */
        foreach ($iterator as $item) {
            $table = new Table($io);
            $table->addRow(['Description:', sprintf('<info>%s</info>', $item->getDescription())]);
            $table->addRow(['Client Key:', sprintf('<info>%s</info>', $item->getId())]);
            $table->addRow(['Client Secret:', sprintf('<info>%s</info>', $item->getSecret())]);
            $table->addRow(['System Key:', sprintf('<info>%s</info>', $item->getSystemUserKey() ?: '-')]);
            $table->addRow(['Created:', sprintf('<info>%s</info>', $item->getCreatedTimestamp()->toIso8601String())]);
            $table->addRow(['IPs:', sprintf('<info>%s</info>', implode(', ', $item->getAllowedIps()))]);

            if ($io->getOption('show-disabled')) {
                $table->addRow(['Enabled:', sprintf(
                    '<info>%s</info>',
                    $item->isEnabled() ? $io->check() . ' Yes' : $io->x() . ' No'
                )]);
            }

            $table->render();
            $io->newLine();
        }

        return 0;
    }
}
