<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 4:15 PM
 */

namespace Saluki\Authentication\ConsoleCommand;

use Saluki\Authentication\Setup\SetupOAuthKeysStep;
use Saluki\Console\ConsoleIO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SetupKeys
 * @package Saluki\Authentication\ConsoleCommand
 */
class SetupKeys extends Command
{
    private SetupOAuthKeysStep $setupOAuthKeys;

    /**
     * SetupKeys constructor.
     * @param SetupOAuthKeysStep $setupOAuthKeys
     */
    public function __construct(SetupOAuthKeysStep $setupOAuthKeys)
    {
        parent::__construct();
        $this->setupOAuthKeys = $setupOAuthKeys;
    }


    protected function configure()
    {
        $this->setName('auth:keys');
        $this->setDescription('Setup OAuth public/private key');
        $this->addOption(
            'force',
            'f',
            InputOption::VALUE_NONE,
            'Set keys even if existing keys already exist'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new ConsoleIO($input, $output);
        $result = $this->setupOAuthKeys->__invoke($io);
        $io->success($result->getMessage());
    }
}
