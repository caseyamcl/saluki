<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\OAuthEntity;

use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;
use Doctrine\ORM\Mapping as ORM;
use LogicException;
use Saluki\Authentication\Model\AnonymousUser;

/**
 * Class OAuthAccessToken
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
#[ORM\Entity(repositoryClass: OAuthAccessTokenRepository::class)]
class OAuthAccessToken implements AccessTokenEntityInterface
{
    use AccessTokenTrait;

    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 255)]
    private string $id;

    #[ORM\ManyToOne(targetEntity: 'OAuthClient')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    private OAuthClient|ClientEntityInterface $client;

    #[ORM\Column(type: 'string', nullable: false)]
    private string $userId;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    private ChronosInterface $createdTimestamp;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    private ChronosInterface $expiresTimestamp;

    /**
     * Not currently used, but required by interface
     *
     * @var Collection<int,ScopeEntityInterface>
     */
    #[ORM\JoinTable]
    #[ORM\InverseJoinColumn(name: 'scope_name', referencedColumnName: 'name')]
    #[ORM\ManyToMany(targetEntity: 'Saluki\Authentication\OAuthEntity\OAuthScope')]
    private Collection $scopes;

    /**
     * OAuthAccessToken constructor.
     *
     * @param OAuthClient $client
     * @param string $userId
     * @param iterable|ScopeEntityInterface[] $scopes
     */
    public function __construct(OAuthClient $client, string $userId, iterable $scopes = [])
    {
        $this->createdTimestamp = Chronos::now();
        $this->scopes = new ArrayCollection();

        $this->client = $client;
        $this->userId = $userId;

        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }

    public function getClient(): OAuthClient
    {
        return $this->client;
    }

    public function getExpiryDateTime(): ChronosInterface
    {
        return $this->expiresTimestamp;
    }

    public function getUserIdentifier(): string
    {
        return $this->userId;
    }

    /**
     * @return Collection<int,ScopeEntityInterface>
     */
    public function getScopes(): Collection
    {
        return $this->scopes;
    }

    /**
     * Get the token's identifier.
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->id;
    }

    /**
     * Set the token's identifier.
     *
     * @param $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->id = $identifier;
    }

    /**
     * Set the date time when the token expires.
     *
     * @param DateTimeImmutable $dateTime
     */
    public function setExpiryDateTime(DateTimeImmutable $dateTime)
    {
        $this->expiresTimestamp = Chronos::instance($dateTime);
    }

    /**
     * Set the identifier of the user associated with the token.
     *
     * @param string|int $identifier The identifier of the user
     */
    public function setUserIdentifier($identifier): string|int
    {
        if ($this->userId && $identifier != $this->getIdentifier()) {
            throw new LogicException("User ID is immutable once set");
        }

        return $this->userId = $identifier ?: AnonymousUser::ANONYMOUS_USER_ID;
    }

    /**
     * Set the client that the token was issued to.
     *
     * @param ClientEntityInterface $client
     */
    public function setClient(ClientEntityInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Associate a scope with the token.
     *
     * @param ScopeEntityInterface $scope
     */
    public function addScope(ScopeEntityInterface $scope)
    {
        $this->scopes->add($scope);
    }

    /**
     * @return ChronosInterface
     */
    public function getCreatedTimestamp(): ChronosInterface
    {
        return $this->createdTimestamp;
    }
}
