<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\OAuthEntity;

use Doctrine\DBAL\ParameterType;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Saluki\Persistence\Model\AbstractEntityRepository;

/**
 * Class OAuthClientRepository
 * @method OAuthClient get(string $identifier)
 * @method OAuthClient find($id, $lockMode = null, $lockVersion = null)
 */
class OAuthClientRepository extends AbstractEntityRepository implements ClientRepositoryInterface
{
    /**
     * @param OAuthClient $authClient
     */
    public function add(OAuthClient $authClient): void
    {
        $this->getEntityManager()->persist($authClient);
    }

    /**
     * Get a client.
     *
     * @param string $clientIdentifier The client's identifier
     * @return ClientEntityInterface|null
     */
    public function getClientEntity($clientIdentifier): ?ClientEntityInterface
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')->from(OAuthClient::class, 'c');
        $qb->where($qb->expr()->eq('c.id', ':id'));
        $qb->setParameter(':id', $clientIdentifier, ParameterType::STRING);

        /** @var OAuthClient $client */
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Return TRUE if at-least one valid (enabled) client exists
     * @return bool
     */
    public function atLeastOneValidClientExists(): bool
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')->from(OAuthClient::class, 'c');
        $qb->andWhere($qb->expr()->eq('c.enabled', ':true'));
        $qb->setParameter(':true', true);
        $qb->setMaxResults(1);

        $stmt = $qb->getQuery();
        $stmt->execute();
        return (bool) $stmt->getResult();
    }

    /**
     * @param OAuthClient $client
     */
    public function remove(OAuthClient $client)
    {
        $this->getEntityManager()->remove($client);
    }

    public function validateClient($clientIdentifier, $clientSecret, $grantType)
    {
        // Get the client by its identifier or bail
        if (! $client = $this->getClientEntity($clientIdentifier)) {
            return false;
        }

        // If there was a client secret passed, compare it to the stored secret
        if ($clientSecret) {
            return strcmp($client->getSecret(), $clientSecret) === 0;
        }

        return true;
    }
}
