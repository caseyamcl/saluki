<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\OAuthEntity;

use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use Ramsey\Uuid\Uuid;
use Doctrine\ORM\Mapping as ORM;
use RuntimeException;
use Saluki\Authentication\Contract\ClientAppInterface;

/**
 * Class OAuthClient
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
#[ORM\Entity(repositoryClass: OAuthClientRepository::class)]
class OAuthClient implements ClientEntityInterface, ClientAppInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 64)]
    protected string $id;

    #[ORM\Column(type: 'encrypted_string', length: 512)]
    protected string $secret;

    #[ORM\Column(type: 'text', nullable: true)]
    protected string $description;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    protected ChronosInterface $createdTimestamp;

    /**
     * @var array|string[]
     */
    #[ORM\Column(type: 'json', nullable: false)]
    protected array $allowedIps = [];

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'string', nullable: true)]
    protected ?string $ownerId;

    /**
     * @var string|null
     */
    #[ORM\Column(type: 'encrypted_string', length: 512, nullable: true)]
    protected ?string $systemKey;

    /**
     * @var bool
     */
    #[ORM\Column(type: 'boolean', nullable: false)]
    protected bool $enabled = true;

    /**
     * @var array|string[]
     */
    #[ORM\Column(type: 'json', nullable: false)]
    protected array $domainNames = [];

    /**
     * OAuthClient constructor.
     *
     * @param string $description
     * @param array $allowedIps
     * @param array $domainNames
     * @param string|null $ownerId
     */
    public function __construct(
        string $description,
        array $allowedIps = self::ALL,
        array $domainNames = [],
        string $ownerId = null
    ) {
        $this->regenerateKeyAndSecret();

        $this->createdTimestamp = Chronos::now();
        $this->description      = $description;
        $this->allowedIps       = $allowedIps;
        $this->ownerId          = $ownerId;
        $this->domainNames      = $domainNames;
    }

    /**
     * Get the client's identifier.
     */
    public function getIdentifier(): string
    {
        return (string) $this->id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get the client's name.
     *
     * Alias for self::getDescription()
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->description;
    }

    /**
     * Returns the registered redirect URI (as a string).
     *
     * Since this an API, we don't allow OAuth grants that use redirectUris, and we return an empty array
     *
     * @return string|iterable<int,string>
     */
    public function getRedirectUri(): string|iterable
    {
        return [];
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCreatedTimestamp(): ChronosInterface
    {
        return $this->createdTimestamp;
    }

    /**
     * @return array<int,string>
     */
    public function getAllowedIps(): array
    {
        return $this->allowedIps;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @param array<int,string> $allowedIps
     */
    public function setAllowedIps(array $allowedIps)
    {
        $this->allowedIps = $allowedIps;
    }

    /**
     * Generate a new API Key and secret
     */
    public function regenerateKeyAndSecret()
    {
        $this->id     = hash('sha256', Uuid::uuid4());
        $this->secret = hash('sha256', Uuid::uuid4());
    }

    public function getOwnerId(): ?string
    {
        return $this->ownerId;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * Allow the 'SYSTEM' user from this client (set a systemKey)
     *
     * You can specify a system key or create a random one
     *
     * @param string|null $systemKey
     */
    public function allowSystemUser(?string $systemKey = null): void
    {
        if (! $this->allowsSystemAccount()) {
            $this->generateSystemUserKey($systemKey);
        }
    }

    /**
     * @param string|null $systemKey
     */
    public function regenerateSystemKey(string $systemKey = null): void
    {
        if ($this->allowsSystemAccount()) {
            $this->generateSystemUserKey($systemKey);
        } else {
            throw new RuntimeException("Cannot regenerate system user key when system user not allowed");
        }
    }

    /**
     * @param string|null $systemKey
     */
    protected function generateSystemUserKey(?string $systemKey = null): void
    {
        $this->systemKey = $systemKey ?: hash('sha256', Uuid::uuid4());
    }

    /**
     * Disallow the 'SYSTEM' user from this client
     */
    public function disallowSystemUser(): void
    {
        $this->systemKey = null;
    }

    /**
     * @return bool
     */
    public function allowsSystemAccount(): bool
    {
        return (bool) $this->systemKey;
    }

    /**
     * Get the unique key that allows a non-person request to map to the system account
     *
     * @return string|null
     */
    public function getSystemUserKey(): ?string
    {
        return $this->systemKey;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * Enable this
     */
    public function enable()
    {
        $this->enabled = true;
    }

    /**
     * Disable
     */
    public function disable()
    {
        $this->enabled = false;
    }

    /**
     * @return bool
     */
    public function allowsAllIps(): bool
    {
        return $this->allowedIps == self::ALL;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function isOwnedByPersonWithId(string $id): bool
    {
        return $this->ownerId === $id;
    }

    /**
     * @param array $domainNames
     */
    public function setDomainNames(array $domainNames): void
    {
        $this->domainNames = $domainNames;
    }

    /**
     * @param string $domainName
     */
    public function addDomainName(string $domainName): void
    {
        if (! in_array($domainName, $this->domainNames)) {
            $this->domainNames[] = $domainName;
        }
    }

    /**
     * @return array<int,string>
     */
    public function getDomainNames(): array
    {
        return $this->domainNames;
    }

    /**
     * @param string $domain
     * @return bool
     */
    public function isAllowedDomain(string $domain): bool
    {
        return in_array(strtolower($domain), array_map('strtolower', $this->domainNames));
    }


    public function isConfidential()
    {
        return true;
    }
}
