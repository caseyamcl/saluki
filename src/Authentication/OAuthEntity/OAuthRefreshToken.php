<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\OAuthEntity;

use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;
use DateTimeImmutable;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class OAuthRefreshToken
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
#[ORM\Entity(repositoryClass:OAuthRefreshTokenRepository::class)]
class OAuthRefreshToken implements RefreshTokenEntityInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 100)]
    protected string $id;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    protected ChronosInterface $expires;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    protected ChronosInterface $createdDateTime;

    #[ORM\ManyToOne(targetEntity: 'OAuthAccessToken')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    protected OAuthAccessToken $accessToken;

    #[ORM\Column(type: 'chronos_datetime', nullable: true)]
    private ?ChronosInterface $revoked = null;

    /**
     * OAuthRefreshToken constructor.
     */
    public function __construct()
    {
        $this->createdDateTime = Chronos::now();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreatedDateTime(): ChronosInterface
    {
        return $this->createdDateTime;
    }

    public function getIdentifier(): string
    {
        return $this->id;
    }

    public function setIdentifier($identifier): void
    {
        $this->id = $identifier;
    }

    public function getExpiryDateTime(): ?ChronosInterface
    {
        return $this->expires;
    }

    /**
     * Set the date time when the token expires.
     *
     * @param DateTimeImmutable $dateTime
     */
    public function setExpiryDateTime(DateTimeImmutable $dateTime): void
    {
        $this->expires = Chronos::instance($dateTime);
    }

    /**
     * Set the access token that the refresh token was associated with.
     *
     * @param AccessTokenEntityInterface $accessToken
     */
    public function setAccessToken(AccessTokenEntityInterface $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Get the access token that the refresh token was originally associated with.
     *
     * @return AccessTokenEntityInterface
     */
    public function getAccessToken(): AccessTokenEntityInterface
    {
        return $this->accessToken;
    }

    /**
     * Has the token expired?
     *
     * @return bool
     */
    public function isExpired(): bool
    {
        return (Chronos::now() > $this->expires);
    }

    /**
     * Revoke the access token
     */
    public function revoke(): void
    {
        if (! $this->revoked) {
            $this->revoked = Chronos::now();
        }
    }

    /**
     * @return ChronosInterface
     */
    public function getRevoked(): ChronosInterface
    {
        return $this->revoked;
    }

    public function isRevoked(): bool
    {
        return (bool) $this->revoked;
    }
}
