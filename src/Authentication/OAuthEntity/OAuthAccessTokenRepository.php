<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\OAuthEntity;

use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterval;
use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Saluki\Authentication\Model\AnonymousUser;
use Saluki\Authentication\Setting\OAuthAutoPurgeOldTokens;
use Saluki\Persistence\Model\AbstractEntityRepository;
use SettingsManager\Contract\SettingProvider;

/**
 * Class OAuthAccessTokenRepository
 * @method OAuthAccessToken find($id, $lockMode = null, $lockVersion = null)
 * @method OAuthAccessToken get($id, $lockMode = null, $lockVersion = null)
 * @method OAuthAccessToken findOneBy(array $criteria, array $orderBy = null)
 */
class OAuthAccessTokenRepository extends AbstractEntityRepository implements AccessTokenRepositoryInterface
{
    private SettingProvider $settings;

    /**
     * OAuthAccessTokenRepository constructor.
     * @param EntityManagerInterface $em
     * @param SettingProvider $settings
     * @throws \ReflectionException
     */
    public function __construct(EntityManagerInterface $em, SettingProvider $settings)
    {
        parent::__construct($em);
        $this->settings = $settings;
    }

    /**
     * Create a new access token
     * @param ClientEntityInterface|OAuthClient $clientEntity
     * @param ScopeEntityInterface[] $scopes
     * @param mixed $userIdentifier
     * @return AccessTokenEntityInterface
     * @throws \Exception  Should only be thrown in the case that the programmer/dev has done something horribly wrong
     */
    public function getNewToken(
        ClientEntityInterface $clientEntity,
        array $scopes,
        $userIdentifier = null
    ): AccessTokenEntityInterface {
        if (! $clientEntity instanceof OAuthClient) {
            throw new \Exception("Invalid implementation of ClientEntityInterface: " . get_class($clientEntity));
        }

        return new OAuthAccessToken($clientEntity, $userIdentifier ?: AnonymousUser::ANONYMOUS_USER_ID, $scopes);
    }

    /**
     * Persists a new access token to permanent storage.
     *
     * @param AccessTokenEntityInterface $accessTokenEntity
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity): void
    {
        if ($purgePeriod = $this->settings->findValue(OAuthAutoPurgeOldTokens::NAME)) {
            $this->removeExpiredAccessTokens(
                ChronosInterval::instance(new \DateInterval($purgePeriod)),
                $accessTokenEntity->getUserIdentifier()
            );
        }

        $this->getEntityManager()->persist($accessTokenEntity);
    }

    /**
     * Revoke an access token.
     *
     * @param ChronosInterval|null $olderThan NULL means remove all expired tokens
     * @param string|null $userId
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeExpiredAccessTokens(ChronosInterval $olderThan = null, string $userId = null): void
    {
        $qb = $this->createQueryBuilder('t');
        $qb->andWhere($qb->expr()->lt('t.expiresTimestamp', ':ts'));
        $qb->setParameter(':ts', ($olderThan ? Chronos::now()->sub($olderThan) : Chronos::now()));


        if ($userId) {
            $qb->andWhere($qb->expr()->eq('t.userId', ':user_id'));
            $qb->setParameter(':user_id', $userId);
        }

        // Remove tokens
        foreach ($qb->getQuery()->getResult() as $token) {
            $this->getEntityManager()->remove($token);
        }
    }

    /**
     * Check if the access token has been revoked.
     *
     * @param string $tokenId
     *
     * @return bool Return true if this token has been revoked
     */
    public function isAccessTokenRevoked($tokenId): bool
    {
        return (! (bool) $this->find($tokenId));
    }

    /**
     * Revoke an access token.
     *
     * @param string $tokenId
     */
    public function revokeAccessToken($tokenId)
    {
        if ($token = $this->find($tokenId)) {
            $this->getEntityManager()->remove($token);
        }
    }
}
