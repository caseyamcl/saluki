<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\OAuthEntity;

use League\OAuth2\Server\Entities\ScopeEntityInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class OAuthScope
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
#[ORM\Entity(repositoryClass: OAuthScopeRepository::class)]
class OAuthScope implements ScopeEntityInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'string', length: 100)]
    protected string $name;

    /**
     * OAuthScope constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->getName();
    }

    public function getIdentifier(): string
    {
        return $this->getName();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Serialize this to JSON
     */
    public function jsonSerialize(): string
    {
        return json_encode($this->getIdentifier());
    }
}
