<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Behavior;

use Saluki\Authentication\Contract\ClientAppInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use M6Web\Component\Firewall\Firewall;
use Psr\Http\Message\ServerRequestInterface;
use Saluki\Authentication\OAuthEntity\OAuthClient;
use Saluki\WebInterface\Behavior\RequestHelper;

/**
 * Class EnsureValidClientTrait
 * @package Saluki\Authentication\Behavior
 */
trait EnsureValidClientTrait
{
    use RequestHelper;

    protected function ensureClientCanBeUsed(ClientAppInterface $clientApp, ServerRequestInterface $request): void
    {
        if (! $clientApp->isEnabled()) {
            throw new OAuthServerException(
                'Client is disabled',
                20,
                'invalid_client',
                401
            );
        }

        if (! $this->checkIp($clientApp->getAllowedIps(), $this->getIpFromRequest($request))) {
            throw OAuthServerException::accessDenied('This client is not allowed from this IP');
        }
    }

    /**
     * Check the IP
     *
     * @param array $allowedIps
     * @param string $clientIp
     * @return bool
     */
    private function checkIp(array $allowedIps, string $clientIp)
    {
        if ($allowedIps === ClientAppInterface::ALL) {
            return true;
        } else {
            $firewall = new Firewall();
            $firewall->setDefaultState(false);
            $firewall->addList($allowedIps, 'good', true);
            $firewall->setIpAddress($clientIp);
            return $firewall->handle();
        }
    }
}
