<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 4:51 PM
 */

namespace Saluki\Authentication\Service;

use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Grant\GrantTypeInterface;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\ResourceServer;
use Saluki\Authentication\Contract\OAuthServiceInterface;
use Saluki\Authentication\OAuthEntity\OAuthAccessToken;
use Saluki\Authentication\OAuthEntity\OAuthClient;
use Saluki\Authentication\OAuthEntity\OAuthScope;
use Saluki\Authentication\Setting\OAuthAccessTokenExpiration;
use Saluki\Authentication\Setting\OAuthPrivateKey;
use Saluki\Authentication\Setting\OAuthPublicKey;
use Saluki\Authentication\Setting\OAuthRefreshTokenExpiration;
use Saluki\Config\Model\Config;
use SettingsManager\Contract\SettingProvider;

class OAuthService implements OAuthServiceInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var SettingProvider
     */
    private $settings;

    /**
     * @var iterable|GrantTypeInterface[]
     */
    private $grantTypes;

    /**
     * OAuthService constructor.
     * @param EntityManagerInterface $em
     * @param Config $config
     * @param SettingProvider $settingsProvider
     * @param iterable|GrantTypeInterface[] $grantTypes
     */
    public function __construct(
        EntityManagerInterface $em,
        Config $config,
        SettingProvider $settingsProvider,
        iterable $grantTypes = []
    ) {
        $this->em = $em;
        $this->config = $config;
        $this->settings = $settingsProvider;
        $this->grantTypes = $grantTypes;
    }

    /**
     * @param GrantTypeInterface $grantType
     */
    public function addGrantType(GrantTypeInterface $grantType)
    {
        $this->grantTypes[$grantType->getIdentifier()] = $grantType;
    }

    /**
     * Get an authorization server
     *
     * @return AuthorizationServer
     */
    public function getAuthorizationServer(): AuthorizationServer
    {

        // Private key value
        if (! $privateKeySetting = $this->settings->findValueInstance(OAuthPrivateKey::NAME)) {
            throw new \RuntimeException(sprintf(
                'Missing %s setting (run console command to correct this: `%s`)',
                OAuthPrivateKey::NAME,
                'setup:keys'
            ));
        }

        // Build server
        $server = new AuthorizationServer(
            $this->em->getRepository(OAuthClient::class),
            $this->em->getRepository(OAuthAccessToken::class),
            $this->em->getRepository(OAuthScope::class),
            $privateKeySetting->getValue(),
            $this->config->getEncryptionKey()
        );

        $accessTokenTTL = new \DateInterval($this->settings->getValue(OAuthAccessTokenExpiration::NAME));

        foreach ($this->grantTypes as $grantType) {
            if ($grantType instanceof RefreshTokenGrant) {
                $refreshTTL = new \DateInterval(
                    $this->settings->getValue(OAuthRefreshTokenExpiration::NAME)
                );
                $grantType->setRefreshTokenTTL($refreshTTL);
            }

            $server->enableGrantType($grantType, $accessTokenTTL);
        }

        return $server;
    }

    /**
     * Get a resource server
     *
     * @return ResourceServer
     */
    public function getResourceServer(): ResourceServer
    {
        // Private key value
        if (! $publicKeySetting = $this->settings->findValueInstance(OAuthPublicKey::NAME)) {
            throw new \RuntimeException(sprintf(
                'Missing %s setting (run console command to correct this: `%s`)',
                OAuthPublicKey::NAME,
                'setup:keys'
            ));
        }

        return new ResourceServer(
            $this->em->getRepository(OAuthAccessToken::class),
            $publicKeySetting->getValue()
        );
    }
}
