<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Service;

use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\FindUserByIdentifier;
use Saluki\Authentication\Contract\OAuthUserLookupInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Contract\UserResolverInterface;
use Saluki\Authentication\Exception\InvalidAccessTokenException;
use Saluki\Authentication\OAuthEntity\OAuthAccessTokenRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;

/**
 * Class OAuthUserRepository
 * @package Saluki\Authentication\Service
 */
class OAuthUserRepository implements OAuthUserLookupInterface
{
    /**
     * @var UserResolverInterface
     */
    private $userResolver;

    /**
     * @var OAuthAccessTokenRepository
     */
    private $accessTokenRepository;

    /**
     * @var FindUserByIdentifier
     */
    private $userFinder;

    /**
     * OAuthUserRepository constructor.
     *
     * @param UserResolverInterface $userResolver
     * @param OAuthAccessTokenRepository $accessTokenRepository
     * @param FindUserByIdentifier $userFinder
     */
    public function __construct(
        UserResolverInterface $userResolver,
        OAuthAccessTokenRepository $accessTokenRepository,
        FindUserByIdentifier $userFinder
    ) {
        $this->userResolver = $userResolver;
        $this->accessTokenRepository = $accessTokenRepository;
        $this->userFinder = $userFinder;
    }

    /**
     * Resolve an access token to a user
     * @param string $accessTokenId
     * @return UserInterface
     */
    public function resolveAccessTokenToUser(string $accessTokenId): UserInterface
    {
        // Security measure to prevent timing attacks
        usleep(rand(10000, 50000));

        $token = $this->accessTokenRepository->get($accessTokenId);
        if ($user = $this->userFinder->findByIdentifier($token->getUserIdentifier())) {
            return $user;
        } else {
            throw new InvalidAccessTokenException('Could not resolve access token to a user: ' . $accessTokenId);
        }
    }

    /**
     * Get a user entity by a username and password
     *
     * @param string $username
     * @param string $password
     * @param string $grantType The grant type used
     * @param ClientEntityInterface|ClientAppInterface $clientEntity
     *
     * @return UserEntityInterface
     */
    public function getUserEntityByUserCredentials(
        $username,
        $password,
        $grantType,
        ClientEntityInterface $clientEntity
    ) {
        // Security measure to prevent timing attacks
        usleep(rand(10000, 50000));


        // Sanity test
        if (! $clientEntity instanceof ClientAppInterface) {
            throw new \InvalidArgumentException('Client App must be an instance of ' . ClientAppInterface::class);
        }

        return $this->userResolver->resolve($username, $password, $clientEntity);
    }
}
