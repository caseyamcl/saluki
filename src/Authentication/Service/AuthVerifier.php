<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 1:21 PM
 */

namespace Saluki\Authentication\Service;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use M6Web\Component\Firewall\Firewall;
use Psr\Http\Message\ServerRequestInterface;
use Saluki\Authentication\Contract\AuthVerifierInterface;
use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Exception\InvalidAuthException;
use Saluki\Authentication\OAuthEntity\OAuthClient;
use Saluki\Authentication\Setting\AuthRequireApproval;
use Saluki\Authentication\Setting\AuthRequireVerification;
use Saluki\Authentication\UserEntity\User;
use Saluki\WebInterface\Behavior\RequestHelper;
use SettingsManager\Contract\SettingProvider;

/**
 * Class AuthVerifier
 * @package Saluki\Authentication\Service
 */
class AuthVerifier implements AuthVerifierInterface
{
    use RequestHelper;

    private SettingProvider $settingsProvider;
    private ClientRepositoryInterface $repository;

    /**
     * AuthVerifier constructor.
     * @param SettingProvider $settingsProvider
     * @param ClientRepositoryInterface $repository
     */
    public function __construct(SettingProvider $settingsProvider, ClientRepositoryInterface $repository)
    {
        $this->settingsProvider = $settingsProvider;
        $this->repository = $repository;
    }

    /**
     * Ensure App Client is valid
     *
     * @param ServerRequestInterface $request
     * @param string|null $clientId
     * @return ClientAppInterface
     */
    public function ensureClientIsValid(ServerRequestInterface $request, string $clientId = null): ClientAppInterface
    {
        /** @var ClientAppInterface|ClientEntityInterface $client */
        $client = $this->repository->getClientEntity($clientId);

        if (! $client) {
            throw new \RuntimeException(sprintf('Could not resolve client (ID: %s)', $clientId));
        }

        if (! $client->isEnabled()) {
            throw new InvalidAuthException('client is not enabled');
        }

        if (! $this->validateClientIp($client->getAllowedIps(), $this->getIpFromRequest($request))) {
            throw new InvalidAuthException('this client is not allowed from this IP');
        }

        return $client;
    }

    /**
     * @param UserInterface $user
     * @throws InvalidAuthException
     */
    public function ensureUserIsValid(UserInterface $user): void
    {
        $verify = $this->settingsProvider->getValue(AuthRequireVerification::NAME);
        $approve = $this->settingsProvider->getValue(AuthRequireApproval::NAME);

        if ($verify && ($user instanceof User or is_callable([$user, 'isVerified']))) {
            if (! $user->isVerified()) {
                throw new InvalidAuthException('user is pending verification');
            }
        }

        if ($approve && ($user instanceof User or is_callable([$user, 'isApproved']))) {
            if (! $user->isApproved()) {
                throw new InvalidAuthException('user is not approved');
            }
        }
    }


    /**
     * Check the IP
     *
     * @param array $allowedIps
     * @param string $clientIp
     * @return bool
     */
    private function validateClientIp(array $allowedIps, string $clientIp)
    {
        if ($allowedIps === ClientAppInterface::ALL) {
            return true;
        } else {
            $firewall = new Firewall();
            $firewall->setDefaultState(false);
            $firewall->addList($allowedIps, 'good', true);
            $firewall->setIpAddress($clientIp);
            return $firewall->handle();
        }
    }
}
