<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Service;

use GenPhrase\Password;
use Saluki\Authentication\Contract\PasswordServiceInterface;

/**
 * Class UserPasswordChecker
 * @package Saluki\Authentication\Service
 */
class PasswordService implements PasswordServiceInterface
{
    /**
     * @var Password
     */
    private $generator;

    /**
     * PasswordService constructor.
     * @param Password|null $generator
     */
    public function __construct(Password $generator = null)
    {
        $this->generator = $generator ?: new Password();
    }


    /**
     * @param string $password  Clear-text password
     * @return string  Hashed password
     */
    public function hash(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Validate a password
     *
     * This implementation expects the User object to have a getPassword() method, else it throws an exception.
     *
     * @param string $password Clear-text password
     * @param string $storedPassword
     * @return bool
     */
    public function verify(string $password, string $storedPassword): bool
    {
        // Slow down attackers with random sub-0.5sec delay
        usleep(rand(100000, 500000));
        return password_verify($password, $storedPassword);
    }

    /**
     * @return string
     */
    public function generateRandomPassword(): string
    {
        return $this->generator->generate();
    }
}
