<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 2:19 PM
 */

namespace Saluki\Authentication\Setup;

use Saluki\Access\Model\UserRoles;
use Saluki\Authentication\Contract\UserRepositoryInterface;
use Saluki\Authentication\Service\PasswordService;
use Saluki\Authentication\UserEntity\User;
use Saluki\Console\ConsoleIO;
use Saluki\Persistence\Service\TransactionManager;
use Saluki\Persistence\Setup\DbUpToDateSetupStep;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\Validation\ValueTransformer;

/**
 * Class SetupAdminUserStep
 * @package Saluki\Authentication\Setup
 */
class SetupAdminUserStep implements SetupStepInterface
{
    public const USER_CLASS = User::class;

    protected const DEFAULT_ROLES        = [UserRoles::ADMIN];
    protected const DEFAULT_USERNAME     = 'admin';
    protected const DISPLAY_DISPLAY_NAME = 'The Administrator';
    protected const DEFAULT_EMAIL        = 'root@dev.local';

    private UserRepositoryInterface $userRepository;
    private PasswordService $passwordService;

    /**
     * @var string[]
     */
    private array $rolesToApply;
    private TransactionManager $transactionManager;

    /**
     * SetupAdminUserStep constructor.
     * @param TransactionManager $transactionManager
     * @param UserRepositoryInterface $userRepository
     * @param PasswordService $passwordService
     * @param array $rolesToApply
     */
    public function __construct(
        TransactionManager $transactionManager,
        UserRepositoryInterface $userRepository,
        PasswordService $passwordService,
        array $rolesToApply = self::DEFAULT_ROLES
    ) {
        $this->userRepository = $userRepository;
        $this->passwordService = $passwordService;
        $this->rolesToApply = $rolesToApply;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::POST_CONTAINER;
    }

    /**
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [DbUpToDateSetupStep::class];
    }

    /**
     * Check if the item that this step sets up has been setup
     *
     * @return bool
     */
    public function isSetup(): bool
    {
        return $this->userRepository->countAll() > 0;
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        if ($this->isSetup()) {
            return SetupStepResult::success('User(s) already setup');
        }

        $email       = $io->hasOption('email') ? $io->getOption('email') : null;
        $displayName = $io->hasOption('display_name') ? $io->getOption('display_name') : null;
        $username    = $io->hasOption('username') ? $io->getOption('username') : null;
        $password    = $io->hasOption('password') ? $io->getOption('password') : null;

        if (! $username) {
            $username = $io->ask(
                'Provide a username for the administrative user',
                self::DEFAULT_USERNAME,
                function ($value) {
                    $validator = (new ValidatorRuleSet())
                        ->notBlank()
                        ->unique('username', $this->userRepository)
                        ->transform('strtolower')
                        ->alphanumeric('_')
                        ->length(2, 255);

                    return $validator->prepare($value);
                }
            );
        }

        if (! $displayName) {
            $displayName = $io->ask(
                'Provide a display name for the administrative user',
                self::DISPLAY_DISPLAY_NAME,
                function ($value) {
                    $validator = (new ValidatorRuleSet())->notBlank()->sanitize()->length(2, 512);
                    return $validator->prepare($value);
                }
            );
        }

        if (! $password && ! $io->isInteractive()) {
            $password = $this->passwordService->generateRandomPassword();
            $io->note(sprintf('Generated random password: <info>%s</info>', $password));
        } elseif (! $password) {
            $password = $io->askHidden('Provide a password for the administrative user', function ($val) {
                $validator = (new ValidatorRuleSet())
                    ->length(8, 512)
                    ->zxcvbn(2)
                    ->notBlank()
                    ->transform(function (string $plainTextPassword) {
                        return $this->passwordService->hash($plainTextPassword);
                    }, ValueTransformerInterface::AFTER_VALIDATION);
                return $validator->prepare($val);
            });
        }
        $password = $this->passwordService->hash($password);

        if (! $email) {
            $email = $io->ask(
                'Provide an email for the administrative user',
                static::DEFAULT_EMAIL,
                function ($val) {
                    $validator = (new ValidatorRuleSet())->emailAddress();
                    return $validator->prepare($val);
                }
            );
        }

        $this->transactionManager->beginTransaction();

        /** @var User $user */
        $user = (new \ReflectionClass(static::USER_CLASS))->newInstanceArgs([
            $email,
            $username,
            $password,
            $displayName,
            true,
            true,
            $this->rolesToApply
        ]);

        // Sanity test
        if (! $user instanceof User) {
            throw new \RuntimeException('Cannot use %s with non-user instance.  You will have to code your own');
        }

        $this->userRepository->add($user);
        $this->transactionManager->completeTransaction();

        return SetupStepResult::success('Setup administrative user: <info>' . $username . '</info>', true, [
            'user' => $user
        ]);
    }
}
