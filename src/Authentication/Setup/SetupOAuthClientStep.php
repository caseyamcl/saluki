<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 4:25 PM
 */

namespace Saluki\Authentication\Setup;

use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\OAuthEntity\OAuthClient;
use Saluki\Authentication\OAuthEntity\OAuthClientRepository;
use Saluki\Console\ConsoleIO;
use Saluki\Persistence\Service\TransactionManager;
use Saluki\Persistence\Setup\DbUpToDateSetupStep;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;
use Saluki\Utility\UnpackCSV;
use Saluki\Validation\Rule\DomainName;
use Saluki\Validation\Rule\IpAddress;
use Saluki\Validation\ValidatorRuleSet;

/**
 * Class SetupOAuthClientStep
 * @package Saluki\Authentication\Setup
 */
class SetupOAuthClientStep implements SetupStepInterface
{
    protected const DEFAULT_IPS = ['127.0.0.1'];
    protected const DEFAULT_DESCRIPTION = 'development app';
    protected const DEFAULT_DOMAINS = ['127.0.0.1', 'localhost'];

    /**
     * @var OAuthClientRepository
     */
    private $repository;

    /**
     * @var TransactionManager
     */
    private $transactional;

    /**
     * SetupOAuthClientStep constructor.
     * @param OAuthClientRepository $repository
     * @param TransactionManager $transactionManager
     */
    public function __construct(OAuthClientRepository $repository, TransactionManager $transactionManager)
    {
        $this->repository = $repository;
        $this->transactional = $transactionManager;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::POST_CONTAINER;
    }

    /**
     * @return bool
     */
    public function isSetup(): bool
    {
        return $this->repository->atLeastOneValidClientExists();
    }

    /**
     * @param ConsoleIO $io
     * @param bool $force
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io, bool $force = null): SetupStepResultInterface
    {
        // Force means to create one if even if one exists

        if (is_null($force)) {
            $force = ($io->hasOption('force') && $io->getOption('force'));
        }

        $ipRanges    = ($io->hasOption('ip')) ? $io->getOption('ip') : [];
        $domainNames = ($io->hasOption('domains')) ? $io->getOption('domains') : [];
        $description = ($io->hasOption('description')) ? $io->getOption('description') : '';
        $sysAccount  = ($io->hasOption('enable-system-acct'))
            ? $io->getOption('enable-system-acct')
            : false;

        if ($this->isSetup() && ! $force) {
            return SetupStepResult::success('OAuth Client App already exists');
        }

        if ($description == '') {
            $description = $io->ask(
                'Provide a description for the initial OAuth client (e.g. "testing")',
                self::DEFAULT_DESCRIPTION
            );
        }

        if (empty($ipRanges)) {
            $ipRanges = $io->ask(
                'Please provide a comma-separated list of IPs or IP ranges that this client'
                . 'is allowed from (use "*" for all)',
                implode(', ', self::DEFAULT_IPS),
                function (string $value) {
                    if ($value === '*') {
                        return ClientAppInterface::ALL;
                    } else {
                        return (new ValidatorRuleSet())
                            ->csvList()
                            ->each(ValidatorRuleSet::new()->regex(
                                '/^([a-f0-9*-:\/]+)$/i',
                                'value must be a valid IP address or IP address range'
                            ))
                            ->prepare($value);
                    }
                }
            );
        }

        if (empty($domainNames)) {
            $domainNames = $io->ask(
                'Please provide a comma-separated list of domain names this client is authorized to use (or none)',
                implode(', ', self::DEFAULT_DOMAINS),
                function (string $value) {
                    return (new ValidatorRuleSet())
                        ->csvList()
                        ->each((new ValidatorRuleSet())->orX([new DomainName(), new IpAddress()]))
                        ->prepare($value);
                }
            );
        }

        /** @var OAuthClient $client */
        $client = $this->transactional->execute(function () use ($ipRanges, $description, $domainNames, $sysAccount) {
            $client = new OAuthClient(
                $description,
                is_array($ipRanges) ? $ipRanges : UnpackCSV::un($ipRanges),
                is_array($domainNames) ? $domainNames : UnpackCSV::un($domainNames)
            );

            if ($sysAccount) {
                $client->allowSystemUser();
            }

            $this->repository->add($client);
            return $client;
        });

        $io->note(sprintf('Client Key: <info>%s</info>', $client->getIdentifier()));
        $io->note(sprintf('Secret: <info>%s</info>', $client->getSecret()));

        if ($sysAccount) {
            $io->note(sprintf('System Key: <info>%s</info>', $client->getSystemUserKey()));
        }

        $io->note(sprintf(
            'Allowed IPs: <info>%s</info>',
            implode('</info>, <info>', $client->getAllowedIps())
        ));

        return SetupStepResult::success(
            sprintf('Created OAuth client: <info>%s</info>', $client->getDescription()),
            true,
            ['client' => $client]
        );
    }

    /**
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [DbUpToDateSetupStep::class];
    }
}
