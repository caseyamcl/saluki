<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 4:24 PM
 */

namespace Saluki\Authentication\Setup;

use phpseclib3\Crypt\RSA;
use Saluki\Authentication\Setting\OAuthPrivateKey;
use Saluki\Authentication\Setting\OAuthPublicKey;
use Saluki\Console\ConsoleIO;
use Saluki\Persistence\Service\TransactionManager;
use Saluki\Persistence\Setup\DbUpToDateSetupStep;
use Saluki\Settings\Entity\CachedDbSettingRepository;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;
use SettingsManager\Contract\SettingProvider;

class SetupOAuthKeysStep implements SetupStepInterface
{
    const NEW_KEYS_MSG      = 'Saved new OAuth keypair';
    const ALREADY_SETUP_MSG = 'OAuth key pair exists';

    private CachedDbSettingRepository $settingRepository;
    private SettingProvider $settingsProvider;
    private TransactionManager $transactionManager;

    /**
     * SetupKeys constructor.
     *
     * @param CachedDbSettingRepository $settingRepository
     * @param SettingProvider $settingsProvider
     * @param TransactionManager $transactionManager
     */
    public function __construct(
        CachedDbSettingRepository $settingRepository,
        SettingProvider $settingsProvider,
        TransactionManager $transactionManager
    ) {
        $this->settingRepository  = $settingRepository;
        $this->settingsProvider   = $settingsProvider;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::POST_CONTAINER;
    }

    public function isSetup(): bool
    {
        return $this->settingsProvider->findValue(OAuthPublicKey::NAME)
            && $this->settingsProvider->findValue(OAuthPrivateKey::NAME);
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        $existingPrivate = $this->settingsProvider->findValueInstance(OAuthPrivateKey::NAME);
        $existingPublic = $this->settingsProvider->findValueInstance(OAuthPublicKey::NAME);

        if (
            (! $existingPrivate or ! $existingPrivate->getValue())
            or (! $existingPublic or ! $existingPublic->getValue())
            or ($io->hasOption('force') && $io->getOption('force'))
        ) {
            $privateKey = RSA::createKey(4096);

            $this->transactionManager->execute(function () use ($privateKey) {
                $this->settingRepository->saveValue(OAuthPrivateKey::NAME, $privateKey->toString('OpenSSH'));
                $this->settingRepository->saveValue(
                    OAuthPublicKey::NAME,
                    $privateKey->getPublicKey()->toString('OpenSSH')
                );
            });
            return SetupStepResult::success(static::NEW_KEYS_MSG);
        } else {
            return SetupStepResult::success(static::ALREADY_SETUP_MSG);
        }
    }

    /**
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [DbUpToDateSetupStep::class];
    }
}
