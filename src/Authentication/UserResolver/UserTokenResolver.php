<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserResolver;

use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Contract\UserRepositoryInterface;
use Saluki\Authentication\Contract\UserResolverInterface;
use Saluki\Authentication\UserTokenEntity\UserTokenRepository;

/**
 * Class OneTimeTokenUserResolver
 * @package Saluki\Authentication\UserResolver
 */
class UserTokenResolver implements UserResolverInterface
{
    const TOKEN_USERNAME = 'TOKEN';

    /**
     * @var \Saluki\Authentication\UserTokenEntity\UserTokenRepository
     */
    private $repository;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * OneTimeTokenCredentialsChecker constructor.
     *
     * @param \Saluki\Authentication\UserTokenEntity\UserTokenRepository $repository
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserTokenRepository $repository, UserRepositoryInterface $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    /**
     * Resolve a user for a given set of credentials
     *
     * If user cannot be resolved (invalid credentials, or no user found), then return NULL
     *
     * @param string $username
     * @param string $password
     * @param ClientAppInterface $clientApp
     * @return UserInterface|null
     */
    public function resolve(string $username, string $password, ClientAppInterface $clientApp): ?UserInterface
    {
        // if the username is 'TOKEN', then the password will be the one-time-token identifier
        if ($username !== static::TOKEN_USERNAME) {
            return null;
        }

        // Find the token
        if (! $token = $this->repository->find($password)) {
            return null;
        }

        if ($token->isValid()) {
            $token->revoke();
            return $this->userRepository->find($token->getUserId());
        } else {
            return null;
        }
    }
}
