<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserResolver;

use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Contract\UserResolverInterface;

/**
 * Class ChainUserResolver
 * @package Saluki\Authentication\UserResolver
 */
class ChainUserResolver implements UserResolverInterface
{
    /**
     * @var array|UserResolverInterface[]
     */
    private $resolvers;

    /**
     * Constructor
     *
     * @param iterable $resolvers
     */
    public function __construct(iterable $resolvers)
    {
        foreach ($resolvers as $resolver) {
            $this->addResolver($resolver);
        }
    }

    /**
     * @param UserResolverInterface $resolver
     */
    private function addResolver(UserResolverInterface $resolver)
    {
        $this->resolvers[] = $resolver;
    }

    /**
     * Resolve a user for a given set of credentials
     *
     * If user cannot be resolved (invalid credentials, or no user found), then return NULL
     *
     * @param string $username
     * @param string $password
     * @param ClientAppInterface $clientApp
     * @return UserInterface|null
     */
    public function resolve(string $username, string $password, ClientAppInterface $clientApp): ?UserInterface
    {
        foreach ($this->resolvers as $resolver) {
            if ($user = $resolver->resolve($username, $password, $clientApp)) {
                return $user;
            }
        }

        // If made it here..
        return null;
    }
}
