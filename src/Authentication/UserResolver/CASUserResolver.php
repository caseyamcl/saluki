<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserResolver;

use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Contract\UserResolverInterface;

/**
 * Class CASUserResolver
 * @package Saluki\Authentication\UserResolver
 */
class CASUserResolver implements UserResolverInterface
{
    const USERNAME = 'CAS_TOKEN';

    private string $serviceUrl;

    /**
     * CASUserResolver constructor.
     * @param string $serviceUrl
     */
    public function __construct(string $serviceUrl)
    {
        $this->serviceUrl = $serviceUrl;
    }

    /**
     * Resolve a user for a given set of credentials
     *
     * If user cannot be resolved (invalid credentials, or no user found), then return NULL
     *
     * @param string $username
     * @param string $password
     * @param ClientAppInterface $clientApp
     * @return UserInterface|null
     */
    public function resolve(string $username, string $password, ClientAppInterface $clientApp): ?UserInterface
    {
        // TODO: Implement resolve() method.
        return null;

//        try {
//            $response = $this->guzzleClient->get($this->casValidateUrl, ['query' => [
//                'token'   => $password,
//                'service' => $this->serviceUrl
//            ]]);
//
//            /** @var Person $person */
//            if ($person = $this->entityManager->getRepository(Person::class)->findOneBy(['uid' => $username])) {
//                return OAuthIdentifier::forPerson($person);
//            } else {
//                return null;
//            }
//
//        } catch (BadResponseException $e) {
//            return null;
//        }
    }
}
