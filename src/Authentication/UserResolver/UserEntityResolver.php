<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\UserResolver;

use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\FindUserByIdentifier;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Contract\PasswordServiceInterface;
use Saluki\Authentication\Contract\UserResolverInterface;

/**
 * Class PersonAccountUserResolver
 * @package Saluki\Authentication\UserResolver
 */
class UserEntityResolver implements UserResolverInterface
{
    /**
     * @var PasswordServiceInterface
     */
    private $passwordService;

    /**
     * @var FindUserByIdentifier
     */
    private $userFinder;

    /**
     * PersonAccountUserResolver constructor.
     *
     * @param FindUserByIdentifier $userFinder
     * @param PasswordServiceInterface $passwordService
     */
    public function __construct(FindUserByIdentifier $userFinder, PasswordServiceInterface $passwordService)
    {
        $this->passwordService = $passwordService;
        $this->userFinder = $userFinder;
    }

    /**
     * Resolve a user for a given set of credentials
     *
     * If user cannot be resolved (invalid credentials, or no user found), then return NULL
     *
     * @param string $username
     * @param string $password
     * @param ClientAppInterface $clientApp
     * @return UserInterface|null
     */
    public function resolve(string $username, string $password, ClientAppInterface $clientApp): ?UserInterface
    {
        if (! $user = $this->userFinder->findByIdentifier($username)) {
            return null;
        }

        if (is_callable([$user, 'getPassword'])) {
            return $this->passwordService->verify($password, call_user_func([$user, 'getPassword'])) ? $user : null;
        } else {
            throw new \LogicException(sprintf(
                "Cannot validate password (expected method %s::getPassword()).  Provide an alternative"
                . "implementation of %s, or implement %s::getPassword()",
                get_class($user),
                get_called_class(),
                get_class($user)
            ));
        }
    }
}
