<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 12:21 PM
 */

namespace Saluki\Authentication\Endpoint;

use Psr\Http\Message\ResponseInterface;
use Saluki\Authentication\Contract\UserRepositoryInterface;
use Saluki\RestResource\Endpoint\AbstractRestEndpoint;
use Saluki\WebInterface\Model\SalukiRequest;
use Saluki\WebInterface\RequestParam\ParameterList;
use Laminas\Diactoros\Response;

class UserForgotEndpoint extends AbstractRestEndpoint
{
    private UserRepositoryInterface $userRepository;

    /**
     * UserForgotEndpoint constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Does this endpoint operate on a single resource (e.g. retrieve) or multiple resources (e.g. index)
     * @return bool
     */
    protected function isSingle(): bool
    {
        return true;
    }

    public function listBodyParameters(): ParameterList
    {
        $params = parent::listBodyParameters();
        $params->optional('forgot_url')->url()->contains('{token}')->setDescription(
            'Verification URL in client App (include "{token}" placeholder in URL'
        );

        return $params;
    }


    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        // TODO: Implement __invoke() method.
        return new Response();
    }
}
