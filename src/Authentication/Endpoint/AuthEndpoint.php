<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Endpoint;

use Aura\Router\Route;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\Plain;
use RuntimeException;
use Saluki\Authentication\Contract\OAuthServiceInterface;
use Saluki\Authentication\Contract\OAuthUserLookupInterface;
use Saluki\Authentication\Event\AuthenticationFailEvent;
use Saluki\Authentication\Event\AuthenticationSuccessEvent;
use Saluki\Authentication\Exception\InvalidAuthException;
use Saluki\Authentication\Middleware\AddRolesMiddleware;
use Saluki\Authentication\Model\AnonymousUser;
use Saluki\Authentication\Service\AuthVerifier;
use Saluki\WebInterface\RequestParam\ParameterList;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Saluki\WebInterface\Model\AbstractEndpoint;
use Saluki\WebInterface\Model\SalukiRequest;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Laminas\Diactoros\Response;

/**
 * Class AuthEndpoint
 * @package Saluki\Authentication\Endpoint
 */
class AuthEndpoint extends AbstractEndpoint
{
    private OAuthServiceInterface $oauthService;
    private AuthVerifier $authVerifier;
    private OAuthUserLookupInterface $oAuthUserLookup;
    private EventDispatcherInterface $dispatcher;

    /**
     * AuthEndpoint constructor.
     *
     * @param OAuthServiceInterface $oauthService
     * @param AuthVerifier $authVerifier
     * @param OAuthUserLookupInterface $oAuthUserLookup
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        OAuthServiceInterface $oauthService,
        AuthVerifier $authVerifier,
        OAuthUserLookupInterface $oAuthUserLookup,
        EventDispatcherInterface $dispatcher
    ) {
        $this->oauthService = $oauthService;
        $this->authVerifier = $authVerifier;
        $this->oAuthUserLookup = $oAuthUserLookup;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Route $route
     */
    public function validateRoute(Route $route): void
    {
        $this->ensureMethod($route->allows, 'POST');
    }

    /**
     * @return ParameterList
     */
    public function listBodyParameters(): ParameterList
    {
        $params = new ParameterList();

        $params->required('grant_type', false)
            ->setDescription('OAuth Grant Type')
            ->alphanumeric('_');

        $params->optional('client_id', false)
            ->setDescription('OAuth Client Key')
            ->sanitize()
            ->length(2, 255);

        $params->optional('client_secret', false)
            ->setDescription('OAuth Client Secret')
            ->sanitize()
            ->length(2, 255);

        $params->optional('username', false)
            ->setDescription('Username (if using "password" grant)')
            ->sanitize()
            ->length(2, 255);

        $params->optional('password', false)
            ->notBlank()
            ->setDescription('Password (if using "password" grant)')
            ->length(2, 1024);

        $params->optional('refresh_token', false)
            ->notBlank()
            ->setDescription('Refresh token (if using "refresh token" grant)')
            ->length(2, 2048);

        return $params;
    }

    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        try {
            // Authenticate the request through the OAuth server
            $response = $this->oauthService->getAuthorizationServer()->respondToAccessTokenRequest(
                $request,
                new Response()
            );

            // Get the user from the access token from the response
            $parser = new Parser(new JoseEncoder());
            $rawJwtToken = json_decode($response->getBody()->__toString())->access_token;

            /** @var Plain $parsedToken */
            $parsedToken = $parser->parse($rawJwtToken);

            // Get the access token ID from the parsed token
            $accessTokenId = $parsedToken->claims()->get('jti');
            $user = $this->oAuthUserLookup->resolveAccessTokenToUser($accessTokenId);

            try {
                $this->authVerifier->ensureUserIsValid($user);
                $this->authVerifier->ensureClientIsValid($request, $this->getClientIdFromAuthRequest($request));
            } catch (InvalidAuthException $e) {
                throw OAuthServerException::accessDenied($e->getMessage());
            }

            $response = AddRolesMiddleware::addRolesToResponse($response, $user->getRoles());
            $this->dispatcher->dispatch(
                new AuthenticationSuccessEvent(
                    $user,
                    $request->findBodyParam('grant_type') == 'password'
                        ? $request->findBodyParam('username')
                        : AnonymousUser::ANONYMOUS_USER_ID,
                    $this->getClientIdFromAuthRequest($request),
                    $request->getClientIp()
                ),
                AuthenticationSuccessEvent::EVENT_NAME
            );

            return $response;
        } catch (OAuthServerException $e) {
            // Attempt to get the client ID from the request, but if it is not resolvable, give up.
            try {
                $clientId = $this->getClientIdFromAuthRequest($request);
            } catch (RuntimeException $e) {
                $clientId = '';
                $e = (OAuthServerException::invalidClient($request));
            }

            // Dispatch an event
            $this->dispatcher->dispatch(new AuthenticationFailEvent(
                $request->findBodyParam('username') ?: '',
                $clientId,
                $request->getClientIp(),
                $e->getMessage()
            ), AuthenticationFailEvent::EVENT_NAME);

            // Return the 4xx response
            return $e->generateHttpResponse(new Response());
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    private function getClientIdFromAuthRequest(ServerRequestInterface $request): string
    {
        // Try getting it from the POST body
        if (isset($request->getParsedBody()['client_id'])) {
            return $request->getParsedBody()['client_id'];
        }

        // Next, try getting it from basic authorization
        if ($authHeader = $request->getHeaderLine('Authorization')) {
            $header = explode(':', base64_decode(substr($authHeader, 6)), 2);
            return $header[0];
        }

        throw new RuntimeException('Unable to parse client ID from request');
    }
}
