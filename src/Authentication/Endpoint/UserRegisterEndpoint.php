<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Endpoint;

use Saluki\Authentication\RestResource\UserResource;
use Saluki\Authentication\RestResource\UserResourceCreate;
use Saluki\RestResource\Model\RestRequestContext;

/**
 * Class UserRegisterEndpoint
 * @package Saluki\Authentication\Endpoint
 */
class UserRegisterEndpoint extends UserResourceCreate
{
    /**
     * UserRegisterEndpoint constructor.
     * @param UserResource $resource
     */
    public function __construct(UserResource $resource)
    {
        parent::__construct($resource);
    }


    /**
     * @param RestRequestContext $requestContext
     * @param null $record
     * @return bool
     */
    protected function authorizeAccess(RestRequestContext $requestContext, $record = null): bool
    {
        return $requestContext->getUser()->isAnonymous();
    }
}
