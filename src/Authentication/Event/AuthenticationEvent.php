<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class AuthenticationEvent
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
abstract class AuthenticationEvent extends GenericEvent
{
    /**
     * @return bool
     */
    abstract public function wasSuccessful(): bool;
}
