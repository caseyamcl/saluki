<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Event;

/**
 * Class AuthenticationFailEvent
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class AuthenticationFailEvent extends AuthenticationEvent
{
    public const EVENT_NAME = 'auth.fail';

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $attemptedUsername;

    /**
     * @var string
     */
    private $clientAppId;

    /**
     * @var string
     */
    private $clientIp;

    /**
     * Authentication Event
     *
     * @param string $attemptedUsername
     * @param string $clientAppId
     * @param string $clientIp
     * @param string $message
     */
    public function __construct(string $attemptedUsername, string $clientAppId, string $clientIp, string $message)
    {
        $this->message = $message;
        $this->attemptedUsername = $attemptedUsername;
        $this->clientAppId = $clientAppId;
        $this->clientIp = $clientIp;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getAttemptedUsername(): string
    {
        return $this->attemptedUsername;
    }

    /**
     * @return string
     */
    public function getClientAppId(): string
    {
        return $this->clientAppId;
    }

    /**
     * @return string
     */
    public function getClientIp(): string
    {
        return $this->clientIp;
    }

    /**
     * @return bool
     */
    public function wasSuccessful(): bool
    {
        return false;
    }
}
