<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Event;

use Saluki\Authentication\Contract\UserInterface;

/**
 * Class AuthenticationSuccessEvent
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class AuthenticationSuccessEvent extends AuthenticationEvent
{
    public const EVENT_NAME = 'auth.success';

    private UserInterface $user;
    private string $ipAddress;
    private string $clientAppId;
    private string $username;

    /**
     * AuthenticationSuccessEvent constructor.
     * @param UserInterface $user
     * @param string $username
     * @param string $clientAppId
     * @param string $ipAddress
     */
    public function __construct(UserInterface $user, string $username, string $clientAppId, string $ipAddress)
    {
        parent::__construct();
        $this->user = $user;
        $this->ipAddress = $ipAddress;
        $this->clientAppId = $clientAppId;
        $this->username = $username;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getClientAppId(): string
    {
        return $this->clientAppId;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ipAddress;
    }

    /**
     * @return bool
     */
    public function wasSuccessful(): bool
    {
        return true;
    }
}
