<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Setting;

use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

/**
 * Class OAuthSessionExpire
 * @package Saluki\Authentication\Setting
 */
class OAuthAccessTokenExpiration extends AbstractSettingDefinition
{
    const NAME         = 'oauth_access_token_expiration';
    const DISPLAY_NAME = 'OAuth Access Token Expiration';
    const NOTES        = "Length of time before the access token expires (e.g. 'P1D' or 'P2W')";
    const DEFAULT      = 'P1W';
    const SENSITIVE    = true;

    /**
     * Process, validate, and store a new value
     *
     * @param mixed $value The raw value
     * @return mixed  The processed value to store
     */
    public function processValue($value)
    {
        try {
            new \DateInterval($value);
        } catch (\Exception $e) {
            throw new InvalidSettingValueException('Invalid OAuth Token Expiration', $e->getCode(), $e);
        }

        return $value;
    }
}
