<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Setting;

use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

/**
 * Class OAuthRefreshTokenExpiration
 * @package Saluki\Authentication\Setting
 */
class OAuthRefreshTokenExpiration extends AbstractSettingDefinition
{
    const NAME         = 'oauth_refresh_token_expiration';
    const DISPLAY_NAME = 'OAuth Refresh Token Expiration';
    const NOTES        = "Length of time before the refresh token expires (e.g. 'P1D' or 'P2W')";
    const DEFAULT      = 'P2W';
    const SENSITIVE    = true;

    /**
     * Process, validate, and store a new value
     *
     * @param mixed $value The raw value
     * @return mixed  The processed value to store
     */
    public function processValue($value)
    {
        try {
            new \DateInterval($value);
        } catch (\Exception $e) {
            throw new InvalidSettingValueException('Invalid OAuth Token Expiration', $e->getCode(), $e);
        }

        return $value;
    }
}
