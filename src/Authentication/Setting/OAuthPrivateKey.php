<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Setting;

use League\OAuth2\Server\CryptKey;
use phpseclib3\Crypt\RSA;
use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

/**
 * Class OAuthPrivateKey
 * @package Saluki\Authentication\Setting
 */
class OAuthPrivateKey extends AbstractSettingDefinition
{
    const NAME = 'oauth_private_key';
    const DISPLAY_NAME = 'OAuth Private Key';
    const NOTES = 'Private key used by OAuth server';
    const SENSITIVE = true;

    /**
     * Process, validate, and store a new value
     *
     * @param mixed $value The raw value
     * @return mixed  The processed value to store
     */
    public function processValue($value)
    {
        if (! $value instanceof RSA) {
            try {
                new CryptKey($value);
            } catch (\LogicException $e) {
                throw new InvalidSettingValueException('Value must be a valid key', $e->getCode(), $e);
            }
        }

        return $value;
    }
}
