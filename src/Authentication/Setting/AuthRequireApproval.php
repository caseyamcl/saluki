<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 9:14 AM
 */

namespace Saluki\Authentication\Setting;

use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\ValidatorRuleSet;
use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

/**
 * Class AuthRequireApproval
 * @package Saluki\Authentication\Setting
 */
class AuthRequireApproval extends AbstractSettingDefinition
{
    const NAME         = 'auth_require_approval';
    const DISPLAY_NAME = 'Require user approval';
    const NOTES        = "Require user approval before logging in";
    const DEFAULT      = true;
    const SENSITIVE    = true;

    /**
     * Process, validate, and store a new value
     *
     * @param mixed $value The raw value
     * @return boolean  The processed value to store
     */
    public function processValue($value)
    {
        try {
            return (new ValidatorRuleSet())->boolean()->prepare($value);
        } catch (ValidationRuleException $e) {
            throw new InvalidSettingValueException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
