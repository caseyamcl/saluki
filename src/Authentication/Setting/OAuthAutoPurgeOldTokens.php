<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/30/18
 * Time: 2:20 PM
 */

namespace Saluki\Authentication\Setting;

use SettingsManager\Exception\InvalidSettingValueException;
use SettingsManager\Model\AbstractSettingDefinition;

/**
 * Class OAuthAutoPurgeOldTokens
 * @package Saluki\Authentication\Setting
 */
class OAuthAutoPurgeOldTokens extends AbstractSettingDefinition
{
    const NAME         = 'oauth_auto_purge_old_tokens';
    const DISPLAY_NAME = 'Period of time to keep old tokens before purging';
    const NOTES        = "Length of time that old tokens will be kept before they purged" .
        "(e.g. 'P1D' or 'P2W'); 0 or empty value means do not purge";
    const DEFAULT      = 'P1D';
    const SENSITIVE    = true;

    public function processValue($value)
    {
        // Empty value means do not purge
        if (! $value) {
            return $value;
        }

        try {
            new \DateInterval($value);
        } catch (\Exception $e) {
            throw new InvalidSettingValueException('Invalid OAuth Token Expiration', $e->getCode(), $e);
        }

        return $value;
    }
}
