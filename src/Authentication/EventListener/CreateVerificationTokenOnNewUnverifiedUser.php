<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\EventListener;

use Cake\Chronos\ChronosInterval;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Contract\UserTokenRepositoryInterface;
use Saluki\Authentication\UserEntity\User;
use Saluki\Authentication\UserTokenEntity\UserToken;
use Saluki\Emailer\Service\EmailService;
use Saluki\RestResource\Event\RestFieldUpdateEvent;
use Saluki\RestResource\Event\RestObjectEvent;
use Saluki\RestResource\Event\RestRequestEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CreateVerificationTokenOnNewUnverifiedUser
 * @package Saluki\Authentication\ActionListener
 */
class CreateVerificationTokenOnNewUnverifiedUser implements EventSubscriberInterface
{
    public const USER_ENTITY = User::class;

    private UserTokenRepositoryInterface $repository;
    private EmailService $emailService;

    /**
     * CreateVerificationTokenOnNewUnverifiedUser constructor.
     * @param UserTokenRepositoryInterface $repository
     * @param EmailService $emailService
     */
    public function __construct(UserTokenRepositoryInterface $repository, EmailService $emailService)
    {
        $this->repository = $repository;
        $this->emailService = $emailService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            RestRequestEvents::fieldChange(static::USER_ENTITY, 'verified') => 'handleUpdate',
            RestRequestEvents::create(static::USER_ENTITY) => 'handleNew'
        ];
    }

    /**
     * @param RestObjectEvent $event
     */
    public function handleNew(RestObjectEvent $event)
    {
        /** @var User $user */
        $user = $event->getObject();

        if (! $user->isVerified()) {
            $this->createToken($user, $event->getRequest()->findBodyParam('token_url'));
        }
    }

    /**
     * @param RestFieldUpdateEvent $event
     */
    public function handleUpdate(RestFieldUpdateEvent $event)
    {
        /** @var User $user */
        $user = $event->getObject();

        if ($event->wasChanged() && ! $user->isVerified()) {
            $this->createToken($user, $event->getRequest()->findBodyParam('token_url'));
        }
    }

    /**
     * @param UserInterface $user
     * @param string $tokenUrl
     */
    protected function createToken(UserInterface $user, string $tokenUrl = '')
    {
        $token = UserToken::buildFromExpirationInterval(
            $user->getIdentifier(),
            'verification',
            ChronosInterval::hours(2)
        );

        if (! is_callable([$user, 'getEmail'])) {
            // TODO: log warning here.
            return;
        }

        $email = call_user_func([$user, 'getEmail']);

        if ($tokenUrl) {
            $template = 'user_token_' . $token->getType() . '.twig';
            $this->emailService->enqueue($email, $template, [
                'user'      => $user,
                'token'     => $token,
                'token_url' => str_replace('{token}', $token->getToken(), $tokenUrl)
            ]);
        }

        $this->repository->add($token);
    }
}
