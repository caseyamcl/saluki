<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Contract;

/**
 * Class ClientAppInterface
 * @package Saluki\Authentication\Contract
 */
interface ClientAppInterface
{
    const ALL = ['*'];

    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * Get the unique key that allows a non-person request to map to the system account
     *
     * @return string|null
     */
    public function getSystemUserKey(): ?string;

    /**
     * Does this client allow system account access?
     *
     * @return bool
     */
    public function allowsSystemAccount(): bool;

    /**
     * Get IP addresses or IP address ranges (IPv4 and/or IPv6) that this client allows
     *
     * Return ['*'] for no IP address restrictions (should be used rarely)
     *
     * @return array|string[]
     */
    public function getAllowedIps(): array;

    /**
     * Is this client app enabled?
     *
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @return array|string[]
     */
    public function getDomainNames();

    /**
     * @param string $domain
     * @return bool
     */
    public function isAllowedDomain(string $domain): bool;
}
