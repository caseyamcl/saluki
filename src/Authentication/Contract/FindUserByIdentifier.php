<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/20/18
 * Time: 3:23 PM
 */

namespace Saluki\Authentication\Contract;


interface FindUserByIdentifier
{
    /**
     * @param string $identifier
     * @return null|UserInterface
     */
    public function findByIdentifier(string $identifier): ?UserInterface;
}
