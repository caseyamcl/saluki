<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/4/18
 * Time: 1:22 PM
 */

namespace Saluki\Authentication\Contract;

use League\OAuth2\Server\Repositories\UserRepositoryInterface;

/**
 * Interface OAuthUserLookupInterface
 * @package Saluki\Authentication\Contract
 */
interface OAuthUserLookupInterface extends UserRepositoryInterface
{
    /**
     * @param string $accessTokenId
     * @return UserInterface
     */
    public function resolveAccessTokenToUser(string $accessTokenId): UserInterface;
}
