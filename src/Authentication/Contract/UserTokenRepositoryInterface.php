<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Contract;

use Doctrine\Persistence\ObjectRepository;

/**
 * Interface UserTokenRepositoryInterface
 *
 * @package Saluki\Authentication\Contract
 * @method UserTokenInterface|null find($id)
 */
interface UserTokenRepositoryInterface extends ObjectRepository
{
    /**
     * @param UserTokenInterface $token
     */
    public function add(UserTokenInterface $token): void;
}
