<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Contract;

/**
 * Class CredentialsResolverInterface
 * @package Saluki\Authentication\Contract
 */
interface UserResolverInterface
{
    /**
     * Resolve a user for a given set of credentials
     *
     * If user cannot be resolved (invalid credentials, or no user found), then return NULL
     *
     * @param string $username
     * @param string $password
     * @param ClientAppInterface $clientApp
     * @return UserInterface|null
     */
    public function resolve(string $username, string $password, ClientAppInterface $clientApp): ?UserInterface;
}
