<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Contract;

interface PasswordServiceInterface
{
    /**
     * @param string $password  Clear-text password
     * @return string  Hashed password
     */
    public function hash(string $password): string;

    /**
     * Validate a password
     *
     * @param string $password Clear-text password
     * @param string $storedPassword
     * @return bool
     */
    public function verify(string $password, string $storedPassword): bool;

    /**
     * Generate a random password that meets the password complexity requirements
     * @return string
     */
    public function generateRandomPassword(): string;
}
