<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 1:18 PM
 */

namespace Saluki\Authentication\Contract;

use Psr\Http\Message\ServerRequestInterface;
use Saluki\Authentication\Exception\InvalidAuthException;

/**
 * Interface UserAuthVerifierInterface
 * @package Saluki\Authentication\Contract
 */
interface AuthVerifierInterface
{
    /**
     * @param UserInterface $user
     * @throws InvalidAuthException
     */
    public function ensureUserIsValid(UserInterface $user): void;

    /**
     * Ensure App Client is valid
     *
     * @param ServerRequestInterface $request
     * @param string|null $clientId NULL to auto-guess client ID
     * @return ClientAppInterface
     */
    public function ensureClientIsValid(ServerRequestInterface $request, string $clientId = null): ClientAppInterface;
}
