<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Contract;

use League\OAuth2\Server\Entities\UserEntityInterface;

/**
 * Class UserInterface
 * @package Saluki\Authentication\Contract
 */
interface UserInterface extends UserEntityInterface
{
    /**
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * @return array|string[]
     */
    public function getRoles(): array;

    /**
     * @return bool
     */
    public function isAnonymous(): bool;
}
