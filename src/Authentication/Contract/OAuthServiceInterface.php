<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/13/18
 * Time: 5:02 PM
 */

namespace Saluki\Authentication\Contract;

use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Grant\GrantTypeInterface;
use League\OAuth2\Server\ResourceServer;

/**
 * Interface OAuthServiceInterface
 * @package Saluki\Authentication\Contract
 */
interface OAuthServiceInterface
{
    /**
     * @param GrantTypeInterface $grantType
     * @return mixed
     */
    public function addGrantType(GrantTypeInterface $grantType);

    /**
     * Get an authorization server
     *
     * @return AuthorizationServer
     */
    public function getAuthorizationServer(): AuthorizationServer;

    /**
     * Get a resource server
     *
     * @return ResourceServer
     */
    public function getResourceServer(): ResourceServer;
}
