<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Authentication\Contract;

use Doctrine\Persistence\ObjectRepository;

/**
 * Interface UserRepositoryInterface
 * @package Saluki\Authentication\Contract
 * @method UserInterface|null find($id)
 */
interface UserRepositoryInterface extends ObjectRepository, FindUserByIdentifier
{
    /**
     * @param UserInterface $user
     * @return void
     */
    public function add(UserInterface $user): void;

    /**
     * @param UserInterface $user
     */
    public function remove(UserInterface $user): void;

    /**
     * Count all users
     *
     * @return int
     */
    public function countAll(): int;
}
