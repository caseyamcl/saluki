<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/17/18
 * Time: 4:16 PM
 */

namespace Saluki\Authentication\Contract;


use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Saluki\Authentication\UserEntity\User;

/**
 * Class UserToken
 * @package Saluki\Authentication\Entity
 * @ORM\Entity()
 */
interface UserTokenInterface
{
    /**
     * Revoke this token (can only be done once)
     */
    public function revoke(): void;

    public function getId(): UuidInterface;

    public function getCreatedTimestamp(): ChronosInterface;

    public function getRevokedTimestamp(): ?ChronosInterface;

    public function getExpiresTimestamp(): ?ChronosInterface;

    public function isRevoked(): bool;

    public function isExpired(): bool;

    /**
     * Get the token
     */
    public function getToken(): string;

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @return string
     */
    public function getUserId(): string;

    /**
     * @return string
     */
    public function getType(): string;
}
