<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/6/18
 * Time: 1:49 PM
 */

namespace Saluki\Authentication\Permission;

use Saluki\Authentication\Model\AbstractPermission;

/**
 * Class AdministerUsersPermission
 * @package Saluki\Authentication\Permission
 */
class ManageUsersPermission extends AbstractPermission
{
    const DESCRIPTION     = "create, update, and delete other users' accounts";
    const IS_CONFIGURABLE = true;
}
