<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/7/18
 * Time: 2:13 PM
 */

namespace Saluki\WebInterface\Contract;

use Saluki\Validation\Contract\ValidatorFieldInterface;

/**
 * Interface RequestParamInterface
 * @package Saluki\WebInterface\Contract
 */
interface RequestParamInterface extends ValidatorFieldInterface
{
    /**
     * @return string
     */
    public function getDataType(): string;
}
