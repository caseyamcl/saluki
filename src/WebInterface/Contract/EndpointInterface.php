<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Contract;

use Aura\Router\Route;
use Psr\Http\Message\ResponseInterface;
use Saluki\WebInterface\RequestParam\ParameterList;
use Saluki\WebInterface\Model\SalukiRequest;

/**
 * Interface EndpointInterface
 * @package Saluki\WebInterface\Contract
 */
interface EndpointInterface
{
    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * Validate the route, or throw an InvalidRouteDefinitionException
     *
     * @param Route $route
     */
    public function validateRoute(Route $route): void;

    /**
     * @return \Saluki\WebInterface\RequestParam\ParameterList|RequestParamInterface[]
     */
    public function listQueryParameters(): ParameterList;

    /**
     * @return \Saluki\WebInterface\RequestParam\ParameterList|RequestParamInterface[]
     */
    public function listBodyParameters(): ParameterList;

    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface;
}
