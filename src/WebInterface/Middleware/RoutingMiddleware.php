<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Middleware;

use Aura\Router\Matcher;
use Aura\Router\Route;
use Aura\Router\Rule\Accepts;
use Aura\Router\Rule\Allows;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\WebInterface\Model\JsonApiErrorResponse;

/**
 * Class RoutingMiddleware
 * @package Saluki\WebInterface\Middleware
 */
class RoutingMiddleware implements MiddlewareInterface
{
    /**
     * @var Matcher
     */
    private $matcher;

    /**
     * RoutingMiddleware constructor.
     * @param Matcher $matcher
     */
    public function __construct(Matcher $matcher)
    {
        $this->matcher = $matcher;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var Route $route */
        if ($route = $this->matcher->match($request)) {
            $request = $request->withAttribute('path-segments', $route->attributes);

            $handler = $route->handler;
            return call_user_func($handler, $request);
        } else {
            $failedRoute = $this->matcher->getFailedRoute();

            if (is_null($failedRoute)) {
                return $this->notFound($request);
            }

            switch ($failedRoute->failedRule) {
                case Allows::class:
                    return (JsonApiErrorResponse::build('Method Not Allowed', 405))
                        ->withAddedHeader('Allow', $failedRoute->allows);
                    break;
                case Accepts::class:
                    return JsonApiErrorResponse::build('Not Acceptable', 406);
                    break;
                default:
                    return $this->notFound($request);
                    break;
            }
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @return JsonApiErrorResponse
     */
    private function notFound(ServerRequestInterface $request)
    {
        return JsonApiErrorResponse::notFound(sprintf(
            'Route not found: %s %s',
            $request->getMethod(),
            $request->getUri()->getPath()
        ));
    }
}
