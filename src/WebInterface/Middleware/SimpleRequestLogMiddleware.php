<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/5/18
 * Time: 10:15 AM
 */

namespace Saluki\WebInterface\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Saluki\WebInterface\Service\RequestLogMessageFormatter;

/**
 * Class RequestLogMiddleware
 * @package Saluki\WebInterface\Middleware
 */
class SimpleRequestLogMiddleware implements MiddlewareInterface
{
    /**
     * @var RequestLogMessageFormatter
     */
    private $formatter;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $logLevel;

    /**
     * RequestLogMiddleware constructor.
     * @param LoggerInterface $logger
     * @param RequestLogMessageFormatter $formatter
     * @param string $logLevel
     */
    public function __construct(
        LoggerInterface $logger,
        RequestLogMessageFormatter $formatter = null,
        string $logLevel = LogLevel::INFO
    ) {
        $this->formatter = $formatter ?: new RequestLogMessageFormatter();
        $this->logger = $logger;
        $this->logLevel = $logLevel;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Log request.
        $this->logger->log($this->logLevel, $this->formatter->getRequestLogMessage($request));

        $response = $handler->handle($request);

        // Log response
        $this->logger->log($this->logLevel, $this->formatter->getResponseLogMessage($response));

        return $response;
    }
}
