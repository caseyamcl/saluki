<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Middleware;

use Psr\Log\LoggerInterface;
use Tuupola\Middleware\CorsMiddleware;

/**
 * Class CorsMiddlewareFactory
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class CorsMiddlewareFactory
{
    /**
     * @param LoggerInterface $logger
     * @return CorsMiddleware
     */
    public function __invoke(LoggerInterface $logger): CorsMiddleware
    {
        return new CorsMiddleware([
            'origin'         => ['*'],
            'methods'        => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'],
            'headers.allow'  => ['authorization', 'content-type', 'accept'],
            'headers.expose' => ['*'],
            'credentials'    => true,
            'cache'          => 0,
            'logger'         => $logger // This gets aut-magically set in the CorsMiddleware
        ]);
    }
}
