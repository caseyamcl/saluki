<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\Validation\Exception\FieldSetValidationException;
use Saluki\Validation\ValidatorContext;
use Saluki\WebInterface\Behavior\RequestHelper;
use Saluki\WebInterface\Contract\EndpointInterface;
use Saluki\WebInterface\Model\JsonApiError;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\SalukiRequest;

/**
 * Class EndpointHandlerMiddleware
 *
 * Inner-most handler
 *
 * @package Saluki\WebInterface\Middleware
 */
class EndpointHandlerMiddleware implements MiddlewareInterface
{
    use RequestHelper;

    /**
     * @var EndpointInterface
     */
    private $endpoint;

    /**
     * EndpointHandlerMiddleware constructor.
     * @param EndpointInterface $endpoint
     */
    public function __construct(EndpointInterface $endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (! $request instanceof SalukiRequest) {
            $request = SalukiRequest::fromRequest($request);
        }

        try {
            // Replace query parameters with prepared parameters (FieldSetValidationException thrown on invalid)
            $request = $request->withQueryParams($this->endpoint->listQueryParameters()->prepare(
                $request->getQueryParams(),
                new ValidatorContext($request->getQueryParams(), ['request' => $request]),
                $this->endpoint->listQueryParameters()->isAllowUndefinedFields()
            ));

            // Replace body parameters with prepared parameters (FieldSetValidationException thrown on invalid)
            $request = $request->withParsedBody($this->endpoint->listBodyParameters()->prepare(
                $request->getParsedBody(),
                new ValidatorContext($request->getParsedBody(), ['request' => $request]),
                $this->endpoint->listBodyParameters()->isAllowUndefinedFields()
            ));
        } catch (FieldSetValidationException $e) {
            foreach ($e->getExceptions() as $ex) {
                $errors[] = new JsonApiError($ex->getMessage(), 422, '' /* TODO: Source */);
            }
            return new JsonApiErrorResponse($errors ?? [], 422);
        }

        return call_user_func($this->endpoint, $request);
    }
}
