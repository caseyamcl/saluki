<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/13/18
 * Time: 1:30 PM
 */

namespace Saluki\WebInterface\Middleware;

use LogicException;
use Middlewares\Utils\HttpErrorException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\ErrorReporter\Service\ErrorService;
use Saluki\WebInterface\Behavior\RequestHelper;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Throwable;

/**
 * HTTP Exception Middleware
 *
 * Sometimes Middlewares provided by third-party libraries emit a special 'HTTPErrorException' that should be
 * converted into an HTTP Response.  This middleware catches those and returns the appropriate response.
 *
 * Note: This class does *NOT* handle general PHP errors and exceptions.  When those are thrown, the are handled
 * by the logging library.
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class ErrorHandlerMiddleware implements MiddlewareInterface
{
    use RequestHelper;

    /**
     * @var ErrorService
     */
    private $errorService;

    /**
     * ErrorHandlerMiddleware constructor.
     * @param ErrorService $errorService
     */
    public function __construct(ErrorService $errorService)
    {
        $this->errorService = $errorService;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (HttpErrorException $e) {
            return JsonApiErrorResponse::build($e->getMessage(), $e->getCode());
        } catch (Throwable $e) {
            $this->errorService->report($e, ['request-info' => $this->buildRequestInfo($request)]);
            return JsonApiErrorResponse::internalServerError();
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     */
    private function buildRequestInfo(ServerRequestInterface $request): array
    {
        try {
            $user = $this->getUserFromRequest($request);
            $out['user'] = $user->getIdentifier();
        } catch (LogicException $e) {
            $out['user'] = '(unknown)';
        }

        try {
            $ip = $this->getIpFromRequest($request);
            $out['client-ip'] = $ip;
        } catch (LogicException $e) {
            $out['client-ip'] = '(unknown)';
        }

        try {
            $client = $this->getClientFromRequest($request);
            $out['client-app-id'] = $client->getId();
            $out['client-app-name'] = $client->getName();
        } catch (LogicException $e) {
            $out['client-app-id'] = '(unknown)';
            $out['client-app-name'] = '(unknown)';
        }

        return $out ?? [];
    }
}
