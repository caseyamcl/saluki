<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Behavior;

use Saluki\Validation\ValidatorFieldSet;
use Saluki\WebInterface\RequestParam\RequestParam;

trait PaginationParamsTrait
{
    /**
     * Return default pagination parameter to be included in the list of queryable field names
     *
     * This will ensure that page[offset] and page[limit] are available in the query, and will always have
     * default values (even if NULL)
     *
     * @return RequestParam
     */
    protected function getPaginationParam(): RequestParam
    {
        $pageSubRules = new ValidatorFieldSet();
        $pageSubRules->optional('limit')->min(0);
        if (! is_null($this->getMaximumPaginationLimit())) {
            $pageSubRules->getField('limit')->max($this->getMaximumPaginationLimit());
        }
        $pageSubRules->optional('offset')->min(0);

        $param = new RequestParam('page', 'hash', false);
        $param->transform(function ($value) {
            return [
                'limit' => (int)($value['limit'] ?? $this->getDefaultPaginationLimit()),
                'offset' => (int)($value['offset'] ?? 0)
            ];
        });
        $param->subValues($pageSubRules)->setDefault([
            'limit'  => $this->getDefaultPaginationLimit(),
            'offset' => 0
        ]);

        return $param;
    }


    /**
     * Get default pagination limit
     *
     * @return int|null  NULL means no pagination limit
     */
    abstract public function getDefaultPaginationLimit(): ?int;

    /**
     * Get maximum pagination limit
     *
     * @return int|null
     */
    abstract public function getMaximumPaginationLimit(): ?int;
}
