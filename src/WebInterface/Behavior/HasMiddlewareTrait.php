<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Behavior;

use Psr\Http\Server\MiddlewareInterface;

trait HasMiddlewareTrait
{
    /**
     * @var array|MiddlewareInterface[]
     */
    private $middleware = [];

    /**
     * Add middleware in LIFO order (each middleware is added on-top of, or outside of the the prior one)
     *
     * @param iterable $middlewares
     * @return self
     */
    public function addMiddlewares(iterable $middlewares): self
    {
        foreach ($middlewares as $mw) {
            $this->add($mw);
        }

        return $this;
    }

    /**
     * Prepends middleware to the stack (the outside of the middleware pipeline)
     *
     * @param MiddlewareInterface $middleware
     * @return self
     */
    public function add(MiddlewareInterface $middleware): self
    {
        array_unshift($this->middleware, $middleware);
        return $this;
    }

    /**
     * Push middleware to the end of the stack (the inside of the middleware pipeline)
     *
     * @param MiddlewareInterface $middleware
     * @return self
     */
    public function push(MiddlewareInterface $middleware): self
    {
        $this->middleware[] = $middleware;
        return $this;
    }
}
