<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Behavior;

use Middlewares\ClientIp;
use Psr\Http\Message\ServerRequestInterface as Request;
use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\Authentication\Middleware\OAuthMiddleware;
use Saluki\WebInterface\Middleware\RoutingMiddleware;

/**
 * Trait RequestHelper
 * @package Saluki\WebInterface\Behavior
 */
trait RequestHelper
{
    /**
     * Get a path segment from the request
     *
     * @param Request $request
     * @param string $segment
     * @return mixed|null
     */
    protected function getPathSegmentFromRequest(Request $request, string $segment)
    {
        return $this->getPathSegmentsFromRequest($request)[$segment] ?? null;
    }

    /**
     * Get path segments from request
     *
     * @param Request $request
     * @return array
     */
    protected function getPathSegmentsFromRequest(Request $request): array
    {
        $segments = $request->getAttribute('path-segments');
        if (is_array($segments)) {
            return $segments;
        } else {
            throw new \LogicException(sprintf(
                "%s requires 'path-segments' attribute present in request.  Did you forget to run %s first?",
                get_called_class(),
                RoutingMiddleware::class
            ));
        }
    }

    /**
     * @param Request $request
     * @return callable
     */
    protected function getRouteHandlerFromRequest(Request $request): callable
    {
        if (! $routeHandler = $request->getAttribute('route-handler')) {
            throw new \LogicException(sprintf(
                "%s requires 'route-handler' attribute present in request.  Did you forget to run %s first?",
                get_called_class(),
                RoutingMiddleware::class
            ));
        }

        return $routeHandler;
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function getIpFromRequest(Request $request): string
    {
        if (! $ipAddress = $request->getAttribute('client-ip')) {
            throw new \LogicException(sprintf(
                "%s requires 'client-ip' attribute present in request.  Did you forget to run %s first?",
                get_called_class(),
                ClientIp::class
            ));
        }

        return $ipAddress;
    }

    /**
     * @param Request $request
     * @return UserInterface
     */
    protected function getUserFromRequest(Request $request): UserInterface
    {
        if (! $user = $request->getAttribute('user')) {
            throw new \LogicException(sprintf(
                "%s requires 'user' attribute present in request.  Did you forget to run %s first?",
                get_called_class(),
                OAuthMiddleware::class
            ));
        }

        return $user;
    }

    /**
     * Get client from request
     *
     * @param Request $request
     * @param bool $exceptionOnMissing
     * @return ClientAppInterface
     */
    public function getClientFromRequest(Request $request, bool $exceptionOnMissing = true): ClientAppInterface
    {
        $client = $request->getAttribute('client_app');

        if ($exceptionOnMissing && ! $client) {
            throw new \LogicException(sprintf(
                "%s requires 'client_app' attribute present in request.  Did you forget to run %s first?",
                get_called_class(),
                OAuthMiddleware::class
            ));
        }

        return $client;
    }
}
