<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface;

use Aura\Router\Generator;
use Aura\Router\Map;
use Aura\Router\Matcher;
use Aura\Router\RouterContainer;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\WebInterface\BackOff\BackOffInterface;
use Saluki\WebInterface\BackOff\Psr6Backoff;
use Saluki\WebInterface\Factory\WebRequestHandlerFactory;
use Saluki\WebInterface\Middleware\CorsMiddlewareFactory;
use Saluki\WebInterface\Model\RouteGroup;
use Saluki\WebInterface\Service\WebRequestHandler;
use Tuupola\Middleware\CorsMiddleware;

/**
 * Class WebInterfaceProvider
 * @package Saluki\WebInterface
 */
class WebInterfaceProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        $routerContainer = new RouterContainer();

        $builder->addServiceDefinitions([
            Generator::class         => \DI\factory([$routerContainer, 'getGenerator']),
            Map::class               => \DI\factory([$routerContainer, 'getMap']),
            Matcher::class           => \DI\factory([$routerContainer, 'getMatcher']),
            WebRequestHandler::class => \DI\factory(new WebRequestHandlerFactory()),
            BackOffInterface::class  => \DI\autowire(Psr6Backoff::class),
            RouteGroup::class        => \DI\autowire(RouteGroup::class),
            CorsMiddleware::class    => \DI\factory(CorsMiddlewareFactory::class)
        ]);
    }
}
