<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Service;

use Psr\Http\Message\ResponseInterface;
use Laminas\HttpHandlerRunner\Emitter\EmitterInterface;
use Laminas\HttpHandlerRunner\Emitter\SapiEmitter;
use Laminas\HttpHandlerRunner\Emitter\SapiStreamEmitter;

class DecidingResponseEmitter implements EmitterInterface
{
    protected const FORCE_STREAM_HEADER = 'X-Streamed';

    private SapiEmitter $emitter;
    private SapiStreamEmitter $streamEmitter;

    /**
     * DecidingEmitter constructor.
     *
     * @param SapiEmitter|null $emitter
     * @param SapiStreamEmitter|null $streamEmitter
     */
    public function __construct(SapiEmitter $emitter = null, SapiStreamEmitter $streamEmitter = null)
    {
        $this->emitter = $emitter ?: new SapiEmitter();
        $this->streamEmitter = $streamEmitter ?: new SapiStreamEmitter();
    }

    /**
     * Emit a response.
     *
     * Emits a response, including status line, headers, and the message body,
     * according to the environment.
     *
     * Implementations of this method may be written in such a way as to have
     * side effects, such as usage of header() or pushing output to the
     * output buffer.
     *
     * Implementations MAY raise exceptions if they are unable to emit the
     * response; e.g., if headers have already been sent.
     *
     * Implementations MUST return a boolean. A boolean `true` indicates that
     * the emitter was able to emit the response, while `false` indicates
     * it was not.
     *
     * @param ResponseInterface $response
     * @return bool
     */
    public function emit(ResponseInterface $response): bool
    {
        if (
            $response->hasHeader('Content-Disposition')
            or $response->hasHeader('Content-Range')
            or $response->hasHeader(self::FORCE_STREAM_HEADER)
        ) {
            return $this->streamEmitter->emit($response);
        } else {
            return $this->emitter->emit($response);
        }
    }
}
