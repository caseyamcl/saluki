<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Service;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Laminas\Diactoros\ServerRequestFactory;
use Laminas\HttpHandlerRunner\Emitter\EmitterInterface;
use Laminas\Stratigility\MiddlewarePipeInterface;

class WebRequestHandler
{
    private MiddlewarePipeInterface $webApp;
    private EmitterInterface $emitter;

    /**
     * WebRequestHandler constructor.
     *
     * @param MiddlewarePipeInterface $webApp
     * @param EmitterInterface $emitter
     */
    public function __construct(MiddlewarePipeInterface $webApp, EmitterInterface $emitter)
    {
        $this->webApp = $webApp;
        $this->emitter = $emitter;
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request = null): ResponseInterface
    {
        if (! $request) {
            $request = ServerRequestFactory::fromGlobals();
        }

        return $this->webApp->handle($request);
    }

    /**
     * Handle and emit the response
     *
     * Returns a boolean (TRUE if emitted, FALSE otherwise)
     *
     * @param ServerRequestInterface|null $request
     * @return bool
     */
    public function handleAndEmit(ServerRequestInterface $request = null): bool
    {
        $response = $this->handle($request);
        return $this->emitter->emit($response);
    }
}
