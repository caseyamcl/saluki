<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Service;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class SimpleRequestLogger
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class RequestLogMessageFormatter
{
    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    public function getRequestLogMessage(ServerRequestInterface $request): string
    {
        // Get path
        $path = sprintf(
            '%s%s%s',
            $request->getUri()->getPath(),
            $request->getUri()->getQuery() ? '?' . $request->getUri()->getQuery() : '',
            $request->getUri()->getFragment() ? '#' . $request->getUri()->getFragment() : ''
        );

        // Log request
        return trim(sprintf('> %s %s', $request->getMethod(), $path));
    }

    /**
     * @param ResponseInterface $response
     * @return string
     */
    public function getResponseLogMessage(ResponseInterface $response): string
    {
        return trim(sprintf('< %s %s', $response->getStatusCode(), $response->getReasonPhrase()));
    }
}
