<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Service;

use Saluki\WebInterface\BackOff\BackOffInterface;
use Saluki\WebInterface\BackOff\Middleware\AbstractBackOffMiddleware as BackOffMiddleware;
use Saluki\WebInterface\BackOff\Middleware\BackOffAll;
use Saluki\WebInterface\BackOff\Middleware\BackOffByBodyParam;
use Saluki\WebInterface\BackOff\Middleware\BackOffByHttpHeader;
use Saluki\WebInterface\BackOff\Middleware\BackOffByIp;
use Saluki\WebInterface\BackOff\Middleware\BackOffByQueryParam;

class BackOffMiddlewareFactory
{
    /**
     * @var BackOffInterface
     */
    private $backOff;

    /**
     * BackoffMiddlewareFactory constructor.
     * @param BackOffInterface $backOff
     */
    public function __construct(BackOffInterface $backOff)
    {
        $this->backOff = $backOff;
    }

    /**
     * @param float $multiplier
     * @param int $timeWindow
     * @return BackOffMiddleware
     */
    public function all(float $multiplier = 0.001, int $timeWindow = 60): BackOffMiddleware
    {
        return new BackOffAll($this->backOff, $multiplier, $timeWindow);
    }

    /**
     * @param string $paramName
     * @param float $multiplier
     * @param int $timeWindow
     * @return BackOffMiddleware
     */
    public function byQueryParam(string $paramName, float $multiplier = 0.001, int $timeWindow = 60): BackOffMiddleware
    {
        return new BackOffByQueryParam($this->backOff, $paramName, $multiplier, $timeWindow);
    }

    /**
     * @param string $headerName
     * @param float $multiplier
     * @param int $timeWindow
     * @return BackOffMiddleware
     */
    public function byHttpHeader(string $headerName, float $multiplier = 0.001, int $timeWindow = 60): BackOffMiddleware
    {
        return new BackOffByHttpHeader($this->backOff, $headerName, $multiplier, $timeWindow);
    }

    /**
     * @param string $paramName
     * @param float $multiplier
     * @param int $timeWindow
     * @return BackOffMiddleware
     */
    public function byBodyParam(string $paramName, float $multiplier = 0.001, int $timeWindow = 60): BackOffMiddleware
    {
        return new BackOffByBodyParam($this->backOff, $paramName, $multiplier, $timeWindow);
    }

    /**
     * @param float $multiplier
     * @param int $timeWindow
     * @return BackOffMiddleware
     */
    public function byIp(float $multiplier = 0.001, int $timeWindow = 60): BackOffMiddleware
    {
        return new BackOffByIp($this->backOff, $multiplier, $timeWindow);
    }
}
