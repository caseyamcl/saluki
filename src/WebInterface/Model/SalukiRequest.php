<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Model;

use Psr\Http\Message\ServerRequestInterface;
use Saluki\Authentication\Contract\ClientAppInterface;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\WebInterface\Behavior\RequestHelper;
use Laminas\Diactoros\ServerRequest;

class SalukiRequest extends ServerRequest
{
    use RequestHelper;

    /**
     * @param ServerRequestInterface $request
     * @return SalukiRequest
     */
    public static function fromRequest(ServerRequestInterface $request)
    {
        $that = new static(
            $request->getServerParams(),
            $request->getUploadedFiles(),
            $request->getUri(),
            $request->getMethod(),
            $request->getBody(),
            $request->getHeaders(),
            $request->getCookieParams(),
            $request->getQueryParams(),
            $request->getParsedBody(),
            $request->getProtocolVersion()
        );

        // Add attributes
        foreach ($request->getAttributes() as $name => $value) {
            $that = $that->withAttribute($name, $value);
        }

        return $that;
    }

    public function __construct(
        array $serverParams = [],
        array $uploadedFiles = [],
        $uri = null,
        ?string $method = null,
        $body = 'php://input',
        array $headers = [],
        array $cookies = [],
        array $queryParams = [],
        $parsedBody = null,
        string $protocol = '1.1'
    ) {
        parent::__construct(
            $serverParams,
            $uploadedFiles,
            $uri,
            $method,
            $body,
            $headers,
            $cookies,
            $queryParams,
            $parsedBody,
            $protocol
        );
    }

    /**
     * @return string
     */
    public function getClientIp(): string
    {
        return $this->getIpFromRequest($this);
    }

    /**
     * @param bool $required
     * @return ClientAppInterface|null
     */
    public function getClientApp(bool $required = true): ?ClientAppInterface
    {
        return $this->getClientFromRequest($this, $required);
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->getUserFromRequest($this);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getBodyParam(string $name)
    {
        $val = $this->findBodyParam($name);
        if (! is_null($val)) {
            return $val;
        } else {
            throw new \RuntimeException("Body parameter not found: " . $name);
        }
    }

    /**
     * Does parameter exist (will return TRUE even if it is NULL)
     *
     * @param string $name
     * @return bool
     */
    public function hasQueryParam(string $name): bool
    {
        return array_key_exists($name, $this->getQueryParams());
    }

    /**
     * Does parameter exist (will return TRUE even if it is NULL)
     *
     * @param string $name
     * @return bool
     */
    public function hasBodyParam(string $name): bool
    {
        return array_key_exists($name, $this->getParsedBody());
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getQueryParam(string $name)
    {
        $val = $this->findQueryParam($name);
        if (! is_null($val)) {
            return $val;
        } else {
            throw new \RuntimeException("Query parameter not found: " . $name);
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function findBodyParam(string $name)
    {
        return $this->getParsedBody()[$name] ?? null;
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function findQueryParam(string $name)
    {
        return $this->getQueryParams()[$name] ?? null;
    }

    /**
     * @param string $segment
     * @return null|string
     */
    public function getPathSegment(string $segment): ?string
    {
        return ($item = $this->getPathSegmentFromRequest($this, $segment)) ? (string) $item : null;
    }

    /**
     * Check if the request user has a specific role
     * @param string ...$role
     * @return bool
     */
    public function userHasRole(string ...$role): bool
    {
        return count(array_intersect($this->getUser()->getRoles(), $role)) > 0;
    }

    /**
     * Get path and query
     *
     * @param array $setQueryItems  Optionally supply additional parameters to be included in the query
     * @return string
     */
    public function getPathAndQuery(array $setQueryItems = []): string
    {
        parse_str(urldecode($this->getUri()->getQuery()), $queryItems);

        if ($setQueryItems) {
            parse_str(http_build_query($setQueryItems), $setItems);
            $query = array_replace($queryItems, $setItems);
        } else {
            $query = $queryItems;
        }

        return urldecode(sprintf(
            '%s%s%s',
            $this->getUri()->getPath(),
            (! empty(array_filter($query))) ? ('?' . urlencode(http_build_query($query))) : '',
            $this->getUri()->getFragment() ? '#' . $this->getUri()->getFragment() : ''
        ));
    }
}
