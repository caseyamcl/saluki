<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Model;

use Psr\Http\Server\MiddlewareInterface;
use Saluki\WebInterface\Contract\EndpointInterface;
use Saluki\WebInterface\Middleware\EndpointHandlerMiddleware;

/**
 * Class EndpointRoute
 * @package Saluki\WebInterface\Model
 */
class EndpointRoute extends RouteWithMiddleware
{
    /**
     * @var EndpointInterface
     */
    private $endpoint;

    /**
     * EndpointRoute constructor.
     * @param EndpointInterface $endpoint
     */
    public function __construct(EndpointInterface $endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @return EndpointInterface
     */
    public function getEndpoint(): EndpointInterface
    {
        return $this->endpoint;
    }

    /**
     * @return MiddlewareInterface
     */
    protected function getInnerMiddleware(): MiddlewareInterface
    {
        return new EndpointHandlerMiddleware($this->endpoint);
    }
}
