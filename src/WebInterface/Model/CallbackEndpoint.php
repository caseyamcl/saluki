<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *   For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *   ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/20/18
 * Time: 12:56 PM
 */

namespace Saluki\WebInterface\Model;

use Psr\Http\Message\ResponseInterface;
use Saluki\WebInterface\RequestParam\ParameterList;

/**
 * Class CallbackEndpoint
 * @package Saluki\WebInterface\Model
 */
class CallbackEndpoint extends AbstractEndpoint
{
    /**
     * @var callable
     */
    private $callback;

    /**
     * CallbackEndpoint constructor.
     * @param callable $callback
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * @return ParameterList
     */
    public function listQueryParameters(): ParameterList
    {
        return (new ParameterList())->setAllowUndefinedFields(true);
    }

    /**
     * @return ParameterList
     */
    public function listBodyParameters(): ParameterList
    {
        return (new ParameterList())->setAllowUndefinedFields(true);
    }

    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        return call_user_func($this->callback, $request);
    }
}
