<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Model;

use Aura\Router\Route;
use Saluki\WebInterface\Behavior\HasMiddlewareTrait;
use Psr\Http\Server\MiddlewareInterface;
use Laminas\Stratigility\Middleware\CallableMiddlewareDecorator;
use Laminas\Stratigility\MiddlewarePipe;

/**
 * Class RouteWithMiddleware
 * @package Saluki\WebInterface\Model
 */
class RouteWithMiddleware extends Route
{
    use HasMiddlewareTrait;

    /**
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        $value = parent::__get($key);

        if ($key == 'handler') {
            $pipe = new MiddlewarePipe();
            foreach ($this->middleware as $mw) {
                $pipe->pipe($mw);
            }
            $pipe->pipe($this->getInnerMiddleware());
            return [$pipe, 'handle'];
        } else {
            return $value;
        }
    }

    /**
     * @return MiddlewareInterface
     */
    protected function getInnerMiddleware(): MiddlewareInterface
    {
        return new CallableMiddlewareDecorator(parent::__get('handler'));
    }
}
