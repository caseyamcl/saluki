<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Model;

use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\WebInterface\Behavior\HasMiddlewareTrait;
use Saluki\WebInterface\Contract\EndpointInterface;
use Psr\Http\Server\MiddlewareInterface;

/**
 * Class RouteGroup
 * @package Saluki\WebInterface\Model
 */
class RouteGroup implements \IteratorAggregate
{
    use HasMiddlewareTrait;

    /**
     * @var array|RouteWithMiddleware[]
     */
    private $routes = [];

    /**
     * @var array|RouteGroup[]
     */
    private $groups = [];

    /**
     * @var RouteGroup|null
     */
    private $parent;

    /**
     * @var string
     */
    private $routePrefix;

    /**
     * RouteGroup constructor.
     *
     * @param RouteGroup|null $parent
     * @param string $routePrefix
     */
    public function __construct(?RouteGroup $parent = null, string $routePrefix = '')
    {
        $this->parent = $parent;
        $this->routePrefix = $routePrefix;
    }

    /**
     * Get ALL middleware (mine and my parents')
     *
     * @return array|MiddlewareInterface[]
     */
    public function getMiddleware(): array
    {
        return ($this->getParent())
            ? array_merge($this->middleware, $this->getParent()->getMiddleware())
            : $this->middleware;
    }

    /**
     * @return string
     */
    public function getRoutePrefix(): string
    {
        return $this->routePrefix;
    }

    /**
     * @return RouteGroup|null
     */
    public function getParent(): ?RouteGroup
    {
        return $this->parent;
    }

    /**
     * Iterate over this and all child group routes
     *
     * @return \Iterator|RouteWithMiddleware[]
     */
    public function getIterator()
    {
        $iterator = new \AppendIterator();

        // Add my routes with my middleware (and my parents')
        $iterator->append(new \ArrayIterator(array_map(function (RouteWithMiddleware $route) {
            return $route->addMiddlewares($this->getMiddleware());
        }, $this->routes)));

        // Add my children
        foreach ($this->groups as $group) {
            $iterator->append($group->getIterator());
        }

        return $iterator;
    }

    // --------------------------------------------------------------

    /**
     * Create new route group under this route group
     *
     * The route group will inherit all of the middleware that this route group has
     * @param string $routePrefix
     * @return RouteGroup|static
     */
    public function newGroup(string $routePrefix = ''): RouteGroup
    {
        $group = new static($this, $routePrefix);
        $this->groups[] = $group;
        return $group;
    }

    // --------------------------------------------------------------

    /**
     * Add a GET endpoint
     *
     * @param string $path
     * @param EndpointInterface $endpoint
     * @param array $routeMiddleware
     * @return RouteWithMiddleware
     */
    public function get(string $path, EndpointInterface $endpoint, array $routeMiddleware = []): RouteWithMiddleware
    {
        return $this->route($path, $endpoint, ['GET'], $routeMiddleware);
    }

    /**
     * Add a POST endpoint
     *
     * @param string $path
     * @param EndpointInterface $endpoint
     * @param array $routeMiddleware
     * @return RouteWithMiddleware
     */
    public function post(string $path, EndpointInterface $endpoint, array $routeMiddleware = []): RouteWithMiddleware
    {
        return $this->route($path, $endpoint, ['POST'], $routeMiddleware);
    }

    /**
     * Add a PUT endpoint
     *
     * @param string $path
     * @param EndpointInterface $endpoint
     * @param array $routeMiddleware
     * @return RouteWithMiddleware
     */
    public function put(string $path, EndpointInterface $endpoint, array $routeMiddleware = []): RouteWithMiddleware
    {
        return $this->route($path, $endpoint, ['PUT'], $routeMiddleware);
    }

    /**
     * Add a PATCH endpoint
     *
     * @param string $path
     * @param EndpointInterface $endpoint
     * @param array $routeMiddleware
     * @return RouteWithMiddleware
     */
    public function patch(string $path, EndpointInterface $endpoint, array $routeMiddleware = []): RouteWithMiddleware
    {
        return $this->route($path, $endpoint, ['PATCH'], $routeMiddleware);
    }

    /**
     * Add a DELETE endpoint
     *
     * @param string $path
     * @param EndpointInterface $endpoint
     * @param array $routeMiddleware
     * @return RouteWithMiddleware
     */
    public function delete(string $path, EndpointInterface $endpoint, array $routeMiddleware = []): RouteWithMiddleware
    {
        return $this->route($path, $endpoint, ['DELETE'], $routeMiddleware);
    }

    /**
     * Add a REST Resource
     *
     * @param string $path
     * @param RestResourceInterface $resource
     * @return RouteGroup  REST Resource route group (different than this one)
     */
    public function resource(string $path, RestResourceInterface $resource): RouteGroup
    {
        return $resource->registerEndpoints($path, $this);
    }

    /**
     * Add a route
     *
     * @param string $path
     * @param EndpointInterface $endpoint
     * @param array $routeMiddleware
     * @param array $methods
     * @return RouteWithMiddleware
     * @throws \Aura\Router\Exception\ImmutableProperty
     */
    public function route(
        string $path,
        EndpointInterface $endpoint,
        array $methods = [],
        array $routeMiddleware = []
    ): RouteWithMiddleware {

        $path = $this->routePrefix ? $this->routePrefix . $path : $path;

        if ($path !== '/') {
            $path = rtrim($path, '/');
        }

        $route = (new EndpointRoute($endpoint))->path($path);
        $endpoint->validateRoute($route);

        if (! empty($methods)) {
            $route->allows(array_map('strtoupper', $methods));
        }

        $route->addMiddlewares($routeMiddleware);
        $this->routes[] = $route;
        return $route;
    }
}
