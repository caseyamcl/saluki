<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Model;

/**
 * Class JsonApiError
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class JsonApiError
{
    public const AUTO = null;

    private ?string $id;
    private ?array $links;
    private string $status;
    private int|string|null $code;
    private string $title;
    private string $detail;
    private ?array $source = null;
    private ?array $meta;

    /**
     * JsonApiError constructor.
     */
    public function __construct(
        string $title,
        int $status,
        string $detail = '',
        string $source = '',
        ?string $aboutLink = null,
        ?array $meta = null,
        string|int|null $code = self::AUTO,
        ?string $id = self::AUTO
    ) {
        // these values are set as-is
        $this->title = $title;
        $this->detail = $detail;
        $this->meta = $meta;
        $this->status = (string) $status;

        // id and code are auto-derived if not supplied
        $this->id = $id ?: null;
        $this->code = $code ?: null;

        // if links or source are specified, build objects out of them
        $this->links = $aboutLink ? ['about' => $aboutLink] : [];
        $this->source = $source ? ['pointer' => $source] : null;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLinks(): ?array
    {
        return $this->links ?: null;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDetail(): string
    {
        return $this->detail;
    }

    public function getSource(): ?array
    {
        return $this->source;
    }

    public function getMeta(): array
    {
        return $this->meta;
    }

    public function __toArray(): array
    {
        return array_filter(get_object_vars($this));
    }
}
