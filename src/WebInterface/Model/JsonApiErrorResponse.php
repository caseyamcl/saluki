<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Model;

use Laminas\Diactoros\Response\JsonResponse;

/**
 * JSON API Error Response
 *
 * @package Saluki\WebInterface\Model
 */
class JsonApiErrorResponse extends JsonResponse
{
    public const CONTENT_TYPE = 'application/vnd.api+json';

    /**
     * @var JsonApiError[]
     */
    private array $errors = [];
    private array $jsonData;

    /**
     * @param string $message
     * @return JsonApiErrorResponse
     */
    public static function internalServerError(string $message = 'Internal Server Error'): JsonApiErrorResponse
    {
        return static::build($message, 500, 'refer to logs for details');
    }

    /**
     * @param string $message
     * @return JsonApiErrorResponse
     */
    public static function forbidden(string $message = 'Forbidden')
    {
        return static::build($message, 403);
    }

    /**
     * @param string|string[] $messages
     * @param string $detail
     * @return JsonApiErrorResponse
     */
    public static function notFound($messages = 'Not Found', string $detail = '')
    {
        if (is_iterable($messages)) {
            foreach ($messages as $msg) {
                $errors = new JsonApiError($msg, 404, $detail);
            }
            return new static($errors ?? [], 404);
        } else {
            return static::build($messages, 404, $detail);
        }
    }

    public static function conflict(string $conflictMessage, string $detail = '')
    {
        return static::build($conflictMessage, 409, $detail);
    }

    /**
     * Build a single error response
     *
     * @param string $title
     * @param int $status
     * @param string $detail
     * @param string $source
     * @param string|null $aboutLink
     * @param array|null $meta
     * @param string|null $code
     * @param string|null $id
     * @return JsonApiErrorResponse
     */
    public static function build(
        string $title,
        int $status,
        string $detail = '',
        string $source = '',
        ?string $aboutLink = null,
        ?array $meta = null,
        ?string $code = JsonApiError::AUTO,
        ?string $id = JsonApiError::AUTO
    ) {
        return new static(
            [new JsonApiError($title, $status, $detail, $source, $aboutLink, $meta, $code, $id)],
            $status
        );
    }

    /**
     * ErrorJsonResponse constructor.
     *
     * @param iterable|JsonApiError[] $errors
     * @param int $status  HTTP status code
     */
    public function __construct(iterable $errors, int $status)
    {
        if ($status < 400 or $status >= 600) {
            throw new \InvalidArgumentException('HTTP error status must be between 400 and 599');
        }

        $addError = function (JsonApiError $error) use ($status) {
            if ($error->getStatus() !== (string) $status) {
                throw new \InvalidArgumentException(sprintf(
                    'JsonApiError status mismatch: %s/%s',
                    $error->getStatus(),
                    $status
                ));
            }
            $this->errors[] = $error;
        };

        foreach ($errors as $error) {
            $addError($error);
            $data[] = $error->__toArray();
        }

        if (count($this->errors) == 0) {
            throw new \RuntimeException("A JsonApiErrorResponse must contain at-least one error");
        }

        $this->jsonData = ['errors' => $data ?? [], 'jsonapi'  => ['version' => '1.0']];

        parent::__construct($this->jsonData, $status, ['Content-type' => static::CONTENT_TYPE]);
    }

    /**
     * @return array|JsonApiError[]
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getJsonData(): array
    {
        return $this->jsonData;
    }
}
