<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Model;

use Aura\Router\Route;
use Saluki\WebInterface\RequestParam\ParameterList;
use Saluki\WebInterface\Contract\EndpointInterface;
use Saluki\WebInterface\Exeption\InvalidRouteDefinitionException;

abstract class AbstractEndpoint implements EndpointInterface
{
    public function getDescription(): string
    {
        return '';
    }

    /**
     * By default, all routes are valid
     *
     * @param Route $route
     * @return void
     */
    public function validateRoute(Route $route): void
    {
        // pass, by default
    }

    /**
     * By default, return an empty field set
     *
     * @return ParameterList
     */
    public function listQueryParameters(): ParameterList
    {
        return new ParameterList();
    }

    /**
     * By default, return an empty field set
     *
     * @return ParameterList
     */
    public function listBodyParameters(): ParameterList
    {
        return new ParameterList();
    }

    /**
     * @param string|string[] $methods
     * @param string ...$allowedMethods
     */
    protected function ensureMethod($methods, string ...$allowedMethods): void
    {
        $methods = (array) $methods;

        $diff = array_diff(array_map('strtoupper', $methods), array_map('strtoupper', $allowedMethods));

        if (! empty($diff)) {
            throw new InvalidRouteDefinitionException(sprintf(
                '%s method(s) not supported for %s (allowed methods: %s)',
                implode(', ', $methods),
                get_called_class(),
                implode(', ', $allowedMethods)
            ));
        }
    }

    /**
     * @param string $path
     * @param string $paramName
     */
    protected function ensurePathParameter(string $path, string $paramName): void
    {
        $pattern = sprintf('/\{((.+?)\b)?%s((.+?)\b)?\}/i', $paramName);

        if (! preg_match($pattern, $path)) {
            throw new InvalidRouteDefinitionException(sprintf(
                'path must contain parameter: %s (path: %s)',
                $paramName,
                $path
            ));
        }
    }

    /**
     *
     */
    protected function ensureNoPathParameters(string $path): void
    {
        if (preg_match('/\{(.+)?\}/', $path)) {
            throw new InvalidRouteDefinitionException(sprintf(
                'path must not contain any parameters (path: %s)',
                $path
            ));
        }
    }
}
