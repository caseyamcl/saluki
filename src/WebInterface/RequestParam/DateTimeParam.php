<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/7/18
 * Time: 2:22 PM
 */

namespace Saluki\WebInterface\RequestParam;

use Saluki\Validation\ValidatorField;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\Contract\RequestParamInterface;

/**
 * Class TimestampBodyParam
 * @package Saluki\WebInterface\RequestParam
 */
class DateTimeParam extends ValidatorField implements RequestParamInterface
{
    public function __construct(string $fieldName, bool $required = true, ValidatorRuleSet $ruleSet = null)
    {
        parent::__construct($fieldName, $required, $ruleSet);
        $this->temporal();
    }

    /**
     * @return string
     */
    public function getDataType(): string
    {
        return 'timestamp';
    }
}
