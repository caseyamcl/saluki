<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/7/18
 * Time: 2:24 PM
 */

namespace Saluki\WebInterface\RequestParam;

use Saluki\Validation\ValidatorField;
use Saluki\WebInterface\Contract\RequestParamInterface;

class StringParam extends ValidatorField implements RequestParamInterface
{
    /**
     * @return string
     */
    public function getDataType(): string
    {
        return 'string';
    }
}
