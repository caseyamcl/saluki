<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/10/18
 * Time: 3:52 PM
 */

namespace Saluki\WebInterface\RequestParam;

use Doctrine\Persistence\ObjectRepository;
use Saluki\Validation\Rule\Entity;
use Saluki\Validation\ValidatorField;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\Contract\RequestParamInterface;

/**
 * Class EntityParam
 *
 * @package Saluki\WebInterface\RequestParam
 */
class EntityParam extends ValidatorField implements RequestParamInterface
{
    public function __construct(
        string $fieldName,
        ObjectRepository $repository,
        ?string $entityIdField = null,
        ValidatorRuleSet $ruleSet = null
    ) {
        parent::__construct($fieldName, true, $ruleSet);
        $this->addRule(new Entity($repository, null, $entityIdField));
    }


    /**
     * @return string
     */
    public function getDataType(): string
    {
        return 'entity';
    }
}
