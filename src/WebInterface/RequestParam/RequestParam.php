<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 10/22/18
 * Time: 1:49 PM
 */

namespace Saluki\WebInterface\RequestParam;


use Saluki\Validation\ValidatorField;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\Contract\RequestParamInterface;

/**
 * Class RequestParam
 * @package Saluki\WebInterface\RequestParam
 */
class RequestParam extends ValidatorField implements RequestParamInterface
{
    /**
     * @var string
     */
    private $dataType;

    /**
     * RequestParam constructor.
     * @param string $fieldName
     * @param string $dataType
     * @param bool $required
     * @param ValidatorRuleSet|null $ruleSet
     */
    public function __construct(
        string $fieldName,
        string $dataType,
        bool $required = true,
        ValidatorRuleSet $ruleSet = null
    ) {
        parent::__construct($fieldName, $required, $ruleSet);
        $this->dataType = $dataType;
    }

    /**
     * @return string
     */
    public function getDataType(): string
    {
        return $this->dataType;
    }
}
