<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/22/18
 * Time: 4:24 PM
 */

namespace Saluki\WebInterface\RequestParam;

use Saluki\Validation\Contract\ValidatorFieldInterface;
use Saluki\Validation\ValidatorField;
use Saluki\Validation\ValidatorFieldSet;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\Contract\RequestParamInterface;

/**
 * Class ParameterList
 * @package Saluki\Validation
 */
class ParameterList extends ValidatorFieldSet
{
    /**
     * @var bool
     */
    private $allowUndefinedFields = false;

    /**
     * @param bool $allowUndefined
     * @return ParameterList|static
     */
    public function setAllowUndefinedFields(bool $allowUndefined): self
    {
        $this->allowUndefinedFields = $allowUndefined;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowUndefinedFields(): bool
    {
        return $this->allowUndefinedFields;
    }

    /**
     * @param string $fieldName
     * @param bool $required
     * @param iterable $rules
     * @return ValidatorField
     */
    public function field(string $fieldName, bool $required = true, iterable $rules = []): ValidatorFieldInterface
    {
        return $this->add(new RequestParam($fieldName, 'field', $required, new ValidatorRuleSet($rules)));
    }

    /**
     * @param ValidatorFieldInterface $field
     * @return ValidatorFieldInterface
     */
    public function add(ValidatorFieldInterface $field): ValidatorFieldInterface
    {
        if (! $field instanceof RequestParamInterface) {
            throw new \InvalidArgumentException(sprintf(
                '%s must be an instance of %s',
                $field->getName(),
                RequestParamInterface::class
            ));
        }
        return parent::add($field);
    }
}
