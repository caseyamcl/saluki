<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\BackOff;

use Psr\Cache\CacheItemPoolInterface;

/**
 * Class Psr6RateLimiter
 */
class Psr6Backoff implements BackOffInterface
{
    use BackOffTrait;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * Psr6RateLimiter constructor.
     *
     * @param CacheItemPoolInterface $cache
     */
    public function __construct(CacheItemPoolInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Delay execution with sleep based on number of calls in a given time period
     *
     * @param string $key
     * @param float $multiplier
     * @param int $timeWindow
     * @return float  Time waited in seconds
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function makeWait(string $key, $multiplier = 0.001, $timeWindow = 60): float
    {
        $countFrom = time() - $timeWindow;

        $cacheItem = $this->cache->getItem($key);
        $items     = (array) $cacheItem->get() ?: [];

        // Count the number of actions that have occurred since the timestamp
        $reqCount = count(array_filter($items, function ($v) use ($countFrom) {
            return $v >= $countFrom;
        }));

        $waitTime = $reqCount * $multiplier;
        $this->doWait($waitTime);

        $items[] = time();

        $cacheItem->set($this->doClean($items, $countFrom));
        $this->cache->save($cacheItem);

        return $waitTime;
    }

    /**
     * Clean out unix timestamps from an array that are older than a given timestamp
     *
     * @param array $items
     * @param int $earliestTimestamp
     * @return array
     */
    private function doClean(array $items, int $earliestTimestamp)
    {
        return array_filter($items, function (int $timestamp) use ($earliestTimestamp) {
            return $timestamp >= $earliestTimestamp;
        });
    }
}
