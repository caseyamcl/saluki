<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\BackOff;

/**
 * Class RateLimiterTrait
 */
trait BackOffTrait
{
    /**
     * @param float $waitSeconds Can be partial seconds
     */
    protected function doWait(float $waitSeconds)
    {
        usleep($waitSeconds * 1000000);
    }
}
