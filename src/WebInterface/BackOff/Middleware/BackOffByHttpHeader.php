<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\BackOff\Middleware;

use Saluki\WebInterface\BackOff\BackOffInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class BackOffByHttpHeader
 * @package Saluki\WebInterface\BackOff\Middleware
 */
class BackOffByHttpHeader extends AbstractBackOffMiddleware
{
    /**
     * @var string
     */
    private $headerName;

    /**
     * BackOffByBodyParam constructor.
     *
     * @param BackOffInterface $backOff
     * @param string $headerName
     * @param float $multiplier
     * @param int $timeWindow
     */
    public function __construct(
        BackOffInterface $backOff,
        string $headerName,
        float $multiplier = 0.01,
        int $timeWindow = 60
    ) {
        parent::__construct($backOff, $multiplier, $timeWindow);
        $this->headerName = $headerName;
    }

    /**
     * Get cache key name to track timeouts
     *
     * @param ServerRequestInterface $request
     * @return string  Return empty string if key could not be determined (no timeout will occur)
     */
    protected function getKey(ServerRequestInterface $request): string
    {
        return $request->getHeaderLine($this->headerName) ?: '';
    }
}
