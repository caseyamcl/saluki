<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\BackOff\Middleware;

use Saluki\WebInterface\BackOff\BackOffInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class AbstractBackoffMiddleware
 * @package Saluki\WebInterface\BackOff\Middleware
 */
abstract class AbstractBackOffMiddleware implements MiddlewareInterface
{
    /**
     * @var BackOffInterface
     */
    protected $backOff;

    /**
     * @var float
     */
    protected $multiplier;

    /**
     * @var int
     */
    protected $timeWindow;

    /**
     * AbstractBackOffMiddleware constructor.
     * @param BackOffInterface $backOff
     * @param float $multiplier
     * @param int $timeWindow
     */
    public function __construct(BackOffInterface $backOff, float $multiplier = 0.01, int $timeWindow = 60)
    {
        $this->backOff = $backOff;
        $this->multiplier = $multiplier;
        $this->timeWindow = $timeWindow;
    }

    /**
     * Process the request
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($key = $this->getKey($request)) {
            $this->backOff->makeWait($key, $this->multiplier, $this->timeWindow);
        }

        return $handler->handle($request);
    }

    /**
     * Get cache key name to track timeouts
     *
     * @param ServerRequestInterface $request
     * @return string  Return empty string if key could not be determined (no timeout will occur)
     */
    abstract protected function getKey(ServerRequestInterface $request): string;
}
