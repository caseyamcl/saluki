<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\BackOff\Middleware;

use Saluki\WebInterface\BackOff\BackOffInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class BackOffByQueryParam
 * @package Saluki\WebInterface\BackOff\Middleware
 */
class BackOffByQueryParam extends AbstractBackOffMiddleware
{
    /**
     * @var string
     */
    private $queryParamName;

    /**
     * BackOffByBodyParam constructor.
     *
     * @param BackOffInterface $backOff
     * @param string $queryParamName
     * @param float $multiplier
     * @param int $timeWindow
     */
    public function __construct(
        BackOffInterface $backOff,
        string $queryParamName,
        float $multiplier = 0.01,
        int $timeWindow = 60
    ) {
        parent::__construct($backOff, $multiplier, $timeWindow);
        $this->queryParamName = $queryParamName;
    }


    /**
     * Get cache key name to track timeouts
     *
     * @param ServerRequestInterface $request
     * @return string  Return empty string if key could not be determined (no timeout will occur)
     */
    protected function getKey(ServerRequestInterface $request): string
    {
        return (string) $request->getQueryParams()[$this->queryParamName] ?? '';
    }
}
