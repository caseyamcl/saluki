<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\BackOff\Middleware;

use Psr\Http\Message\ServerRequestInterface;

/**
 * Class BackOffAll
 * @package Saluki\WebInterface\BackOff\Middleware
 */
class BackOffAll extends AbstractBackOffMiddleware
{
    /**
     * Get cache key name to track timeouts
     *
     * @param ServerRequestInterface $request
     * @return string  Return empty string if key could not be determined (no timeout will occur)
     */
    protected function getKey(ServerRequestInterface $request): string
    {
        return 'backoff_all';
    }
}
