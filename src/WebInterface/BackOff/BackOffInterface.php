<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\BackOff;

/**
 * Class RateLimiterInterface
 */
interface BackOffInterface
{
    /**
     * Delay execution with sleep based on number of calls in a given time period
     *
     * @param string $key
     * @param float $multiplier
     * @param int $timeWindow
     * @return float  Time waited in seconds
     */
    public function makeWait(string $key, $multiplier = 0.001, $timeWindow = 60): float;
}
