<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\WebInterface\Factory;

use Middlewares\ClientIp;
use Middlewares\JsonPayload;
use Middlewares\TrailingSlash;
use Middlewares\UrlEncodePayload;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Saluki\Authentication\Middleware\AddRolesMiddleware;
use Saluki\Authentication\Middleware\OAuthMiddleware;
use Saluki\ErrorReporter\Service\ErrorService;
use Saluki\Persistence\Middleware\TransactionMiddleware;
use Saluki\Serialization\Middleware\SerializeMiddleware;
use Saluki\WebInterface\Middleware\ErrorHandlerMiddleware;
use Saluki\WebInterface\Middleware\RoutingMiddleware;
use Saluki\WebInterface\Middleware\SimpleRequestLogMiddleware;
use Saluki\WebInterface\Service\DecidingResponseEmitter;
use Saluki\WebInterface\Service\WebRequestHandler;
use Tuupola\Middleware\CorsMiddleware;
use Laminas\Stratigility\MiddlewarePipe;

/**
 * Class WebRequestHandlerFactory
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class WebRequestHandlerFactory
{
    public function __invoke(ContainerInterface $container): WebRequestHandler
    {
        $app = new MiddlewarePipe();

        // Add middlewares that apply to EVERY REQUEST.  NOTE: ORDER MATTERS!
        // Notice auth middleware isn't here.  It is added in the App class.

        // Outer group - Basic Stuff
        $app->pipe(new SimpleRequestLogMiddleware($container->get(LoggerInterface::class)));
        $app->pipe(new ErrorHandlerMiddleware($container->get(ErrorService::class)));
        $app->pipe((new ClientIp())->proxy());
        $app->pipe(new TrailingSlash());
        $app->pipe(new UrlEncodePayload());
        $app->pipe(new JsonPayload());
        $app->pipe($container->get(CorsMiddleware::class));

        // Middle group - Transactions and logging
        $app->pipe($container->get(TransactionMiddleware::class));
        $app->pipe($container->get(OAuthMiddleware::class));
        $app->pipe($container->get(AddRolesMiddleware::class));

        // Inner group
        $app->pipe($container->get(SerializeMiddleware::class));
        $app->pipe($container->get(RoutingMiddleware::class));

        return new WebRequestHandler($app, new DecidingResponseEmitter());
    }
}
