<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Console\Runner;

use Saluki\Console\Registry\ConsoleCommandRegistry;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

class ConsoleRunner
{
    /**
     * @var Application
     */
    private $consoleApp;

    /**
     * ConsoleRunner constructor.
     * @param Application $consoleApp
     * @param ConsoleCommandRegistry $registry
     */
    public function __construct(Application $consoleApp, ConsoleCommandRegistry $registry)
    {
        // Setup console app
        $this->consoleApp = $consoleApp;

        // Add commands
        foreach ($registry->getIterator() as $command) {
            $this->consoleApp->add($command);
        }
    }

    /**
     * @param null|InputInterface $input
     * @param null|OutputInterface $output
     * @return int
     */
    public function run(?InputInterface $input = null, ?OutputInterface $output = null): int
    {
        $output = $output ?: new ConsoleOutput();
        return (int) $this->consoleApp->run($input, $output);
    }
}
