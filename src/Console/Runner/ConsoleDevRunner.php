<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Console\Runner;

use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;
use RuntimeException;
use Saluki\Config\Model\Config;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Doctrine\Migrations\Tools\Console\Command as ConsoleCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConsoleDevRunner
 * @package Saluki\Console
 */
class ConsoleDevRunner
{
    private Application $consoleApp;

    private bool $isDevMode;

    public function __construct(
        Application $consoleApp,
        Config $appConfig,
        ?DependencyFactory $dependencyFactory = null,
        ?EntityManagerInterface $entityManager = null
    ) {
        $this->isDevMode = $appConfig->isDevMode();
        $this->consoleApp = $consoleApp;

        // Append 'DEVELOPMENT UTILITY' to app name
        $this->consoleApp->setName(trim($this->consoleApp->getName() . ' (DEVELOPMENT UTILITY)'));

        // Add Doctrine commands to development utility
        if ($dependencyFactory) {
            $this->addDoctrineCommands($dependencyFactory, $entityManager);
        }
    }

    public function run(?InputInterface $input = null, ?OutputInterface $output = null): int
    {
        $output = $output ?: new ConsoleOutput();

        if (! $this->isDevMode) {
            throw new RuntimeException('Cannot run dev console when app is not in development mode');
        }

        return (int) $this->consoleApp->run($input, $output);
    }

    protected function addDoctrineCommands(
        DependencyFactory $dependencyFactory,
        ?EntityManagerInterface $entityManager
    ): void {
        $helperSet = new HelperSet();
        $helperSet->set(new QuestionHelper(), 'question');

        if ($entityManager) {
            $helperSet->set(new EntityManagerHelper($entityManager), 'em');
        }

        $commands = [
            new ConsoleCommand\DumpSchemaCommand($dependencyFactory),
            new ConsoleCommand\ExecuteCommand($dependencyFactory),
            new ConsoleCommand\GenerateCommand($dependencyFactory),
            new ConsoleCommand\LatestCommand($dependencyFactory),
            new ConsoleCommand\DiffCommand($dependencyFactory),
            new ConsoleCommand\ListCommand($dependencyFactory),
            new ConsoleCommand\MigrateCommand($dependencyFactory),
            new ConsoleCommand\RollupCommand($dependencyFactory),
            new ConsoleCommand\StatusCommand($dependencyFactory),
            new ConsoleCommand\SyncMetadataCommand($dependencyFactory),
            new ConsoleCommand\VersionCommand($dependencyFactory)
        ];

        $this->consoleApp->addCommands($commands);
    }
}
