<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Console\Registry;

use Saluki\Container\Contract\SalukiRegistryInterface;
use Symfony\Component\Console\Command\Command;

class ConsoleCommandRegistry implements \IteratorAggregate, SalukiRegistryInterface
{
    /**
     * @var array|Command[]
     */
    private $items = [];

    /**
     * @param Command $command
     */
    public function add(Command $command)
    {
        $this->items[] = $command;
    }

    /**
     * @return \ArrayIterator|Command[]
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    /**
     * Add an item to the registry
     *
     * @param $item
     * @return mixed
     */
    public function addItem($item): void
    {
        $this->add($item);
    }
}
