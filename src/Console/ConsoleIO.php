<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class ConsoleIO
 */
class ConsoleIO extends SymfonyStyle
{
    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * ConsoleIO constructor.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function __construct(InputInterface $input, OutputInterface $output)
    {
        parent::__construct($input, $output);
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * @return InputInterface
     */
    public function getInput(): InputInterface
    {
        return $this->input;
    }

    /**
     * Returns TRUE if input is piped
     *
     * @return bool
     */
    public function inputIsPiped(): bool
    {
        return (function_exists('posix_isatty')) ? (! posix_isatty(STDIN)) : false;
    }

    /**
     * @param $messages
     * @param int $type
     */
    public function writeTTYLn($messages, $type = self::OUTPUT_NORMAL)
    {
        if (!$this->outputIsPiped()) {
            $this->writeln($messages, $type);
        }
    }

    /**
     * @param $messages
     * @param int $type
     */
    public function writeTTY($messages, $type = self::OUTPUT_NORMAL)
    {
        if (!$this->outputIsPiped()) {
            $this->write($messages, $type);
        }
    }

    /**
     * @param $messages
     * @param int $type
     */
    public function writePipedLn($messages, $type = self::OUTPUT_NORMAL)
    {
        if ($this->outputIsPiped()) {
            $this->writeln($messages, $type);
        }
    }

    /**
     * @param $messages
     * @param int $type
     */
    public function writePiped($messages, $type = self::OUTPUT_NORMAL)
    {
        if ($this->outputIsPiped()) {
            $this->write($messages, $type);
        }
    }

    /**
     * Is the output piped in a POSIX environment?
     *
     * @return bool
     */
    public function outputIsPiped()
    {
        // LOL.. Windows.
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return false;
        }

        return ($this->output instanceof ConsoleOutputInterface)
            ? (! posix_isatty(STDOUT))
            : false;
    }

    /**
     * Read from piped "stdin" input
     *
     * @param bool $blocking
     * @return string
     */
    public function readFromStdIn(bool $blocking = false)
    {
        $stream = fopen("php://stdin", 'r');

        if (! $blocking) {
            stream_set_blocking($stream, 0);
        }

        $data = '';
        while ($content = fread($stream, 8192)) {
            $data .= $content;
        }
        fclose($stream);

        return $data;
    }

    /**
     * Is interactive?
     *
     * Returns FALSE if the output is piped or if the '-n' option is specified in the input
     *
     * @return bool
     */
    public function isInteractive()
    {
        return (!$this->outputIsPiped() && $this->input->isInteractive());
    }

    /**
     * @param string $optionName
     * @return string|array|string[]|null
     */
    public function getOption($optionName)
    {
        return $this->input->getOption($optionName);
    }

    /**
     * @param $optionName
     * @return bool
     */
    public function hasOption($optionName): bool
    {
        return $this->input->hasOption($optionName);
    }

    /**
     * @param string $argumentName
     * @return string|array|string[]|null
     */
    public function getArgument($argumentName)
    {
        return $this->input->getArgument($argumentName);
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->input->getArguments();
    }

    /**
     * @param string|null $color
     * @return string
     */
    public function star($color = null)
    {
        return $color ? sprintf("<fg=%s>*</fg=%s>", $color, $color) : '*';
    }

    /**
     * Get a GREEN CHECK
     *
     * @return string
     */
    public function check()
    {
        return "<fg=green>✓</fg=green>";
    }

    /**
     * Get a RED X
     *
     * @return string
     */
    public function x()
    {
        return "<fg=red>✗</fg=red>";
    }

    /**
     * Formats a command title.
     *
     * @param string $message
     */
    public function title($message)
    {
        $this->writeln($message);
        $this->writeln(str_pad('', strlen($message), '-'));
        $this->writeln('');
    }

    /**
     * Formats a section title.
     *
     * @param string $message
     */
    public function section($message)
    {
        $this->writeln('##' . $message);
    }

    /**
     * Formats a success result bar.
     *
     * @param string|array $message
     */
    public function success($message)
    {
        $this->writeln($this->check() . ' ' . trim($message));
    }

    /**
     * Formats an error result bar.
     *
     * @param string|array $message
     */
    public function fail($message)
    {
        $this->writeln($this->x() . ' ' . $message);
    }


    /**
     * @param string $message
     * @param int $exitCode
     * @return int
     */
    public function abort(string $message = '', int $exitCode = 1): int
    {
        if ($message) {
            $exitCode === 0 ? $this->writeln($message) : $this->error($message);
        }

        return $exitCode;
    }

    /**
     * Formats an warning result bar.
     *
     * @param string|array $message
     */
    public function warning($message)
    {
        $this->writeln('<fg=yellow>!</fg=yellow> ' . $message);
    }

    /**
     * Formats a note admonition.
     *
     * @param string|array $message
     */
    public function note($message)
    {
        $this->writeln('. ' . $message);
    }

    /**
     * Formats a caution admonition.
     *
     * @param string|array $message
     */
    public function caution($message)
    {
        $this->warning($message);
    }

    /**
     * Exit with error
     *
     * @param string $message
     * @param int $exitCode
     * @throws \Exception
     */
    public function exitWithUserError(string $message, int $exitCode = 4)
    {
        if ($exitCode == 0) {
            throw new \Exception("Error exit code cannot be 0");
        }

        $this->error($message);
        exit($exitCode);
    }

    /**
     * @param array $values Key/values
     * @return string
     */
    public function getDottedList(array $values): string
    {
        $dotLen = max(array_map('strlen', array_keys($values))) + 3;

        $output = [];
        foreach ($values as $k => $val) {
            $output[] = sprintf(
                "%s<info>%s</info>",
                str_pad($k, $dotLen, '.', STR_PAD_RIGHT),
                (string) $val
            );
        }

        return implode(PHP_EOL, $output);
    }

    /**
     * @param string $optionName
     * @param string $promptMessage
     * @param callable|null $validator
     * @param bool $hidden
     * @return mixed
     */
    public function getOptionOrPrompt(
        string $optionName,
        string $promptMessage = '',
        callable $validator = null,
        bool $hidden = false
    ) {

        if ($val = $this->input->getOption($optionName)) {
            if ($validator) {
                call_user_func($validator, $val);
            }

            return $val;
        } else {
            $msg = $promptMessage ?: sprintf('specify <info>%s</info>: ', $optionName);

            return $hidden
                ? $this->askHidden($msg, $validator)
                : $this->ask($msg, null, $validator);
        }
    }

    /**
     * Persist in getting the required non-empty value from the user until it is correct
     *
     * @param string $optionName
     * @param string $promptMessage
     * @param callable|null $validator
     * @param bool $hidden
     * @return mixed
     */
    public function requireOptionOrPrompt(
        string $optionName,
        string $promptMessage = '',
        callable $validator = null,
        bool $hidden = false
    ) {
        if (! $this->input->isInteractive() && ! $this->getOption($optionName)) {
            throw new \RuntimeException("Missing required option: "  . $optionName);
        }

        $result = null;

        while (! $result) {
            try {
                $result = $this->getOptionOrPrompt($optionName, $promptMessage, $validator, $hidden);
            } catch (\RuntimeException $e) {
                // Try again.
                $result = null;
            }
        }

        return $result;
    }

    /**
     * @param string $question
     * @param bool $yesByDefault
     * @return bool
     */
    public function askYesOrNo(string $question, $yesByDefault = true): bool
    {
        $response = $this->ask($question . ' [y/n]', $yesByDefault ? 'y' : 'n');
        return strtolower(substr($response, 0, 1)) == 'y';
    }
}
