<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Console;

use Saluki\ErrorReporter\Service\ErrorService;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ConsoleAppBuilder
{
    private ErrorService $reporter;
    private EventDispatcherInterface $dispatcher;

    /**
     * ConsoleAppBuilder constructor.
     *
     * @param ErrorService $reporter
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(ErrorService $reporter, EventDispatcherInterface $dispatcher = null)
    {
        $this->reporter = $reporter;
        $this->dispatcher = $dispatcher ?: new EventDispatcher();
    }

    /**
     * Build a console application
     *
     * @param string $appName
     * @param string $appVersion
     * @return Application
     */
    public function build(string $appName = '', string $appVersion = ''): Application
    {
        $app = new Application($appName, $appVersion);

        // Yes; catch exceptions and setup dispatcher
        $app->setCatchExceptions(true);
        $app->setDispatcher($this->dispatcher);

        // Report errors
        $this->dispatcher->addListener(ConsoleEvents::ERROR, function (ConsoleErrorEvent $event) {
            $this->reporter->report($event->getError());
        });

        return $app;
    }
}
