<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Container\Service;

use Aura\Router\Map;
use DI\ContainerBuilder;
use Psr\Container\ContainerInterface as Container;
use Saluki\Config\Factory\ConfigFactory;
use Saluki\Config\Model\Config;
use Saluki\Container\Contract\SalukiRegistryInterface;
use Saluki\Container\Model\SalukiParams;
use Saluki\WebInterface\Model\RouteGroup;

/**
 * Class SalukiBuilder
 */
class SalukiContainerBuilder
{
    /**
     * @var callable|null
     */
    private $routesCallback;

    /**
     * SalukiBuilder constructor.
     * @param callable|null $routesCallback
     */
    public function __construct(?callable $routesCallback = null)
    {
        $this->routesCallback = $routesCallback ?: function () {
            // Default; do nothing.
        };
    }

    /**
     * Setup the container builder
     *
     * @param SalukiParams $params
     * @param Config $config
     * @return ContainerBuilder
     */
    public function setupContainerBuilder(SalukiParams $params, Config $config): ContainerBuilder
    {
        // Create a container builder
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->useAutowiring(true);
        $containerBuilder->useAnnotations(false);

        // Add built-in definitions (will be overridden below if they are defined by the user)
        // Add built-in definitions (will be overridden below if they are defined by the user)
        $containerBuilder->addDefinitions([
            SalukiParams::class    => $params,
            Config::class          => $config,
        ]);

        // Also add extra config definition if using custom config class
        if ($params->getConfigClass() !== Config::class) {
            $containerBuilder->addDefinitions([$params->getConfigClass() => $config]);
        }

        return $containerBuilder;
    }

    /**
     * @param SalukiParams $params
     * @return Container
     * @throws \Exception
     */
    public function build(SalukiParams $params): Container
    {
        $config = $this->buildConfig($params);
        $containerBuilder = $this->setupContainerBuilder($params, $config);

        // Compilation?
        if ($params->isCompilationEnabled() && ! $config->isDevMode()) {
            $containerBuilder->enableCompilation($params->getCompilationPath());
        }

        // Add definitions from params
        $containerBuilder->addDefinitions($params->getServiceDefinitions());

        // Apply decorators
        $this->applyDecorators($params, $containerBuilder);

        // Build the thing
        $container = $containerBuilder->build();

        // Cycle through build callbacks
        foreach ($params->getBuildCallbacks() as $callback) {
            call_user_func($callback, $container);
        }

        // Register web routes
        $map = $container->get(Map::class);
        $routes = $container->get(RouteGroup::class);
        call_user_func($this->routesCallback, $container->get(RouteGroup::class), $container);
        foreach ($routes as $route) {
            $map->addRoute($route);
        }

        return $container;
    }


    /**
     * @param SalukiParams $params
     * @return Config
     */
    public function buildConfig(SalukiParams $params): Config
    {
        $factory = new ConfigFactory(
            $params->getConfigLoaders(),
            $params->getConfigDefaults(),
            $params->getConfigClass()
        );

        return $factory->__invoke();
    }


    /**
     * Apply decorators
     *
     * @param SalukiParams $params
     * @param ContainerBuilder $containerBuilder
     */
    private function applyDecorators(SalukiParams $params, ContainerBuilder $containerBuilder): void
    {
        // Add registry decorators
        // TODO: I thought of a way to make this more efficient. Create a map
        // TODO: of ['interface_name' => ['registry', 'registry', etc...]]
        // TODO: Then just iterate over the service definitions one time and map any that are in the interface name
        // TODO: map to the appropriate registry.
        foreach ($params->getRegistries() as $serviceName => $decoratesInterface) {
            $containerBuilder->addDefinitions([
                $serviceName => $this->decorateRegistry(
                    array_keys($params->getServiceDefinitions()),
                    $decoratesInterface
                )
            ]);
        }

        // Add decorators from providers
        foreach ($params->getDecorators() as $serviceName => $decorator) {
            $containerBuilder->addDefinitions([$serviceName => \DI\decorate($decorator)]);
        }

        /*// Web Routes decorator
        if ($params->serviceIsDefined(Matcher::class)) {
            $containerBuilder->addDefinitions([
                Matcher::class => \DI\decorate(function (Matcher $matcher, Container $c) {

                    $map = $c->get(Map::class);
                    $routes = $c->get(RouteGroup::class);

                    if ($this->routesCallback) {
                        call_user_func($this->routesCallback, $routes, $c);
                    }

                    foreach ($routes as $route) {
                        $map->addRoute($route);
                    }

                    return $matcher;
                })
            ]);
        }*/
    }

    /**
     * Helper method to decorate a registry
     *
     * NOTE: This relies on the assumption that service names are class/interface names
     *
     * @param iterable $items
     * @param null|string $interface
     * @return \DI\Definition\Helper\FactoryDefinitionHelper
     */
    private function decorateRegistry(iterable $items, ?string $interface = null)
    {
        return \DI\decorate(function (SalukiRegistryInterface $registry, Container $c) use ($items, $interface) {
            foreach ($items as $item) {
                // Don't register self in-case the registry happens to also implement the interface
                if (is_a($item, get_class($registry), true)) {
                    continue;
                }
                // Register all other instances
                if ((! $interface) or is_a($item, $interface, true)) {
                    $registry->addItem((is_string($item)) ? $c->get($item) : $item);
                }
            }

            return $registry;
        });
    }
}
