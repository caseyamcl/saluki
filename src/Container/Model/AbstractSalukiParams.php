<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Container\Model;

use Configula\ConfigLoaderInterface;
use Saluki\Config\Model\Config;
use Saluki\Container\Contract\ServiceDefinitionListInterface;

/**
 * Class SalukiParams
 * @package Saluki
 */
abstract class AbstractSalukiParams
{
    const AUTO = null;

    /**
     * @var array|ConfigLoaderInterface[]
     */
    protected $configLoaders = [];

    /**
     * @var ServiceDefinitionListInterface|mixed[]
     */
    protected $serviceDefinitionList;

    /**
     * @var array|mixed[]
     */
    protected $configDefaults = [];

    /**
     * @var string
     */
    protected $migrationsDirectory = '';

    /**
     * @var string
     */
    protected $migrationsNamespace = '';

    /**
     * @var \ArrayObject|string[]
     */
    protected $entityDirectories = [];

    /**
     * @var \ArrayObject|string[]  Email template directories
     */
    protected $emailTemplateDirectories = [];

    /**
     * @var string
     */
    protected $configClass = Config::class;

    /**
     * @var string|null
     */
    protected $compilationPath = null;

    /**
     * @var array|string[]  Keys are registry classname, values are interface/classes that should be registered
     */
    protected $registries = [];

    /**
     * @var array|callable[]  Keys are classname/service being decorated; values are
     */
    protected $decorators = [];

    /**
     * @var array|array[]  Array of arrays of callables, prioritized appropriately.
     */
    protected $buildCallbacks = [];

    /**
     * @var array|callable[]
     */
    protected $afterBootCallbacks = [];

    /**
     * @var array|string[]  Names of SetupStep classes
     */
    protected $setupSteps = [];

    /**
     * @return array|ConfigLoaderInterface[]
     */
    public function getConfigLoaders()
    {
        return $this->configLoaders;
    }

    /**
     * @return array|mixed[]
     */
    public function getConfigDefaults()
    {
        return $this->configDefaults;
    }

    /**
     * @return string
     */
    public function getConfigClass(): string
    {
        return $this->configClass;
    }

    /**
     * @return string
     */
    public function getMigrationsDirectory(): string
    {
        return $this->migrationsDirectory;
    }

    /**
     * @return string
     */
    public function getMigrationsNamespace(): string
    {
        return $this->migrationsNamespace;
    }

    /**
     * @return array|string[]
     */
    public function getEntityDirectories(): array
    {
        return $this->entityDirectories->getArrayCopy();
    }

    /**
     * @return null|string
     */
    public function getCompilationPath(): ?string
    {
        return $this->compilationPath;
    }

    /**
     * @return bool
     */
    public function isCompilationEnabled(): bool
    {
        return (bool) $this->compilationPath;
    }

    /**
     * @return array
     */
    public function getServiceDefinitions(): array
    {
        return $this->serviceDefinitionList->getServiceDefinitions();
    }

    /**
     * @return array|callable  Keys are service/class name, value is callable.
     */
    public function getDecorators()
    {
        return $this->decorators;
    }

    /**
     * @return array|string[]
     */
    public function getRegistries(): array
    {
        return $this->registries;
    }

    /**
     * @return iterable|callable[]
     */
    public function getBuildCallbacks(): iterable
    {
        foreach ($this->buildCallbacks as $priority => $callbacks) {
            foreach ($callbacks as $callback) {
                yield $callback;
            }
        }
    }

    /**
     * @return iterable|callable[]
     */
    public function getAfterBootCallbacks(): iterable
    {
        foreach ($this->afterBootCallbacks as $priority => $callbacks) {
            foreach ($callbacks as $callback) {
                yield $callback;
            }
        }
    }

    /**
     * @return \ArrayObject|string[]
     */
    public function getEmailTemplateDirectories(): \ArrayObject
    {
        return $this->emailTemplateDirectories;
    }

    /**
     * @return array|string[]
     */
    public function getSetupSteps()
    {
        return $this->setupSteps;
    }
}
