<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *   For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *   ------------------------------------------------------------------
 */

namespace Saluki\Container\Model;

/**
 * Class SalukiParams
 * @package Saluki\Container
 */
class SalukiParams extends AbstractSalukiParams
{
    private SalukiBuilder $builder;

    /**
     * SalukiParams constructor.
     * @param SalukiBuilder $builder
     */
    public function __construct(SalukiBuilder $builder)
    {
        foreach ($builder->toArray() as $propName => $propValue) {
            $this->$propName = $propValue;
        }
        $this->builder = $builder;
    }

    /**
     * Returns TRUE if service is EXPLICITLY defined
     *
     * Will return FALSE for services that are not explicitly added to the container builder (but are
     * autowired by PHP-DI), since this is called before container compilation.
     *
     * @param string $serviceName
     * @return bool
     */
    public function serviceIsDefined(string $serviceName): bool
    {
        return $this->serviceDefinitionList->hasServiceDefinition($serviceName);
    }
}
