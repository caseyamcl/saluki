<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *   For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *   ------------------------------------------------------------------
 */

namespace Saluki\Container\Model;

use DI\Definition\Helper\DefinitionHelper;
use DI\Definition\Reference;
use Saluki\Container\Contract\ServiceDefinitionListInterface;

/**
 * Class SalukiServiceDefinitionList
 * @package Saluki\Container
 */
class SalukiServiceDefinitionList implements \IteratorAggregate, ServiceDefinitionListInterface
{
    /**
     * @var \ArrayObject
     */
    protected $serviceDefinitions = [];

    /**
     * SalukiServiceDefinitionList constructor.
     * @param iterable $serviceDefinitions
     */
    public function __construct(iterable $serviceDefinitions = [])
    {
        $this->serviceDefinitions = new \ArrayObject($serviceDefinitions);
    }

    /**
     * Manually set a service definition
     *
     * This will override any built-in service definitions
     *
     * @param string $serviceName
     * @param DefinitionHelper|Reference|null $serviceDefinition
     */
    public function addServiceDefinition(string $serviceName, $serviceDefinition = null): void
    {
        $this->serviceDefinitions[$serviceName] = $serviceDefinition ?: \DI\autowire($serviceName);
    }

    /**
     * @param array $definitions
     */
    public function addServiceDefinitions(array $definitions): void
    {
        foreach ($definitions as $name => $definition) {
            $this->addServiceDefinition($name, $definition);
        }
    }

    /**
     * Returns TRUE if there is an explicit entry for the specified service definition
     *
     * NOTE: If services are auto-wired during container build, but not explicitely defined here, this
     * will return FALSE.
     *
     * @param string $definition
     * @return bool
     */
    public function hasServiceDefinition(string $definition): bool
    {
        return array_key_exists($definition, $this->serviceDefinitions->getArrayCopy());
    }

    /**
     * @return array
     */
    public function getServiceDefinitions(): array
    {
        return $this->serviceDefinitions->getArrayCopy();
    }

    /**
     * Retrieve an external iterator
     */
    public function getIterator()
    {
        return $this->serviceDefinitions;
    }
}
