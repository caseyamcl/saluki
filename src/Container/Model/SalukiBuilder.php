<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Container\Model;

use ArrayObject;
use CallbackFilterIterator;
use Configula\ConfigLoaderInterface;
use Configula\Loader\YamlFileLoader;

use function DI\autowire;
use function DI\create;

use DI\Definition\Helper\DefinitionHelper;
use DI\Definition\Reference;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use Saluki\Authentication\Model\AbstractPermission;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Contract\ServiceDefinitionListInterface;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Utility\DirectoryClassFinder;

/**
 * Class SalukiParamsBuilder
 * @package Saluki\Container
 */
class SalukiBuilder extends AbstractSalukiParams implements ServiceDefinitionListInterface
{
    public const LOW_PRIORITY = 1;
    public const NORMAL = 0;
    public const HIGH_PRIORITY = -1;

    private DirectoryClassFinder $directoryClassFinder;

    /**
     * SalukiParams constructor.
     * @param string $sourceDirectory
     */
    public function __construct(string $sourceDirectory)
    {
        $this->entityDirectories = new ArrayObject();
        $this->emailTemplateDirectories = new ArrayObject();

        $this->serviceDefinitionList = new SalukiServiceDefinitionList();
        $this->directoryClassFinder = new DirectoryClassFinder($sourceDirectory);
    }

    /**
     * Set the directory and namespace where database migrations are stored
     *
     * @param string $directory      The real directory name
     * @param string|null $namespace Set to AUTO to try and guess the namespace
     */
    public function setMigrationsLocation(string $directory, ?string $namespace = self::AUTO): void
    {
        $this->migrationsDirectory = $directory;
        $this->migrationsNamespace = $namespace;
    }

    /**
     * Enable container compilation
     *
     * @param string $path Must be writable
     */
    public function enableCompilation(string $path): void
    {
        $this->compilationPath = $path;
    }

    /**
     * @param bool $enabled
     */
    public function setEncryptionEnabled(bool $enabled): void
    {
        $this->encryptionEnabled = $enabled;
    }


    /**
     * Add a configuration loader
     *
     * merged values loaded from this file will override any existing values loaded from earlier loaders
     *
     * @param ConfigLoaderInterface ...$loader
     */
    public function addConfigLoader(ConfigLoaderInterface ...$loader): void
    {
        foreach ($loader as $load) {
            $this->configLoaders[] = $load;
        }
    }

    /**
     * @param string $name
     * @param $value
     */
    public function setConfigDefault(string $name, $value): void
    {
        $this->configDefaults[$name] = $value;
    }

    /**
     * Set configuration defaults (merge into existing)
     *
     * @param array $defaults
     */
    public function setConfigDefaults(array $defaults): void
    {
        $this->configDefaults = array_replace($this->configDefaults, $defaults);
    }


    /**
     * Set the configuration class that should be instantiated when the application boots
     *
     * @param string $class A class that extends Saluki\Config\Config
     */
    public function setConfigClass(string $class)
    {
        $this->configClass = $class;
    }

    /**
     * Add a YAML configuration file to the end of the config loader stack
     *
     * merged values loaded from this file will override any existing values loaded from earlier loaders
     *
     * @param string $file
     * @param bool $required
     */
    public function addYamlConfigFile(string $file, $required = false): void
    {
        $this->addConfigLoader(new YamlFileLoader($file, $required));
    }

    /**
     * @param array $definitions
     */
    public function addServiceDefinitions(array $definitions): void
    {
        $this->serviceDefinitionList->addServiceDefinitions($definitions);
    }

    /**
     * @param string $class
     * @param callable $decorator Callback should accept an instance of the object being decorated and the container
     */
    public function addDecorator(string $class, callable $decorator)
    {
        $this->decorators[$class] = $decorator;
    }

    /**
     * @param SalukiProviderInterface $provider
     */
    public function addProvider(SalukiProviderInterface $provider): void
    {
        $provider->register($this);
    }

    /**
     * Register a directory that contains Doctrine entities
     *
     * @param string ...$directory
     */
    public function addEntityDirectory(string ...$directory): void
    {
        foreach ($directory as $dir) {
            $this->entityDirectories->append($dir);
        }
    }

    /**
     * Add a setup step
     *
     * @param string $setupStep
     * @param null $helper
     */
    public function addSetupStep(string $setupStep, $helper = null): void
    {
        if (! is_a($setupStep, SetupStepInterface::class, true)) {
            throw new InvalidArgumentException(sprintf(
                'Instance of %s expected.  You passed: %s',
                SetupStepInterface::class,
                get_class($setupStep) ?: gettype($setupStep)
            ));
        }

        $this->setupSteps[$setupStep] = $helper;
        $this->addServiceDefinition($setupStep, $helper);
    }

    /**
     * @param string $class
     * @param DefinitionHelper|Reference|null $helper
     */
    public function addServiceDefinition(string $class, $helper = null): void
    {
        $this->serviceDefinitionList->addServiceDefinition($class, $helper);
    }

    /**
     * @param string $definition
     * @return bool
     */
    public function hasServiceDefinition(string $definition): bool
    {
        return $this->serviceDefinitionList->hasServiceDefinition($definition);
    }

    /**
     * @return array
     */
    public function getServiceDefinitions(): array
    {
        return $this->serviceDefinitionList->getServiceDefinitions();
    }

    /**
     * @param string $registryClass
     * @param string|null $itemInterface
     * @param bool $defer  If defer, items will be added to the registry AFTER the container is built, not before
     */
    public function addRegistry($registryClass, string $itemInterface = null, bool $defer = false)
    {
        if (!$this->serviceDefinitionList->hasServiceDefinition($registryClass)) {
            $this->addServiceDefinition($registryClass);
        }

        if ($defer) {
            $this->addBuildCallback(function (ContainerInterface $container) use ($registryClass, $itemInterface) {
                $params = $container->get(SalukiParams::class);
                foreach ($params->getServiceDefinitions() as $serviceName => $service) {
                    if (is_a($serviceName, $itemInterface, true)) {
                        $container->get($registryClass)->add($container->get($serviceName));
                    }
                }
            }, 1);
        } else {
            $this->registries[$registryClass] = $itemInterface;
        }
    }

    /**
     * @param string $directory Directory to scan
     * @param bool $recursive Scan directory recursively?
     * @param null|string $instanceOf Only add classes that are instances of a particular class or interface
     */
    public function addFromDirectory(string $directory, bool $recursive = true, ?string $instanceOf = null)
    {
        $iterator = $this->directoryClassFinder->find($directory, $recursive, $instanceOf);
        $iterator = new CallbackFilterIterator($iterator, function (string $className) {
            return (new ReflectionClass($className))->isInstantiable();
        });

        foreach ($iterator as $className) {
            $this->addServiceDefinitions([$className => autowire($className)]);
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * Add a build callback.
     *
     * Method is function(ContainerInterface $c): void
     *
     * The callables are called directly after the container is built.
     *
     * @param callable $callback The callback can accept an instance of ContainerInterface
     * @param int $priority
     */
    public function addBuildCallback(callable $callback, int $priority = self::NORMAL): void
    {
        $this->buildCallbacks[$priority][] = $callback;
        ksort($this->buildCallbacks);
    }

    /**
     * Add an after Boot callback (accepts container instance)
     *
     * NOTE: These will not be run when the app is being setup (`--setup` invoked)
     *
     * If you need to add a callback after the container is built but available during setup, then
     * use SalukiBuilder::addBuildCallback() method.
     *
     * @param callable $callback
     * @param int $priority
     */
    public function afterBoot(callable $callback, int $priority = self::NORMAL): void
    {
        $this->afterBootCallbacks[$priority][] = $callback;
        ksort($this->afterBootCallbacks);
    }

    /**
     * Add a permission with configured roles
     *
     * @param string $permissionClass
     * @param array $roles
     */
    public function registerPermission(string $permissionClass, array $roles): void
    {
        if (!is_a($permissionClass, AbstractPermission::class, true)) {
            throw new InvalidArgumentException(sprintf(
                '%s::setPermission() requires class to be an instance of %s (use %s::setServiceDefinition())'
                . ' for permissions that do not extend AbstractPermission',
                get_called_class(),
                AbstractPermission::class,
                get_called_class()
            ));
        }

        $this->addServiceDefinition($permissionClass, create($permissionClass)->constructor($roles));
    }


    /**
     * @param string $directory  Full path to directory with templates
     */
    public function addEmailTemplateDirectory(string $directory): void
    {
        if (! is_dir($directory)) {
            throw new InvalidArgumentException('Email template directory is not readable: ' . $directory);
        }
        $this->emailTemplateDirectories[] = $directory;
    }
}
