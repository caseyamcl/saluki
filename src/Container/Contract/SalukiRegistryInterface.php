<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Container\Contract;

/**
 * Interface RegistryInterface
 * @package Saluki\Container\Contract
 */
interface SalukiRegistryInterface
{
    /**
     * Add an item to the registry
     *
     * @param $item
     * @return mixed
     */
    public function addItem($item): void;
}
