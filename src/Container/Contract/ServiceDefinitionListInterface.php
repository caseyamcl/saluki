<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *   For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *   ------------------------------------------------------------------
 */

namespace Saluki\Container\Contract;

use DI\Definition\Helper\DefinitionHelper;
use DI\Definition\Reference;

interface ServiceDefinitionListInterface
{
    /**
     * @param string $serviceName
     * @param DefinitionHelper|Reference|null $serviceDefinition
     */
    public function addServiceDefinition(string $serviceName, $serviceDefinition = null): void;

    /**
     * @param array $definitions
     */
    public function addServiceDefinitions(array $definitions): void;

    /**
     * @param string $definition
     * @return bool
     */
    public function hasServiceDefinition(string $definition): bool;

    /**
     * @return array  Keys are service/class name, values are helpers
     */
    public function getServiceDefinitions(): array;
}
