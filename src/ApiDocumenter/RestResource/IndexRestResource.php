<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ApiDocumenter\RestResource;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Saluki\ApiDocumenter\Model\EndpointDocumentation;
use Saluki\ApiDocumenter\Model\ParameterDocumentation;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Endpoint\IndexEndpoint;
use Saluki\RestResource\Endpoint\RetrieveEndpoint;
use Saluki\RestResource\Field\Field;
use Saluki\RestResource\Field\FieldList;
use Saluki\RestResource\Field\FieldListNormalizer;
use Saluki\RestResource\Model\FieldChangeTracker;
use Saluki\RestResource\Model\IndexListOptions;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\RestResource\QueryFilter\FilterType\CustomQueryFilter;
use Saluki\Serialization\Contract\NormalizedItemInterface;
use Saluki\Serialization\Contract\NormalizerInterface;
use Saluki\Serialization\Model\NormalizerContext;
use Saluki\WebInterface\Model\RouteGroup;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class IndexRestResource
 * @package Saluki\ApiDocumenter\RestResource
 */
class IndexRestResource implements RestResourceInterface, NormalizerInterface
{
    const DISABLED = null;

    /**
     * @var string
     */
    private $name;

    /**
     * @var RouteGroup
     */
    private $routes;

    /**
     * @var string|null
     */
    private $singleName;

    /**
     * @var string
     */
    private $indexRouteName;

    /**
     * @var string|null
     */
    private $singleRouteName;

    /**
     * @var FieldListNormalizer
     */
    private $normalizer;

    /**
     * @var FieldList
     */
    private $fieldList;

    /**
     * IndexResource constructor.
     *
     * @param RouteGroup $routes
     * @param string $indexName
     * @param string|null $singleName
     */
    public function __construct(
        RouteGroup $routes,
        string $indexName = 'index',
        ?string $singleName = self::DISABLED
    ) {
        $this->name = $singleName ?: $indexName;
        $this->indexRouteName = $indexName;
        $this->singleRouteName = $singleName;
        $this->routes = $routes;
        $this->singleName = $singleName;

        $this->fieldList = new FieldList($this->getObjectClass(), 'name');
        $this->normalizer = new FieldListNormalizer($this->fieldList, $this->name, 'name');
    }

    /**
     * Get the route name; this does not necessarily have to match the path name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * If this resource is for a specific class, then return that
     *
     * @return null|string
     */
    public function getObjectClass(): ?string
    {
        return EndpointDocumentation::class;
    }

    public function init()
    {
        $this->fieldList->add(new Field('name', 'string'));
        $this->fieldList->add(new Field('description', 'string'));
        $this->fieldList->add(new Field('route', 'string'));
        $this->fieldList->add(new Field('path', 'string'));
        $this->fieldList->add(new Field('methods', 'string'));

        $queryParams = $this->fieldList->add(new Field('queryParameters', 'hash'));
        $queryParams->readable->getter(function (EndpointDocumentation $documentation) {
            return array_map(function (ParameterDocumentation $doc) {
                return $doc->__toArray();
            }, $documentation->getQueryParameters());
        });

        $bodyParams = $this->fieldList->add(new Field('bodyParameters', 'hash'));
        $bodyParams->readable->getter(function (EndpointDocumentation $documentation) {
            return array_map(function (ParameterDocumentation $doc) {
                return $doc->__toArray();
            }, $documentation->getBodyParameters());
        });
    }

    /**
     * Register endpoints for this resource
     *
     * @param string $path
     * @param RouteGroup $routeGroup
     * @return RouteGroup
     */
    public function registerEndpoints(string $path, RouteGroup $routeGroup): RouteGroup
    {
        $this->init();

        $group = $routeGroup->newGroup($path);
        $group->get('/', new IndexEndpoint($this))->name($this->indexRouteName);

        if ($this->singleRouteName) {
            $group->get('/{name}', new RetrieveEndpoint($this))->name($this->singleRouteName);
        }

        return $group;
    }

    /**
     * @param string $action
     * @return callable  Callback signature is (SalukiRequest $request): bool
     */
    public function getAccessCallback(string $action): callable
    {
        // All valid.
        return function () {
            return true;
        };
    }

    /**
     * @return FieldList
     */
    public function getFieldList(): FieldList
    {
        return $this->fieldList;
    }

    /**
     * Get the ID path segment name; e.g. "id"
     * @return string
     */
    public function getIdName(): string
    {
        return 'name';
    }

    /**
     * List the default field names
     *
     * @return null|array|string[]  Return default field names
     */
    public function listDefaultFields(): ?array
    {
        return null;
    }

    /**
     * List the default include names
     *
     * @return array  Return default include names
     */
    public function listDefaultIncludes(): array
    {
        return []; // no relationships to include
    }

    /**
     * List the default sort in the form that the client should specify
     *
     * @return array  ['fieldName' => 'asc/desc', etc..]
     */
    public function getDefaultSort(): array
    {
        return ['methodAndPath'];
    }

    /**
     * List the fields that are allowed to be used for sorting
     * @return array|string[]
     */
    public function getAllowedSortFields(): array
    {
        return ['path'];
    }

    /**
     * Get the default pagination limit
     *
     * @return int|null  NULL for no pagination by default, positive integer for default limit
     */
    public function getDefaultPaginationLimit(): ?int
    {
        return 100;
    }

    /**
     * Get the maximum pagination limit
     *
     * @return int|null  NULL for no pagination limit
     */
    public function getMaximumPaginationLimit(): ?int
    {
        return 500;
    }

    /**
     * @return array|QueryFilterInterface[]
     */
    public function listIndexQueryFields(): array
    {
        $nullFunc = function () {
        };

        // Not applicable..
        return [
            (new CustomQueryFilter('filter', 'string', $nullFunc))
                ->setRequired(false)
                ->setDescription("Filter based on method and path (use '%' as wildcard)")
        ];
    }

    /**
     * @param EndpointDocumentation $item
     * @return string
     */
    public function getIdFromItem($item): string
    {
        return $item->getName();
    }

    /**
     * Create a new object based on the request
     *
     * @param RestRequestContext $requestContext
     * @return object
     */
    public function create(RestRequestContext $requestContext)
    {
        return null;
    }

    /**
     * Update an object based on the request
     *
     * @param object $item
     * @param RestRequestContext $requestContext
     * @param FieldChangeTracker|null $changeTracker
     * @return object  The updated object
     */
    public function update($item, RestRequestContext $requestContext, ?FieldChangeTracker $changeTracker = null)
    {
        return null;
    }

    /**
     * Delete an item based on the request
     *
     * @param $item
     * @param RestRequestContext $requestContext
     */
    public function delete($item, RestRequestContext $requestContext): void
    {
        // do nothing.
    }

    /**
     * Retrieve an object or NULL if the object was not found
     *
     * @param string $id
     * @param RestRequestContext $requestContext
     * @return null|object
     */
    public function retrieve(string $id, ?RestRequestContext $requestContext)
    {
        $list = $this->getList();
        return $list->containsKey($id) ? $list->get($id) : null;
    }

    /**
     * Retrieve a list of objects based off of the parameters
     *
     * This is used in the index endpoint
     *
     * @param array $params
     * @param RestRequestContext $requestContext
     * @param IndexListOptions $listOptions
     * @return array|object[]
     */
    public function list(array $params, RestRequestContext $requestContext, IndexListOptions $listOptions): array
    {
        $filter = $requestContext->getRequest()->findQueryParam('filter');
        $limit  = $requestContext->getRequest()->findQueryParam('page[limit]') ?: null;
        $offset = $requestContext->getRequest()->findQueryParam('page[offset]') ?: 0;

        return $this->getList($filter, $limit, $offset)->toArray();
    }

    /**
     * Count the total number of records based off a list of the parameters
     *
     * This is used in the index endpoint
     *
     * @param array $params
     * @param RestRequestContext $requestContext
     * @return int
     */
    public function count(array $params, RestRequestContext $requestContext): int
    {
        $filter = $requestContext->getRequest()->findQueryParam('filter');
        return $this->getList($filter)->count();
    }

    /**
     * @param string|null $filter
     * @param int|null $limit
     * @param int|null $offset
     * @return Collection|EndpointDocumentation[]
     */
    private function getList(?string $filter = null, ?int $limit = null, ?int $offset = 0): Collection
    {
        $items = new ArrayCollection();

        $regex = sprintf(
            '/^%s$/i',
            str_replace(['%', '{', '}', "\\", '/'], ['(.+?)?', '\{', '\}', '\\', '\/'], $filter)
        );

        foreach ($this->routes as $routeDoc) {
            $routeDoc = EndpointDocumentation::fromRoute($routeDoc);
            if ((! $filter) or preg_match($regex, $routeDoc->getRoute())) {
                $items->set($routeDoc->getName(), $routeDoc);
            }
        }

        if ($offset) {
            $items = $items->slice($offset);
        }
        if ($limit) {
            $items = $items->slice(0, $limit);
        }

        return $items;
    }

    /**
     * @return string
     */
    public function getTypeName(): string
    {
        return $this->normalizer->getTypeName();
    }

    /**
     * @return string  The type of class that is normalized
     */
    public function normalizesClass(): string
    {
        return $this->normalizer->normalizesClass();
    }

    /**
     * @param mixed $item
     * @param NormalizerContext $context
     * @param array|null $fields
     * @return NormalizedItemInterface
     */
    public function normalizeItem(
        $item,
        NormalizerContext $context,
        ?array $fields = self::USE_DEFAULTS
    ): NormalizedItemInterface {
        return $this->normalizer->normalizeItem($item, $context, $fields);
    }

    /**
     * @return EventDispatcherInterface|null
     */
    public function getEventDispatcher(): ?EventDispatcherInterface
    {
        return null;
    }
}
