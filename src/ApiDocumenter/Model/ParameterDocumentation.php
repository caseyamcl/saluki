<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/27/18
 * Time: 12:43 PM
 */

namespace Saluki\ApiDocumenter\Model;

use Saluki\WebInterface\Contract\RequestParamInterface;

/**
 * Class ParameterDocumentation
 * @package Saluki\ApiDocumenter\Model
 */
class ParameterDocumentation
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var mixed|null
     */
    private $default = null;
    /**
     * @var bool
     */
    private $required;

    // TODO: Add validation rules documentation..

    /**
     * @param RequestParamInterface $param
     * @return ParameterDocumentation
     */
    public static function fromParameter(RequestParamInterface $param)
    {
        return new static(
            $param->getName(),
            $param->getDescription(),
            $param->isRequired(),
            $param->getDefault()
        );
    }

    /**
     * ParameterDocumentation constructor.
     * @param string $name
     * @param string $description
     * @param bool $required
     * @param null $default
     */
    public function __construct(string $name, string $description, bool $required, $default = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->default = $default;
        $this->required = $required;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return mixed|null
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @return array
     */
    public function __toArray(): array
    {
        return get_object_vars($this);
    }
}
