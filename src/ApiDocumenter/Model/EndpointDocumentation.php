<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ApiDocumenter\Model;

use Aura\Router\Route;
use Saluki\WebInterface\Model\EndpointRoute;
use Saluki\WebInterface\RequestParam\ParameterList;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Class EndpointDocumentation
 *
 * @package Saluki\ApiDocumenter\Model
 */
class EndpointDocumentation
{
    private string $name;
    private string $description = '';
    private string $path;
    private string $route;

    /**
     * @var string[]
     */
    private array $methods = [];

    /**
     * @var ParameterDocumentation[]
     */
    private array $queryParameters = [];

    /**
     * @var ParameterDocumentation[]
     */
    private array $bodyParameters = [];

    /**
     * @var string[]
     */
    private array $notes;

    /**
     * @param Route $route
     * @return EndpointDocumentation
     */
    public static function fromRoute(Route $route): self
    {
        $serializeParams = function (ParameterList $paramList) {
            foreach ($paramList as $param) {
                $out[$param->getName()] = ParameterDocumentation::fromParameter($param);
            }
            return $out ?? [];
        };

        if ($route instanceof EndpointRoute) {
            $queryParams = $serializeParams($route->getEndpoint()->listQueryParameters());
            $bodyParams = $serializeParams($route->getEndpoint()->listBodyParameters());
            $description = $route->getEndpoint()->getDescription();
        } else {
            $queryParams = [];
            $bodyParams = [];
            $description = null;
        }

        return new static(
            $route->name ?: (new AsciiSlugger())->slug($route->path),
            $route->path,
            $route->allows,
            $description,
            $queryParams,
            $bodyParams,
            []
        );
    }

    /**
     * EndpointDocumentation constructor.
     * @param string $name
     * @param string $path
     * @param array $methods
     * @param string $description
     * @param iterable|ParameterDocumentation[] $queryParameters
     * @param iterable|ParameterDocumentation[] $bodyParameters
     * @param array|string[] $notes
     */
    public function __construct(
        string $name,
        string $path,
        array $methods,
        string $description = '',
        iterable $queryParameters = [],
        iterable $bodyParameters = [],
        array $notes = []
    ) {
        $this->name = $name;
        $this->path = $path;
        $this->route = implode(',', $methods) . ' ' . $path;
        $this->description = $description;


        foreach ($queryParameters as $param) {
            $this->addQueryParam($param);
        }
        foreach ($bodyParameters as $param) {
            $this->addBodyParam($param);
        }

        $this->methods = array_map(function (string $method) {
            $this->methods[] = $method;
        }, $methods);

        $this->notes = array_map(function (string $note) {
            $this->notes[] = $note;
        }, $notes);
    }

    private function addQueryParam(ParameterDocumentation $paramDoc)
    {
        $this->queryParameters[] = $paramDoc;
    }

    private function addBodyParam(ParameterDocumentation $paramDoc)
    {
        $this->bodyParameters[] = $paramDoc;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return array|string[]
     */
    public function getMethods(): array
    {
        return $this->methods;
    }

    /**
     * @return array|ParameterDocumentation[]
     */
    public function getQueryParameters(): array
    {
        return $this->queryParameters;
    }

    /**
     * @return array|ParameterDocumentation[]
     */
    public function getBodyParameters(): array
    {
        return $this->bodyParameters;
    }

    /**
     * @return array|string[]
     */
    public function getNotes(): array
    {
        return $this->notes;
    }
}
