<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ApiDocumenter;

use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;

/**
 * Class ApiDocumenterProvider
 * @package Saluki\ApiDocumenter
 */
class ApiDocumenterProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addServiceDefinition(RestResource\IndexRestResource::class);
    }
}
