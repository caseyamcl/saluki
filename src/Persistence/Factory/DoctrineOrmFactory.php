<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Factory;

use Doctrine\Common\Proxy\AbstractProxyFactory;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\UnderscoreNamingStrategy;
use Doctrine\ORM\ORMSetup;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Container\ContainerInterface;
use Saluki\Config\Model\Config;
use Saluki\Container\Model\SalukiParams;
use Saluki\Encryption\Contract\EncryptorInterface;
use Saluki\Persistence\DoctrineType\EncryptedStringType;
use Saluki\Persistence\Model\UuidSafeType;
use Saluki\Persistence\Service\ContainerRepositoryFactory;
use Saluki\Persistence\Service\EncryptedFieldSubscriber;
use Warhuhn\Doctrine\DBAL\Types\ChronosDateTimeType;
use Warhuhn\Doctrine\DBAL\Types\ChronosDateType;

/**
 * Class DoctrineOrmFactory
 * @package Saluki\Persistence\Factory
 */
class DoctrineOrmFactory
{
    /**
     * Build Doctrine ORM
     *
     * @param ContainerInterface $container
     * @param Connection $dbConnection
     * @param Config $config
     * @param EncryptorInterface $encryptor
     * @param SalukiParams $params
     * @return EntityManagerInterface
     */
    public function __invoke(
        ContainerInterface $container,
        Connection $dbConnection,
        Config $config,
        CacheItemPoolInterface $cache,
        EncryptorInterface $encryptor,
        SalukiParams $params
    ): EntityManagerInterface {

        $ormConfig = ORMSetup::createAttributeMetadataConfiguration(
            paths: $params->getEntityDirectories(),
            isDevMode: $config->isDevMode(),
            proxyDir: sys_get_temp_dir(),
            cache: $cache
        );

        $ormConfig->setRepositoryFactory(new ContainerRepositoryFactory($container));
        $ormConfig->setAutoGenerateProxyClasses(
            $config->isDevMode()
                ? AbstractProxyFactory::AUTOGENERATE_ALWAYS
                : AbstractProxyFactory::AUTOGENERATE_FILE_NOT_EXISTS
        );

        // Setup caches
        $ormConfig->setQueryCache($cache);
        $ormConfig->setResultCache($cache);

        // Custom types
        Type::addType('uuid', UuidSafeType::class);
        Type::addType('chronos_date', ChronosDateType::class);
        Type::addType('chronos_datetime', ChronosDateTimeType::class);
        Type::addType('encrypted_string', EncryptedStringType::class);

        $ormConfig->setNamingStrategy(new UnderscoreNamingStrategy(CASE_LOWER));

        // Create entity manager
        $em = EntityManager::create($dbConnection, $ormConfig);

        // Add a subscriber for encryption annotation
        //$subscriber = new DoctrineEncryptSubscriber(new AnnotationReader(), $encryptor);
        $subscriber = new EncryptedFieldSubscriber($encryptor);
        $em->getEventManager()->addEventSubscriber($subscriber);

        return $em;
    }
}
