<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Factory;

use Doctrine\DBAL\Connection;
use Doctrine\Migrations\Configuration\Configuration as MigrationsConfiguration;
use Doctrine\Migrations\Configuration\Connection\ExistingConnection;
use Doctrine\Migrations\Configuration\EntityManager\ExistingEntityManager;
use Doctrine\Migrations\Configuration\Migration\ExistingConfiguration;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManagerInterface;
use Saluki\Container\Model\SalukiParams;

/**
 * Class DoctrineMigrationsFactory
 * @package Saluki\Persistence\Factory
 */
class DoctrineMigrationsFactory
{
    public function fromEntityManager(
        EntityManagerInterface $entityManager,
        SalukiParams $params
    ): DependencyFactory {
        return DependencyFactory::fromEntityManager(
            new ExistingConfiguration($this->buildConfiguration($params)),
            new ExistingEntityManager($entityManager)
        );
    }

    /**
     * Build Doctrine Migrations
     */
    public function __invoke(Connection $dbConnection, SalukiParams $params): DependencyFactory
    {
        return DependencyFactory::fromConnection(
            new ExistingConfiguration($this->buildConfiguration($params)),
            new ExistingConnection($dbConnection)
        );
    }

    private function buildConfiguration(SalukiParams $params): MigrationsConfiguration
    {
        $configuration = new MigrationsConfiguration();

        if (! $params->getMigrationsDirectory()) {
            throw new \RuntimeException("No migrations directory set");
        }
        if (! $params->getMigrationsNamespace()) {
            throw new \RuntimeException("No migrations namespace set");
        }

        $configuration->addMigrationsDirectory(
            $params->getMigrationsNamespace(),
            $params->getMigrationsDirectory()
        );

        return $configuration;
    }
}
