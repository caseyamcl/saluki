<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Factory;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Saluki\Config\Model\Config;
use Saluki\Persistence\Configuration\DbConfig;

/**
 * Class DoctrineDbalFactory
 * @package Saluki\Persistence\Factory
 */
class DoctrineDbalFactory
{
    /**
     * Build Database Connection
     *
     * @param Config $config
     * @return Connection
     */
    public function __invoke(Config $config): Connection
    {
        $params = new DbConfig($config->get('db'));

        // DB Configuration
        $dbConfig = new Configuration();

        return DriverManager::getConnection($params->asDoctrineArray(), $dbConfig);
    }
}
