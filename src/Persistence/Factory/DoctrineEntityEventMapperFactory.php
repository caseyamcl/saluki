<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Saluki\Persistence\EventMapper\EntityEventAdapter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DoctrineEntityEventMapperFactory
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @return EntityEventAdapter
     */
    public function __invoke(
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher
    ): EntityEventAdapter {
        $service = new EntityEventAdapter($eventDispatcher);
        $entityManager->getEventManager()->addEventSubscriber($service);
        return $service;
    }
}
