<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class DbDebugLogger
 * @package Saluki\Persistence\Service
 */
class DbDebugLogger
{
    private LoggerInterface $logger;
    private Stopwatch $timer;

    /**
     * DbDebugLogger constructor.
     *
     * @param LoggerInterface $logger
     * @param Stopwatch|null $timer
     */
    public function __construct(LoggerInterface $logger, ?Stopwatch $timer = null)
    {
        $this->logger = $logger;
        $this->timer = $timer ?: new Stopwatch();
    }

    /**
     * Logs a SQL statement somewhere.
     *
     * @param string $sql The SQL to be executed.
     * @param array|null $params The SQL parameters.
     * @param array|null $types The SQL parameter types.
     *
     * @return void
     */
    public function startQuery($sql, array $params = null, array $types = null): void
    {
        $message = sprintf("QUERY START > %s", $sql);

        $this->logger->debug($message, ['params' => $params, 'types' => $types]);
        $this->timer->start('query');
    }

    /**
     * Marks the last started query as stopped. This can be used for timing of queries.
     *
     * @return void
     */
    public function stopQuery(): void
    {
        $this->timer->stop('query');
        $this->logger->debug('QUERY COMPLETE > ' . $this->timer->getEvent('query'));
    }
}
