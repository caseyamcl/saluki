<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Service;

use Doctrine\ORM\EntityManagerInterface;
use Saluki\Events\SalukiEvents;
use Saluki\Persistence\Event\TransactionFailedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class TransactionManager
{
    private EntityManagerInterface $entityManager;
    private EventDispatcherInterface $eventDispatcher;

    /**
     * Transactional constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param callable $action
     * @return mixed
     */
    public function execute(callable $action)
    {
        $this->beginTransaction();
        $result = call_user_func($action, $this->entityManager);
        $this->completeTransaction();
        return $result;
    }

    /**
     * Begin a transaction
     */
    public function beginTransaction()
    {
        $this->entityManager->beginTransaction();
    }

    /**
     * @throws \Throwable
     */
    public function completeTransaction()
    {
        try {
            $this->entityManager->flush();

            if ($this->entityManager->getConnection()->isTransactionActive()) {
                $this->entityManager->getConnection()->commit();
            }

            $this->eventDispatcher->dispatch(
                new GenericEvent(SalukiEvents::TRANSACTION_COMPLETE),
                SalukiEvents::TRANSACTION_COMPLETE
            );
        } catch (\Throwable $e) {
            $this->cancelTransaction();
            $this->eventDispatcher->dispatch(new TransactionFailedEvent($e), SalukiEvents::TRANSACTION_FAILED);
            throw $e;
        }
    }

    /**
     * Cancel a transaction if it is running
     */
    public function cancelTransaction()
    {
        if ($this->entityManager->getConnection()->isTransactionActive()) {
            $this->entityManager->rollback();
        }
    }
}
