<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Service;

use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Repository\RepositoryFactory;
use Psr\Container\ContainerInterface;

/**
 * Class ContainerRepositoryFactory
 * @package NewSci\AudioPipelineServer\Persistence\Service
 */
class ContainerRepositoryFactory implements RepositoryFactory
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Manually instantiated repositories
     *
     * @var EntityRepository[]
     */
    private $repositoryList = [];

    /**
     * ContainerRepositoryFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Gets the repository for an entity class.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager The EntityManager instance.
     * @param string $entityName The name of the entity.
     *
     * @return ObjectRepository
     */
    public function getRepository(EntityManagerInterface $entityManager, $entityName)
    {
        $metadata = $entityManager->getClassMetadata($entityName);
        $repositoryClassName = $metadata->customRepositoryClassName
            ?: $entityManager->getConfiguration()->getDefaultRepositoryClassName();

        if ($repositoryClassName === $entityManager->getConfiguration()->getDefaultRepositoryClassName()) {
            $repositoryHash = $entityManager->getClassMetadata($entityName)->getName()
                . spl_object_hash($entityManager);

            if (isset($this->repositoryList[$repositoryHash])) {
                return $this->repositoryList[$repositoryHash];
            } else {
                $this->repositoryList[$repositoryHash] = new $repositoryClassName($entityManager, $metadata);
                return $this->repositoryList[$repositoryHash];
            }
        } elseif ($this->container->has($repositoryClassName)) {
            return $this->container->get($repositoryClassName);
        } else {
            throw new \LogicException(
                "Cannot resolve repository for entity: %s (hint: you must create a custom repository for each entity)",
                $entityName
            );
        }
    }
}
