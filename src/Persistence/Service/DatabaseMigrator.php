<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Service;

use Doctrine\Migrations\DependencyFactory;
use Doctrine\Migrations\MigratorConfiguration;

/**
 * The database migrator class uses Doctrine Migrations 3.x and upgrades the database to the latest version.
 * This class is used primary for the setup command; all other database migration tasks should be run in 'dev' mode.
 */
class DatabaseMigrator
{
    /**
     * @var DependencyFactory
     */
    private DependencyFactory $dependencyFactory;

    public function __construct(DependencyFactory $dependencyFactory)
    {
        $this->dependencyFactory = $dependencyFactory;
    }

    /**
     * Check if database is up-to-date
     *
     * @return bool
     */
    public function databaseIsUpToDate(): bool
    {
        return count($this->updateDatabase(true)) === 0;
    }

    /**
     * Update the database schema (migrate to latest version)
     *
     * @param bool $dryRun If TRUE, database will not be updated; this will simply report queries that would be run
     * @return array|string[]  Array of all the queries run
     */
    public function updateDatabase(bool $dryRun = false): array
    {
        $this->dependencyFactory->getMetadataStorage()->ensureInitialized();
        $version = $this->dependencyFactory->getVersionAliasResolver()->resolveVersionAlias('latest');
        $plan = $this->dependencyFactory->getMigrationPlanCalculator()->getPlanUntilVersion($version);
        $config = (new MigratorConfiguration())->setDryRun($dryRun);
        $queries = $this->dependencyFactory->getMigrator()->migrate($plan, $config);

        $out = [];
        foreach ($queries as $qSet) {
            $out = array_merge($out, array_map('strval', $qSet));
        }
        return $out;
    }
}
