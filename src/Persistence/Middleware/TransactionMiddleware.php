<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\Persistence\Service\TransactionManager;

class TransactionMiddleware implements MiddlewareInterface
{
    /**
     * @var TransactionManager
     */
    private $transactionManager;

    /**
     * DbTransactionMiddleware constructor.
     * @param TransactionManager $transactionManager
     */
    public function __construct(TransactionManager $transactionManager)
    {
        $this->transactionManager = $transactionManager;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $this->transactionManager->beginTransaction();
        $response = $handler->handle($request);

        // Complete the transaction only if there was no error.
        if ($response->getStatusCode() < 400) {
            $this->transactionManager->completeTransaction();
        } else {
            $this->transactionManager->cancelTransaction();
        }


        return $response;
    }
}
