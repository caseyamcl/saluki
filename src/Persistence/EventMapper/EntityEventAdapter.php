<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Persistence\EventMapper;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Saluki\Persistence\Event\EntityEvent;
use Saluki\Persistence\Event\EntityPropertyEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Doctrine Event <--> Symfony Event adapter
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class EntityEventAdapter implements EventSubscriber
{
    private EventDispatcherInterface $dispatcher;

    // --------------------------------------------------------------
    // Event Names

    /**
     * Event name for create entity
     *
     * @param string $entityClass
     * @return string
     */
    public static function create(string $entityClass): string
    {
        return sprintf('entity.%s.create', $entityClass);
    }

    /**
     * Event name for update entity
     *
     * @param string $entityClass
     * @return string
     */
    public static function update(string $entityClass): string
    {
        return sprintf('entity.%s.update', $entityClass);
    }

    /**
     * Event name for delete entity
     *
     * @param string $entityClass
     * @return string
     */
    public static function delete(string $entityClass): string
    {
        return sprintf('entity.%s.delete', $entityClass);
    }

    /**
     * Event name for entity property
     *
     * @param string $entityClass
     * @param string $property
     * @return string
     */
    public static function propertyChange(string $entityClass, string $property): string
    {
        return sprintf('entity.%s.property.%s', $entityClass, $property);
    }

    // --------------------------------------------------------------

    /**
     * EntityEventMapper constructor.
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->dispatcher->dispatch(new EntityEvent($args->getObject()), static::create(get_class($args->getObject())));
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $this->dispatcher->dispatch(new EntityEvent($args->getObject()), static::delete(get_class($args->getObject())));
    }

    /**
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $this->dispatcher->dispatch(
            new EntityEvent($args->getObject(), $args->getEntityChangeSet()),
            static::update(get_class($args->getObject()))
        );

        foreach ($args->getEntityChangeSet() as $propName => [$origValue, $actualValue]) {
            $this->dispatcher->dispatch(
                new EntityPropertyEvent($args->getObject(), $propName, $origValue, $actualValue),
                static::propertyChange(get_class($args->getObject()), $propName)
            );
        }
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents(): array
    {
        return ['prePersist', 'preRemove', 'preUpdate'];
    }
}
