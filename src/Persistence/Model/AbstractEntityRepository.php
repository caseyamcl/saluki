<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Model;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\Uuid;

/**
 * Class AbstractEntityRepository
 */
class AbstractEntityRepository extends EntityRepository
{
    const ENTITY_CLASS = null;

    /**
     * PersonRepository constructor.
     * @param EntityManagerInterface $em
     * @throws \ReflectionException
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, $em->getClassMetadata($this->getEntityClass()));
    }

    /**
     * Get an entity by its identifier or throw exception
     *
     * @param string|Uuid $identifier
     * @return object
     * @throws EntityNotFoundException
     */
    public function get($identifier)
    {
        if ($result = $this->find($identifier)) {
            return $result;
        } else {
            throw new EntityNotFoundException();
        }
    }

    /**
     * List IDs
     *
     * @param callable|null $qbCallback
     * @return iterable|string[]|int[]|mixed[]
     */
    public function listIds(?callable $qbCallback = null): iterable
    {
        $idFields = $this->getClassMetadata()->getIdentifier();

        $qb = $this->createQueryBuilder('t');
        $qb->select(implode(', ', array_map(function (string $fieldName) {
            return 't.' . $fieldName;
        }, $idFields)));

        if ($qbCallback) {
            call_user_func($qbCallback, $qb);
        }

        return (count($idFields) === 1)
            ? $qb->getQuery()->getScalarResult()
            : $qb->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * Get the count of all entities of this type
     *
     * @param callable|null $qbCallback  Query-builder callback (accepts query builder instance; returns void)
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll(?callable $qbCallback = null): int
    {
        $qb = $this->createQueryBuilder('t');
        $qb->select("COUNT(t)");

        if ($qbCallback) {
            call_user_func($qbCallback, $qb);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * Get the name of the entity class that this repository services
     *
     * Three possible ways:
     * 1. Explicitly define the static::ENTITY_CLASS constant
     * 2. Override getEntityClass() method
     * 3. If the class is in the same namespace as the repository, auto-detect based on short repository class name
     *    e.g. Some\Namespace\PersonRepository would resolve Some\Namespace\Person class name
     *
     * @return string
     */
    public function getEntityClass(): string
    {
        if (static::ENTITY_CLASS) {
            return static::ENTITY_CLASS;
        } else { // Attempt to guess class by using short name and this namespace
            $reflection = new \ReflectionClass(get_called_class());

            $shortName = $reflection->getShortName();
            $namespace = rtrim($reflection->getNamespaceName(), "\\");

            if (substr($shortName, -10) == 'Repository') {
                $className = sprintf(
                    "%s\\%s",
                    $namespace,
                    substr($shortName, 0, strlen($shortName) - strlen('Repository'))
                );

                if (class_exists($className)) {
                    return $className;
                }
            }
        }

        // If made it here, fail..
        throw new \LogicException(sprintf(
            "%s must set ENTITY_CLASS constant or override getEntityClass()",
            get_called_class()
        ));
    }
}
