<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/20/18
 * Time: 3:20 PM
 */

namespace Saluki\Persistence\Model;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Ramsey\Uuid\Doctrine\UuidType;

/**
 * Class UuidSafeType
 * @package Saluki\Persistence\Model
 */
class UuidSafeType extends UuidType
{
    /**
     * @param null|\Ramsey\Uuid\UuidInterface|string $value
     * @param AbstractPlatform $platform
     * @return mixed|null|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        try {
            return parent::convertToDatabaseValue($value, $platform);
        } catch (ConversionException $e) {
            return null;
        }
    }
}
