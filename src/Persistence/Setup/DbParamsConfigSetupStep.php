<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 3:18 PM
 */

namespace Saluki\Persistence\Setup;

use Configula\Exception\ConfigException;
use Saluki\Config\Model\Config;
use Saluki\Console\ConsoleIO;
use Saluki\Persistence\Configuration\DbConfig;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;
use Saluki\Setup\Step\SetupEnvironment;

/**
 * Class DbParamsConfigSetupStep
 * @package Saluki\Persistence\Setup
 */
class DbParamsConfigSetupStep implements SetupStepInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * DbParamsConfigSetupStep constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function isSetup(): bool
    {
        if ($db = $this->config->find('db')) {
            try {
                (new DbConfig($db))->asDoctrineArray();
                return true;
            } catch (ConfigException $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::PRE_CONTAINER;
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        if ($db = $this->config->find('db')) {
            try {
                (new DbConfig($db))->asDoctrineArray();
                return SetupStepResult::success('Database parameters set in config');
            } catch (ConfigException $e) {
                return SetupStepResult::fail(sprintf("Invalid 'db' parameter in config (%s)", $e->getMessage()));
            }
        } else {
            return SetupStepResult::fail("Database parameters ('db') not found in config");
        }
    }

    /**
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [SetupEnvironment::class];
    }
}
