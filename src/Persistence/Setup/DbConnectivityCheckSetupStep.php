<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 3:17 PM
 */

namespace Saluki\Persistence\Setup;

use Doctrine\DBAL\ConnectionException;
use Saluki\Config\Model\Config;
use Saluki\Console\ConsoleIO;
use Saluki\Persistence\Factory\DoctrineDbalFactory;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;

/**
 * Class DatabaseConnectivityCheckSetupPhase
 * @package Saluki\Persistence\Setup
 */
class DbConnectivityCheckSetupStep implements SetupStepInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * DatabaseConnectivityCheckSetupPhase constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::PRE_CONTAINER;
    }

    /**
     * @return iterable|SetupStepInterface[]
     */
    public static function getDependsOn(): array
    {
        return [DbParamsConfigSetupStep::class];
    }

    public function isSetup(): bool
    {
        try {
            $connection = (new DoctrineDbalFactory())->__invoke($this->config);
            $connection->connect();
            return true;
        } catch (ConnectionException $e) {
            return false;
        }
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        return $this->isSetup()
            ? SetupStepResult::success('Database connectivity succeeded')
            : SetupStepResult::fail('Database connectivity failed (check params in config)');
    }
}
