<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Setup;

use Saluki\Config\Model\Config;
use Saluki\Console\ConsoleIO;
use Saluki\Container\Model\SalukiParams;
use Saluki\Persistence\Factory\DoctrineDbalFactory;
use Saluki\Persistence\Factory\DoctrineMigrationsFactory;
use Saluki\Persistence\Service\DatabaseMigrator;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;

/**
 * Class DatabaseUpToDateSetupPhase
 * @package Saluki\Persistence\Setup
 */
class DbUpToDateSetupStep implements SetupStepInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var SalukiParams
     */
    private $params;

    /**
     * DatabaseUpToDateSetupPhase constructor.
     *
     * @param Config $config
     * @param SalukiParams $params
     */
    public function __construct(Config $config, SalukiParams $params)
    {
        $this->config = $config;
        $this->params = $params;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::PRE_CONTAINER;
    }

    /**
     * @return iterable|SetupStepInterface[]
     */
    public static function getDependsOn(): array
    {
        return [DbConnectivityCheckSetupStep::class];
    }

    public function isSetup(): bool
    {
        return $this->buildDatabaseMigrator()->databaseIsUpToDate();
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        // Already up-to-date?
        if ($this->isSetup()) {
            return SetupStepResult::success('Database is up-to-date');
        }

        $doMigrate = $io->askYesOrNo('Update database?');

        if ($doMigrate) {
            $this->buildDatabaseMigrator()->updateDatabase();
            return SetupStepResult::success('Database was updated');
        } else {
            return SetupStepResult::fail('Database is not up-to-date (no pending migrations)');
        }
    }

    /**
     * @return DatabaseMigrator
     */
    private function buildDatabaseMigrator(): DatabaseMigrator
    {
        $dbConnection = (new DoctrineDbalFactory())->__invoke($this->config);
        $migrationConfig = (new DoctrineMigrationsFactory())->__invoke($dbConnection, $this->params);
        return new DatabaseMigrator($migrationConfig);
    }
}
