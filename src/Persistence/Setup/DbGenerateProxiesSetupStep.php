<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 10/19/18
 * Time: 11:32 AM
 */

namespace Saluki\Persistence\Setup;


use Doctrine\ORM\EntityManagerInterface;
use Saluki\Console\ConsoleIO;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;

class DbGenerateProxiesSetupStep implements SetupStepInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * DbGenerateProxiesSetupStep constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::POST_CONTAINER;
    }

    /**
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [DbUpToDateSetupStep::class];
    }

    /**
     * Check if the item that this step sets up has been setup
     *
     * @return bool
     */
    public function isSetup(): bool
    {
        // TODO: See if there is a better way to check this.
        return true;
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        $destPath = $this->entityManager->getConfiguration()->getProxyDir();
        $metadatas = $this->entityManager->getMetadataFactory()->getAllMetadata();
        $this->entityManager->getProxyFactory()->generateProxyClasses($metadatas, $destPath);
        return SetupStepResult::success('Generated database proxy classes');
    }
}
