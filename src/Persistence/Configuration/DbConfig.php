<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Configuration;

use Configula\ConfigValues;
use Doctrine\DBAL\Platforms\MySqlPlatform;

/**
 * Class DbConfig
 * @package Saluki\Persistence\Configuration
 */
class DbConfig extends ConfigValues
{
    public function asDoctrineArray()
    {
        $params = $this->getArrayCopy();

        $params['driver'] = (in_array($params['driver'] ?? 'pdo_sqlite', ['mysql', 'pgsql', 'sqlite']))
            ? 'pdo_' . $this->get('driver')
            : ($params['driver'] ?? 'pdo_sqlite');

        // Merge with defaults
        if (strpos($params['driver'], 'mysql') !== false) {
            $params = array_replace(
                [
                    'charset' => 'utf8mb4',
                    'default_table_options' => [
                        'charset' => 'utf8mb4',
                        'collate' => 'utf8mb4_unicode_ci'
                    ]
                ],
                $params
            );
        } else {
            $params = array_replace(['charset' => 'utf8'], $params);
        }

        return $params;
    }
}
