<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 4:04 PM
 */

namespace Saluki\Persistence\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class EntityEvent
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class EntityEvent extends GenericEvent
{
    /**
     * EntityPropertyEvent constructor.
     * @param object $entity
     * @param array $changeSet  Change-set, if it exists
     */
    public function __construct($entity, array $changeSet = [])
    {
        parent::__construct($entity, $changeSet);
    }

    /**
     * @return object
     */
    public function getEntity()
    {
        return $this->getSubject();
    }

    /**
     * Return the change set (only for updates to existing entities)
     *
     * Keys are property name, values are tuples: [oldValue, newValue]
     * e.g. ["fsuid" => "cam02h", "cam03h"]
     *
     * @return array
     */
    public function getChangeSet(): array
    {
        return $this->getArguments();
    }

    /**
     * @param string $name
     * @return bool
     */
    public function valueWasChanged(string $name): bool
    {
        return array_key_exists($name, $this->getArguments());
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getOldValue(string $name)
    {
        if (! $this->valueWasChanged($name)) {
            throw new \RuntimeException('Value was not changed: ' . $name);
        }
        return $this->getArguments()[$name][0];
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getNewValue(string $name)
    {
        if (! $this->valueWasChanged($name)) {
            throw new \RuntimeException('Value was not changed: ' . $name);
        }
        return $this->getArguments()[$name][1];
    }
}
