<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class TransactionFailedEvent
 * @package Saluki\Persistence\Event
 */
class TransactionFailedEvent extends GenericEvent
{
    public function __construct(\Throwable $error)
    {
        parent::__construct($error);
    }

    /**
     * @return \Throwable
     */
    public function getError(): \Throwable
    {
        return $this->getSubject();
    }
}
