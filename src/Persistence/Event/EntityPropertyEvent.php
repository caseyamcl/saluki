<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 4:04 PM
 */

namespace Saluki\Persistence\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class EntityPropertyEvent
 * @package Saluki\Persistence\Event
 */
class EntityPropertyEvent extends GenericEvent
{
    private object $entity;
    private string $propertyName;

    /**
     * @var mixed
     */
    private $oldValue;

    /**
     * @var mixed
     */
    private $newValue;


    /**
     * EntityPropertyEvent constructor.
     * @param object $entity
     * @param string $propertyName
     * @param mixed $oldValue
     * @param mixed $newValue
     */
    public function __construct(object $entity, string $propertyName, $oldValue, $newValue)
    {
        parent::__construct($entity);
        $this->entity = $entity;
        $this->oldValue = $oldValue;
        $this->newValue = $newValue;
        $this->propertyName = $propertyName;
    }

    public function getEntity(): object
    {
        return $this->entity;
    }

    /**
     * @return mixed
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @return mixed
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @return bool
     */
    public function fieldHasChanged(): bool
    {
        return $this->newValue !== $this->oldValue;
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }
}
