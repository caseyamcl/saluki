<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Persistence;

use Doctrine\DBAL\Connection;
use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Container\Model\SalukiParams;
use Saluki\Persistence\EventMapper\EntityEventAdapter;
use Saluki\Persistence\Factory\DoctrineDbalFactory;
use Saluki\Persistence\Factory\DoctrineEntityEventMapperFactory;
use Saluki\Persistence\Factory\DoctrineMigrationsFactory;
use Saluki\Persistence\Factory\DoctrineOrmFactory;
use Saluki\Persistence\Service\DatabaseMigrator;
use Saluki\Persistence\Service\TransactionManager;
use Saluki\Persistence\Setup\DbConnectivityCheckSetupStep;
use Saluki\Persistence\Setup\DbGenerateProxiesSetupStep;
use Saluki\Persistence\Setup\DbUpToDateSetupStep;
use Saluki\Persistence\Setup\DbParamsConfigSetupStep;

/**
 * Class DoctrinePersistenceProvider
 * @package Saluki\Persistence
 */
class DoctrinePersistenceProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addSetupStep(DbParamsConfigSetupStep::class);
        $builder->addSetupStep(DbConnectivityCheckSetupStep::class);
        $builder->addSetupStep(DbUpToDateSetupStep::class);
        $builder->addSetupStep(DbGenerateProxiesSetupStep::class);

        $builder->addServiceDefinitions([
            Connection::class             => \DI\factory(new DoctrineDbalFactory()),
            EntityManagerInterface::class => \DI\factory(new DoctrineOrmFactory()),
            EntityEventAdapter::class      => \DI\factory(new DoctrineEntityEventMapperFactory()),
            DependencyFactory::class      => \DI\factory(new DoctrineMigrationsFactory()),
            DatabaseMigrator::class       => \DI\autowire(DatabaseMigrator::class),
            TransactionManager::class     => \DI\autowire(TransactionManager::class)
        ]);

        // Invoke the entity event mapper so that it is properly registered
        $builder->addBuildCallback(function (ContainerInterface $container) {
            $container->get(EntityEventAdapter::class);
        });

        // Replace the temporary dependency factory used during setup with one that is aware of entities
        $builder->addDecorator(DependencyFactory::class, function (DependencyFactory $df, ContainerInterface $c) {
            $params = $c->get(SalukiParams::class);
            return (new DoctrineMigrationsFactory())->fromEntityManager(
                $c->get(EntityManagerInterface::class),
                $params
            );
        });
    }
}
