<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki;

use Doctrine\Migrations\Configuration\Configuration;
use Doctrine\Migrations\DependencyFactory;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Saluki\Config\Model\Config as SalukiConfig;
use Saluki\Console\ConsoleAppBuilder;
use Saluki\Console\Runner\ConsoleDevRunner;
use Saluki\Console\Runner\ConsoleRunner;
use Saluki\Console\Registry\ConsoleCommandRegistry;
use Saluki\Container\Service\SalukiContainerBuilder;
use Saluki\Container\Model\SalukiParams;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\ErrorReporter\Registry\ErrorReporterRegistry;
use Saluki\ErrorReporter\Reporter\PsrLoggerErrorReporter;
use Saluki\ErrorReporter\Service\ErrorService;
use Saluki\Setup\Service\SetupService;
use Saluki\Setup\SetupRunner;
use Saluki\Utility\RuntimeEnvironment;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\RouteGroup;
use Saluki\WebInterface\Service\WebRequestHandler;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class Saluki
 * @package Saluki
 */
abstract class SalukiApp
{
    private const AUTO = null;

    /**
     * @var bool|string
     */
    private $basePath;

    /**
     * @var string
     */
    private $sourceDirectory;

    /**
     * @var SalukiContainerBuilder
     */
    private $containerBuilder;

    /**
     * @param string|null $basePath
     */

    public static function main(?string $basePath = self::AUTO)
    {
        $that = new static($basePath);
        (php_sapi_name() == 'cli') ? $that->runConsole() : $that->runWebSapiHandler();
    }

    /**
     * SalukiApp constructor.
     *
     * @param string|null $basePath
     */
    public function __construct(?string $basePath = self::AUTO)
    {
        $reflection = new \ReflectionClass($this);

        $this->sourceDirectory = dirname($reflection->getFileName());

        if (! $basePath) {
            $basePath = (extension_loaded('phar') && \Phar::running())
                ? dirname(\Phar::running(false))
                : realpath($this->sourceDirectory . '/../../');
        }

        $this->basePath = $basePath;
        $this->containerBuilder = new SalukiContainerBuilder([$this, 'routes']);
    }

    /**
     * Configure the application
     *
     * @param SalukiBuilder $builder
     */
    abstract protected function configure(SalukiBuilder $builder): void;

    /**
     * Register routes
     *
     * @param RouteGroup $routes
     * @param ContainerInterface $container
     */
    abstract public function routes(RouteGroup $routes, ContainerInterface $container): void;

    /**
     * Get the human-friendly application name
     *
     * @return string
     */
    abstract public function appName(): string;

    /**
     * @return SalukiParams
     */
    public function buildParams(): SalukiParams
    {
        $paramsBuilder = new SalukiBuilder($this->getSourcePath());
        $paramsBuilder->addServiceDefinition(RuntimeEnvironment::class, new RuntimeEnvironment(
            get_called_class(),
            $this->getBasePath(),
            $this->getSourcePath()
        ));

        $this->configure($paramsBuilder);

        return new SalukiParams($paramsBuilder);
    }

    /**
     * Boot the application
     *
     * Build and compile the container
     *
     * @return ContainerInterface
     * @throws \Exception
     */
    public function boot(): ContainerInterface
    {
        // Start timer
        $timer = new Stopwatch();
        $timer->start('boot');

        $params = $this->buildParams();
        $container = $this->containerBuilder->build($params);

        $timer->stop('boot');
        // Monolog will take over error handling from here on out...
        $logger = $container->get(LoggerInterface::class);
        $logger->debug('Bootstrap complete.  Took: ' . $timer->getEvent('boot'));

        // After build callbacks
        foreach ($params->getAfterBootCallbacks() as $callback) {
            call_user_func($callback, $container);
        }

        unset($timer);
        return $container;
    }

    /**
     * Run the console app
     */
    public function runConsole(): void
    {
        $args = $_SERVER['argv'];

        $modeMap = ['--setup' => 'setup', '--dev'   => 'dev'];

        if (isset($args[1]) && in_array($args[1], array_keys($modeMap))) {
            $mode = $modeMap[$args[1]];
            unset($args[1]);
        } else {
            $mode = 'run';
        }

        $input  = new ArgvInput($args);
        $output = new ConsoleOutput();

        if ($mode == 'setup') {
            $runner = new SetupRunner(new SetupService($this), $this->appName());
        } else {
            $container = $this->boot();
            $consoleApp = $container->get(ConsoleAppBuilder::class)->build($this->appName());

            if ($mode == 'dev') {
                $entityManager = $container->has(EntityManagerInterface::class)
                    ? $container->get(EntityManagerInterface::class)
                    : null;

                $runner = new ConsoleDevRunner(
                    $consoleApp,
                    $container->get(SalukiConfig::class),
                    $container->get(DependencyFactory::class),
                    $entityManager
                );
            } else {
                $runner = new ConsoleRunner($consoleApp, $container->get(ConsoleCommandRegistry::class));
            }
        }

        $runner->run($input, $output);
    }

    /**
     * Run the Web SAPI handler
     *
     * This is run for single web requests through PHP-FPM, Apache mod_php, CGI, or similar
     */
    public function runWebSapiHandler(): void
    {
        try {
            $container = $this->boot();
            // Ensure the PSR logger is in the error reporter stack (it won't be added twice if it is already in there)
            $container->get(ErrorReporterRegistry::class)->add($container->get(PsrLoggerErrorReporter::class));
            $container->get(WebRequestHandler::class)->handleAndEmit();
        } catch (\Throwable $e) {
            // Catch bootstrap errors here (or in the case that errors were not caught in the handler, catch them here)
            if (isset($container)) {
                $container->get(ErrorService::class)->report($e);
            } else {
                error_log($e); // Fallback to just logging the error
            }

            if (! headers_sent() && ! ini_get('display_errors')) {
                http_response_code(500);
                header("Content-type: application/json");

                $response = JsonApiErrorResponse::internalServerError();
                echo json_encode($response->getJsonData(), JSON_PRETTY_PRINT);
            }
        }
    }

    /**
     * Get the base path (with optional sub-path)
     *
     * @param null|string $subPath
     * @return string
     */
    protected function getBasePath(?string $subPath = null): string
    {
        return Path::join($this->basePath, (string) $subPath);
    }

    /**
     * Get the source path (with optional sub-path)
     *
     * @param null|string $subPath
     * @return string
     */
    protected function getSourcePath(?string $subPath = null): string
    {
        return Path::join($this->sourceDirectory, (string) $subPath);
    }

    /**
     * @return SalukiContainerBuilder
     */
    public function getContainerBuilder(): SalukiContainerBuilder
    {
        return $this->containerBuilder;
    }

    /**
     * Register built-in services
     *
     * @param SalukiBuilder $builder
     */
    protected function registerBuiltInServices(SalukiBuilder $builder): void
    {
        $builder->addProvider(new Console\ConsoleAppProvider());
        $builder->addProvider(new Access\AccessProvider());
        $builder->addProvider(new Config\ConfigProvider());
        $builder->addProvider(new Events\EventsProvider());
        $builder->addProvider(new Cache\CacheProvider());
        $builder->addProvider(new Console\ConsoleProvider());
        $builder->addProvider(new Encryption\EncryptorProvider());
        $builder->addProvider(new Logging\LoggingProvider());
        $builder->addProvider(new ErrorReporter\ErrorReporterProvider());
        $builder->addProvider(new RestResource\RestResourceProvider());
        $builder->addProvider(new Settings\SettingsProvider());
        $builder->addProvider(new Authentication\AuthenticationProvider());
        $builder->addProvider(new WebInterface\WebInterfaceProvider());
        $builder->addProvider(new Persistence\DoctrinePersistenceProvider());
        $builder->addProvider(new Emailer\EmailerProvider());
        $builder->addProvider(new Setup\SetupProvider());
        $builder->addProvider(new Serialization\SerializationProvider());
        $builder->addProvider(new HttpServer\HttpServerProvider());
        $builder->addProvider(new ApiDocumenter\ApiDocumenterProvider());
    }
}
