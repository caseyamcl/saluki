<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/18/18
 * Time: 2:15 PM
 */

namespace Saluki\Settings\Endpoint;

use Aura\Router\Route;
use Psr\Http\Message\ResponseInterface;
use Saluki\Access\Service\AccessChecker;
use Saluki\RestResource\AbstractEndpoint\AbstractRestEndpoint;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Settings\Entity\CachedDbSettingRepository;
use Saluki\Settings\Permission\ManageSettingsPermission;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\SalukiRequest;
use Saluki\WebInterface\RequestParam\ParameterList;
use SettingsManager\Contract\SettingRepository;
use SettingsManager\Contract\SettingProvider;
use SettingsManager\Registry\SettingDefinitionRegistry;

class SettingsPutEndpoint extends AbstractRestEndpoint
{
    private SettingDefinitionRegistry $definitionRegistry;
    private SettingRepository $repository;
    private AccessChecker $accessChecker;
    private SettingProvider $settingsProvider;

    /**
     * SettingsPutEndpoint constructor.
     * @param SettingDefinitionRegistry $definitionRegistry
     * @param SettingProvider $settingsProvider
     * @param SettingRepository $repository
     * @param AccessChecker $accessChecker
     */
    public function __construct(
        SettingDefinitionRegistry $definitionRegistry,
        SettingProvider $settingsProvider,
        SettingRepository $repository,
        AccessChecker $accessChecker
    ) {
        $this->definitionRegistry = $definitionRegistry;
        $this->repository = $repository;
        $this->accessChecker = $accessChecker;
        $this->settingsProvider = $settingsProvider;
    }

    /**
     * Does this endpoint operate on a single resource (e.g. retrieve) or multiple resources (e.g. index)
     * @return bool
     */
    protected function isSingle(): bool
    {
        return true;
    }

    public function validateRoute(Route $route): void
    {
        $this->ensureMethod($route->allows, 'PUT');
        $this->ensurePathParameter($route->path, 'name');
    }

    /**
     * @return ParameterList
     */
    public function listBodyParameters(): ParameterList
    {
        $params = parent::listBodyParameters();
        $params->required('value');
        return $params;
    }

    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        $context = new RestRequestContext($request);

        if (! $this->accessChecker->isAllowed(ManageSettingsPermission::class, $request->getUser())) {
            return JsonApiErrorResponse::forbidden();
        }

        if (! $definition = $this->definitionRegistry->get($request->getPathSegment('name'))) {
            return JsonApiErrorResponse::notFound('setting not found');
        }

        $existingValue = $this->settingsProvider->findValueInstance($request->getPathSegment('name'));
        if (! $existingValue->isMutable()) {
            return JsonApiErrorResponse::forbidden('setting is not mutable');
        }

        $newValue = $definition->processValue($request->getBodyParam('value'));
        $this->repository->saveValue($request->getPathSegment('name'), $newValue, true);

        return $this->prepareResponse(
            $context,
            [$this->settingsProvider->findValueInstance($request->getPathSegment('name'))]
        );
    }
}
