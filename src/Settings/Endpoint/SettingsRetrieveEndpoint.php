<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Settings\Endpoint;

use Aura\Router\Route;
use Saluki\Access\Service\AccessChecker;
use Saluki\RestResource\AbstractEndpoint\AbstractRetrieveEndpoint;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Serialization\Contract\NormalizedItemInterface;
use Saluki\Serialization\Contract\NormalizerInterface;
use Saluki\Serialization\Model\NormalizedItem;
use Saluki\Serialization\Model\NormalizerContext;
use Saluki\Settings\Permission\ManageSettingsPermission;
use SettingsManager\Contract\SettingProvider;
use SettingsManager\Model\SettingValue;
use SettingsManager\Registry\SettingDefinitionRegistry;

class SettingsRetrieveEndpoint extends AbstractRetrieveEndpoint implements NormalizerInterface
{
    const RESOURCE_NAME = 'settings';


    private SettingProvider $settingsProvider;
    private AccessChecker $accessChecker;
    private SettingDefinitionRegistry $definitionRegistry;

    /**
     * SettingsRetrieveEndpoint constructor.
     * @param SettingDefinitionRegistry $definitionRegistry
     * @param SettingProvider $settingsProvider
     * @param AccessChecker $checker
     */
    public function __construct(
        SettingDefinitionRegistry $definitionRegistry,
        SettingProvider $settingsProvider,
        AccessChecker $checker
    ) {
        $this->settingsProvider = $settingsProvider;
        $this->accessChecker = $checker;
        $this->definitionRegistry = $definitionRegistry;
    }

    public function validateRoute(Route $route): void
    {
        $this->ensureMethod($route->allows, 'GET');
        $this->ensurePathParameter($route->path, $this->getIdName());

        if (! $route->name) {
            $route->name(static::RESOURCE_NAME);
        }
    }


    /**
     * @param RestRequestContext $requestContext
     * @return object|null
     */
    protected function retrieveRecord(RestRequestContext $requestContext)
    {
        return $this->settingsProvider->findValueInstance($requestContext->getRequest()->getPathSegment('name'));
    }

    /**
     * @param RestRequestContext $requestContext
     * @param SettingValue $record
     * @return bool
     */
    protected function authorizeAccess(RestRequestContext $requestContext, $record): bool
    {
        return (
            (! $this->definitionRegistry->get($record->getName())->isSensitive())
            or $this->accessChecker->isAllowed(ManageSettingsPermission::class, $requestContext->getUser())
        );
    }

    /**
     * @return string  The type of class that is normalized
     */
    public function normalizesClass(): string
    {
        return SettingValue::class;
    }


    /**
     * @return string
     */
    public function getIdName(): string
    {
        return 'name';
    }

    public function getTypeName(): string
    {
        return self::RESOURCE_NAME;
    }


    /**
     * Normalize item
     *
     * @param SettingValue $item
     * @param NormalizerContext $context
     * @param array|null $fields
     * @return NormalizedItemInterface
     */
    public function normalizeItem($item, NormalizerContext $context, ?array $fields = null): NormalizedItemInterface
    {
        $definition = $this->definitionRegistry->get($item->getName());

        $attrs = [
            'name'        => $item->getName(),
            'value'       => $item->getValue(),
            'mutable'     => $item->isMutable(),
            'provider'    => $item->getProviderName(),
            'default'     => $definition->getDefault(),
            'displayName' => $definition->getDisplayName(),
            'sensitive'   => $definition->isSensitive(),
            'notes'       => $definition->getNotes()
        ];

        // Filter on fields if applicable
        if ($fields) {
            $attrs = array_intersect_key($attrs, array_flip($fields));
        }

        return new NormalizedItem(
            static::RESOURCE_NAME,
            $item->getName(),
            $this->getIdName(),
            $attrs
        );
    }

    /**
     * @return array|string[]
     */
    public function listDefaultFields(): ?array
    {
        return null;
    }
}
