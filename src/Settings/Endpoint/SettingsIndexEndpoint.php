<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/18/18
 * Time: 1:49 PM
 */

namespace Saluki\Settings\Endpoint;

use Saluki\Access\Service\AccessChecker;
use Saluki\Authentication\Contract\UserInterface;
use Saluki\RestResource\AbstractEndpoint\AbstractIndexEndpoint;
use Saluki\RestResource\Model\IndexListOptions;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Settings\Permission\ManageSettingsPermission;
use SettingsManager\Contract\SettingProvider;
use SettingsManager\Contract\SettingDefinition;
use SettingsManager\Registry\SettingDefinitionRegistry;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class SettingsIndexEndpoint
 * @package Saluki\Settings\Endpoint
 */
class SettingsIndexEndpoint extends AbstractIndexEndpoint
{
    public const DEFAULT_SORT = ['settingName' => 'asc'];

    private SettingProvider $settingsProvider;
    private SettingDefinitionRegistry $definitions;
    private AccessChecker $accessChecker;
    private PropertyAccessor $propertyAccessor;

    /**
     * SettingsIndexEndpoint constructor.
     * @param SettingProvider $settingsProvider
     * @param SettingDefinitionRegistry $definitions
     * @param AccessChecker $accessChecker
     * @param PropertyAccessor|null $propertyAccessor
     */
    public function __construct(
        SettingProvider $settingsProvider,
        SettingDefinitionRegistry $definitions,
        AccessChecker $accessChecker,
        PropertyAccessor $propertyAccessor = null
    ) {
        $this->settingsProvider = $settingsProvider;
        $this->definitions = $definitions;
        $this->accessChecker = $accessChecker;
        $this->propertyAccessor = $propertyAccessor ?: PropertyAccess::createPropertyAccessor();
    }

    /**
     * All users can access the settings index
     *
     * @param RestRequestContext $requestContext
     * @return bool
     */
    protected function authorizeIndex(RestRequestContext $requestContext): bool
    {
        // Yes to access the endpoint generally, but settings are filtered based on permissions
        return true;
    }

    /**
     * Retrieve item count that would be returned without pagination
     *
     * @param RestRequestContext $context
     * @return int
     */
    protected function retrieveTotalItemCount(RestRequestContext $context): int
    {
        return count($this->retrieveSettingList($context->getRequest()->getUser()));
    }

    /**
     * Retrieve items (apply pagination if applicable)
     *
     * @param RestRequestContext $request
     * @param IndexListOptions $listOptions
     * @return iterable  The records
     */
    protected function retrieveItems(RestRequestContext $request, IndexListOptions $listOptions): iterable
    {

        $settingList = $this->retrieveSettingList($request->getRequest()->getUser());

        // Sort
        foreach ($listOptions->getSort() as $field => $dir) {
            usort($settingList, function (SettingValueInterface $a, SettingValueInterface $b) use ($field, $dir) {
                $val = strcasecmp(
                    $this->propertyAccessor->getValue($a, $field),
                    $this->propertyAccessor->getValue($b, $field)
                );
                return (strtolower($dir) === 'desc') ? abs($val) * -1 : $val;
            });
        }

        // Apply limit and/or offset
        $offset = $listOptions->getOffset();
        if ($listOptions->getLimit() or $offset) {
            return ($offset + $listOptions->getLimit() <= count($settingList))
                ? array_slice($settingList, $offset, $listOptions->getLimit())
                : ($offset < count($settingList) ? array_slice($settingList, $offset) : $settingList);
        }

        return $settingList;
    }

    /**
     * @return array
     */
    public function getAllowedSortFields(): array
    {
        return ['settingName'];
    }

    /**
     * Get default pagination limit
     *
     * @return int|null  NULL means no pagination limit
     */
    public function getDefaultPaginationLimit(): ?int
    {
        return 1000;
    }

    /**
     * Get maximum pagination limit
     *
     * @return int|null
     */
    public function getMaximumPaginationLimit(): ?int
    {
        return null;
    }

    /**
     * Get default sort
     *
     * @return array  Values are ['field' => 'asc/desc']
     */
    public function getDefaultSort(): array
    {
        return self::DEFAULT_SORT;
    }

    /**
     * Get setting list, filtering if the user is not allowed to see sensitive settings
     *
     * @param UserInterface $user
     * @return array
     */
    protected function retrieveSettingList(UserInterface $user): array
    {
        $showSensitive = $this->accessChecker->isAllowed(ManageSettingsPermission::class, $user);

        foreach ($this->settingsProvider->getSettingValues() as $value) {
            if ($showSensitive or !$this->definitions->get($value->getSettingName())->isSensitive()) {
                $out[] = $value;
            }
        }

        return $out ?? [];
    }
}
