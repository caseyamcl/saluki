<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/18/18
 * Time: 2:42 PM
 */

namespace Saluki\Settings\Endpoint;

use Aura\Router\Route;
use Saluki\Access\Service\AccessChecker;
use Saluki\RestResource\AbstractEndpoint\AbstractDeleteEndpoint;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Settings\Entity\Setting;
use Saluki\Settings\Entity\CachedDbSettingRepository;
use Saluki\Settings\Permission\ManageSettingsPermission;
use SettingsManager\Contract\SettingRepository;
use SettingsManager\Contract\SettingProvider;

/**
 * Class SettingsDeleteEndpoint
 * @package Saluki\Settings\Endpoint
 */
class SettingsDeleteEndpoint extends AbstractDeleteEndpoint
{
    /**
     * @var CachedDbSettingRepository
     */
    private SettingRepository $repository;
    private AccessChecker $accessChecker;
    private SettingProvider $settingsProvider;

    /**
     * SettingsPutEndpoint constructor.
     * @param SettingProvider $settingsProvider
     * @param SettingRepository $repository
     * @param AccessChecker $accessChecker
     */
    public function __construct(
        SettingProvider $settingsProvider,
        SettingRepository $repository,
        AccessChecker $accessChecker
    ) {

        $this->repository = $repository;
        $this->accessChecker = $accessChecker;
        $this->settingsProvider = $settingsProvider;
    }

    public function validateRoute(Route $route): void
    {
        parent::validateRoute($route);
        $this->ensurePathParameter($route->path, 'name');
    }

    /**
     * @param RestRequestContext $request
     * @return object|null
     */
    protected function retrieveRecord(RestRequestContext $request)
    {
        return $this->settingsProvider->findValueInstance($request->getRequest()->getPathSegment('name'));
    }

    /**
     * @param RestRequestContext $requestContext
     * @param Setting $record
     * @return bool
     */
    protected function authorizeAccess(RestRequestContext $requestContext, $record): bool
    {
        return $this->accessChecker->isAllowed(ManageSettingsPermission::class, $requestContext->getUser());
    }

    /**
     * @param Setting $record
     * @param RestRequestContext $requestContext
     * @throws \Doctrine\ORM\ORMException
     */
    protected function delete($record, RestRequestContext $requestContext): void
    {
        $this->repository->removeValue($record->getName(), true);
        throw new \Exception("TODO: REFACTOR SettingsEndpoints around JSON-API model");
    }
}
