<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Settings\Factory;

use Saluki\Config\Model\Config;
use SettingsManager\Contract\SettingRepository;
use SettingsManager\Contract\SettingProvider;
use SettingsManager\Provider\ArrayValuesProvider;
use SettingsManager\Provider\CascadingSettingProvider;
use SettingsManager\Provider\DefaultValuesProvider;
use SettingsManager\Provider\SettingRepositoryProvider;
use SettingsManager\Registry\SettingDefinitionRegistry;

/**
 * Class SettingsProviderFactory
 *
 * @package Saluki\Settings\Factory
 */
class SettingsProviderFactory
{
    /**
     * @param Config $config
     * @param SettingDefinitionRegistry $definitionRegistry
     * @param SettingRepository $settingRepository
     * @return SettingProvider
     */
    public function __invoke(
        Config $config,
        SettingDefinitionRegistry $definitionRegistry,
        SettingRepository $settingRepository
    ): SettingProvider {

        return new CascadingSettingProvider([
            new DefaultValuesProvider($definitionRegistry),
            new ArrayValuesProvider(
                $config->getSettings(),
                $definitionRegistry,
                'config',
                'Configuration file'
            ),
            new SettingRepositoryProvider($settingRepository)
        ]);
    }
}
