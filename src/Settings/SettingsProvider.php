<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Settings;

use Saluki\Access\Model\UserRoles;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Settings\Endpoint\SettingsRetrieveEndpoint;
use Saluki\Settings\Entity\CachedDbSettingRepository;
use Saluki\Settings\Factory\SettingsProviderFactory;
use Saluki\Settings\Permission\ManageSettingsPermission;
use Saluki\Settings\Registry\SettingsRegistry;
use SettingsManager\Contract\SettingDefinition;
use SettingsManager\Contract\SettingProvider;
use SettingsManager\Contract\SettingRepository;
use SettingsManager\Registry\SettingDefinitionRegistry;

/**
 * Class SettingsProvider
 * @package Saluki\Settings
 */
class SettingsProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addRegistry(SettingDefinitionRegistry::class, SettingDefinition::class);
        $builder->addEntityDirectory(__DIR__  . '/Entity');
        $builder->registerPermission(ManageSettingsPermission::class, [UserRoles::ADMIN]);

        $builder->addServiceDefinitions([
            SettingDefinitionRegistry::class => \DI\autowire(SettingsRegistry::class),
            SettingProvider::class  => \DI\factory(new SettingsProviderFactory()),
            SettingRepository::class => \DI\autowire(CachedDbSettingRepository::class),
            SettingsRetrieveEndpoint::class => \DI\autowire(SettingsRetrieveEndpoint::class)
        ]);
    }
}
