<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Settings\Registry;

use Saluki\Container\Contract\SalukiRegistryInterface;
use SettingsManager\Contract\SettingDefinition;
use SettingsManager\Registry\SettingDefinitionRegistry as BaseRegistry;

/**
 * Class SettingDefinitionRegistry
 * @package Saluki\Settings
 */
class SettingsRegistry extends BaseRegistry implements SalukiRegistryInterface
{
    /**
     * @param SettingDefinition $item
     * @return void
     */
    public function addItem($item): void
    {
        $this->add($item);
    }
}
