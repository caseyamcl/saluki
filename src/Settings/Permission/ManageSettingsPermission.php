<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/18/18
 * Time: 1:37 PM
 */

namespace Saluki\Settings\Permission;


use Saluki\Authentication\Model\AbstractPermission;

class ManageSettingsPermission extends AbstractPermission
{
    const DESCRIPTION     = "manage settings";
    const IS_CONFIGURABLE = true;
}
