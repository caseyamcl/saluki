<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Settings\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Setting
 */
#[ORM\Entity(repositoryClass: CachedDbSettingRepository::class)]
class Setting
{
    #[ORM\Column(type: 'string', length: 255, unique: true, nullable: false)]
    #[ORM\Id]
    private string $name;

    #[ORM\Column(type: 'json', nullable: false)]
    private array $value;

    /**
     * SettingEntity constructor.
     * @param string $name
     * @param mixed $value
     */
    public function __construct(string $name, $value)
    {
        $this->name = $name;
        $this->setValue($value);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): mixed
    {
        return $this->value[0];
    }

    /**
     * Set the value
     *
     * @param bool|float|int|string|array $value
     */
    public function setValue(mixed $value): void
    {
        if (is_array($value)) {
            array_walk_recursive($value, function ($val) {
                if (! is_scalar($val) && ! is_array($val)) {
                    throw new \InvalidArgumentException(
                        'The value must be a scalar (string, int, etc), or array'
                    );
                }
            });
            $this->value = [$value];
        } elseif (is_scalar($value)) {
            $this->value = [$value];
        } else {
            throw new \InvalidArgumentException(
                'The value must be a scalar (string, int, etc), or array'
            );
        }
    }

    public function __toString(): string
    {
        $value = $this->getValue();
        return is_array($value) ? implode('', $value) : (string) $this->getValue();
    }

    /**
     * @return array
     */
    public function __toArray(): array
    {
        return [
            'name'  => $this->getName(),
            'value' => $this->getValue()
        ];
    }
}
