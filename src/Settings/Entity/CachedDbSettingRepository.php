<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Settings\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\CacheItemPoolInterface;
use Saluki\Persistence\Model\AbstractEntityRepository;
use SettingsManager\Contract\SettingRepository;
use SettingsManager\Exception\SettingValueNotFoundException;

/**
 * Class SettingRepository
 *
 * @method Setting find($id, $lockMode = null, $lockVersion = null)
 * @method Setting findOneBy(array $criteria, array $orderBy = null)
 */
class CachedDbSettingRepository extends AbstractEntityRepository implements SettingRepository
{
    public const AUTO_COMMIT = true;
    public const ENTITY_CLASS = Setting::class;
    private const SETTING_PREFIX = 'setting_';

    private CacheItemPoolInterface $cache;

    /**
     * SettingRepository constructor.
     * @param EntityManagerInterface $em
     * @param CacheItemPoolInterface $cache
     * @throws \ReflectionException
     */
    public function __construct(EntityManagerInterface $em, CacheItemPoolInterface $cache)
    {
        parent::__construct($em);
        $this->cache = $cache;
    }

    /**
     * Find a setting value by its name
     *
     * @param string $settingName
     * @return mixed|null
     */
    public function findValue(string $settingName)
    {
        $item = $this->cache->getItem(static::SETTING_PREFIX . $settingName);
        if ($item->isHit()) {
            $val = $item->get();
        } else {
            $val = ($setting = $this->find($settingName)) ? $setting->getValue() : null;
            $item->set($val);
            $this->cache->save($item);
        }

        return $val;
    }

    /**
     * Return a key/value set of setting names/values
     *
     * @return iterable
     */
    public function listValues(): iterable
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('s')->from(Setting::class, 's')->orderBy('s.name', 'ASC');

        $out = [];

        /** @var Setting $setting */
        foreach ($qb->getQuery()->getResult() as $setting) {
            $out[$setting->getName()] = $setting->getValue();
        }

        return $out;
    }

    /**
     * @param string $name Setting name
     * @param mixed $value Must be a scalar/array
     * @param bool $autoCommit
     * @return Setting
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveValue(string $name, $value, bool $autoCommit = false): Setting
    {
        if ($setting = $this->findOneBy(['name' => $name])) {
            $setting->setValue($value);
        } else {
            $setting = new Setting($name, $value);
        }

        $this->getEntityManager()->persist($setting);

        if ($autoCommit === self::AUTO_COMMIT) {
            $this->getEntityManager()->flush($setting);
        }

        $cacheItem = $this->cache->getItem(static::SETTING_PREFIX . $name);
        $cacheItem->set($value);
        $this->cache->save($cacheItem);

        return $setting;
    }

    /**
     * @param string $name
     * @param bool $autoCommit
     * @return bool  TRUE if found and removed, FALSE if no action taken
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeValue(string $name, bool $autoCommit = false): bool
    {
        $this->cache->deleteItem(static::SETTING_PREFIX . $name);

        if ($setting = $this->findOneBy(['name' => $name])) {
            $this->getEntityManager()->remove($setting);

            if ($autoCommit === self::AUTO_COMMIT) {
                $this->getEntityManager()->flush($setting);
            }

            return true;
        } else {
            return false;
        }
    }

    public function getValue(string $settingName)
    {
        if ($val = $this->findValue($settingName)) {
            return $val;
        } else {
            throw SettingValueNotFoundException::fromName($settingName);
        }
    }
}
