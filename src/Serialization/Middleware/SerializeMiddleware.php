<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Saluki\Serialization\Exception\CouldNotNegotiateSerializerException;
use Saluki\Serialization\Model\SerializableResponse;
use Saluki\Serialization\Service\SerializerService;
use Saluki\WebInterface\Model\JsonApiErrorResponse;

/**
 * Class SerializeMiddleware
 * @package Saluki\Serialization\Middleware
 */
class SerializeMiddleware implements MiddlewareInterface
{
    /**
     * @var SerializerService
     */
    private $serializerService;

    /**
     * SerializeMiddleware constructor.
     * @param SerializerService $serializerService
     */
    public function __construct(SerializerService $serializerService)
    {
        $this->serializerService = $serializerService;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);

        if ($response instanceof SerializableResponse) {
            try {
                $response = $this->serializerService->serialize($response->getSerializableData(), $response);
            } catch (CouldNotNegotiateSerializerException $e) {
                $response = JsonApiErrorResponse::build('could not negotiate serializer', 406);
            }
        }

        return $response;
    }
}
