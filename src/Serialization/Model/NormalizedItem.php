<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 12:47 PM
 */

namespace Saluki\Serialization\Model;

use Saluki\Serialization\Contract\NormalizedItemInterface;
use Saluki\Serialization\Exception\RelationNotFoundException;

/**
 * Class NormalizedItem
 * @package Saluki\Serialization\Model
 */
class NormalizedItem implements NormalizedItemInterface
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var array|mixed[]
     */
    private $attributes = [];

    /**
     * @var array|iterable[]
     */
    private $relatedItems = [];

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $idName;

    /**
     * @var array
     */
    private $meta;

    /**
     * NormalizedItem constructor.
     * @param string $type
     * @param string $id
     * @param string $idName
     * @param iterable|mixed[] $attributes
     * @param iterable|NormalizedRelationship[] $relatedItems
     * @param iterable|mixed[] $meta
     */
    public function __construct(
        string $type,
        string $id,
        string $idName = 'id',
        iterable $attributes = [],
        iterable $relatedItems = [],
        iterable $meta = []
    ) {
        $this->type = $type;
        $this->id = $id;
        $this->idName = $idName;

        $this->addAttributes($attributes);
        $this->addRelationships($relatedItems);
        $this->addMetaItems($meta);
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdName(): string
    {
        return $this->idName;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get ALL of the available attributes that the current user can see
     *
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param string $name
     * @param $value
     */
    public function addAttribute(string $name, $value)
    {
        // Check for invalid field names
        $this->ensureValidFieldName($name);
        $this->attributes[$name] = $value;
        ksort($this->attributes);
    }

    /**
     * Get ALL of the available relationships that the current user can see
     *
     * @return array|iterable[]
     */
    public function getRelationships(): array
    {
        return $this->relatedItems;
    }

    /**
     * @return array
     */
    public function getMetaItems(): array
    {
        return $this->meta;
    }

    /**
     * Add a relationship (single or multiple)
     *
     * @param string $relationshipName
     * @param NormalizedRelationship $relationship
     */
    public function addRelationship(string $relationshipName, NormalizedRelationship $relationship)
    {
        $this->relatedItems[$relationshipName] = $relationship;
        ksort($this->relatedItems);
    }

    /**
     * Add a meta item
     *
     * @param string $metaName
     * @param $item
     */
    public function addMeta(string $metaName, $item)
    {
        $this->ensureValidFieldName($metaName);
        $this->meta[$metaName] = $item;
        ksort($this->meta);
    }

    /**
     * @param string $name
     * @return NormalizedRelationship
     */
    public function getRelationship(string $name): NormalizedRelationship
    {
        if (array_key_exists($name, $this->relatedItems)) {
            return $this->relatedItems[$name];
        } else {
            throw new RelationNotFoundException('Relationship not found in item: ' . $name);
        }
    }


    /**
     * @param string $name
     * @return bool
     */
    public function hasRelationship(string $name): bool
    {
        return array_key_exists($name, $this->relatedItems);
    }

    /**
     * A field name cannot be 'type' or 'id' or be an existing relationship or attribute name
     *
     * @param string $name
     */
    private function ensureValidFieldName(string $name): void
    {
        // This rule is borrowed from JSON-API spec, but it is a good one.
        if (in_array(strtolower($name), ['id', 'type'])) {
            throw new \InvalidArgumentException(sprintf("Cannot used reserved field name: '%s'", $name));
        }
        if (array_key_exists($name, $this->relatedItems) or array_key_exists($name, $this->attributes)) {
            throw new \InvalidArgumentException(sprintf(
                "Field name collision for type %s : %s" .
                ' (note relationships and attributes share a common namespace)',
                $this->getType(),
                $name
            ));
        }
    }

    /**
     * @return array|string[]
     */
    public function listRelationshipNames(): array
    {
        return array_keys($this->relatedItems);
    }

    /**
     * @return array|string[]
     */
    public function listAttributeNames(): array
    {
        return array_keys($this->attributes);
    }

    /**
     * @param iterable $attributes
     */
    public function addAttributes(iterable $attributes): void
    {
        foreach ($attributes as $key => $value) {
            $this->addAttribute($key, $value);
        }
    }

    /**
     * @param iterable|NormalizedRelationship[] $relationships
     */
    public function addRelationships(iterable $relationships): void
    {
        foreach ($relationships as $key => $value) {
            $this->addRelationship($key, $value);
        }
    }

    /**
     * @param iterable $items
     */
    public function addMetaItems(iterable $items): void
    {
        foreach ($items as $key => $value) {
            $this->addMeta($key, $value);
        }
    }
}
