<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Model;

/**
 * Class PaginationInfo
 * @package Saluki\Serialization\Model
 */
class PaginationInfo
{
    const SAME_AS_DISPLAY_COUNT = null;
    const NO_LIMIT_SPECIFIED    = null;

    private int $displayCount;
    private ?int $totalCount;
    private int $offset;
    private ?int $limit;

    /**
     * PaginationInfo constructor.
     *
     * @param int       $displayCount
     * @param int|null  $totalCount
     * @param int       $offset
     * @param int|null  $limit
     */
    public function __construct(
        int $displayCount,
        ?int $totalCount = self::SAME_AS_DISPLAY_COUNT,
        int $offset = 0,
        ?int $limit = self::NO_LIMIT_SPECIFIED
    ) {
        $this->displayCount = $displayCount;
        $this->totalCount   = $totalCount ?: $displayCount;
        $this->offset       = $offset;
        $this->limit        = $limit;

        if ($totalCount && $this->displayCount > $totalCount) {
            throw new \LogicException(sprintf(
                "Pagination display count (%s) cannot be greater than total count (%s)",
                $displayCount,
                $totalCount
            ));
        }
    }

    /**
     * @return int
     */
    public function getDisplayCount()
    {
        return $this->displayCount;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return bool
     */
    public function isAtStart(): bool
    {
        return $this->offset === 0;
    }

    /**
     * @return bool
     */
    public function isAtEnd()
    {
        return ($this->offset + $this->limit >= $this->totalCount or ! $this->isPaginated());
    }

    /**
     * @return bool
     */
    public function isPaginated(): bool
    {
        return $this->totalCount > $this->displayCount;
    }

    /**
     * Get the next offset (NULL if on last page)
     *
     * @return int|null
     */
    public function getNextOffset()
    {
        return ($this->offset + $this->limit >= $this->totalCount)
            ? null
            : $this->offset + $this->limit;
    }

    /**
     * @return int
     */
    public function getPrevOffset(): int
    {
        return max(0, $this->offset - $this->limit);
    }

    /**
     * @return int|null
     */
    public function getLastOffset()
    {
        if (! $this->isPaginated()) {
            return null;
        }

        $numShown = $this->limit ?: $this->displayCount;

        return ($this->limit * floor($this->totalCount / $numShown) == $this->totalCount)
            ? (int) (($numShown * floor($this->totalCount / $numShown)) - $numShown)
            : (int) ($numShown * floor($this->totalCount / $numShown));
    }


    /**
     * Get the first record displayed
     * @return int
     */
    public function getDisplayFirst(): int
    {
        return $this->offset + 1;
    }

    /**
     * Get the last record shown
     * @return int
     */
    public function getDisplayLast(): int
    {
        return $this->getOffset() + $this->getDisplayCount();
    }

    /**
     * Get the number of pages
     *
     * @return int
     */
    public function getNumPages(): int
    {
        if (! $this->limit) {
            return 1;
        } else {
            return ($this->totalCount % $this->limit == 0)
                ? $this->totalCount / $this->limit
                : ($this->totalCount / $this->limit) + 1;
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(get_object_vars($this), [
            'isPaginated'       => $this->isPaginated(),
            'nextOffset'        => $this->getNextOffset(),
            'prevOffset'        => $this->getPrevOffset(),
            'lastOffset'        => $this->getLastOffset(),
            'displayFirstIndex' => $this->getDisplayFirst(),
            'displayLastIndex'  => $this->getDisplayLast(),
            'numPages'          => $this->getNumPages()
        ]);
    }
}
