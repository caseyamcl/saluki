<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Model;

use Saluki\Authentication\Contract\UserInterface;
use Saluki\WebInterface\Model\SalukiRequest;

/**
 * Class SerializationParams
 * @package Saluki\Serialization\Model
 */
class SerializationParams
{
    /**
     * @var PaginationInfo
     */
    private $pagination;

    /**
     * @var SerializationOptions
     */
    private $options;

    /**
     * @var iterable|mixed[]
     */
    private $items = [];

    /**
     * @var SalukiRequest
     */
    private $request;

    /**
     * SerializationParams constructor.
     * @param iterable|mixed[] $items
     * @param SalukiRequest $request
     * @param SerializationOptions $options
     * @param PaginationInfo $pagination
     */
    public function __construct(
        iterable $items,
        SalukiRequest $request,
        SerializationOptions $options,
        ?PaginationInfo $pagination = null
    ) {
        $this->pagination = $pagination ?: new PaginationInfo($this->count());
        $this->options = $options;
        $this->items = $items;
        $this->request = $request;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->request->getUser();
    }

    /**
     * @return SalukiRequest
     */
    public function getRequest(): SalukiRequest
    {
        return $this->request;
    }

    /**
     * @return PaginationInfo
     */
    public function getPagination(): PaginationInfo
    {
        return $this->pagination;
    }

    /**
     * @return SerializationOptions
     */
    public function getOptions(): SerializationOptions
    {
        return $this->options;
    }


    /**
     * @return iterable|mixed[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        if (is_array($this->items) or is_callable([$this->items, 'count'])) { // fast, preferable.
            return count($this->items);
        } else { // slow, expensive..
            $count = 0;
            foreach ($this->items as $item) {
                $count++;
            }
            return $count;
        }
    }
}
