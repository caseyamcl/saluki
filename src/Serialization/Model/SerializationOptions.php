<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Model;

/**
 * Class SerializationOptions
 * @package Saluki\Serialization\Model
 */
class SerializationOptions
{
    const DEFAULTS = null;

    /**
     * @var bool
     */
    private $isSingleRecord;

    /**
     * @var string
     */
    private $paginationOffsetParamName = 'page[offset]';

    /**
     * @var string
     */
    private $paginationLimitParamName = 'page[limit]';

    /**
     * @var array|string[]  Keys are the resource type, values are the fields for that resource type
     */
    private $requestedFields = [];

    /**
     * @var array|string[]  Dot-delimited relationships, relative to the rood document
     */
    private $requestedIncludes = [];

    /**
     * @var array
     */
    private $meta = [];

    /**
     * SerializationOptions constructor.
     *
     * @param bool $isSingleRecord
     * @param array|array[]|null $fields  Keys are resource type names; each sub-array is a list of fields for that type
     * @param array|string[] $includes  Dot-notated includes (e.g. "books" or "books.tags")
     * @param array|mixed[] $meta       Each item is added to the top-level document meta
     * @param string $paginationLimitParamName
     * @param string $paginationOffsetParamName
     */
    public function __construct(
        bool $isSingleRecord,
        ?array $fields = self::DEFAULTS,
        array $includes = [],
        array $meta = [],
        string $paginationLimitParamName = 'page[limit]',
        string $paginationOffsetParamName = 'page[offset]'
    ) {
        $this->isSingleRecord = $isSingleRecord;
        $this->requestedFields = $fields ?: [];
        $this->requestedIncludes = $includes;
        $this->meta = $meta;
        $this->paginationLimitParamName = $paginationLimitParamName;
        $this->paginationOffsetParamName = $paginationOffsetParamName;
    }

    /**
     * @return bool
     */
    public function isSingleRecord(): bool
    {
        return $this->isSingleRecord;
    }

    /**
     * @return string
     */
    public function getPaginationOffsetParamName(): string
    {
        return $this->paginationOffsetParamName;
    }

    /**
     * @return string
     */
    public function getPaginationLimitParamName(): string
    {
        return $this->paginationLimitParamName;
    }

    /**
     * List requested attributes for a given resource type
     *
     * Returns NULL to use defaults for the given resource type
     *
     * @param string $resourceTypeName
     * @return null|array|string[]  List of attributes to display for a given resource
     */
    public function getRequestedFields(string $resourceTypeName): ?array
    {
        return (array_key_exists($resourceTypeName, $this->requestedFields))
            ? $this->requestedFields[$resourceTypeName]
            : self::DEFAULTS;
    }

    /**
     * @return array|string[]  List of relationships to include; transitive relationships are dot-delimited
     */
    public function getRequestedIncludes(): array
    {
        return $this->requestedIncludes;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public function getMetaItem(string $key, $default = null)
    {
        return (array_key_exists($key, $this->meta)) ? $this->meta[$key] : $default;
    }
}
