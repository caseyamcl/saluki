<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Model;

/**
 * Class RelatedItem
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class NormalizedRelationship implements \IteratorAggregate
{
    /**
     * @var iterable|mixed[]
     */
    private $items;

    /**
     * @var bool
     */
    private $isSingle;

    /**
     * @param $value
     * @return NormalizedRelationship
     */
    public static function single($value): NormalizedRelationship
    {
        return new static([$value], true);
    }

    /**
     * @param iterable $values
     * @return NormalizedRelationship
     */
    public static function multiple(iterable $values = []): NormalizedRelationship
    {
        return new static($values, false);
    }

    /**
     * RelatedItem constructor.
     * @param iterable $values
     * @param bool $isSingle
     */
    protected function __construct(iterable $values, bool $isSingle)
    {
        $this->items = $values;
        $this->isSingle = $isSingle;
    }

    /**
     * Get items as array
     *
     * @return iterable|mixed[]
     */
    public function getItems(): iterable
    {
        return $this->items;
    }

    /**
     * Get first/single item (or NULL if empty)
     *
     * @return mixed|null
     */
    public function getSingleItem()
    {
        return current($this->items) ?: false;
    }

    /**
     * Is this a to-one relationship?
     *
     * @return bool
     */
    public function isSingle(): bool
    {
        return $this->isSingle;
    }

    /**
     * Is this a to-many relationship?
     *
     * @return bool
     */
    public function isMultiple(): bool
    {
        return ! $this->isSingle;
    }

    /**
     * @return \Generator
     */
    public function getIterator()
    {
        foreach ($this->items as $idx => $item) {
            yield $idx => $item;
        }
    }
}
