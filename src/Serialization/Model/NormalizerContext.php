<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/27/18
 * Time: 3:15 PM
 */

namespace Saluki\Serialization\Model;

use Saluki\Authentication\Contract\UserInterface;
use Saluki\Serialization\Contract\NormalizedItemInterface;

/**
 * Class NormalizerContext
 *
 * Value object containing options and an array of already-normalized items
 *
 * @package Saluki\Serialization\Model
 */
class NormalizerContext
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var array|NormalizedItemInterface[]
     */
    private $items = [];

    /**
     * @var array|array[]|NormalizedItemInterface[]  Keys are type, values are array of IDs => spl_object_hash()
     */
    private $itemsByTypeAndId = [];

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * NormalizerContext constructor.
     * @param UserInterface $user
     * @param array $options
     */
    public function __construct(UserInterface $user, array $options = [])
    {
        $this->options = $options;
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @param object $object
     * @param NormalizedItemInterface $item
     */
    public function addItem($object, NormalizedItemInterface $item)
    {
        $this->items[spl_object_hash($object)] = $item;
        $this->itemsByTypeAndId[$item->getType()][$item->getId()] = $item;
    }

    /**
     * @param object $object
     * @return null|NormalizedItemInterface
     */
    public function find($object): ?NormalizedItemInterface
    {
        return in_array(spl_object_hash($object), $this->items) ? $this->items[spl_object_hash($object)] : null;
    }

    /**
     * @param string $type
     * @param string $id
     * @return null|NormalizedItemInterface
     */
    public function findByTypeAndId(string $type, string $id): ?NormalizedItemInterface
    {
        return isset($this->itemsByTypeAndId[$type][$id]) ? $this->itemsByTypeAndId[$type][$id] : null;
    }

    /**
     * @param string $type
     * @return array|NormalizedItemInterface[]
     */
    public function findByType(string $type): array
    {
        return $this->itemsByTypeAndId[$type] ?? [];
    }

    /**
     * @return array|array[]  Each sub-array is ['id' => NormalizedItem]
     */
    public function listByTypeAndId(): array
    {
        return $this->itemsByTypeAndId;
    }
}
