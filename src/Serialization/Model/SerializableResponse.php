<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/6/18
 * Time: 2:25 PM
 */

namespace Saluki\Serialization\Model;

use Psr\Http\Message\StreamInterface;
use Saluki\Serialization\Middleware\SerializeMiddleware;
use Saluki\Utility\StringStream;
use Laminas\Diactoros\Response;

/**
 * Class SerializableResponse
 * @package Saluki\Serialization\Model
 */
class SerializableResponse extends Response
{
    private SerializationParams $data;
    private StringStream $defaultBody;

    /**
     * SerializableResponse constructor.
     *
     * @param SerializationParams $data
     * @param int $status
     */
    public function __construct(SerializationParams $data, int $status = 200)
    {
        $this->data = $data;
        $this->defaultBody = new StringStream('');
        parent::__construct($this->defaultBody, $status, []);
    }

    /**
     * @return SerializationParams
     */
    public function getSerializableData(): SerializationParams
    {
        return $this->data;
    }

    public function getBody(): StreamInterface
    {
        $body = parent::getBody();
        if ($body === $this->defaultBody) {
            throw new \LogicException(sprintf(
                'Body not serialized.  Did you forget to run %s?',
                SerializeMiddleware::class
            ));
        }
        return $body;
    }
}
