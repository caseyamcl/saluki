<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 1:00 PM
 */

namespace Saluki\Serialization\Registry;

use Doctrine\ORM\Proxy\Proxy;
use Saluki\Container\Contract\SalukiRegistryInterface;
use Saluki\Serialization\Contract\NormalizerInterface;

/**
 * Class NormalizerRegistry
 * @package Saluki\Serialization\Registry
 */
class NormalizerRegistry implements \IteratorAggregate, SalukiRegistryInterface
{
    /**
     * @var \ArrayObject|NormalizerInterface[]  Key is object class, value is Normalizer
     */
    private \ArrayObject $items;

    /**
     * @var array|string[]  Key is normalizer type name, value is class name
     */
    private array $typeNameIndex;

    /**
     * NormalizerRegistry constructor.
     */
    public function __construct()
    {
        $this->items = new \ArrayObject();
    }

    /**
     * @param NormalizerInterface $normalizer
     */
    public function add(NormalizerInterface $normalizer)
    {
        // Sanity test; prevent two normalizers from being registered that normalize the same class
        if (isset($this->items[$normalizer->normalizesClass()])) {
            throw new \RuntimeException(sprintf(
                'Two normalizers cannot normalize the same class: %s (%s, %s)',
                $normalizer->normalizesClass(),
                get_class($this->items[$normalizer->normalizesClass()]),
                get_class($normalizer)
            ));
        }

        $this->items[$normalizer->normalizesClass()] = $normalizer;
        $this->typeNameIndex[$normalizer->getTypeName()] = $normalizer->normalizesClass();
    }

    /**
     * Is there a registered normalizer that normalizes the given class?
     *
     * @param string $className
     * @return bool
     */
    public function hasForClass(string $className): bool
    {
        if (is_a($className, Proxy::class, true)) {
            $className = (new \ReflectionClass($className))->getParentClass()->getName();
        }

        return isset($this->items[$className]);
    }

    /**
     * Get the normalizer for a given class
     *
     * @param string $className
     * @return NormalizerInterface
     */
    public function getForClass(string $className): NormalizerInterface
    {
        if (is_a($className, Proxy::class, true)) {
            $className = (new \ReflectionClass($className))->getParentClass()->getName();
        }

        if ($this->hasForClass($className)) {
            return $this->items[$className];
        } else {
            throw new \RuntimeException("No normalizer found for class: " . $className);
        }
    }

    /**
     * @param string $type
     * @return NormalizerInterface
     */
    public function getForType(string $type): NormalizerInterface
    {
        if ($this->hasForType($type)) {
            return $this->items[$this->typeNameIndex[$type]];
        } else {
            throw new \RuntimeException("No normalizer found with type name: " . $type);
        }
    }

    public function hasForType(string $type): bool
    {
        return array_key_exists($type, $this->typeNameIndex);
    }

    /**
     * Add an item to the registry
     *
     * @param NormalizerInterface $item
     * @return mixed
     */
    public function addItem($item): void
    {
        $this->add($item);
    }

    /**
     * @return \ArrayObject|NormalizerInterface[]  Keys are entity class name, values are NormalizerInterface instance
     */
    public function getIterator()
    {
        return $this->items;
    }
}
