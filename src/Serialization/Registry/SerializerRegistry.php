<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Registry;

use Saluki\Container\Contract\SalukiRegistryInterface;
use Saluki\Serialization\Contract\SerializerInterface;

/**
 * Class SerializerRegistry
 * @package Saluki\Serialization\Registry
 */
class SerializerRegistry implements SalukiRegistryInterface
{
    /**
     * @var array|SerializerInterface[]
     */
    private $items = [];

    /**
     * @param SerializerInterface $serializer
     */
    public function add(SerializerInterface $serializer)
    {
        $this->items[$serializer->getContentTypeMime()] = $serializer;
    }

    /**
     * @param string $mime
     * @return SerializerInterface
     */
    public function get(string $mime): SerializerInterface
    {
        if (array_key_exists($mime, $this->items)) {
            return $this->items[$mime];
        } else {
            throw new \RuntimeException('No serializer found for mime type: ' . $mime);
        }
    }

    /**
     * @return array|string[]
     */
    public function listMimes(): array
    {
        return array_keys($this->items);
    }

    /**
     * Add an item to the registry
     *
     * @param SerializerInterface $item
     * @return mixed
     */
    public function addItem($item): void
    {
        $this->add($item);
    }
}
