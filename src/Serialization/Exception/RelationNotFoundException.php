<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/27/18
 * Time: 1:45 PM
 */

namespace Saluki\Serialization\Exception;

/**
 * Class RelationNotFoundException
 * @package Saluki\Serialization\Exception
 */
class RelationNotFoundException extends SerializationException
{
    // pass..
}
