<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Exception;

/**
 * Serialization Exception
 *
 * This type of exception is generated when serialization is not possible due to invalid parameters.
 *
 * It should be reserved for client errors, so that it can reliably be translated into an HTTP 400 Bad Request
 *
 * @package Saluki\Serialization\Exception
 */
abstract class SerializationException extends \RuntimeException
{
    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return 400;
    }
}
