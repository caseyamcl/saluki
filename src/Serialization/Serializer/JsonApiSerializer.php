<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/7/18
 * Time: 1:15 PM
 */

namespace Saluki\Serialization\Serializer;

use Aura\Router\Generator;
use Psr\Http\Message\StreamInterface;
use Saluki\Serialization\Contract\NormalizedItemInterface;
use Saluki\Serialization\Contract\SerializerInterface;
use Saluki\Serialization\Model\NormalizerContext;
use Saluki\Serialization\Model\SerializationOptions;
use Saluki\Serialization\Model\SerializationParams;
use Saluki\Serialization\Service\NormalizerService;
use Saluki\Utility\StringStream;
use Laminas\Diactoros\Response\JsonResponse;

/**
 * Class JsonApiSerializer
 * @package Saluki\Serialization\Serializer\JsonApiSerializer
 */
class JsonApiSerializer implements SerializerInterface
{
    private NormalizerService $normalizer;
    private Generator $urlGenerator;

    /**
     * JsonApiSerializer constructor.
     *
     * @param NormalizerService $normalizer
     * @param Generator $urlGenerator
     */
    public function __construct(NormalizerService $normalizer, Generator $urlGenerator)
    {
        $this->normalizer = $normalizer;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @return string
     */
    public function getContentTypeMime(): string
    {
        return 'application/vnd.api+json';
    }

    /**
     * @param SerializationParams $params
     * @return StreamInterface
     */
    public function serialize(SerializationParams $params): StreamInterface
    {
        $normalizerContext = new NormalizerContext($params->getUser());
        $isSingle = $params->getOptions()->isSingleRecord();

        $meta = array_replace(
            $params->getOptions()->getMeta(),
            ($isSingle ? [] : ['pagination' => $params->getPagination()->toArray()])
        );

        // Setup JSON-API document
        $out = [
            'links'    => ['self' => $params->getRequest()->getPathAndQuery()],
            'data'     => [],
            'included' => [],
            'meta'     => $meta,
            'jsonapi'  => ['version' => '1.0']
        ];

        // Keys are 'type:id', values are serialized array
        $included = [];

        // Setup data
        foreach ($params->getItems() as $item) {
            $normalized = $this->normalize($item, $normalizerContext, $params->getOptions());
            $out['data'][] = $this->serializeItem($normalized, $normalizerContext, $params->getOptions());
            $included = array_replace(
                $included,
                $this->resolveIncludes($normalized, $params->getOptions(), $normalizerContext)
            );
        }

        //
        // Cleanup
        //

        // Set included
        $out['included'] = array_values($included);

        // JSON-API spec states that retrieving single records return object for data instead of array of objects.
        if ($isSingle === true && count($out['data']) == 1) {
            $out['data'] = $out['data'][0];
        }

        // Set these to objects (in case they are empty)
        $out['meta'] = (object) $out['meta'];

        // Add pagination links to the document if they exist
        $out['links'] = array_merge($out['links'], $this->buildPaginationLinks($params));


        return new StringStream(json_encode($out, JsonResponse::DEFAULT_JSON_FLAGS));
    }

    /**
     * @param NormalizedItemInterface $normalized
     * @param NormalizerContext $context
     * @param SerializationOptions $options
     * @return array  ['data' => '{item}', 'include' => [{item}, {item}]']
     * @throws \Aura\Router\Exception\RouteNotFound
     */
    protected function serializeItem(
        NormalizedItemInterface $normalized,
        NormalizerContext $context,
        SerializationOptions $options
    ): array {

        // Setup relationships
        foreach ($normalized->getRelationships() as $relationshipName => $relationshipData) {
            // There are two sections to each relationship item in the serialized 'relationships' list.
            // Both are optional, but at-least one must exist.

            //
            // The first is an optional 'links' section, with a 'related' URL to the API endpoint with the relationship
            // TODO: We'll implement this later, but it is not the 'Saluki' way (at least for now)
            //

            //
            // The second is a 'data' object, with a list of resource identifier objects ('type', 'id', ['meta'])
            // For now, we'll stick to using this.
            //

            $prepareResourceIdObject = function (NormalizedItemInterface $item) {
                return ['type' => $item->getType(), 'id' => $item->getId()];
            };

            // Single relationship should be the object or NULL if it doesn't exist
            if ($relationshipData->isSingle()) {
                $relationships[$relationshipName]['data'] = ($item = $relationshipData->getSingleItem())
                    ? $prepareResourceIdObject($this->normalize($item, $context, $options))
                    : null;
            } else {
                $relationships[$relationshipName]['data'] = []; // Include item even if it is empty list
                foreach ($relationshipData->getItems() as $item) {
                    $relationships[$relationshipName]['data'][] = $prepareResourceIdObject(
                        $this->normalize($item, $context, $options)
                    );
                }
            }
        }

        // Build the serialized output
        $serializedRecord = [
            'id'            => $normalized->getId(),
            'type'          => $normalized->getType(),
            'attributes'    => (object) $normalized->getAttributes(),
            'relationships' => (object) ($relationships ?? []),
            'links'         => [
                'self' => $this->generateUrl($normalized->getType(), [$normalized->getIdName() => $normalized->getId()])
            ]
        ];

        return $serializedRecord;
    }

    /**
     * Resolve includes
     *
     * @param NormalizedItemInterface $item
     * @param SerializationOptions $options
     * @param NormalizerContext $context
     * @return array
     */
    protected function resolveIncludes(
        NormalizedItemInterface $item,
        SerializationOptions $options,
        NormalizerContext $context
    ): array {
        foreach ($options->getRequestedIncludes() as $includePath) {
            $includeSegments = array_filter(explode('.', $includePath));
            foreach ($this->includeResolver($item, $context, $options, $includeSegments) as $i) {
                $out[$i->getType() . ':' . $i->getId()] = $this->serializeItem($i, $context, $options);
            }
        }
        return $out ?? [];
    }

    /**
     * Recursive method to retrieve a list of relationships that should be normalized
     *
     * @param NormalizedItemInterface $item
     * @param NormalizerContext $context
     * @param SerializationOptions $options
     * @param array $remainingRelationships
     * @return iterable|\Generator|NormalizedItemInterface[]  Keys are 'type:id'
     */
    private function includeResolver(
        NormalizedItemInterface $item,
        NormalizerContext $context,
        SerializationOptions $options,
        array $remainingRelationships
    ): iterable {
        $relationToFind = array_shift($remainingRelationships);

        if ($item->hasRelationship($relationToFind)) {
            foreach ($item->getRelationship($relationToFind) as $relatedItem) {
                // If item is NULL, skip it.
                if (! $relatedItem) {
                    continue;
                }
                $normalized = $this->normalize($relatedItem, $context, $options);

                // Per the JSON-API 1.0 spec, intermediate resources in a compound path MUST be returned, even
                // if they were not explicitly asked for
                // TODO: UNLESS the client requested sparse fieldsets but requested no fields from this type.
                // if we're on the lowest level, return it.
                if (! empty($remainingRelationships)) {
                    yield from $this->includeResolver($normalized, $context, $options, $remainingRelationships);
                }
                yield $normalized;
            }
        }
    }

    /**
     * Normalize an item
     *
     * @param $item
     * @param NormalizerContext $context
     * @param SerializationOptions $options
     * @return NormalizedItemInterface
     */
    private function normalize(
        $item,
        NormalizerContext $context,
        SerializationOptions $options
    ): NormalizedItemInterface {
        return $this->normalizer->normalize(
            $item,
            $context,
            $options->getRequestedFields($this->normalizer->getNormalizerType($item))
        );
    }

    /**
     * @param string $type
     * @param array $pathSegments
     * @return string
     * @throws \Aura\Router\Exception\RouteNotFound
     */
    private function generateUrl(string $type, array $pathSegments = []): string
    {
        return $this->urlGenerator->generate($type, $pathSegments);
    }

    /**
     * Build pagination links
     *
     * @param SerializationParams $params
     * @return array
     */
    private function buildPaginationLinks(SerializationParams $params): array
    {
        $request = $params->getRequest();
        $offsetParamName = $params->getOptions()->getPaginationOffsetParamName();
        $limitParamName = $params->getOptions()->getPaginationLimitParamName();
        $pagination = $params->getPagination();

        $buildUri = function (int $offset) use ($request, $offsetParamName, $limitParamName) {

            $withParams = (strpos(urldecode($request->getUri()->getQuery()), $limitParamName) !== false)
                ? [$limitParamName => (int) $request->findQueryParam($limitParamName)]
                : [];
            $withParams[$offsetParamName] = $offset;
            return $request->getPathAndQuery($withParams);
        };


        if (! $pagination->isAtStart()) {
            $out['first'] = $buildUri(0);
            $out['prev'] = $buildUri($pagination->getPrevOffset());
        }
        if (! $pagination->isAtEnd()) {
            $out['next'] = $buildUri($pagination->getNextOffset());
            $out['last'] = $buildUri($pagination->getLastOffset());
        }

        return $out ?? [];
    }
}
