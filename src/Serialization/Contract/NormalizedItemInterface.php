<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 12:43 PM
 */

namespace Saluki\Serialization\Contract;

use Saluki\Serialization\Exception\RelationNotFoundException;
use Saluki\Serialization\Model\NormalizedRelationship;

/**
 * Interface NormalizedItemInterface
 *
 * @package Saluki\Serialization\Contract
 */
interface NormalizedItemInterface
{
    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getIdName(): string;

    /**
     * Get ALL of the available attributes that the current user can see
     * @return array
     */
    public function getAttributes(): array;

    /**
     * Get ALL of the available relationships that the current user can see
     * @return array|NormalizedRelationship[]
     */
    public function getRelationships(): array;

    /**
     * Get a relationship by name
     *
     * @param string $name
     * @return NormalizedRelationship
     * @throws RelationNotFoundException
     */
    public function getRelationship(string $name): NormalizedRelationship;

    /**
     * Does a relationship exist with the given name
     * @param string $name
     * @return bool
     */
    public function hasRelationship(string $name): bool;


    /**
     * @return array|string[]
     */
    public function listRelationshipNames(): array;

    /**
     * @return array|string[]
     */
    public function listAttributeNames(): array;
}
