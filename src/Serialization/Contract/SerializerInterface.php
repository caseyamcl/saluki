<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 3:35 PM
 */

namespace Saluki\Serialization\Contract;

use Psr\Http\Message\StreamInterface;
use Saluki\Serialization\Model\SerializationParams;

/**
 * Interface SerializerInterface
 * @package Saluki\Serialization\Contract
 */
interface SerializerInterface
{
    const ALL = ['*'];

    /**
     * @return string
     */
    public function getContentTypeMime(): string;

    /**
     * @param SerializationParams $params
     * @return StreamInterface
     */
    public function serialize(SerializationParams $params): StreamInterface;
}
