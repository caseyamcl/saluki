<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 12:43 PM
 */

namespace Saluki\Serialization\Contract;

use Saluki\Serialization\Model\NormalizerContext;

/**
 * Interface NormalizerInterface
 * @package Saluki\Serialization\Contract
 */
interface NormalizerInterface
{
    public const ALL = ['*'];
    public const USE_DEFAULTS = null;

    /**
     * @return string
     */
    public function getTypeName(): string;

    /**
     * @return string
     */
    public function getIdName(): string;

    /**
     * @return null|array|string[]
     */
    public function listDefaultFields(): ?array;

    /**
     * @return string  The type of class that is normalized
     */
    public function normalizesClass(): string;

    /**
     * @param mixed $item
     * @param NormalizerContext $context
     * @param array|null $fields
     * @return NormalizedItemInterface
     */
    public function normalizeItem(
        $item,
        NormalizerContext $context,
        ?array $fields = self::USE_DEFAULTS
    ): NormalizedItemInterface;
}
