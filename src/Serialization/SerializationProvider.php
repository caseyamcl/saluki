<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/28/18
 * Time: 10:39 AM
 */

namespace Saluki\Serialization;

use Psr\Container\ContainerInterface;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Container\Model\SalukiParams;
use Saluki\Serialization\Contract\NormalizerInterface;
use Saluki\Serialization\Contract\SerializerInterface;
use Saluki\Serialization\Registry\NormalizerRegistry;
use Saluki\Serialization\Registry\SerializerRegistry;
use Saluki\Serialization\Serializer\JsonApiSerializer;
use Saluki\Serialization\Service\NormalizerService;
use Saluki\Serialization\Service\SerializerService;

/**
 * Class SerializationProvider
 * @package Saluki\Serialization
 */
class SerializationProvider implements SalukiProviderInterface
{
    /**
     * @var bool
     */
    private $registerDefaultSerializers;

    /**
     * SerializationProvider constructor.
     * @param bool $registerDefaultSerializers
     */
    public function __construct(bool $registerDefaultSerializers = true)
    {
        $this->registerDefaultSerializers = $registerDefaultSerializers;
    }

    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        // Add the default services
        $builder->addServiceDefinition(SerializerService::class);

        $builder->addRegistry(SerializerRegistry::class, SerializerInterface::class);
        $builder->addRegistry(NormalizerRegistry::class, NormalizerInterface::class, true);

        $builder->addServiceDefinition(NormalizerService::class);

        if ($this->registerDefaultSerializers) {
            $builder->addServiceDefinition(JsonApiSerializer::class);
        }
    }
}
