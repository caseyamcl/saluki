<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 1:07 PM
 */

namespace Saluki\Serialization\Service;

use Saluki\Serialization\Contract\NormalizedItemInterface;
use Saluki\Serialization\Contract\NormalizerInterface;
use Saluki\Serialization\Model\NormalizerContext;
use Saluki\Serialization\Registry\NormalizerRegistry;

class NormalizerService
{
    /**
     * @var NormalizerRegistry
     */
    private $normalizerRegistry;

    /**
     * NormalizerService constructor.
     * @param NormalizerRegistry $normalizerRegistry
     */
    public function __construct(NormalizerRegistry $normalizerRegistry)
    {
        $this->normalizerRegistry = $normalizerRegistry;
    }

    /**
     * @param NormalizerRegistry $normalizerRegistry
     */
    public function setNormalizerRegistry(NormalizerRegistry $normalizerRegistry): void
    {
        $this->normalizerRegistry = $normalizerRegistry;
    }

    /**
     * Get type name for a given class
     *
     * @param object $item
     * @return string
     */
    public function getNormalizerType($item): string
    {
        return $this->normalizerRegistry->getForClass(get_class($item))->getTypeName();
    }

    /**
     * @param object $item
     * @param NormalizerContext $context
     * @param array|string[]|null $fields
     * @return NormalizedItemInterface
     */
    public function normalize(
        $item,
        NormalizerContext $context,
        ?array $fields = NormalizerInterface::USE_DEFAULTS
    ): NormalizedItemInterface {
        // Sanity test
        if (! is_object($item)) {
            throw new \InvalidArgumentException('Normalizer requires an object');
        }

        // Performance: Find the normalized item in the context if already normalized (so we don't do it twice)
        if (! $normalized = $context->find($item)) {
            $normalized = $this->normalizerRegistry
                ->getForClass(get_class($item))
                ->normalizeItem($item, $context, $fields);

            $context->addItem($item, $normalized);
        }

        return $normalized;
    }
}
