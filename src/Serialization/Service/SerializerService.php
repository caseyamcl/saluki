<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Serialization\Service;

use Negotiation\Accept;
use Negotiation\Negotiator;
use Psr\Http\Message\ResponseInterface;
use Saluki\Serialization\Exception\CouldNotNegotiateSerializerException;
use Saluki\Serialization\Model\SerializationParams;
use Saluki\Serialization\Registry\SerializerRegistry;
use Laminas\Diactoros\Response;

class SerializerService
{
    private SerializerRegistry $serializers;
    private Negotiator $negotiator;

    /**
     * SerializerService constructor.
     * @param SerializerRegistry $serializers
     * @param Negotiator|null $negotiator
     */
    public function __construct(SerializerRegistry $serializers, ?Negotiator $negotiator = null)
    {
        $this->serializers = $serializers;
        $this->negotiator = $negotiator ?: new Negotiator();
    }

    /**
     * @param SerializationParams $params
     * @param null|ResponseInterface $response
     * @return ResponseInterface
     */
    public function serialize(SerializationParams $params, ?ResponseInterface $response = null): ResponseInterface
    {
        $accepted = $params->getRequest()->getHeaderLine('Accept') ?: '*/*';
        $available = $this->serializers->listMimes();

        if (empty($available)) {
            throw new \RuntimeException("There are no registered serializers");
        }

        /** @var Accept $mediaType */
        if ($mediaType = $this->negotiator->getBest($accepted, $available)) {
            $mime = $mediaType->getValue();

            $body = $this->serializers->get($mime)->serialize($params);
            return ($response ?: new Response())->withBody($body)->withHeader('Content-type', $mime);
        } else {
            throw new CouldNotNegotiateSerializerException(sprintf(
                $accepted,
                implode(', ', $available),
                'Requested: %s; Available: %s'
            ));
        }
    }
}
