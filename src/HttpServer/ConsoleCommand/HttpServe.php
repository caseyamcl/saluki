<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\HttpServer\ConsoleCommand;

use Composer\Autoload\ClassLoader;
use Saluki\Console\ConsoleIO;
use Saluki\Utility\RuntimeEnvironment;
use Saluki\WebInterface\Service\WebRequestHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * HTTP Serve - serve HTTP application
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class HttpServe extends Command
{
    private RuntimeEnvironment $environment;

    public function __construct(RuntimeEnvironment $environment)
    {
        $this->environment = $environment;
        parent::__construct();
    }


    protected function configure()
    {
        $this->setName('http:serve');
        $this->setDescription('Startup HTTP server (swoole)');
        $this->addOption(
            'address',
            'a',
            InputOption::VALUE_REQUIRED,
            'Address to listen on',
            '0.0.0.0'
        );

        $this->addOption(
            'port',
            'p',
            InputOption::VALUE_REQUIRED,
            'Port to listen on',
            80
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new ConsoleIO($input, $output);
        $this->devServer($io);
    }

    /**
     * @param ConsoleIO $io
     * @return int
     */
    protected function devServer(ConsoleIO $io): int
    {
        $io->writeln('Starting <fg=red>DEVELOPMENT</fg=red> server...');

        // Guess location of autoload.php (relative to Composer ClassLoader file)
        $fileName = (new \ReflectionClass(ClassLoader::class))->getFileName();
        if ($autoLoadFile = realpath(dirname($fileName) . '/../autoload.php')) {
            $io->writeln('Using autoload file: ' . $autoLoadFile, $io::VERBOSITY_VERBOSE);
        } else {
            return $io->abort('Could not determine autoload.php location.  Specify router script');
        }

        // Guess the main class
        $io->writeln('Using class: ' . $this->environment->getAppClass(), $io::VERBOSITY_VERBOSE);

        // Create temporary file to run app
        $io->writeln('Writing runtime script to : ' . sys_get_temp_dir() . '/run_app.php');
        file_put_contents(sys_get_temp_dir() . '/run_app.php', sprintf(
            "<?php\nrequire_once('%s');\n%s::main('%s');",
            $autoLoadFile,
            $this->environment->getAppClass(),
            $this->environment->getBaseDirectory()
        ));

        $routerScript = sys_get_temp_dir() . '/run_app.php';

        // How? When the router is part of the implementing app?
        // Run php -S [address]:[port] [router-script]

        // Determine php runtime
        if (! $phpRuntime = $_SERVER['_'] ?? null) {
            return $io->abort('Could not determine PHP runtime.');
        }

        // Run php with built-in server and temporary location
        $cmd = sprintf(
            '%s -S %s:%s -t %s %s',
            $phpRuntime,
            $io->getOption('address'),
            $io->getOption('port'),
            $this->environment->getBaseDirectory(),
            $routerScript
        );
        $io->writeln(sprintf('Running server: <info>%s</info>', $cmd));
        `$cmd`;
        return self::SUCCESS;
    }

    /**
     * @param string $message
     * @param ConsoleIO $io
     */
    protected function logReqResp(string $message, ConsoleIO $io): void
    {
        $io->writeln(sprintf('[%s] <fg=yellow>%s</fg=yellow>', date('Y-m-d H:i:s'), $message));
    }
}
