<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation;

use Saluki\Validation\Contract\ValueTransformerInterface;

/**
 * Class ValidationFilter
 */
class ValueTransformer implements ValueTransformerInterface
{
    /**
     * @var callable
     */
    private $filter;

    /**
     * @var bool
     */
    private $beforeValidation;

    /**
     * ValidationFilter constructor.
     * @param callable $transformer  Accepts value and returns transformed value
     * @param bool $beforeValidation
     */
    public function __construct(callable $transformer, bool $beforeValidation = self::BEFORE_VALIDATION)
    {
        $this->filter = $transformer;
        $this->beforeValidation = $beforeValidation;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function __invoke($value)
    {
        return call_user_func($this->filter, $value);
    }

    /**
     * @return callable
     */
    public function getCallback(): callable
    {
        return $this->filter;
    }

    /**
     * @return bool
     */
    public function isBeforeValidation(): bool
    {
        return $this->beforeValidation;
    }
}
