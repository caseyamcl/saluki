<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/25/18
 * Time: 1:41 PM
 */

namespace Saluki\Validation;

/**
 * Class ValidatorContext
 * @package Saluki\Validation
 */
class ValidatorContext
{
    /**
     * @var array|mixed[]
     */
    private $fields;

    /**
     * @var array
     */
    private $otherData;

    /**
     * @param $value
     * @param array $otherData
     * @return ValidatorContext
     */
    public static function singleValue($value, array $otherData = [])
    {
        return new static(['value' => $value], $otherData);
    }

    /**
     * ValidatorContext constructor.
     * @param array $fields
     * @param array $otherData
     */
    public function __construct(array $fields, array $otherData = [])
    {
        $this->fields = $fields;
        $this->otherData = $otherData;
    }

    /**
     * Has other data item via magic method
     * @param string $key
     * @return bool
     */
    public function __isset(string $key)
    {
        return $this->hasOtherDataItem($key);
    }

    /**
     * Get other data item via magic method
     * @param string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->getOtherDataItem($key);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasOtherDataItem(string $key): bool
    {
        return array_key_exists($key, $this->otherData);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getOtherDataItem(string $key)
    {
        if ($this->hasOtherDataItem($key)) {
            return $this->otherData[$key];
        } else {
            throw new \RuntimeException('Other data item does not exist in ValidatorContext: ' . $key);
        }
    }

    /**
     * @return array
     */
    public function getOtherData(): array
    {
        return $this->otherData;
    }

    /**
     * Merge other data in and return a new instance
     *
     * @param array $otherData
     * @return ValidatorContext
     */
    public function withAddedOtherData(array $otherData): ValidatorContext
    {
        $that = clone $this;
        $that->otherData = array_replace($that->otherData, $otherData);
        return $that;
    }

    /**
     * Add or set an additional item and return a new instance
     *
     * @param string $key
     * @param $value
     * @return ValidatorContext
     */
    public function withAddedOtherDataItem(string $key, $value): ValidatorContext
    {
        $that = clone $this;
        $that->otherData[$key] = $value;
        return $that;
    }

    /**
     * Set (clobber) values and return a new instance
     *
     * @param array $values
     * @return ValidatorContext
     */
    public function withFields(array $values): ValidatorContext
    {
        $that = clone $this;
        $that->fields = $values;
        return $that;
    }

    /**
     * Add or set a data value and return a new instance
     *
     * @param string $name
     * @param $value
     * @return ValidatorContext
     */
    public function withAddedField(string $name, $value): ValidatorContext
    {
        $that = clone $this;
        $that->fields[$name] = $value;
        return $that;
    }

    /**
     * @return array|mixed[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasField(string $name): bool
    {
        return array_key_exists($name, $this->fields);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getFieldValue(string $name)
    {
        if ($this->hasField($name)) {
            return $this->fields[$name];
        } else {
            throw new \RuntimeException("ValidatorContext does not contain value: " . $name);
        }
    }

    /**
     * @return bool
     */
    public function isSingleValue(): bool
    {
        return count($this->fields) == 1;
    }

    /**
     * @return mixed
     */
    public function getSingleValue()
    {
        if ($this->hasField('value') && $this->isSingleValue()) {
            return $this->fields['value'];
        } else {
            throw new \RuntimeException('ValidatorContext does not contain a single value');
        }
    }
}
