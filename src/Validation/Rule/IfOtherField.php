<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/27/18
 * Time: 3:37 PM
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\ValidatorBuiltInRules;
use Saluki\Validation\ValidatorContext;
use Saluki\Validation\ValidatorRuleSet;

/**
 * Class IfOtherField
 * @package Saluki\Validation\Rule
 */
class IfOtherField extends AbstractValidatorRule
{
    use ValidatorBuiltInRules;

    /**
     * @var string
     */
    protected $otherField;

    /**
     * @var ValidatorRuleSet
     */
    protected $ruleSet;

    /**
     * Alternative constructor
     *
     * @param string $otherField
     * @param null|string $description
     * @return IfOtherField
     */
    public static function fieldName(string $otherField, ?string $description = null)
    {
        return new static($otherField, $description);
    }

    /**
     * IfOtherField constructor.
     * @param string $otherField
     * @param null|string $description
     */
    public function __construct(string $otherField, ?string $description = null)
    {
        parent::__construct($description);
        $this->ruleSet = new ValidatorRuleSet();
        $this->otherField = $otherField;
    }

    /**
     * @return ValidatorRuleSet
     */
    public function getRuleSet(): ValidatorRuleSet
    {
        return $this->ruleSet;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return sprintf("rules must pass if '%s' field is present", $this->otherField);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        return ($context->hasField($this->otherField))
            ? $this->ruleSet->prepare($value, $context)
            : $value;
    }

    // --------------------------------------------------------------

    /**
     * @param ValidatorRuleInterface $rule
     * @return self
     */
    public function addRule(ValidatorRuleInterface $rule): ValidatorBuiltInRules
    {
        $this->ruleSet->addRule($rule);
        return $this;
    }

    /**
     * @param ValueTransformerInterface $transformer
     * @return self
     */
    public function addTransformer(ValueTransformerInterface $transformer): ValidatorBuiltInRules
    {
        $this->ruleSet->addTransformer($transformer);
        return $this;
    }
}
