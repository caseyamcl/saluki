<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Max
 * @package Saluki\Validation\Rule
 */
class Max extends AbstractValidatorRule
{
    /**
     * @var int|float
     */
    private $max;

    /**
     * Min constructor.
     * @param int|float $max
     * @param null|string $description
     */
    public function __construct($max, ?string $description = null)
    {
        parent::__construct($description);
        $this->max = $max;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be less than or equal to ' . (float) $this->max;
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue((float) $value <= (float) $this->max);
        return $value;
    }
}
