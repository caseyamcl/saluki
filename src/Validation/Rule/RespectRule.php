<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Respect\Validation\Exceptions\NestedValidationException;
use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\Exception\ValidationException;
use Saluki\Validation\Exception\ValidationRuleException;
use Respect\Validation\Validatable;
use Saluki\Validation\ValidatorContext;

/**
 * Class RespectRule
 * @package Saluki\Validation\Rule
 */
abstract class RespectRule extends AbstractValidatorRule
{
    private Validatable $validatable;
    private ?string $description;

    /**
     * RespectRule constructor.
     * @param Validatable $validatable
     * @param null|string $description
     */
    public function __construct(Validatable $validatable, ?string $description = null)
    {
        parent::__construct($description);
        $this->validatable = $validatable;
        $this->description = $description;
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        try {
            $this->validatable->assert($value);
            return $value;
        } catch (ValidationException | NestedValidationException $e) {
            throw new ValidationRuleException($this->__toString(), $e);
        }
    }
}
