<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Boolean
 * @package Saluki\Validation\Rule
 */
class Boolean extends AbstractValidatorRule
{
    private const DEFAULT_MAP = [
        'true' => true, 1 => true, 'yes' => true, 'on' => true,
        'false' => false, 0 => false, 'no' => false, 'off' => false
    ];

    private bool $convertToBoolean;
    private array $map;

    /**
     * Boolean constructor.
     *
     * @param null|string $description
     * @param bool $convertToBoolean
     * @param array $map
     */
    public function __construct(
        ?string $description = null,
        bool $convertToBoolean = true,
        array $map = self::DEFAULT_MAP
    ) {
        $this->convertToBoolean = $convertToBoolean;
        $this->map = $map;

        parent::__construct($description);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return bool|mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue(in_array($value, array_keys($this->map)));
        return ($this->convertToBoolean) ? $this->map[$value] : $value;
    }

    /**
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'Value must be one of: ' . implode(', ', array_keys($this->map));
    }
}
