<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;
use Saluki\Validation\ValidatorRuleSet;

/**
 * Class Each
 * @package Saluki\Validation\Rule
 */
class Each extends AbstractValidatorRule
{
    /**
     * @var ValidatorRuleSet
     */
    private $rules;

    /**
     * Each constructor.
     *
     * @param ValidatorRuleSet $rules
     * @param null|string $description
     */
    public function __construct(ValidatorRuleSet $rules, ?string $description = null)
    {
        $this->rules = $rules;
        parent::__construct($description);
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        $messages = array_map('strval', $this->rules->listRules());
        return implode(PHP_EOL, array_merge(['The rules must match for all values in the list'], $messages));
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        // Make the value an array
        if (! is_array($value) && ! $value instanceof \Traversable) {
            $value = (array) $value;
        }

        foreach ($value as $k => $v) {
            $value[$k] = $this->rules->prepare($v);
        }

        return $value;
    }
}
