<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class ValidJson
 * @package Saluki\Validation\Rule
 */
class ValidJson extends AbstractValidatorRule
{
    /**
     * @var bool
     */
    private $decodeJson;

    /**
     * ValidJson constructor.
     * @param null|string $description
     * @param bool $decodeJson
     */
    public function __construct(?string $description = null, bool $decodeJson = false)
    {
        parent::__construct($description);
        $this->decodeJson = $decodeJson;
    }


    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $decoded = @json_decode($value);
        $this->assertTrue($decoded !== null);
        return $this->decodeJson ? $decoded : $value;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be valid JSON';
    }
}
