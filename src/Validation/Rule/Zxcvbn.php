<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/5/18
 * Time: 3:21 PM
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Zxcvbn
 * @package Saluki\Validation\Rule
 */
class Zxcvbn extends AbstractValidatorRule
{
    /**
     * @var int
     */
    private $level;

    /**
     * Zxcvbn constructor.
     * @param int $minLevel
     * @param null|string $description
     */
    public function __construct(int $minLevel = 2, ?string $description = null)
    {
        parent::__construct($description);
        $this->level = $minLevel;
    }


    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return sprintf('value must be sufficiently complex (zxcvbn score >= %s)', $this->level);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $strength = (new \ZxcvbnPhp\Zxcvbn())->passwordStrength($value);
        $this->assertTrue($strength['score'] >= $this->level);
        return $value;
    }
}
