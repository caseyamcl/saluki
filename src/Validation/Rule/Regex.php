<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Regex
 * @package Saluki\Validation\Rule
 */
class Regex extends AbstractValidatorRule
{
    /**
     * @var string
     */
    private $regex;

    /**
     * Regex constructor.
     * @param string $regex
     * @param null|string $description
     */
    public function __construct(string $regex, ?string $description = null)
    {
        parent::__construct($description);
        $this->regex = $regex;
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue((int) preg_match($this->regex, $value) > 0);
        return $value;
    }

    /**
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must match the pattern: ' . $this->regex;
    }
}
