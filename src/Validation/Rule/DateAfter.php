<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Cake\Chronos\ChronosInterface;
use DateTimeInterface;
use Saluki\Validation\ValidatorContext;

/**
 * Class DateAfter
 * @package Saluki\Validation\Rule
 */
class DateAfter extends Temporal
{
    /**
     * @var \DateTimeInterface
     */
    private $date;

    /**
     * DateAfter constructor.
     * @param \DateTimeInterface|string $date
     * @param null|string $description
     * @param bool $convertToChronos
     */
    public function __construct($date, ?string $description = null, bool $convertToChronos = true)
    {
        $this->date = $this->interpretDateValue($date);
        parent::__construct($description, $convertToChronos);
    }

    /**
     * Run the rule
     *
     * @param \DateTimeInterface|string $value
     * @param ValidatorContext $context
     * @return ChronosInterface|string
     */
    public function __invoke($value, ValidatorContext $context)
    {
        parent::__invoke($value, $context);
        $chronosValue = $this->interpretDateValue($value);
        $this->assertTrue($chronosValue->gt($this->date));
        return $this->shouldConvert() ? $chronosValue : $value;
    }

    /**
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return parent::getDefaultDescription() . ' newer than ' . $this->date->format(DateTimeInterface::ATOM);
    }
}
