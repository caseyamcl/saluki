<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/27/18
 * Time: 3:24 PM
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class CustomRule
 * @package Saluki\Validation\Rule
 */
class CustomRule extends AbstractValidatorRule
{
    /**
     * @var callable
     */
    private $callback;

    /**
     * ValidatorRule constructor.
     *
     * @param callable $callback  Callback accepts the method signature ($value, ValidatorContext $context) and
     *                        return the prepared value. It should throw a ValidationRuleException if invalid value
     * @param string $description
     */
    public function __construct(callable $callback, ?string $description = null)
    {
        $this->callback = $callback;
        parent::__construct($description);
    }

    /**
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        return call_user_func($this->callback, $value, $context);
    }

    /**
     * Get a default description to use if a custom description was not provided
     *
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be valid';
    }
}
