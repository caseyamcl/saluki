<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Cake\Chronos\ChronosInterface;
use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\Behavior\DateValidatorTrait;
use Saluki\Validation\ValidatorContext;

/**
 * Class Temporal
 * @package Saluki\Validation\Rule
 */
class Temporal extends AbstractValidatorRule
{
    use DateValidatorTrait;

    /**
     *
     * @var bool
     */
    private $convertToChronos;

    /**
     * Temporal constructor.
     *
     * @param null|string $description
     * @param bool $convertToChronos
     */
    public function __construct(?string $description = null, bool $convertToChronos = true)
    {
        $this->convertToChronos = $convertToChronos;
        parent::__construct($description);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return string|ChronosInterface
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $chronos = $this->interpretDateValue($value);
        return $this->shouldConvert() ? $chronos : $value;
    }

    /**
     * @return bool
     */
    protected function shouldConvert(): bool
    {
        return $this->convertToChronos;
    }

    /**
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be any date/time representation (e.g. "now", "last Tuesday", "2018-05-04", etc..)';
    }
}
