<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Length
 * @package Saluki\Validation\Rule
 */
class Length extends AbstractValidatorRule
{
    /**
     * @var int|null
     */
    private $min;
    /**
     * @var int|null
     */
    private $max;

    /**
     * Length constructor.
     * @param int|null $min
     * @param int|null $max
     * @param null|string $description
     */
    public function __construct(?int $min, ?int $max = null, ?string $description = null)
    {
        $this->min = $min;
        $this->max = $max;

        parent::__construct($description);
    }

    /**
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $minGood = strlen($value) >= $this->min or is_null($this->min);
        $maxGood = strlen($value) <= $this->max or is_null($this->max);
        $this->assertTrue($minGood && $maxGood);
        return $value;
    }

    public function getDefaultDescription(): string
    {
        return sprintf(
            'the value length must be between %s and %s',
            $this->min ?: 'any',
            $this->max ?: 'any'
        );
    }
}
