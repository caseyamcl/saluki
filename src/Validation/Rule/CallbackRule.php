<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class CallbackRule
 * @package Saluki\Validation\Rule
 */
class CallbackRule extends AbstractValidatorRule
{
    /**
     * @var callable
     */
    private $rule;

    /**
     * ValidatorRule constructor.
     *
     * @param callable $rule  Callback accepts the method signature ($value, ValidatorContext $context) and
     *                        return boolean
     * @param string $description
     */
    public function __construct(callable $rule, ?string $description = null)
    {
        $this->rule = $rule;
        parent::__construct($description);
    }

    /**
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue(call_user_func($this->rule, $value, $context) !== false);
        return $value;
    }

    /**
     * Get a default description to use if a custom description was not provided
     *
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be valid';
    }
}
