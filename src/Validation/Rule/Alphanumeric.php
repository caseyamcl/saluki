<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Respect\Validation\Validator;
use Saluki\Validation\ValidatorContext;

/**
 * Class Alphanumeric
 * @package Saluki\Validation\Rule
 */
class Alphanumeric extends RespectRule
{
    /**
     * @var string
     */
    private $additionalChars;

    /**
     * Alphanumeric constructor.
     * @param null|string $description
     * @param string $additionalChars
     */
    public function __construct(?string $description = null, string $additionalChars = '')
    {
        $validator = preg_match('/\s/', $additionalChars)
            ? Validator::alnum($additionalChars)
            : Validator::alnum($additionalChars)->noWhitespace();

        parent::__construct($validator, $description);
        $this->additionalChars = $additionalChars;
    }

    public function __invoke($value, ValidatorContext $context)
    {
        return (! empty($value)) ? parent::__invoke($value, $context) : $value;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        $message = 'value must be alphanumeric';
        if ($this->additionalChars) {
            $message .= ' (also allowed: ' . $this->additionalChars . ')';
        }
        return $message;
    }
}
