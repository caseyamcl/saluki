<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Cake\Chronos\ChronosInterface;
use DateTimeInterface;
use Saluki\Validation\Behavior\DateValidatorTrait;
use Saluki\Validation\ValidatorContext;

/**
 * Class DateBetween
 * @package Saluki\Validation\Rule
 */
class DateBetween extends Temporal
{
    use DateValidatorTrait;

    /**
     * @var ChronosInterface
     */
    private $earliest;

    /**
     * @var ChronosInterface
     */
    private $latest;

    /**
     * DateBetween constructor.
     * @param DateTimeInterface|string $earliest
     * @param DateTimeInterface|string $latest
     * @param string $description
     * @param bool $convertToChronos
     */
    public function __construct($earliest, $latest, ?string $description = null, bool $convertToChronos = true)
    {
        $this->earliest = $this->interpretDateValue($earliest);
        $this->latest   = $this->interpretDateValue($latest);

        parent::__construct($description, $convertToChronos);
    }

    /**
     * Run the rule
     *
     * @param DateTimeInterface|string $value
     * @param ValidatorContext $context
     * @return ChronosInterface|string
     */
    public function __invoke($value, ValidatorContext $context)
    {
        parent::__invoke($value, $context);
        $chronosValue = $this->interpretDateValue($value);
        $this->assertTrue($chronosValue->between($this->earliest, $this->latest));
        return $this->shouldConvert() ? $chronosValue : $value;
    }


    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return sprintf(
            '%s between %s and %s',
            parent::getDefaultDescription(),
            $this->earliest->format(DateTimeInterface::ATOM),
            $this->earliest->format(DateTimeInterface::ATOM)
        );
    }
}
