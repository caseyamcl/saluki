<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Doctrine\Persistence\ObjectRepository;
use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

class Unique extends AbstractValidatorRule
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var ObjectRepository
     */
    private $repository;

    /**
     * Unique constructor.
     *
     * @param string $fieldName
     * @param ObjectRepository $repository
     * @param null|string $description
     */
    public function __construct($fieldName, ObjectRepository $repository, ?string $description = null)
    {
        parent::__construct($description);
        $this->fieldName = $fieldName;
        $this->repository = $repository;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value already exists';
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue(empty($this->repository->findOneBy([$this->fieldName => $value])));
        return $value;
    }
}
