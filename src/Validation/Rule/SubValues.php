<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/5/18
 * Time: 1:05 PM
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\Contract\ValidatorFieldInterface;
use Saluki\Validation\ValidatorContext;
use Saluki\Validation\ValidatorFieldSet;

class SubValues extends IsList
{
    /**
     * @var ValidatorFieldSet
     */
    private $fieldSet;

    /**
     * Each constructor.
     *
     * @param iterable|ValidatorFieldSet|ValidatorFieldInterface[]
     * @param null|string $description
     */
    public function __construct(iterable $fields, ?string $description = null)
    {
        $this->fieldSet = $fields instanceof ValidatorFieldSet ? $fields : new ValidatorFieldSet($fields);
        parent::__construct($description);
    }

    /**
     * Run the rule
     *
     * @param array $value
     * @param ValidatorContext $context
     * @return array  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $value = parent::__invoke($value, $context);
        $newContext = (new ValidatorContext($value))->withAddedOtherData($context->getOtherData());
        return $this->fieldSet->prepare($value, $newContext);
    }
}
