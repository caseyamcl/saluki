<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

class EqualTo extends AbstractValidatorRule
{
    /**
     * @var mixed
     */
    private $equals;

    /**
     * EqualTo constructor.
     * @param mixed $equals
     * @param null|string $description
     */
    public function __construct($equals, ?string $description = null)
    {
        parent::__construct($description);
        $this->equals = $equals;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be equal to ' . $this->equals;
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue($this->equals == $value);
        return $value;
    }
}
