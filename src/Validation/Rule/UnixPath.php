<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/2/18
 * Time: 12:05 PM
 */

namespace Saluki\Validation\Rule;

/**
 * Class UnixPath
 * @package Saluki\Validation\Rule
 */
class UnixPath extends Regex
{
    /**
     * UnixPath constructor.
     * @param null|string $description
     */
    public function __construct(?string $description = null)
    {
        $pathRegex = '/^\/$|(^(?=\/)|^\.|^\.\.)(\/(?=[^/\0])[^/\0]+)*\/?$/';
        parent::__construct($pathRegex, $description);
    }

    /**
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be a valid UNIX path';
    }
}
