<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\ValidatorContext;

/**
 * Class EmailLocal
 * @package Saluki\Validation\Rule
 */
class EmailLocal extends EmailAddress
{
    /**
     * @param mixed $value
     * @param ValidatorContext $context
     * @return bool
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $testValue = $value . '@example.org';
        parent::__invoke($testValue, $context);
        return $value;
    }

    public function getDefaultDescription(): string
    {
        return 'value must be a valid local email address (everything before the @)';
    }
}
