<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Respect\Validation\Validator;
use Saluki\Validation\ValidatorContext;

/**
 * Class DomainName
 * @package Saluki\Validation\Rule
 */
class DomainName extends RespectRule
{
    /**
     * @var bool
     */
    private $allowLocalhost;

    public function __construct(bool $allowLocalhost = true, ?string $description = null, bool $tldCheck = false)
    {
        $this->allowLocalhost = $allowLocalhost;
        parent::__construct(Validator::domain($tldCheck), $description);
    }

    public function __invoke($value, ValidatorContext $context)
    {
        if ($this->allowLocalhost && strcasecmp($value, 'localhost') === 0) {
            return $value;
        } else {
            return parent::__invoke($value, $context);
        }
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be a valid domain name';
    }
}
