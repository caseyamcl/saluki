<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\ValidatorContext;
use Saluki\Validation\ValidatorRuleSet;

/**
 * Any of the conditions
 *
 * NOTE: Value is NOT transformed, even if one of the inner values transforms the value
 *
 * @package Saluki\Validation\Rule
 */
class OrX extends AbstractValidatorRule
{
    /**
     * @var ValidatorRuleSet|ValidatorRuleInterface[]
     */
    private $rules;

    /**
     * OrX constructor.
     * @param iterable|ValidatorRuleInterface[] $rules
     * @param null|string $description
     */
    public function __construct(iterable $rules, ?string $description = null)
    {
        $this->rules = new ValidatorRuleSet();

        foreach ($rules as $rule) {
            $this->addRule($rule);
        }

        parent::__construct($description);
    }

    /**
     * @param ValidatorRuleInterface $rule
     */
    private function addRule(ValidatorRuleInterface $rule)
    {
        $this->rules->addRule($rule);
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        foreach ($this->rules->getIterator() as $rule) {
            $messages[] = $rule->__toString();
        }

        return 'At-least one of the following must be true: ' . "\n" . implode(PHP_EOL, $messages ?? []);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        foreach ($this->rules as $rule) {
            try {
                $rule($value, $context);
                return $value;
            } catch (ValidationRuleException $e) {
                // continue
            }
        }

        // If made it here..
        throw $this->invalid();
    }
}
