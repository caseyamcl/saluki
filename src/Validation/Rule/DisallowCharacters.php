<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class DisallowCharacters
 * @package Saluki\Validation\Rule
 */
class DisallowCharacters extends AbstractValidatorRule
{
    /**
     * @var array|string[]
     */
    private $characters;

    /**
     * DisallowCharacters constructor.
     * @param string $characters  Characters to disallow
     * @param null|string $description
     */
    public function __construct(string $characters, ?string $description = null)
    {
        parent::__construct($description);
        $this->characters = array_filter(array_unique(str_split($characters)));
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $foundDisallowed = [];

        foreach ($this->characters as $char) {
            if (strpos($value, $char) !== false) {
                $foundDisallowed[] = $char;
            }
        }

        if (empty($foundDisallowed)) {
            return $value;
        } else {
            throw $this->invalid();
        }
    }

    /**
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must not contain any of the following characters: ' . implode('', $this->characters);
    }
}
