<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/10/18
 * Time: 3:56 PM
 */

namespace Saluki\Validation\Rule;

use Doctrine\Persistence\ObjectRepository;
use Doctrine\DBAL\Types\ConversionException;
use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Entity
 * @package Saluki\Validation\Rule
 */
class Entity extends AbstractValidatorRule
{
    protected const AUTO = null;

    private ObjectRepository $repository;
    private ?string $idField;

    /**
     * Unique constructor.
     *
     * @param ObjectRepository $repository
     * @param null|string $description
     * @param null|string $idField
     */
    public function __construct(
        ObjectRepository $repository,
        ?string $description = null,
        ?string $idField = self::AUTO
    ) {
        parent::__construct($description);
        $this->repository = $repository;
        $this->idField = $idField;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must exist in the database';
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        if (empty($value)) {
            return null;
        }

        try {
            $record = ($this->idField === self::AUTO)
                ? $this->repository->find($value)
                : $this->repository->findOneBy([$this->idField => $value]);
            $this->assertTrue($record !== null);
            return $record;
        } catch (ConversionException $e) {
            return $this->invalid();
        }
    }
}
