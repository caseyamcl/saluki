<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\ValidatorContext;

/**
 * Class AndX
 * @package Saluki\Validation\Rule
 */
class AndX extends AbstractValidatorRule
{
    /**
     * @var array|ValidatorRuleInterface[]
     */
    private $rules;

    /**
     * OrX constructor.
     * @param iterable $rules
     * @param null|string $description
     */
    public function __construct(iterable $rules, ?string $description = null)
    {
        foreach ($rules as $rule) {
            $this->addRule($rule);
        }

        parent::__construct($description);
    }

    /**
     * @param ValidatorRuleInterface $rule
     */
    private function addRule(ValidatorRuleInterface $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        $rules = array_map('__toString', $this->rules);
        return 'All of the following must be true: ' . "\n" . implode(PHP_EOL, $rules);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        foreach ($this->rules as $rule) {
            $rule($value, $context);
        }

        return $value;
    }
}
