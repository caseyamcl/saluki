<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Contains
 * @package Saluki\Validation\Rule
 */
class Contains extends AbstractValidatorRule
{
    /**
     * @var string
     */
    private $mustContain;

    /**
     * @var bool
     */
    private $caseSensitive;

    /**
     * Contains constructor.
     * @param string $mustContain
     * @param null|string $description
     * @param bool $caseSensitive
     */
    public function __construct(string $mustContain, bool $caseSensitive = true, ?string $description = null)
    {
        parent::__construct($description);
        $this->caseSensitive = $caseSensitive;
        $this->mustContain = $mustContain;
    }


    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return sprintf(
            'value must contain: %s (%s)',
            $this->mustContain,
            $this->caseSensitive ? 'case-sensitive' : 'not case-sensitive'
        );
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
         $this->assertTrue($this->caseSensitive
             ? strpos($value, $this->mustContain) !== false
             : stripos($value, $this->mustContain) !== false);

         return $value;
    }
}
