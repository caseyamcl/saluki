<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class EmailAddress
 * @package Saluki\Validation\Rule
 */
class EmailAddress extends AbstractValidatorRule
{
    /**
     * @var bool
     */
    private $allowLocalhost;

    /**
     * EmailAddress constructor.
     * @param bool $allowLocalhost
     * @param null|string $description
     */
    public function __construct(bool $allowLocalhost = true, ?string $description = null)
    {
        parent::__construct($description);
        $this->allowLocalhost = $allowLocalhost;
    }


    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $validFilter = filter_var($value, FILTER_VALIDATE_EMAIL);

        if ($this->allowLocalhost) {
            list($local, $domain) = explode('@', $value, 2);
            $validLocal = filter_var($local . '@example.org', FILTER_VALIDATE_EMAIL);
            $test = $validFilter or ($validLocal && strtolower($domain) === 'localhost');
        } else {
            $test = $validFilter;
        }

        $this->assertTrue($test !== false);
        return $value;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be an email address';
    }
}
