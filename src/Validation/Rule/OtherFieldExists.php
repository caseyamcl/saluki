<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/27/18
 * Time: 3:14 PM
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class OtherFieldExists
 * @package Saluki\Validation\Rule
 */
class OtherFieldExists extends AbstractValidatorRule
{
    /**
     * @var string
     */
    private $otherFieldName;

    /**
     * OtherFieldExists constructor.
     * @param string $otherFieldName
     * @param null|string $description
     */
    public function __construct(string $otherFieldName, ?string $description = null)
    {
        parent::__construct($description);
        $this->otherFieldName = $otherFieldName;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return sprintf("value is only allowed if other field '%s' is specified", $this->otherFieldName);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue($context->hasField($this->otherFieldName));
        return $value;
    }
}
