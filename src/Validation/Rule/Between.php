<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class NumberBetween
 * @package Saluki\Validation\Rule
 */
class Between extends AbstractValidatorRule
{
    /**
     * @var float|int
     */
    private $a;
    /**
     * @var float|int
     */
    private $b;
    /**
     * @var bool
     */
    private $inclusive;

    /**
     * NumberBetween constructor.
     * @param int|float $a
     * @param int|float $b
     * @param bool $inclusive
     * @param null|string $description
     */
    public function __construct($a, $b, bool $inclusive, ?string $description = null)
    {
        parent::__construct($description);
        $this->a = $a;
        $this->b = $b;
        $this->inclusive = $inclusive;
    }


    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return sprintf('value must be between %d and %d', $this->a, $this->b);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue($this->inclusive
            ? ($value >= min([$this->a, $this->b]) && $value <= max([$this->a, $this->b]))
            : ($value > min([$this->a, $this->b]) && $value < max([$this->a, $this->b])));

        return $value;
    }
}
