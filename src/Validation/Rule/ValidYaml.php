<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class IpAddress
 * @package Saluki\Validation\Rule
 */
class ValidYaml extends AbstractValidatorRule
{
    /**
     * @var bool
     */
    private $decodeYaml;

    /**
     * ValidYaml constructor.
     * @param null|string $description
     * @param bool $decodeYaml
     */
    public function __construct(?string $description = null, bool $decodeYaml = false)
    {
        parent::__construct($description);
        $this->decodeYaml = $decodeYaml;
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed
     */
    public function __invoke($value, ValidatorContext $context)
    {
        try {
            $decoded = Yaml::parse($value, Yaml::PARSE_EXCEPTION_ON_INVALID_TYPE);
            return $this->decodeYaml ? $decoded : $value;
        } catch (ParseException $e) {
            throw $this->invalid($e);
        }
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be valid YAML';
    }
}
