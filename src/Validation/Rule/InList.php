<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class InList
 * @package Saluki\Validation\Rule
 */
class InList extends AbstractValidatorRule
{
    /**
     * @var array
     */
    private $list;

    /**
     * InList constructor.
     *
     * @param array $list
     * @param null|string $description
     */
    public function __construct(array $list, ?string $description = null)
    {
        parent::__construct($description);
        $this->list = $list;
    }


    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return (count($this->list) < 40)
            ? sprintf('value must be one of the following: %s', implode(', ', $this->list))
            : sprintf('value must be in the list of allowed values (%s items)', number_format(count($this->list)));
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $test = (array) $value;
        $diff = array_diff(array_values($test), $this->list);

        $this->assertTrue(count($diff) == 0);
        return $value;
    }
}
