<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/27/18
 * Time: 3:40 PM
 */

namespace Saluki\Validation\Rule;


use Saluki\Validation\ValidatorContext;

/**
 * Class IfNotOtherField
 * @package Saluki\Validation\Rule
 */
class IfNotOtherField extends IfOtherField
{
    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return sprintf("rules must pass if '%s' field is absent", $this->otherField);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        return ($context->hasField($this->otherField))
            ? $this->ruleSet->prepare($value, $context)
            : $value;
    }
}
