<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Rule;

use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\ValidatorContext;

/**
 * Class Min
 * @package Saluki\Validation\Rule
 */
class Min extends AbstractValidatorRule
{
    /**
     * @var int|float
     */
    private $min;

    /**
     * Min constructor.
     * @param int|float $min
     * @param null|string $description
     */
    public function __construct($min, ?string $description = null)
    {
        parent::__construct($description);
        $this->min = $min;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return 'value must be equal to or greater than ' . (float) $this->min;
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context)
    {
        $this->assertTrue((float) $value >= (float) $this->min);
        return $value;
    }
}
