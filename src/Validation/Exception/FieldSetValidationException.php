<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Exception;

/**
 * Class FieldSetValidationException
 */
class FieldSetValidationException extends ValidationBaseException
{
    /**
     * @var array|FieldValidationException[]
     */
    private $exceptions;

    /**
     * FieldSetValidationException constructor.
     * @param array|FieldValidationException $fieldValidationExceptions
     */
    public function __construct(array $fieldValidationExceptions)
    {
        array_map(function (FieldValidationException $e) {
            $this->exceptions[] = $e;
        }, $fieldValidationExceptions);

        parent::__construct('There were validation errors', 422);
    }

    /**
     * To serializable array
     *
     * @return array|array[]  Array of arrays of strings (keys are field names; values are errors per field)
     */
    public function toArray(): array
    {
        foreach ($this->exceptions as $exception) {
            $out[$exception->getFieldName()] = $exception->getErrors();
        }

        return $out ?? [];
    }

    /**
     * @return array|FieldValidationException[]
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }
}
