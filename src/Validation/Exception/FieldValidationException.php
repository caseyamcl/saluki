<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Exception;

/**
 * Class ValidationException
 */
class FieldValidationException extends ValidationBaseException
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var array
     */
    private $messages;

    /**
     * @param string $fieldName
     * @param ValidationException $exception
     * @return FieldValidationException
     */
    public static function fromValidationException(string $fieldName, ValidationException $exception)
    {
        return new static($fieldName, $exception->getErrors(), $exception->getCode());
    }

    /**
     * ValidationException constructor.
     *
     * @param string $fieldName
     * @param array $messages
     * @param int $code
     */
    public function __construct(string $fieldName, array $messages, int $code = 422)
    {
        $this->fieldName = $fieldName;
        $this->messages = $messages;

        parent::__construct(sprintf(
            "Validation exception(s) for field '%s': %s",
            $fieldName,
            implode('; ', $messages)
        ), $code);
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return array|string[]
     */
    public function getErrors(): array
    {
        return $this->messages;
    }
}
