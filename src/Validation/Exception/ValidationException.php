<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Exception;

/**
 * Class ValidationException
 */
class ValidationException extends ValidationBaseException
{
    /**
     * @var array|string[]
     */
    private $messages;

    /**
     * ValidationException constructor.
     *
     * @param array|string[] $messages Validation failure messages
     * @param int $code
     */
    public function __construct(array $messages, int $code = 422)
    {
        $this->messages = $messages;
        parent::__construct(sprintf("Validation exception(s): %s", implode('; ', $messages)), $code);
    }

    /**
     * Get the validation failure messages
     *
     * @return array|string[]
     */
    public function getErrors(): array
    {
        return $this->messages;
    }
}
