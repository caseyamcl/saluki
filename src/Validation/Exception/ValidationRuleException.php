<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Exception;

/**
 * Class ValidationRuleException
 * @package Saluki\Validation
 */
class ValidationRuleException extends ValidationBaseException
{
    /**
     * ValidationRuleException constructor.
     *
     * @param string $message
     * @param \Throwable|null $previous
     */
    public function __construct($message, \Throwable $previous = null)
    {
        parent::__construct($message, 422, $previous);
    }
}
