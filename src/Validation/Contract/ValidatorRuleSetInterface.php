<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/7/18
 * Time: 2:58 PM
 */

namespace Saluki\Validation\Contract;

use Saluki\Validation\ValidatorContext;

/**
 * Class ValidatorRuleSet
 */
interface ValidatorRuleSetInterface extends \Traversable
{
    /**
     * Is this field valid?
     *
     * @param string|mixed $value
     * @return bool
     */
    public function isValid($value): bool;

    /**
     * Get error messages for field
     *
     * @param string|mixed $value
     * @param null|ValidatorContext $context
     * @return array|string[]  An empty array means validation passed
     */
    public function getErrorMessages(&$value, ?ValidatorContext $context = null): array;

    /**
     * Validate and prepare value
     *
     * @param mixed $value
     * @param null|ValidatorContext $context
     * @return mixed  The prepared value
     */
    public function prepare($value, ?ValidatorContext $context = null);

    /**
     * @return array|ValidatorRuleInterface[]
     */
    public function listRules(): array;

    /**
     * @return iterable|ValueTransformerInterface[]
     */
    public function listBeforeTransformers(): iterable;

    /**
     * @return iterable|ValueTransformerInterface[]
     */
    public function listAfterTransformers(): iterable;

    /**
     * @param ValidatorRuleSetInterface $rules
     * @return mixed
     */
    public function addRuleSet(ValidatorRuleSetInterface $rules): void;

    /**
     * @param ValidatorRuleInterface $rule
     */
    public function addValidatorRule(ValidatorRuleInterface $rule): void;

    /**
     * @param ValueTransformerInterface $transformer
     */
    public function addTransformation(ValueTransformerInterface $transformer): void;
}
