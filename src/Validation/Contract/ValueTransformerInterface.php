<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Contract;

/**
 * Class ValidationFilter
 */
interface ValueTransformerInterface
{
    const BEFORE_VALIDATION = true;
    const AFTER_VALIDATION = false;

    /**
     * @param $value
     * @return mixed
     */
    public function __invoke($value);

    /**
     * Run before or after validation?
     *
     * @return bool
     */
    public function isBeforeValidation(): bool;
}
