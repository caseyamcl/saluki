<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Contract;

use Saluki\Validation\ValidatorContext;

/**
 * Class ValidatorRule
 */
interface ValidatorRuleInterface
{
    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return mixed  Return a prepared value
     */
    public function __invoke($value, ValidatorContext $context);

    /**
     * Get a description of the rule; also used as error message
     *
     * Usually begins with 'value must be...'
     *
     * @return string
     */
    public function __toString(): string;
}
