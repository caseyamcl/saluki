<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/7/18
 * Time: 2:59 PM
 */

namespace Saluki\Validation\Contract;

/**
 * Class ValidatorField
 */
interface ValidatorFieldInterface extends ValidatorRuleSetInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return bool
     */
    public function isRequired(): bool;

    /**
     * @return bool
     */
    public function isOptional(): bool;

    /**
     * @return mixed|null
     */
    public function getDefault();

    /**
     * @return bool
     */
    public function hasDefault(): bool;

    /**
     * @return string
     */
    public function getDescription(): string;
}
