<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation;

use Doctrine\Persistence\ObjectRepository;
use Saluki\Utility\UnpackCSV;
use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\Rule;

/**
 * Validator Built-In Rules
 *
 */
trait ValidatorBuiltInRules
{
    /**
     * @param ValidatorRuleInterface $rule
     * @return self
     */
    abstract public function addRule(ValidatorRuleInterface $rule): self;

    /**
     * @param ValueTransformerInterface $transformer
     * @return self
     */
    abstract public function addTransformer(ValueTransformerInterface $transformer): self;

    /**
     * @param callable $transformer
     * @param bool $isBeforeValidation
     * @return self
     */
    public function transform(callable $transformer, bool $isBeforeValidation = true): self
    {
        return $this->addTransformer(new ValueTransformer($transformer, $isBeforeValidation));
    }

    /**
     * Add a callable rule (alias for 'callback')
     *
     * Accepts: (value, ValidatorContext $context) and must return BOOL
     *
     * @param callable $callback
     * @param string $message
     * @return self
     */
    public function rule(callable $callback, ?string $message = null): self
    {
        return $this->callback($callback, $message);
    }

    /**
     * Process callback  Accepts: (value, ValidatorContext $context) and must return BOOL
     *
     * @param callable $callback
     * @param string $message
     * @return self
     */
    public function callback(callable $callback, ?string $message = null): self
    {
        return $this->addRule(new Rule\CallbackRule($callback, $message));
    }

    /**
     * Add a custom callback rule   Accepts: (value, ValidatorContext $context) and must return prepared value
     *
     * Throw ValidationRuleException if validation fails
     *
     * @param callable $callback
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function custom(callable $callback, ?string $message = null): self
    {
        return $this->addRule(new Rule\CustomRule($callback, $message));
    }

    /**
     * @param $equals
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function mustEqual($equals, ?string $message = null): self
    {
        return $this->addRule(new Rule\EqualTo($equals, $message));
    }

    /**
     * Ensure valid YAML
     *
     * @param string $message
     * @param bool $parseYaml
     * @return self
     */
    public function yaml(?string $message = null, bool $parseYaml = false): self
    {
        return $this->addRule(new Rule\ValidYaml($message, $parseYaml));
    }

    /**
     * Ensure valid JSON
     *
     * @param string $message
     * @param bool $parseJson
     * @return self
     */
    public function json(?string $message = null, bool $parseJson = false): self
    {
        return $this->addRule(new Rule\ValidJson($message, $parseJson));
    }

    /**
     * Ensure date is between two values
     *
     * @param \DateTimeInterface|string $earliest
     * @param \DateTimeInterface|string $latest
     * @param string $message
     * @param bool $convertToChronos
     * @return self
     */
    public function dateBetween($earliest, $latest, ?string $message = null, bool $convertToChronos = true): self
    {
        return $this->addRule(new Rule\DateBetween($earliest, $latest, $message, $convertToChronos));
    }

    /**
     * Ensure date falls after specified value
     *
     * @param \DateTimeInterface|string $date
     * @param string $message
     * @param bool $convertToChronos
     * @return self
     */
    public function dateAfter($date, ?string $message = null, bool $convertToChronos = true): self
    {
        return $this->addRule(new Rule\DateAfter($date, $message, $convertToChronos));
    }

    /**
     * Ensure date falls before specified value
     *
     * @param \DateTimeInterface|string $date
     * @param string $message
     * @param bool $convertToChronos
     * @return self
     */
    public function dateBefore($date, ?string $message = null, bool $convertToChronos = true): self
    {
        return $this->addRule(new Rule\DateBefore($date, $message, $convertToChronos));
    }

    /**
     * Validate if a value is temporal
     *
     * @param string $message
     * @param bool $convertToChronos
     * @return self
     */
    public function temporal(?string $message = null, bool $convertToChronos = true): self
    {
        return $this->addRule(new Rule\Temporal($message, $convertToChronos));
    }

    /**
     * @param int|null $min
     * @param int|null $max
     * @param string $message
     * @return self
     */
    public function length(?int $min, ?int $max = null, ?string $message = null): self
    {
        return $this->addRule(new Rule\Length($min, $max, $message));
    }

    /**
     * Interpret yes/no, true/false, 'true'/'false', on/off, 1/0 and map to PHP boolean
     *
     * @param string $message
     * @param bool $convertToBoolean
     * @return self
     */
    public function boolean(?string $message = null, bool $convertToBoolean = true): self
    {
        return $this->addRule(new Rule\Boolean($message, $convertToBoolean));
    }

    /**
     * Trim value before validation
     */
    public function trim(): self
    {
        return $this->addTransformer(new ValueTransformer('trim', true));
    }

    /**
     * @param string $regex
     * @param string $message
     * @return self
     */
    public function regex(string $regex, ?string $message = null): self
    {
        return $this->addRule(new Rule\Regex($regex, $message));
    }

    /**
     * @param string $message
     * @return self
     */
    public function url(?string $message = null): self
    {
        return $this->addRule(new Rule\Url($message));
    }

    /**
     * @param string $message
     * @return self
     */
    public function ipAddress(?string $message = null): self
    {
        return $this->addRule(new Rule\IpAddress($message));
    }

    /**
     * @param null|string $message
     * @return self
     */
    public function unixPath(?string $message = null): self
    {
        return $this->addRule(new Rule\UnixPath($message));
    }

    /**
     * @param bool $allowLocalhost
     * @param string $message
     * @return self
     */
    public function emailAddress(bool $allowLocalhost = true, ?string $message = null): self
    {
        return $this->addRule(new Rule\EmailAddress($allowLocalhost, $message));
    }

    /**
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function emailLocal(?string $message = null): self
    {
        return $this->addRule(new Rule\EmailLocal(true, $message));
    }

    /**
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function isList(?string $message = null): self
    {
        $this->addTransformer(new ValueTransformer(function ($value) {
            return (is_iterable($value)) ? $value : (array) $value;
        }));
        return $this->addRule(new Rule\IsList($message));
    }

    /**
     * @param array $allowed
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function keysInList(array $allowed, ?string $message = null): self
    {
        return $this->addRule(new Rule\KeysInList($allowed, $message));
    }

    /**
     * @param ValidatorRuleSet $ruleSet
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function each(ValidatorRuleSet $ruleSet, ?string $message = null): self
    {
        return $this->addRule(new Rule\Each($ruleSet, $message));
    }

    /**
     * Add a sub-value rule
     *
     * @param ValidatorFieldSet $fieldSet
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function subValues(ValidatorFieldSet $fieldSet, ?string $message = null): self
    {
        return $this->addRule(new Rule\SubValues($fieldSet, $message));
    }

    /**
     * @return ValidatorBuiltInRules
     */
    public function sanitize(): self
    {
        return $this->addTransformer(new ValueTransformer(function ($value) {
            return filter_var($value, FILTER_UNSAFE_RAW);
        }));
    }

    /**
     * Ensure value is not blank; this includes empty spaces, EOLS, and other empty-ish strings
     *
     * @param string $message
     * @return self
     */
    public function notBlank(?string $message = null): self
    {
        return $this->addRule(new Rule\NotBlank($message));
    }

    /**
     * @param iterable $rules
     * @param string $message
     * @return self
     */
    public function andX(iterable $rules, ?string $message = null): self
    {
        return $this->addRule(new Rule\AndX($rules, $message));
    }

    /**
     *
     * @param iterable|ValidatorRuleInterface[] $rules
     * @param string $message
     * @return self
     */
    public function orX(iterable $rules, ?string $message = null): self
    {
        return $this->addRule(new Rule\OrX($rules, $message));
    }

    /**
     * Ensure numeric value is between two values
     *
     * @param int|float $a
     * @param int|float $b
     * @param bool $inclusive
     * @param string $message
     * @return self
     */
    public function between($a, $b, bool $inclusive = false, ?string $message = null): self
    {
        return $this->addRule(new Rule\Between($a, $b, $inclusive, $message));
    }

    /**
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function integer(?string $message = null): self
    {
        return $this->addRule(new Rule\Integer($message));
    }

    /**
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function numeric(?string $message = null): self
    {
        return $this->addRule(new Rule\Numeric($message));
    }

    /**
     * @param string $message
     * @return self
     */
    public function naturalNumber(?string $message = null): self
    {
        return $this->addRule(new Rule\NaturalNumber($message));
    }

    /**
     * Ensure does not contain
     *
     * @param string $contains
     * @param bool $caseSensitive
     * @param string $message
     * @return self
     */
    public function doesNotContain(string $contains, bool $caseSensitive = true, $message = ''): self
    {
        return $this->addRule(new Rule\DoesNotContain($contains, $caseSensitive, $message));
    }

    /**
     * Ensure string value contains a substring
     *
     * @param string $contains
     * @param bool $caseSensitive
     * @param string $message
     * @return self
     */
    public function contains(string $contains, bool $caseSensitive = true, $message = ''): self
    {
        return $this->addRule(new Rule\Contains($contains, $caseSensitive, $message));
    }

    /**
     * Ensure numeric value is greater than or equal to value
     *
     * @param int|float $min
     * @param string $message
     * @return self
     */
    public function min($min, $message = ''): self
    {
        return $this->addRule(new Rule\Min($min, $message));
    }

    /**
     * @param bool $allowLocalhost
     * @param null|string $message
     * @param bool $tldCheck
     * @return ValidatorBuiltInRules
     */
    public function domain(bool $allowLocalhost = true, ?string $message = null, bool $tldCheck = false): self
    {
        return $this->addRule(new Rule\DomainName($allowLocalhost, $message, $tldCheck));
    }

    /**
     * Ensure numeric value is less than or equal to value
     *
     * @param int|float $max
     * @param string $message
     * @return self
     */
    public function max($max, ?string $message = null): self
    {
        return $this->addRule(new Rule\Max($max, $message));
    }

    /**
     * Ensure alphanumeric
     *
     * @param string $additionalChars
     * @param string $message
     * @return self
     */
    public function alphanumeric(string $additionalChars = '', ?string $message = null): self
    {
        return $this->addRule(new Rule\Alphanumeric($message, $additionalChars));
    }

    /**
     * Disallow certain characters
     *
     * @param string $charList
     * @param string $message
     * @return self
     */
    public function disallowChars(string $charList, ?string $message = null): self
    {
        return $this->addRule(new Rule\DisallowCharacters($charList, $message));
    }

    /**
     * @param array $list
     * @param string $message
     * @return self
     */
    public function inList(array $list, ?string $message = null): self
    {
        return $this->addRule(new Rule\InList($list, $message));
    }

    /**
     * @param array $list
     * @param string $message
     * @return self
     */
    public function notInList(array $list, ?string $message = null): self
    {
        return $this->addRule(new Rule\NotInList($list, $message));
    }

    /**
     * @param string $separator
     * @return ValidatorBuiltInRules
     */
    public function csvList(string $separator = ','): self
    {
        return $this->transform(function ($value) use ($separator): array {
            return UnpackCSV::un($value, [$separator]);
        });
    }

    /**
     * @param string $propertyName
     * @param ObjectRepository $repository
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function unique(string $propertyName, ObjectRepository $repository, ?string $message = null): self
    {
        return $this->addRule(new Rule\Unique($propertyName, $repository, $message));
    }

    /**
     * @param int $minScore
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function zxcvbn(int $minScore = 2, ?string $message = null): self
    {
        return $this->addRule(new Rule\Zxcvbn($minScore, $message));
    }

    /**
     * @param string $fieldName
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function otherFieldExists(string $fieldName, ?string $message = null): self
    {
        return $this->addRule(new Rule\OtherFieldExists($fieldName, $message));
    }

    /**
     * @param string $fieldName
     * @param null|string $message
     * @return ValidatorBuiltInRules
     */
    public function otherFieldAbsent(string $fieldName, ?string $message = null): self
    {
        return $this->addRule(new Rule\OtherFieldAbsent($fieldName, $message));
    }
}
