<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation\Behavior;

use Cake\Chronos\Chronos;
use Saluki\Validation\Exception\ValidationRuleException;

/**
 * Class DateValidatorTrait
 * @package Saluki\Validation\Behavior
 */
trait DateValidatorTrait
{
    /**
     * Helper function to interpret a date constraint value
     *
     * @param string|\DateTimeInterface $date
     * @return Chronos
     */
    protected function interpretDateValue($date): Chronos
    {
        if ($date instanceof \DateTimeInterface) {
            return Chronos::instance($date);
        } else {
            try {
                if ($date = Chronos::parse($date)) {
                    return $date;
                } else {
                    throw new ValidationRuleException('Could not interpret date constraint: ' .  $date);
                }
            } catch (\Throwable $e) {
                throw new ValidationRuleException('Could not interpret date constraint: ' .  $date, $e);
            }
        }
    }
}
