<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation;

use ArrayIterator;
use IteratorAggregate;
use ReflectionMethod;
use RuntimeException;
use Saluki\Validation\Contract\ValidatorFieldInterface;
use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\Exception\FieldSetValidationException;
use Saluki\Validation\Exception\FieldValidationException;
use Traversable;

/**
 * Class ValidatorFieldSet
 */
class ValidatorFieldSet implements IteratorAggregate
{
    /**
     * @var array|ValidatorField[]
     */
    private $fields = [];

    /**
     * @var ReflectionMethod
     */
    private $getErrorsMethod;

    /**
     * ValidatorFieldSet constructor.
     * @param iterable $fields
     */
    public function __construct(iterable $fields = [])
    {
        $this->addFields($fields);
    }

    /**
     * Add multiple fields
     * @param iterable|ValidatorField[] $fields
     * @return static|ValidatorFieldSet
     */
    public function addFields(iterable $fields): self
    {
        foreach ($fields as $field) {
            $this->add($field);
        }
        return $this;
    }

    /**
     * @param string $fieldName
     * @param bool $required
     * @param iterable|ValidatorRuleInterface[] $rules
     * @return ValidatorField
     */
    public function field(string $fieldName, bool $required = true, iterable $rules = []): ValidatorFieldInterface
    {
        return $this->add(new ValidatorField($fieldName, $required))->addRules($rules);
    }

    /**
     * @param string $fieldName
     * @param bool $allowBlank
     * @param iterable $rules
     * @return ValidatorField
     */
    public function required(string $fieldName, bool $allowBlank = false, iterable $rules = []): ValidatorFieldInterface
    {
        $field = $this->field($fieldName, true, $rules);
        if (! $allowBlank) {
            $field->notBlank();
        }
        return $field;
    }

    /**
     * @param string $fieldName
     * @param null $defaultValue
     * @param iterable $rules
     * @return ValidatorField
     */
    public function optional(string $fieldName, $defaultValue = null, iterable $rules = []): ValidatorFieldInterface
    {
        $field = $this->field($fieldName, false, $rules);
        if ($defaultValue) {
            $field->setDefault($defaultValue);
        }
        return $field;
    }

    /**
     * Add field with rules, or add rules to existing field
     *
     * @param ValidatorFieldInterface $field
     * @return ValidatorField|ValidatorFieldInterface
     */
    public function add(ValidatorFieldInterface $field): ValidatorFieldInterface
    {
        if (isset($this->fields[$field->getName()])) {
            $this->fields[$field->getName()]->addRuleSet($field);
        } else {
            $this->fields[$field->getName()] = $field;
        }

        return $this->fields[$field->getName()];
    }

    /**
     * Get error messages
     *
     * @param iterable $values
     * @param null|ValidatorContext $context
     * @param bool $allowUndefinedFields
     * @return array|array[]  Array of field errors; empty array means validation succeeded
     */
    public function getErrors(
        iterable &$values,
        ?ValidatorContext $context = null,
        bool $allowUndefinedFields = false
    ): array {

        $context = $context ? $context->withFields($values) : new ValidatorContext($values);

        try {
            $values = $this->getValidatableFields($values, $allowUndefinedFields);
        } catch (FieldValidationException $e) {
            $errors[] = $e->getErrors();
        }

        // First, run before transformers on all values that were passed.
        foreach ($values as $name => $value) {
            if ($this->hasField($name)) {
                foreach ($this->getField($name)->listBeforeTransformers() as $transformer) {
                    $values[$name] = $transformer($value);
                }
            }
        }

        // Next, get the error messages each field.
        foreach ($this->fields as $fieldName => $fieldRuleSet) {
            if (array_key_exists($fieldName, $values)) {
                $errors[$fieldName] = $fieldRuleSet->getErrorMessagesForPreparedValue($values[$fieldName], $context);
            } elseif ($fieldRuleSet->isRequired()) {
                $errors[$fieldName][] = 'this field is required';
            }
        }

        // Finally, run after transformers on all values that were passed.
        foreach ($values as $name => $value) {
            if ($this->hasField($name)) {
                foreach ($this->getField($name)->listAfterTransformers() as $transformer) {
                    $values[$name] = $transformer($value);
                }
            }
        }

        return array_filter($errors ?? []);
    }

    /**
     * @param iterable $values
     * @param null|ValidatorContext $context
     * @param bool $allowUndefinedFields
     * @return array  Prepared and validated values
     */
    public function prepare(
        iterable $values,
        ?ValidatorContext $context = null,
        bool $allowUndefinedFields = false
    ): array {

        $defaults = [];

        // First get validatable fields (any exceptions added to exceptions array
        try {
            $values = $this->getValidatableFields($values, $allowUndefinedFields);
        } catch (FieldValidationException $e) {
            $exceptions[] = $e;
        }

        // First, run before transformers on all values that were passed.
        foreach ($values as $name => $value) {
            if ($this->hasField($name)) {
                foreach ($this->getField($name)->listBeforeTransformers() as $transformer) {
                    $values[$name] = $transformer($values[$name]);
                }
            }
        }

        $context = $context ? $context->withFields($values) : new ValidatorContext($values);

        // Next, get the error messages each field.
        foreach ($this->fields as $fieldName => $fieldRuleSet) {
            $errorList = [];
            if (array_key_exists($fieldName, $values)) {
                $errorList = $fieldRuleSet->getErrorMessagesForPreparedValue($values[$fieldName], $context);
            } elseif ($fieldRuleSet->hasDefault()) {
                $defaults[$fieldName] = $fieldRuleSet->getDefault();
            } elseif ($fieldRuleSet->isRequired()) {
                $errorList = ['this field is required'];
            }

            if (! empty($errorList)) {
                $exceptions[] = new FieldValidationException($fieldName, $errorList);
            }
        }

        // Next, run after transformers on all values that were passed.
        foreach ($values as $name => $value) {
            if ($this->hasField($name)) {
                foreach ($this->getField($name)->listAfterTransformers() as $transformer) {
                    $values[$name] = $transformer($values[$name]);
                }
            }
        }

        // Throw or return
        if (isset($exceptions)) {
            throw new FieldSetValidationException($exceptions);
        } else {
            return array_merge($values, $defaults);
        }
    }

    /**
     * Get validatable fields
     *
     * Converts any iterator into an array, and checks to ensure that extra fields are not present
     *
     * @param iterable|Traversable $values
     * @param bool $allowUndefinedFields
     * @return array
     * @throws FieldValidationException  If allowUndefinedFields is FALSE and undefined fields exist in 'values'
     */
    private function getValidatableFields(iterable $values, bool $allowUndefinedFields = false): array
    {
        if ($allowUndefinedFields) {
            return is_array($values) ? $values : iterator_to_array($values);
        }

        // if not an array, make it one.
        if ($values instanceof Traversable) {
            $values = iterator_to_array($values);
        }

        $diff = array_diff(array_keys($values), array_keys($this->fields));

        // Check for undefined keys
        if (! empty($diff)) {
            throw new FieldValidationException('_general', [sprintf(
                'Invalid parameters: %s (allowed: %s)',
                implode(', ', $diff),
                implode(', ', array_keys($this->fields))
            )]);
        }

        return $values;
    }

    /**
     * @return ArrayIterator|ValidatorField[]
     */
    public function getIterator()
    {
        return new ArrayIterator($this->fields);
    }

    /**
     * @param string $fieldName
     * @return bool
     */
    public function hasField(string $fieldName): bool
    {
        return array_key_exists($fieldName, $this->fields);
    }

    /**
     * @param string $fieldName
     * @return ValidatorField
     */
    public function getField(string $fieldName): ValidatorField
    {
        if (array_key_exists($fieldName, $this->fields)) {
            return $this->fields[$fieldName];
        } else {
            throw new RuntimeException('Non-existent validator field: ' . $fieldName);
        }
    }
}
