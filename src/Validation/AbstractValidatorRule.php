<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation;

use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\Exception\ValidationRuleException;

/**
 * Class AbstractValidatorRule
 *
 * @package Saluki\Validation
 */
abstract class AbstractValidatorRule implements ValidatorRuleInterface
{
    /**
     * @var string
     */
    private $description;

    /**
     * AbstractValidatorRule constructor.
     *
     * @param null|string $description
     */
    public function __construct(?string $description = null)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->description ?: $this->getDefaultDescription();
    }

    /**
     * @param \Throwable $priorError
     * @return ValidationRuleException
     */
    protected function invalid(?\Throwable $priorError = null): ValidationRuleException
    {
        return new ValidationRuleException($this->__toString(), $priorError);
    }

    /**
     * If the value is not TRUE
     *
     * @param mixed $value
     * @throws ValidationRuleException
     */
    protected function assertTrue($value): void
    {
        if ($value !== true) {
            throw $this->invalid();
        }
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    abstract public function getDefaultDescription(): string;
}
