<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation;

use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\Contract\ValidatorRuleSetInterface;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\Exception\ValidationException;
use Saluki\Validation\Exception\ValidationRuleException;
use Traversable;

/**
 * Class ValidatorRuleSet
 */
class ValidatorRuleSet implements \IteratorAggregate, ValidatorRuleSetInterface
{
    use ValidatorBuiltInRules;

    /**
     * @var array|callable[]|ValidatorRuleInterface[]
     */
    private $rules = [];

    /**
     * @var array|ValueTransformer[]
     */
    private $beforeTransformers = [];

    /**
     * @var array|ValueTransformer[]
     */
    private $afterTransformers = [];

    /**
     * @return ValidatorRuleSet
     */
    public static function new()
    {
        return new static();
    }

    /**
     * ValidatorRuleSet constructor.
     * @param iterable $rules
     */
    public function __construct(iterable $rules = [])
    {
        foreach ($rules as $rule) {
            $this->addRule($rule);
        }
    }

    /**
     * Add a rule to the end of the stack
     *
     * @param ValidatorRuleInterface $rule
     * @return self|ValidatorRuleSet
     */
    public function addRule(ValidatorRuleInterface $rule): self
    {
        $this->rules[] = $rule;
        return $this;
    }

    /**
     * Prepend a rule to the front of the stack
     *
     * @param ValidatorRuleInterface $rule
     * @return ValidatorRuleSet
     */
    public function prependRule(ValidatorRuleInterface $rule): self
    {
        array_unshift($this->rules, $rule);
        return $this;
    }

    /**
     * @param ValueTransformerInterface $transformer
     * @return self|ValidatorRuleSet
     */
    public function addTransformer(ValueTransformerInterface $transformer): self
    {
        $transformer->isBeforeValidation()
            ? $this->beforeTransformers[] = $transformer
            : $this->afterTransformers[] = $transformer;

        return $this;
    }

    /**
     * Add rules
     *
     * @param iterable $rules
     * @return ValidatorRuleSet
     */
    public function addRules(iterable $rules): self
    {
        foreach ($rules as $rule) {
            $this->addRule($rule);
        }

        return $this;
    }

    /**
     * Merge an existing set into this rule set
     *
     * @param ValidatorRuleSetInterface $rules
     */
    public function addRuleSet(ValidatorRuleSetInterface $rules): void
    {
        foreach ($rules as $rule) {
            $this->addRule($rule);
        }
    }

    /**
     * Add a validator rule (without returning self)
     *
     * @param ValidatorRuleInterface $rule
     */
    public function addValidatorRule(ValidatorRuleInterface $rule): void
    {
        $this->addRule($rule);
    }

    /**
     * Add transformer (without returning self)
     *
     * @param ValueTransformerInterface $transformer
     */
    public function addTransformation(ValueTransformerInterface $transformer): void
    {
        $this->addTransformer($transformer);
    }


    /**
     * Is this field valid?
     *
     * @param string|mixed $value
     * @return bool
     */
    public function isValid($value): bool
    {
        return $this->getErrorMessages($value) === [];
    }

    /**
     * Get error messages for field
     *
     * @param string|mixed $value
     * @param null|ValidatorContext $context
     * @return array|string[]  An empty array means validation passed
     */
    public function getErrorMessages(&$value, ?ValidatorContext $context = null): array
    {
        // Process 'before' filters, so that we validate based on consistent rules
        foreach ($this->beforeTransformers as $filter) {
            $value = $filter($value);
        }

        $messages = $this->getErrorMessagesForPreparedValue($value, $context);

        // Process 'after' filters
        foreach ($this->afterTransformers as $transform) {
            $value = $transform($value);
        }

        return $messages;
    }

    /**
     * Validate and prepare value
     *
     * @param mixed $value
     * @param null|ValidatorContext $context
     * @return mixed
     */
    public function prepare($value, ?ValidatorContext $context = null)
    {
        $errors = $this->getErrorMessages($value, $context);
        if (! empty($errors)) {
            throw new ValidationException($errors);
        }
        return $value;
    }

    /**
     * @param mixed $preparedValue  Value that has had all of the 'before' transformers run
     * @param null|ValidatorContext $context
     * @return array|string[]
     */
    public function getErrorMessagesForPreparedValue(&$preparedValue, ?ValidatorContext $context): array
    {
        if (! $context) {
            $context = ValidatorContext::singleValue($preparedValue);
        }

        foreach ($this->rules as $rule) {
            try {
                $preparedValue = $rule($preparedValue, $context);
            } catch (ValidationRuleException $e) {
                $errors[] = $e->getMessage();
            }
        }

        return $errors ?? [];
    }

    /**
     * Invoke validator; prepare value
     *
     * @param mixed $value
     * @return mixed
     * @throws ValidationException  If validation failure
     */
    public function __invoke($value)
    {
        return $this->prepare($value);
    }

    /**
     * @return array|ValidatorRuleInterface[]
     */
    public function listRules(): array
    {
        return $this->rules;
    }

    /**
     * @return iterable|ValueTransformerInterface[]
     */
    public function listBeforeTransformers(): iterable
    {
        return $this->beforeTransformers;
    }

    /**
     * @return iterable|ValueTransformerInterface[]
     */
    public function listAfterTransformers(): iterable
    {
        return $this->afterTransformers;
    }

    /**
     * @return \ArrayIterator|Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->rules);
    }
}
