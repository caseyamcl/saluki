<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Validation;

use Saluki\Validation\Contract\ValidatorFieldInterface;
use Saluki\Validation\Exception\FieldValidationException;
use Saluki\Validation\Exception\ValidationException;

/**
 * Class ValidatorField
 */
class ValidatorField extends ValidatorRuleSet implements ValidatorFieldInterface
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * @var bool
     */
    private $required;

    /**
     * @var mixed|null
     */
    private $default = null;

    /**
     * @var string
     */
    private $description = '';

    /**
     * @param string $fieldName
     * @param bool $required
     * @return ValidatorField
     */
    public static function new(string $fieldName = '', bool $required = true)
    {
        return new static($fieldName, $required);
    }


    /**
     * ValidatorField constructor.
     * @param string $fieldName
     * @param bool $required
     * @param ValidatorRuleSet $ruleSet  RuleSet to start with (null to use blank rule set)
     */
    public function __construct(string $fieldName, bool $required = true, ValidatorRuleSet $ruleSet = null)
    {
        $this->fieldName = $fieldName;
        $this->required  = $required;
        parent::__construct($ruleSet ? $ruleSet->listRules() : []);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @return bool
     */
    public function isOptional(): bool
    {
        return ! $this->required;
    }

    /**
     * @param mixed $default
     * @return self|ValidatorField
     */
    public function setDefault($default): self
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return self|ValidatorField
     */
    public function removeDefault(): self
    {
        $this->default = null;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @return bool
     */
    public function hasDefault(): bool
    {
        return (! is_null($this->default));
    }


    /**
     * @param bool $isRequired
     * @return static
     */
    public function setRequired(bool $isRequired): self
    {
        $this->required = $isRequired;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return static
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Validate and prepare value
     *
     * @param mixed $value
     * @param null|ValidatorContext $context
     * @return mixed
     */
    public function prepare($value, ?ValidatorContext $context = null)
    {
        try {
            return parent::prepare($value, $context);
        } catch (ValidationException $e) {
            throw FieldValidationException::fromValidationException($this->getName(), $e);
        }
    }
}
