<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Events\Registry;

use Saluki\Container\Contract\SalukiRegistryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EventSubscriberRegistry implements \IteratorAggregate, SalukiRegistryInterface
{
    /**
     * @var \ArrayObject|EventSubscriberInterface[]
     */
    private $items;

    /**
     * EventSubscriberRegistry constructor.
     */
    public function __construct()
    {
        $this->items = new \ArrayObject();
    }

    /**
     * @param EventSubscriberInterface $eventSubscriber
     */
    public function add(EventSubscriberInterface $eventSubscriber): void
    {
        $this->items[] = $eventSubscriber;
    }

    /**
     * Add an item to the registry
     *
     * @param EventSubscriberInterface $item
     */
    public function addItem($item): void
    {
        $this->add($item);
    }

    /**
     * @return \ArrayObject|EventSubscriberInterface[]
     */
    public function getIterator()
    {
        return $this->items;
    }
}
