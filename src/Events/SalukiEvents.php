<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Events;

final class SalukiEvents
{
    /*
     * Dispatched directly after a transaction has completed and data has been written to the database
     */
    const TRANSACTION_COMPLETE = 'transaction.complete';

    /*
     * Dispatched directly after a transaction has failed
     */
    const TRANSACTION_FAILED = 'transaction.failed';
}
