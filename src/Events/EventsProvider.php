<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Events;

use Psr\Container\ContainerInterface;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Events\Factory\EventDispatcherFactory;
use Saluki\Events\Registry\EventSubscriberRegistry;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EventsProvider
 * @package Saluki\Events
 */
class EventsProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addRegistry(EventSubscriberRegistry::class, EventSubscriberInterface::class);

        $builder->addServiceDefinition(
            EventDispatcherInterface::class,
            \DI\factory(EventDispatcherFactory::class)
        );

        // Manually invoke event dispatcher on build to ensure all listeners and subscribers are registered
        $builder->addBuildCallback(function (ContainerInterface $container) {
            $container->get(EventDispatcherInterface::class);
        });
    }
}
