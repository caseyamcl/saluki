<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/12/18
 * Time: 4:23 PM
 */

namespace Saluki\Events\Factory;

use Saluki\Events\Registry\EventSubscriberRegistry;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class EventDispatcherFactory
 * @package Saluki\Events\Factory
 */
class EventDispatcherFactory
{
    public function __invoke(EventSubscriberRegistry $registry): EventDispatcherInterface
    {
        $dispatcher =  new EventDispatcher();
        foreach ($registry as $subscriber) {
            $dispatcher->addSubscriber($subscriber);
        }
        return $dispatcher;
    }
}
