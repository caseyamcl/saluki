<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\ExternalService\Contract;

use Saluki\ExternalService\Exception\ExternalServiceException;

/**
 * Class ExternalServiceInterface
 * @package CaseyAMcL\Saluki\ExternalService\Contract
 */
interface ExternalServiceInterface
{
    /**
     * Ensure that the service is functional.
     *
     * This should throw an ExternalServiceException if anything fails
     *
     * @return void
     * @throws ExternalServiceException
     */
    public function ensureFunctional(): void;

    /**
     * Get slug (machine name) for service
     *
     * @return string
     */
    public function getSlug(): string;

    /**
     * Get (human-friendly) service name
     * @return mixed
     */
    public function getName(): string;

    /**
     * Reset the service state between transactional boundaries
     *
     * For example, clear the database cache between HTTP requests, or
     * reset a Doctrine identity map
     *
     * @return void
     */
    public function reset(): void;
}
