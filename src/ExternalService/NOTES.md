Just about every application connects to external services, and thus, there are some common
utilities and concept that each app could benefit from:

1. The ability to reset external service state between requests (e.g. Doctrine: `clear()` method)
2. The ability to test if the external service is available, configured, and working correctly.
