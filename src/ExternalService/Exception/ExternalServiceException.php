<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\ExternalService\Exception;

/**
 * Class ExternalServiceException
 * @package Saluki\ExternalService\Exception
 */
class ExternalServiceException extends \RuntimeException
{
    /**
     * Connection failed
     *
     * @param string $message
     * @return ExternalServiceException|static
     */
    public static function connectionFailure(string $message = ''): ExternalServiceException
    {
        return new static($message ?: 'Connection failed');
    }

    /**
     * Configuration failed
     *
     * @param string $message
     * @return ExternalServiceException
     */
    public static function configurationFailure(string $message = ''): ExternalServiceException
    {
        return new static($message ?: 'Configuration error');
    }
}
