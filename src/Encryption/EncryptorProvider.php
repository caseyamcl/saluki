<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Encryption;

use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Encryption\Contract\EncryptorInterface;
use Saluki\Encryption\Factory\EncryptorFactory;
use Saluki\Encryption\SetupStep\EnsureEncryptionKeySetupStep;

/**
 * Class EncryptorProvider
 * @package Saluki\Encryption
 */
class EncryptorProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addSetupStep(EnsureEncryptionKeySetupStep::class);
        $builder->addServiceDefinition(EncryptorInterface::class, \DI\factory(new EncryptorFactory()));
    }
}
