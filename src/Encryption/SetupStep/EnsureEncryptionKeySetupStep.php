<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 2:08 PM
 */

namespace Saluki\Encryption\SetupStep;

use Configula\Exception\ConfigValueNotFoundException;
use Configula\Exception\InvalidConfigValueException;
use Defuse\Crypto\Exception\BadFormatException;
use Defuse\Crypto\Key;
use Saluki\Config\Model\Config;
use Saluki\Console\ConsoleIO;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;
use Saluki\Setup\Step\SetupEnvironment;

/**
 * Class EnsureEncryptionKeySetupStep
 * @package Saluki\Encryption\SetupStep
 */
class EnsureEncryptionKeySetupStep implements SetupStepInterface
{
    /**
     * @var Config
     */
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return bool
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     */
    public function isSetup(): bool
    {
        try {
            $key = $this->config->getEncryptionKey(true);
            Key::loadFromAsciiSafeString($key);
            return true;
        } catch (ConfigValueNotFoundException | InvalidConfigValueException | BadFormatException $e) {
            return false;
        }
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::PRE_CONTAINER;
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        try {
            $key = $this->config->getEncryptionKey(true);
            Key::loadFromAsciiSafeString($key);
            return SetupStepResult::success(sprintf(
                'Encryption key setup (...<info>%s</info>)',
                substr($key, -6)
            ));
        } catch (ConfigValueNotFoundException | InvalidConfigValueException $e) {
            $io->note('If you need, you can use this key:');
            $io->note(sprintf('<info>%s</info>', Key::createNewRandomKey()->saveToAsciiSafeString()));
            return SetupStepResult::fail('Encryption key not found in config');
        } catch (BadFormatException $e) {
            return SetupStepResult::fail('Invalid encryption key in config');
        }
    }

    /**
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [SetupEnvironment::class];
    }
}
