<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Encryption\Factory;

use Saluki\Config\Model\Config;
use Saluki\Container\Model\SalukiParams;
use Saluki\Encryption\Contract\EncryptorInterface;
use Saluki\Encryption\Service\DefuseEncryptor;
use Saluki\Encryption\Service\PlainTextEncryptor;

/**
 * Class EncrypterFactory
 * @package Saluki\Encryption\Factory
 */
class EncryptorFactory
{
    /**
     * Build an encryptor from configuration
     *
     * @param Config $config
     * @return EncryptorInterface
     */
    public function __invoke(Config $config): EncryptorInterface
    {
        return ($config->isEncryptionEnabled())
            ? new DefuseEncryptor($config->getEncryptionKey(true))
            : new PlainTextEncryptor();
    }
}
