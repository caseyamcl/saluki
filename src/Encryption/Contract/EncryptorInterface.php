<?php

/*
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Encryption\Contract;

interface EncryptorInterface
{
    /**
     * @param string $plaintextValue
     * @return string
     */
    public function encrypt(string $plaintextValue): string;

    /**
     * @param string $encryptedValue
     * @return string
     */
    public function decrypt(string $encryptedValue): string;
}
