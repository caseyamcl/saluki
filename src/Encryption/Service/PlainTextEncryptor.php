<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Encryption\Service;

use Saluki\Encryption\Contract\EncryptorInterface;

class PlainTextEncryptor implements EncryptorInterface
{
    /**
     * Must accept data and return encrypted data
     * @param string $plaintextValue
     * @return string
     */
    public function encrypt(string $plaintextValue): string
    {
        return $plaintextValue;
    }

    /**
     * Must accept data and return decrypted data
     * @param string $encryptedValue
     * @return string
     */
    public function decrypt(string $encryptedValue): string
    {
        return $encryptedValue;
    }
}
