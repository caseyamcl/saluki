<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Encryption\Service;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;
use Saluki\Encryption\Contract\EncryptorInterface;

/**
 * Class Encrypter
 * @package Saluki\Encryption\Service
 */
class DefuseEncryptor implements EncryptorInterface
{
    private Key $key;

    /**
     * Encrypter constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = Key::loadFromAsciiSafeString($key);
    }

    /**
     * Encrypt a string
     *
     * @param string $plaintextValue
     * @return string
     */
    public function encrypt(string $plaintextValue): string
    {
        return Crypto::encrypt($plaintextValue, $this->key);
    }

    /**
     * Decrypt a string
     *
     * @param string $encryptedValue
     * @return string
     */
    public function decrypt(string $encryptedValue): string
    {
        return Crypto::decrypt($encryptedValue, $this->key);
    }
}
