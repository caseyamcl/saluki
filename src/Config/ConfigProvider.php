<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 2:22 PM
 */

namespace Saluki\Config;

use Saluki\Config\Setup\ConfigCheckLogLevelSetupStep;
use Saluki\Config\Setup\ConfigCheckRuntimeModeSetupStep;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;

/**
 * Class ConfigProvider
 * @package Saluki\Config
 */
class ConfigProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addSetupStep(ConfigCheckLogLevelSetupStep::class);
        $builder->addSetupStep(ConfigCheckRuntimeModeSetupStep::class);
    }
}
