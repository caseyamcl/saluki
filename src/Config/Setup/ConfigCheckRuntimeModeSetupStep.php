<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 2:06 PM
 */

namespace Saluki\Config\Setup;

use Saluki\Config\Model\Config;
use Saluki\Console\ConsoleIO;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;
use Saluki\Setup\Step\SetupEnvironment;

/**
 * Class ConfigSetupPhase
 * @package Saluki\Config
 */
class ConfigCheckRuntimeModeSetupStep implements SetupStepInterface
{
    private Config $config;

    /**
     * ConfigSetupPhase constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::PRE_CONTAINER;
    }

    /**
     * @return iterable|string[]
     */
    public static function getDependsOn(): array
    {
        return [SetupEnvironment::class];
    }

    /**
     * @return bool
     */
    public function isSetup(): bool
    {
        return true;
    }


    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        return SetupStepResult::note(sprintf(
            'Runtime mode is %s',
            $this->config->isDevMode() ? '<fg=red>development</fg=red>' : '<fg=green>production</fg=green>'
        ));
    }
}
