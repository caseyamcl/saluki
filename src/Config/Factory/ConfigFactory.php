<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Config\Factory;

use Configula\ConfigValues;
use Configula\Loader\ArrayValuesLoader;
use Configula\Loader\CascadingConfigLoader;
use Configula\ConfigLoaderInterface;
use LogicException;
use Saluki\Config\Model\Config;

class ConfigFactory
{
    /**
     * @var iterable|ConfigLoaderInterface[]
     */
    private $loaders = [];
    /**
     * @var string
     */
    private $configClass;

    /**
     * ConfigFactory constructor.
     * @param array|ConfigLoaderInterface[] $loaders
     * @param array $defaults
     * @param string $configClass
     */
    public function __construct(array $loaders, array $defaults = [], string $configClass = Config::class)
    {
        if (! is_a($configClass, ConfigValues::class, true)) {
            throw new LogicException($configClass . ' must inherit from or implement ' . ConfigValues::class);
        }

        if ($defaults) {
            $this->loaders[] = new ArrayValuesLoader($defaults);
        }

        $this->loaders = array_merge($this->loaders, $loaders);
        $this->configClass = $configClass;
    }

    /**
     * @return Config
     */
    public function __invoke(): Config
    {
        $loader = new CascadingConfigLoader($this->loaders);
        $values = $loader->load();

        return call_user_func($this->configClass . '::fromConfigValues', $values);
    }
}
