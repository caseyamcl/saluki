<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Config\Model;

use Configula\ConfigValues;
use Configula\Exception\InvalidConfigValueException;
use Monolog\Logger;
use Psr\Log\LogLevel;

/**
 * Class Config
 * @package Saluki\Config
 */
class Config extends ConfigValues
{
    public const DEFAULT_LOG_LEVEL   = 'info';
    public const DEFAULT_IS_DEV_MODE = 'auto';

    /**
     * @return bool
     */
    public function isDevMode(): bool
    {
        $mode = $this->get('dev_mode', static::DEFAULT_IS_DEV_MODE);
        return ($mode === 'auto') ? (bool) getenv('DEV_MODE') : (bool) $mode;
    }

    /**
     * @return bool
     */
    public function isEncryptionEnabled(): bool
    {
        return $this->get('encryption_enabled', true);
    }

    /**
     * @return string
     */
    public function getLogLevel(): string
    {
        return $this->get('log_level', static::DEFAULT_LOG_LEVEL);
    }

    /**
     * @return int
     */
    public function getLogLevelInt(): int
    {
        $logLevelName = $this->getLogLevel();
        $constName = Logger::class . '::' . strtoupper($logLevelName);
        if (defined($constName)) {
            return constant($constName);
        } else {
            throw new InvalidConfigValueException('Invalid log level: ' . $logLevelName);
        }
    }

    /**
     * @return bool
     */
    public function isDebugLogLevel(): bool
    {
        return $this->getLogLevelInt() <= LogLevel::DEBUG;
    }

    /**
     * @param bool $requireKey
     * @return string
     */
    public function getEncryptionKey(bool $requireKey = false): string
    {
        if ($key = trim($this->find('encryption_key'))) {
            return $key;
        } elseif ($requireKey) {
            throw new InvalidConfigValueException('Encryption key (config: encryption_key) cannot be empty');
        } else {
            return '';
        }
    }

    /**
     * @return array
     */
    public function getSettings(): array
    {
        return (array) $this->get('settings', []);
    }
}
