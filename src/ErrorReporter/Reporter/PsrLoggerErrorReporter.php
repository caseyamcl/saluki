<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ErrorReporter\Reporter;

use Monolog\Utils;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Saluki\ErrorReporter\Contract\ErrorReporterInterface;
use Throwable;

/**
 * Class PsrLoggerErrorReporter
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class PsrLoggerErrorReporter implements ErrorReporterInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PsrLoggerErrorReporter constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Report an error
     *
     * @param Throwable $e
     * @param array $extra
     */
    public function report(Throwable $e, array $extra = []): void
    {
        // This logic was copied from ErrorReporter
        $msg = sprintf(
            'Exception %s: "%s" at %s line %s',
            Utils::getClass($e),
            $e->getMessage(),
            $e->getFile(),
            $e->getLine()
        );

        // Log Message
        $this->logger->log(LogLevel::ERROR, $msg, array_merge(['exception' => $e], $extra));
    }
}
