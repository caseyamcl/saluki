<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ErrorReporter\Contract;

use Throwable;

/**
 * Interface ErrorReporterInterface
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
interface ErrorReporterInterface
{
    /**
     * Report an error
     *
     * @param Throwable $e
     * @param array $extra  Extra, contextual information to optionally pass along to the error reporter
     */
    public function report(Throwable $e, array $extra = []): void;
}
