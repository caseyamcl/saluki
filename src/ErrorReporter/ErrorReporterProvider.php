<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ErrorReporter;

use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\ErrorReporter\Contract\ErrorReporterInterface;
use Saluki\ErrorReporter\Registry\ErrorReporterRegistry;
use Saluki\ErrorReporter\Service\ErrorService;

/**
 * Class ErrorReporterProvider
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class ErrorReporterProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addRegistry(ErrorReporterRegistry::class, ErrorReporterInterface::class);
        $builder->addServiceDefinition(ErrorService::class);
    }
}
