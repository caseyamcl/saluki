<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ErrorReporter\Service;

use Saluki\ErrorReporter\Registry\ErrorReporterRegistry;
use Throwable;

/**
 * Class ErrorReporter
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class ErrorService
{
    /**
     * @var ErrorReporterRegistry
     */
    private $reporters;

    /**
     * ErrorReporter constructor.
     * @param ErrorReporterRegistry $reporters
     */
    public function __construct(ErrorReporterRegistry $reporters)
    {
        $this->reporters = $reporters;
    }

    /**
     * Report an error
     *
     * @param Throwable $e
     * @param array $extra
     */
    public function report(Throwable $e, array $extra = []): void
    {
        foreach ($this->reporters->getIterator() as $reporter) {
            $reporter->report($e, $extra);
        }
    }
}
