<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\ErrorReporter\Registry;

use Saluki\Container\Contract\SalukiRegistryInterface;
use Saluki\ErrorReporter\Contract\ErrorReporterInterface;

/**
 * Class ErrorReporterRegistry
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class ErrorReporterRegistry implements \IteratorAggregate, SalukiRegistryInterface
{
    /**
     * @var \ArrayObject|ErrorReporterInterface[]
     */
    private $items;

    /**
     * ErrorReporterRegistry constructor.
     */
    public function __construct()
    {
        $this->items = new \ArrayObject();
    }

    /**
     * @param ErrorReporterInterface $errorReporter
     */
    public function add(ErrorReporterInterface $errorReporter): void
    {
        // Don't add same object instance twice.
        if (! in_array($errorReporter, $this->items->getArrayCopy(), true)) {
            $this->items[] = $errorReporter;
        }
    }

    /**
     * Add an item to the registry
     *
     * @param ErrorReporterInterface $item
     */
    public function addItem($item): void
    {
        $this->add($item);
    }

    /**
     * @return \ArrayObject|ErrorReporterInterface[]
     */
    public function getIterator(): iterable
    {
        return clone $this->items;
    }
}
