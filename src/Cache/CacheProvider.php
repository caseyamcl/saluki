<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *   For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *   ------------------------------------------------------------------
 */

namespace Saluki\Cache;

use Psr\Cache\CacheItemPoolInterface;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Symfony\Component\Cache\Adapter\ApcuAdapter;

/**
 * Class CacheProvider
 * @package Saluki\Cache
 */
class CacheProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        $builder->addServiceDefinition(CacheItemPoolInterface::class, \DI\autowire(ApcuAdapter::class));
    }
}
