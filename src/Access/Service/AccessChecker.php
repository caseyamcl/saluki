<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Access\Service;

use Saluki\Access\Contract\AccessCheckerInterface;
use Saluki\Access\Registry\PermissionRegistry;
use Saluki\Authentication\Contract\UserInterface;

/**
 * Class AccessChecker
 * @package Saluki\Access\Service
 */
class AccessChecker implements AccessCheckerInterface
{
    /**
     * @var PermissionRegistry
     */
    private $registry;

    /**
     * AccessChecker constructor.
     * @param PermissionRegistry $registry
     */
    public function __construct(PermissionRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * Is the user allowed to do a thing
     *
     * @param string $permission
     * @param UserInterface $user
     * @return bool
     */
    public function isAllowed(string $permission, UserInterface $user): bool
    {
        $perm = $this->registry->get($permission);
        return count(array_intersect($perm->listRoles(), $user->getRoles())) > 0;
    }
}
