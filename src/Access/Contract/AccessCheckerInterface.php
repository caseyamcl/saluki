<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Access\Contract;

use Saluki\Authentication\Contract\UserInterface;

/**
 * Interface AccessCheckerInterface
 * @package Saluki\Access\Contract
 */
interface AccessCheckerInterface
{
    /**
     * @param string $permission
     * @param UserInterface $user
     * @return bool
     */
    public function isAllowed(string $permission, UserInterface $user): bool;
}
