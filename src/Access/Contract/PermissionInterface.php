<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/6/18
 * Time: 1:52 PM
 */

namespace Saluki\Access\Contract;

/**
 * Interface PermissionInterface
 * @package Saluki\Authentication\Contract
 */
interface PermissionInterface
{
    /**
     * List roles that have this permission
     *
     * @return array|string[]
     */
    public function listRoles(): array;

    /**
     * Get human-friendly description of permission
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Is this permission configurable by administrators?
     *
     * @return bool
     */
    public function isConfigurable(): bool;
}
