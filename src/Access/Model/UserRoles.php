<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/5/18
 * Time: 3:17 PM
 */

namespace Saluki\Access\Model;

use Saluki\Access\Contract\UserRolesInterface;

/**
 * Class UserRoles
 * @package Saluki\Authentication\Model
 */
class UserRoles implements UserRolesInterface
{
    // Build
    public const ADMIN = 'admin';

    /**
     * @return array|string[]
     */
    public static function list(): array
    {
        return (new static())->getUserRoles();
    }

    /**
     * Get available user roles
     *
     * @return array|string[]
     * @throws \ReflectionException
     */
    public function getUserRoles(): array
    {
        return array_values((new \ReflectionClass(get_called_class()))->getConstants());
    }
}
