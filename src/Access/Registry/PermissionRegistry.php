<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Access\Registry;

use Saluki\Access\Contract\PermissionInterface;
use Saluki\Container\Contract\SalukiRegistryInterface;

/**
 * Class PermissionRegistry
 * @package Saluki\Authentication\Registry
 */
class PermissionRegistry implements SalukiRegistryInterface
{
    /**
     * @var array|PermissionInterface[]
     */
    private $items = [];

    /**
     * @param string $className
     * @return PermissionInterface
     */
    public function get(string $className): PermissionInterface
    {
        if ($this->has($className)) {
            return $this->items[$className];
        } else {
            throw new \RuntimeException("No permission exists: " . $className);
        }
    }

    public function has(string $className): bool
    {
        return array_key_exists($className, $this->items);
    }

    /**
     * @param PermissionInterface $permission
     */
    public function set(PermissionInterface $permission): void
    {
        $this->items[get_class($permission)] = $permission;
    }

    /**
     * Add an item to the registry
     *
     * @param PermissionInterface $item
     */
    public function addItem($item): void
    {
        $this->set($item);
    }
}
