<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/6/18
 * Time: 2:08 PM
 */

namespace Saluki\Access;

use Saluki\Access\Contract\AccessCheckerInterface;
use Saluki\Access\Contract\PermissionInterface;
use Saluki\Access\Contract\UserRolesInterface;
use Saluki\Access\Model\UserRoles;
use Saluki\Access\Registry\PermissionRegistry;
use Saluki\Access\Service\AccessChecker;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;

/**
 * Class AccessProvider
 * @package Saluki\Access
 */
class AccessProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        // Permission registry
        $builder->addRegistry(PermissionRegistry::class, PermissionInterface::class);

        // User roles service
        $builder->addServiceDefinition(UserRolesInterface::class, \DI\autowire(UserRoles::class));
        $builder->addServiceDefinition(AccessCheckerInterface::class, \DI\autowire(AccessChecker::class));
    }
}
