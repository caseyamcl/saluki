<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Endpoint;

use Saluki\RestResource\AbstractEndpoint\AbstractIndexEndpoint;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Model\IndexListOptions;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\WebInterface\RequestParam\ParameterList;

/**
 * Class IndexEndpoint
 * @package Saluki\RestResource\Model
 */
class IndexEndpoint extends AbstractIndexEndpoint
{
    // This can be overridden in child classes
    public const DISALLOWED_QUERY_PARAM_NAMES = ['page', 'sort', 'fields', 'include'];

    /**
     * @var RestResourceInterface
     */
    private $resource;

    /**
     * IndexEndpoint constructor.
     * @param RestResourceInterface $resource
     */
    public function __construct(RestResourceInterface $resource)
    {
        $this->resource = $resource;
    }

    /**
     * @return ParameterList
     */
    public function listQueryParameters(): ParameterList
    {
        $list = parent::listQueryParameters();
        $list->addFields($this->resource->listIndexQueryFields());
        return $list;
    }

    /**
     * @param RestRequestContext $requestContext
     * @return bool
     */
    protected function authorizeIndex(RestRequestContext $requestContext): bool
    {
        return call_user_func($this->resource->getAccessCallback('index'), $requestContext);
    }

    /**
     * Retrieve item count that would be returned without pagination
     *
     * @param RestRequestContext $context
     * @return int
     */
    protected function retrieveTotalItemCount(RestRequestContext $context): int
    {
        return $this->resource->count($context->getRequest()->getQueryParams(), $context);
    }

    /**
     * Retrieve items (apply pagination if applicable)
     *
     * @param RestRequestContext $requestContext
     * @param IndexListOptions $listOptions
     * @return iterable  The records
     */
    protected function retrieveItems(RestRequestContext $requestContext, IndexListOptions $listOptions): iterable
    {
        return $this->resource->list($requestContext->getRequest()->getQueryParams(), $requestContext, $listOptions);
    }

    /**
     * @return array
     */
    public function getAllowedSortFields(): array
    {
        return $this->resource->getAllowedSortFields();
    }

    /**
     * Get default pagination limit
     *
     * @return int|null  NULL means no pagination limit
     */
    public function getDefaultPaginationLimit(): ?int
    {
        return $this->resource->getDefaultPaginationLimit();
    }

    /**
     * Get maximum pagination limit
     *
     * @return int|null
     */
    public function getMaximumPaginationLimit(): ?int
    {
        return $this->resource->getMaximumPaginationLimit();
    }

    /**
     * Get default sort
     *
     * @return array  Values are ['field' => 'asc/desc']
     */
    public function getDefaultSort(): array
    {
        return $this->resource->getDefaultSort();
    }

    // --------------------------------------------------------------

    public function listDefaultIncludes(): array
    {
        return $this->resource->listDefaultIncludes();
    }

    public function listDefaultFields(): array
    {
        return [$this->resource->getName() => $this->resource->getFieldList()->listDefaultFieldNames()];
    }
}
