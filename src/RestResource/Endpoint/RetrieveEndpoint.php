<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Endpoint;

use Saluki\RestResource\AbstractEndpoint\AbstractRetrieveEndpoint;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Model\RestRequestContext;

class RetrieveEndpoint extends AbstractRetrieveEndpoint
{
    /**
     * @var RestResourceInterface
     */
    private $resource;

    /**
     * RetrieveEndpoint constructor.
     * @param RestResourceInterface $resource
     */
    public function __construct(RestResourceInterface $resource)
    {
        $this->resource = $resource;
    }

    /**
     * @param RestRequestContext $requestContext
     * @return object|null
     */
    protected function retrieveRecord(RestRequestContext $requestContext)
    {
        return $this->resource->retrieve(
            $requestContext->getRequest()->getPathSegment($this->resource->getIdName()),
            $requestContext
        );
    }

    /**
     * @param RestRequestContext $requestContext
     * @param object $record
     * @return bool
     */
    protected function authorizeAccess(RestRequestContext $requestContext, $record): bool
    {
        return call_user_func($this->resource->getAccessCallback('retrieve'), $requestContext, $record);
    }

    // --------------------------------------------------------------

    public function listDefaultIncludes(): array
    {
        return $this->resource->listDefaultIncludes();
    }

    public function listDefaultFields(): array
    {
        return [$this->resource->getName() => $this->resource->getFieldList()->listDefaultFieldNames()];
    }
}
