<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/1/18
 * Time: 12:07 PM
 */

namespace Saluki\RestResource\Endpoint;

use Psr\Http\Message\ResponseInterface;
use Saluki\RestResource\AbstractEndpoint\AbstractWriteEndpoint;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Event\RestObjectEvent;
use Saluki\RestResource\Event\RestRequestEvents;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\WebInterface\Model\SalukiRequest;
use Saluki\WebInterface\RequestParam\ParameterList;

/**
 * Class CreateEndpoint
 * @package Saluki\RestResource\Model
 */
class CreateEndpoint extends AbstractWriteEndpoint
{
    private RestResourceInterface $resource;

    /**
     * IndexEndpoint constructor.
     * @param RestResourceInterface $resource
     */
    public function __construct(RestResourceInterface $resource)
    {
        $this->resource = $resource;
    }

    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        // If the response was successful, then force 201 status
        $response = parent::__invoke($request);
        return ($response->getStatusCode() === 200) ? $response->withStatus(201) : $response;
    }

    /**
     * Authorize access to create records of this type
     * @param RestRequestContext $requestContext
     * @param null $record
     * @return bool
     */
    protected function authorizeAccess(RestRequestContext $requestContext, $record = self::NEW_RECORD): bool
    {
        return call_user_func($this->resource->getAccessCallback('create'), $requestContext);
    }

    // --------------------------------------------------------------

    public function listDefaultIncludes(): array
    {
        return $this->resource->listDefaultIncludes();
    }

    public function listDefaultFields(): array
    {
        return [$this->resource->getName() => $this->resource->getFieldList()->listDefaultFieldNames()];
    }

    /**
     * Does this endpoint expect the entire resource to be transmitted ('PUT' and 'POST' do, 'PATCH' does not)
     * @return bool
     */
    protected function expectsEntireResource(): bool
    {
        return true;
    }

    /**
     * Does this endpoint expect the ID field present ('PUT' and 'PATCH' do, 'POST' does not)
     *
     * @return bool
     */
    protected function expectsId(): bool
    {
        return false;
    }

    /**
     * @return ParameterList
     */
    protected function listRelationshipParameters(): ParameterList
    {
        return $this->resource->getFieldList()->listRelationshipParams(! $this->expectsEntireResource());
    }

    /**
     * List available parameters
     *
     * @return ParameterList
     */
    protected function listAttributeParameters(): ParameterList
    {
        return $this->resource->getFieldList()->listAttributeParams(! $this->expectsEntireResource());
    }

    /**
     * @return string
     */
    protected function getValidTypeName(): string
    {
        return $this->resource->getName();
    }

    /**
     * @param $record
     * @return string
     */
    protected function getIdFromItem($record): string
    {
        // pass.. not strictly necessary for CREATE endpoints
        return '';
    }

    /**
     * Process the record
     *
     * @param RestRequestContext $requestContext
     * @param object|null $record
     * @return object
     */
    protected function process(RestRequestContext $requestContext, $record = self::NEW_RECORD)
    {
        $record = $this->resource->create($requestContext);
        $this->resource->getEventDispatcher()->dispatch(
            new RestObjectEvent($record, $requestContext),
            RestRequestEvents::create(get_class($record))
        );
        return $record;
    }

    /**
     * Retrieve record for PATCH requests or PUT requests that update the resource
     *
     * @param RestRequestContext $request
     * @return void
     */
    protected function retrieveRecord(RestRequestContext $request)
    {
        throw new \LogicException('this should not be called here');
    }
}
