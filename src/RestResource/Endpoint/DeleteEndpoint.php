<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/1/18
 * Time: 12:47 PM
 */

namespace Saluki\RestResource\Endpoint;

use Saluki\RestResource\AbstractEndpoint\AbstractDeleteEndpoint;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Event\RestObjectEvent;
use Saluki\RestResource\Event\RestRequestEvents;
use Saluki\RestResource\Model\RestRequestContext;

/**
 * Class DeleteEndpoint
 * @package Saluki\RestResource\Model
 */
class DeleteEndpoint extends AbstractDeleteEndpoint
{
    private RestResourceInterface $resource;

    /**
     * IndexEndpoint constructor.
     * @param RestResourceInterface $resource
     */
    public function __construct(RestResourceInterface $resource)
    {
        $this->resource = $resource;
    }

    /**
     * @param RestRequestContext $requestContext
     * @return object|null
     */
    protected function retrieveRecord(RestRequestContext $requestContext)
    {
        return $this->resource->retrieve(
            $requestContext->getRequest()->getPathSegment($this->resource->getIdName()),
            $requestContext
        );
    }

    /**
     * @param RestRequestContext $requestContext
     * @param object $record
     * @return bool
     */
    protected function authorizeAccess(RestRequestContext $requestContext, $record): bool
    {
        return call_user_func($this->resource->getAccessCallback('delete'), $requestContext, $record);
    }

    /**
     * @param $record
     * @param RestRequestContext $requestContext
     */
    protected function delete($record, RestRequestContext $requestContext): void
    {
        $this->resource->delete($record, $requestContext);

        $this->resource->getEventDispatcher()->dispatch(
            new RestObjectEvent($record, $requestContext),
            RestRequestEvents::delete(get_class($record))
        );
    }

    // --------------------------------------------------------------

    public function listDefaultIncludes(): array
    {
        return $this->resource->listDefaultIncludes();
    }

    public function listDefaultFields(): array
    {
        return [$this->resource->getName() => $this->resource->getFieldList()->listDefaultFieldNames()];
    }
}
