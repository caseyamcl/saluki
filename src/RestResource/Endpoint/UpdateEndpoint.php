<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Endpoint;

use Saluki\RestResource\AbstractEndpoint\AbstractWriteEndpoint;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Event\RestFieldUpdateEvent;
use Saluki\RestResource\Event\RestObjectUpdateEvent;
use Saluki\RestResource\Event\RestRequestEvents;
use Saluki\RestResource\Model\FieldChangeTracker;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\WebInterface\RequestParam\ParameterList;

/**
 * Class UpdateEndpoint
 * @package Saluki\RestResource\Endpoint
 */
class UpdateEndpoint extends AbstractWriteEndpoint
{
    /**
     * @var RestResourceInterface
     */
    private $resource;

    /**
     * IndexEndpoint constructor.
     * @param RestResourceInterface $resource
     */
    public function __construct(RestResourceInterface $resource)
    {
        $this->resource = $resource;
    }

    /**
     * @return string
     */
    protected function getValidTypeName(): string
    {
        return $this->resource->getName();
    }

    /**
     * @param $record
     * @return string
     */
    protected function getIdFromItem($record): string
    {
        return $this->resource->getIdFromItem($record);
    }

    /**
     * @return ParameterList
     */
    protected function listAttributeParameters(): ParameterList
    {
        return $this->resource->getFieldList()->listAttributeParams(true);
    }

    protected function listRelationshipParameters(): ParameterList
    {
        return $this->resource->getFieldList()->listRelationshipParams(true);
    }

    /**
     * @param RestRequestContext $requestContext
     * @return object|null
     */
    protected function retrieveRecord(RestRequestContext $requestContext)
    {
        return $this->resource->retrieve(
            $requestContext->getRequest()->getPathSegment($this->resource->getIdName()),
            $requestContext
        );
    }

    /**
     * @param RestRequestContext $requestContext
     * @param object $record
     * @return bool
     */
    protected function authorizeAccess(RestRequestContext $requestContext, $record = null): bool
    {
        if (is_null($record)) {
            throw new \LogicException();
        }
        return call_user_func($this->resource->getAccessCallback('update'), $requestContext, $record);
    }

    // --------------------------------------------------------------

    public function listDefaultIncludes(): array
    {
        return $this->resource->listDefaultIncludes();
    }

    public function listDefaultFields(): array
    {
        return [$this->resource->getName() => $this->resource->getFieldList()->listDefaultFieldNames()];
    }

    /**
     * Does this endpoint expect the entire resource to be transmitted ('PUT' and 'POST' do, 'PATCH' does not)
     * @return bool
     */
    protected function expectsEntireResource(): bool
    {
        return false;
    }

    /**
     * Does this endpoint expect the ID field present ('PUT' and 'PATCH' do, 'POST' does not)
     *
     * @return bool
     */
    protected function expectsId(): bool
    {
        return true;
    }

    /**
     * Process the record
     *
     * @param RestRequestContext $requestContext
     * @param object|null $record
     * @return object|null  If NULL, then throws 404
     */
    protected function process(RestRequestContext $requestContext, $record = self::NEW_RECORD)
    {
        if (is_null($record)) {
            throw new \LogicException();
        }

        $changeTracker = new FieldChangeTracker(get_class($record));
        $updated = $this->resource->update($record, $requestContext, $changeTracker);
        $this->resource->getEventDispatcher()->dispatch(
            new RestObjectUpdateEvent($updated, $requestContext, $changeTracker->listChanges()),
            RestRequestEvents::update(get_class($updated))
        );

        if ($changeTracker->hasChanges()) {
            $this->resource->getEventDispatcher()->dispatch(
                new RestObjectUpdateEvent($updated, $requestContext, $changeTracker->listChanges()),
                RestRequestEvents::anyFieldChange(get_class($updated))
            );
        }

        foreach ($changeTracker->listChanges() as $fieldName => [$old, $new]) {
            $this->resource->getEventDispatcher()->dispatch(
                new RestFieldUpdateEvent(
                    $old,
                    $new,
                    $fieldName,
                    $updated,
                    $requestContext,
                    $this->resource->getFieldList()->findField($fieldName)->propertyName
                ),
                RestRequestEvents::fieldChange(get_class($updated), $fieldName)
            );
        }

        return $updated;
    }
}
