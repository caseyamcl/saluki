<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Exception;

/**
 * Record Conflict Exception
 *
 * Thrown when client attempts to create a record that conflicts with an existing record (e.g.
 * integrity constraint violation in database)
 *
 * This translates to a HTTP 409 Exception
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class RecordConflictException extends \RuntimeException
{
    // pass..
}
