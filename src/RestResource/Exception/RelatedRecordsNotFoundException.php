<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/19/18
 * Time: 2:32 PM
 */

namespace Saluki\RestResource\Exception;

use Saluki\WebInterface\Model\JsonApiError;

class RelatedRecordsNotFoundException extends \RuntimeException
{
    /**
     * @var array|RelatedRecordNotFoundException[]
     */
    private $exceptions = [];

    /**
     * RelatedRecordsNotFoundException constructor.
     * @param iterable|RelatedRecordNotFoundException[] $errors
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(iterable $errors, string $message = '', int $code = 0, \Throwable $previous = null)
    {
        foreach ($errors as $error) {
            $this->add($error);
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * @param RelatedRecordNotFoundException $error
     */
    private function add(RelatedRecordNotFoundException $error)
    {
        $this->exceptions[] = $error;
    }

    /**
     * @return array|JsonApiError[]
     */
    public function getJsonApiErrors(): array
    {
        foreach ($this->exceptions as $e) {
            $out[] = new JsonApiError(
                sprintf('related record not found: %s %s', $e->getType(), $e->getId()),
                404,
                '',
                $e->getDataPath()
            );
        }

        return $out ?? [];
    }

    /**
     * @return array|RelatedRecordNotFoundException[]
     */
    public function getExceptions()
    {
        return $this->exceptions;
    }

    /**
     * @return array|string[]
     */
    public function getErrorMessages(): array
    {
        foreach ($this->exceptions as $error) {
            $out[] = $error->getMessage();
        }
        return $out ?? [];
    }
}
