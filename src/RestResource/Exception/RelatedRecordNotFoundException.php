<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/19/18
 * Time: 2:19 PM
 */

namespace Saluki\RestResource\Exception;

use Saluki\RestResource\Model\RelationshipParamValue;
use Throwable;

/**
 * Class RelatedRecordNotFoundException
 * @package Saluki\RestResource\Exception
 */
class RelatedRecordNotFoundException extends \RuntimeException
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $dataPath;

    /**
     * @param RelationshipParamValue $relationship
     * @param string $dataPath
     * @return RelatedRecordNotFoundException
     */
    public static function fromParam(RelationshipParamValue $relationship, string $dataPath = '')
    {
        return new static($relationship->getType(), $relationship->getId(), $dataPath);
    }

    /**
     * RelatedRecordNotFoundException constructor.
     * @param string $type
     * @param string $id
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     * @param string $dataPath
     */
    public function __construct(
        string $type,
        string $id,
        string $dataPath = '',
        string $message = "",
        int $code = 0,
        Throwable $previous = null
    ) {
        $this->type = $type;
        $this->id = $id;
        $this->dataPath = $dataPath;

        parent::__construct($message ?: "related record of type '$type' not found: $id", $code, $previous);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDataPath(): string
    {
        return $this->dataPath;
    }

    /**
     * @return RelatedRecordNotFoundException
     */
    public function withDataPath(string $dataPath): RelatedRecordNotFoundException
    {
        $that = clone $this;
        $that->dataPath = $dataPath;
        return $that;
    }
}
