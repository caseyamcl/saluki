<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/6/18
 * Time: 4:29 PM
 */

namespace Saluki\RestResource\Exception;

use Throwable;

/**
 * Class FieldAccessDeniedException
 * @package Saluki\RestResource\Exception
 */
class FieldAccessDeniedException extends \RuntimeException
{
    /**
     * @var string
     */
    private $fieldName;

    /**
     * FieldAccessDeniedException constructor.
     * @param string $fieldName
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $fieldName,
        string $message = "",
        int $code = 0,
        Throwable $previous = null
    ) {
        $this->fieldName = $fieldName;
        parent::__construct($message ?: 'Access denied for field: ' . $fieldName, $code, $previous);
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }
}
