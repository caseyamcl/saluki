<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Behavior;

use Saluki\RestResource\Field\Field;
use Saluki\Serialization\Model\NormalizerContext;
use Symfony\Component\PropertyAccess\PropertyAccessor;

trait ReadFieldValueTrait
{
    /**
     * Read field value
     *
     * @param Field $field
     * @param $item
     * @param NormalizerContext $context -- TODO: This can really just be a user instance
     * @return mixed
     */
    protected function readFieldValue(Field $field, $item, NormalizerContext $context)
    {
        if ($field->readable->getterCallback) {
            return call_user_func($field->readable->getterCallback, $item, $context);
        } elseif ($field->readable->propertyName) {
            return $this->getPropertyAccess()->getValue($item, $field->readable->propertyName);
        } else {
            throw new \RuntimeException(sprintf(
                'Cannot retrieve value %s from class %s',
                $field->name,
                get_class($item)
            ));
        }
    }

    abstract protected function getPropertyAccess(): PropertyAccessor;
}
