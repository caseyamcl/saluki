<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/9/18
 * Time: 4:25 PM
 */

namespace Saluki\RestResource\Event;

use Saluki\RestResource\Model\RestRequestContext;
use Saluki\WebInterface\Model\SalukiRequest;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class FieldUpdateEvent
 *
 * This is dispatched when a REST field is changed
 *
 * @package Saluki\RestResource\Event
 */
class RestFieldUpdateEvent extends GenericEvent
{
    /**
     * @var mixed
     */
    private $oldValue;

    /**
     * @var mixed
     */
    private $newValue;

    private object $object;
    private string $fieldName;
    private ?string $propertyName;
    private RestRequestContext $requestContext;

    /**
     * Field update event
     *
     * @param $oldValue
     * @param $newValue
     * @param string $fieldName
     * @param string $propertyName
     * @param $object
     * @param RestRequestContext $requestContext
     */
    public function __construct(
        $oldValue,
        $newValue,
        string $fieldName,
        $object,
        RestRequestContext $requestContext,
        ?string $propertyName = null
    ) {
        parent::__construct();
        $this->oldValue = $oldValue;
        $this->newValue = $newValue;
        $this->requestContext = $requestContext;
        $this->object = $object;
        $this->fieldName = $fieldName;
        $this->propertyName = $propertyName;
    }

    /**
     * @return mixed
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @return mixed
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @return bool
     */
    public function wasChanged(): bool
    {
        return $this->oldValue !== $this->newValue;
    }

    /**
     * Get the object being worked on
     *
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return SalukiRequest
     */
    public function getRequest(): SalukiRequest
    {
        return $this->requestContext->getRequest();
    }

    /**
     * @return RestRequestContext
     */
    public function getRequestContext(): RestRequestContext
    {
        return $this->requestContext;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return string|null
     */
    public function getPropertyName(): ?string
    {
        return $this->propertyName;
    }
}
