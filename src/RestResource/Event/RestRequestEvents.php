<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Event;

use Saluki\Events\SalukiEvents;
use Saluki\Persistence\Event\EntityEvent;
use Saluki\Persistence\Event\EntityPropertyEvent;
use Saluki\Persistence\EventMapper\EntityEventAdapter;
use Saluki\RestResource\Field\FieldList;
use Saluki\RestResource\Field\FieldListMapper;
use Saluki\RestResource\Model\RestRequestContext;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class RequestEventSubscriber
 * @package Saluki\RestResource\Model
 */
final class RestRequestEvents
{
    public static function anyAction(string $entityClass, bool $includePropertyEvents = false)
    {
        return sprintf('rest.%s.%s', $entityClass, $includePropertyEvents ? '#' : '*');
    }

    /**
     * Create event for given entity
     *
     * @param string $entityClass
     * @return string
     */
    public static function create(string $entityClass)
    {
        return sprintf('rest.%s.create', $entityClass);
    }

    /**
     * Update event for given entity
     *
     * NOTE: This is only dispatched if the entity is actually being updated.  If no changes are detected,
     * the event will not fire.
     *
     * @param string $entityClass
     * @return string
     */
    public static function update(string $entityClass)
    {
        return sprintf('rest.%s.update', $entityClass);
    }

    /**
     * Delete event for given entity
     *
     * @param string $entityClass
     * @return string
     */
    public static function delete(string $entityClass)
    {
        return sprintf('rest.%s.delete', $entityClass);
    }

    /**
     * Event name for any field change for a given entity
     *
     * @param string $entityClass
     * @return string
     */
    public static function anyFieldChange(string $entityClass)
    {
        return RestRequestEvents::fieldChange($entityClass, '*');
    }

    /**
     * Event name for a field change on a given entity
     *
     * @param string $entityClass
     * @param string $fieldName
     * @return string
     */
    public static function fieldChange(string $entityClass, string $fieldName)
    {
        return sprintf('rest.%s.field.%s', $entityClass, $fieldName);
    }
}
