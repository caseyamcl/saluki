<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/13/18
 * Time: 12:48 PM
 */

namespace Saluki\RestResource\Event;

use Saluki\RestResource\Model\RestRequestContext;
use Saluki\WebInterface\Model\SalukiRequest;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class RestObjectEvent
 * @package Saluki\RestResource\Event
 */
class RestObjectEvent extends GenericEvent
{
    private RestRequestContext $requestContext;

    /**
     * RestObjectEvent constructor.
     * @param $object
     * @param RestRequestContext $requestContext
     */
    public function __construct($object, RestRequestContext $requestContext)
    {
        parent::__construct($object);
        $this->requestContext = $requestContext;
    }

    /**
     * @return mixed
     */
    public function getObject()
    {
        return $this->getSubject();
    }

    /**
     * @return SalukiRequest
     */
    public function getRequest(): SalukiRequest
    {
        return $this->requestContext->getRequest();
    }

    /**
     * @return RestRequestContext
     */
    public function getRequestContext(): RestRequestContext
    {
        return $this->requestContext;
    }
}
