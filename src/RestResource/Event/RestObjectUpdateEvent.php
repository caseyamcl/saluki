<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Event;

use Saluki\RestResource\Model\RestRequestContext;

/**
 * Class RestObjectUpdateEvent
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class RestObjectUpdateEvent extends RestObjectEvent
{
    /**
     * @var array
     */
    private $valuesChanged;

    /**
     * RestObjectUpdateEvent constructor.
     *
     * @param $object
     * @param RestRequestContext $requestContext
     * @param array $valuesChanged
     */
    public function __construct($object, RestRequestContext $requestContext, array $valuesChanged)
    {
        parent::__construct($object, $requestContext);
        $this->valuesChanged = $valuesChanged;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function valueWasChanged(string $name): bool
    {
        return array_key_exists($name, $this->valuesChanged);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getOldValue(string $name)
    {
        if (! $this->valueWasChanged($name)) {
            throw new \RuntimeException('Value was not changed: ' . $name);
        }
        return $this->valuesChanged[$name][0];
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getNewValue(string $name)
    {
        if (! $this->valueWasChanged($name)) {
            throw new \RuntimeException('Value was not changed: ' . $name);
        }
        return $this->valuesChanged[$name][1];
    }

    /**
     * @return array  Keys are property names, values are tuples [old, new]
     */
    public function getValuesChanged(): array
    {
        return $this->valuesChanged;
    }
}
