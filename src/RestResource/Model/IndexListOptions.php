<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/7/18
 * Time: 3:39 PM
 */

namespace Saluki\RestResource\Model;

/**
 * List Options for index endpoint
 *
 * @package Saluki\RestResource\Model
 */
class IndexListOptions
{
    /**
     * @var int
     */
    private $offset;

    /**
     * @var int|null
     */
    private $limit;

    /**
     * @var array
     */
    private $sort;

    /**
     * IndexListOptions constructor.
     * @param int $offset
     * @param int|null $limit
     * @param array $sort
     */
    public function __construct(int $offset = 0, ?int $limit = null, array $sort = [])
    {
        $this->offset = $offset;
        $this->limit = $limit;
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }
}
