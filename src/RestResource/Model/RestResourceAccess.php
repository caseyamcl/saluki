<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/1/18
 * Time: 12:08 PM
 */

namespace Saluki\RestResource\Model;

/**
 * Class RestResourceAccess
 *
 * @package Saluki\RestResource\Model
 * @property bool|callable $default
 * @property bool|callable $create
 * @property bool|callable $retrieve
 * @property bool|callable $index
 * @property bool|callable $update
 * @property bool|callable $delete
 * @property-write bool|callable $modify
 * @property-write bool|callable $single
 */
class RestResourceAccess
{
    private $default  = false;
    private $create   = null;
    private $retrieve = null;
    private $index    = null;
    private $update   = null;
    private $delete   = null;

    /**
     * @param string $name
     * @return boolean|callable
     */
    public function __get(string $name)
    {
        if (property_exists($this, $name)) {
            return is_null($this->$name) ? $this->default : $this->$name;
        } else {
            throw new \InvalidArgumentException(
                'Allowed: ' . implode(', ', array_keys(get_object_vars($this)))
            );
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset(string $name): bool
    {
        return (isset($this, $name));
    }

    /**
     * Set the value
     *
     * - Callback signature is function(RestRequestContext $requestContext): bool
     * - NULL means use default permissions for this resource (value of $this->default)
     * - boolean enables/disables the endpoint
     *
     * @param string $name
     * @param $value
     */
    public function __set(string $name, $value): void
    {
        // Validate value
        if (! is_callable($value) && ! is_bool($value) && ! is_null($value)) {
            throw new \InvalidArgumentException('Value must be a callback, NULL, or a boolean');
        }

        if ($name == 'single') {
            $this->update = $value;
            $this->delete = $value;
            $this->retrieve = $value;
        } elseif ($name == 'modify') {
            $this->update = $value;
            $this->delete = $value;
        } elseif (! property_exists($this, $name)) {
            throw new \InvalidArgumentException(
                'Allowed: ' . implode(', ', array_keys(get_object_vars($this)))
            );
        } elseif ($name == 'default' && is_null($value)) {
            throw new \InvalidArgumentException('Default value cannot be NULL');
        } else {
            $this->$name = $value;
        }
    }

    /**
     * Allow all (clobbers any existing rules)
     */
    public function allowAll(): void
    {
        foreach (array_keys(get_object_vars($this)) as $key) {
            $this->$key = true;
        }
    }

    /**
     * Create a callback that checks to see if a user is in the specified role(s)
     *
     * @param string ...$role
     * @return callable
     */
    public function userHasRole(string ...$role): callable
    {
        return function (RestRequestContext $request) use ($role) {
            return call_user_func_array([$request, 'userHasRole'], $role);
        };
    }
}
