<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Model;

use Saluki\Authentication\Contract\UserInterface;
use Saluki\WebInterface\Model\SalukiRequest;

/**
 * Class RestRequestContext
 * @package Saluki\RestResource\Model
 */
class RestRequestContext
{
    /**
     * @var SalukiRequest
     */
    private $request;

    /**
     * @var array|array[]  Additional HTTP headers
     */
    private $httpHeaders = [];

    /**
     * @var array|mixed[]  Meta items to include in the top-level document (must be JSON-serializable)
     */
    private $meta = [];

    /**
     * @var WriteRequestData|null
     */
    private $requestData;

    /**
     * RestRequestContext constructor.
     * @param SalukiRequest $request
     * @param WriteRequestData|null $requestData
     */
    public function __construct(SalukiRequest $request, ?WriteRequestData $requestData = null)
    {
        $this->request = $request;
        $this->requestData = $requestData;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->request->getUser();
    }

    /**
     * @param string ...$role
     * @return bool
     */
    public function userHasRole(string ...$role): bool
    {
        return call_user_func_array([$this->request, 'userHasRole'], $role);
    }

    /**
     * @param string $name
     * @param string $value
     * @return RestRequestContext
     */
    public function addResponseHeader(string $name, string $value): self
    {
        $this->httpHeaders[$name][] = $value;
        return $this;
    }

    /**
     * Add meta to include in the response document
     *
     * @param string $key
     * @param $value
     * @return RestRequestContext
     */
    public function addMeta(string $key, $value): self
    {
        $this->meta[$key] = $value;
        return $this;
    }

    /**
     * @return array|mixed[]
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @return array|array[]
     */
    public function getHttpHeaders()
    {
        return $this->httpHeaders;
    }

    /**
     * Get create/update request data
     *
     * @return WriteRequestData
     */
    public function getData(): WriteRequestData
    {
        if ($this->hasData()) {
            return $this->requestData;
        } else {
            throw new \LogicException('This request does not have any write request data');
        }
    }

    /**
     * Returns TRUE if this request has write (create/update) request data
     * @return bool
     */
    public function hasData(): bool
    {
        return (bool) $this->requestData;
    }

    /**
     * @return SalukiRequest
     */
    public function getRequest(): SalukiRequest
    {
        return $this->request;
    }
}
