<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/19/18
 * Time: 1:11 PM
 */

namespace Saluki\RestResource\Model;

/**
 * Contains prepared data to be written during a PUT/POST/PATCH request
 *
 * @package Saluki\RestResource\Model
 */
class WriteRequestData
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $id;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var array
     */
    private $relationships;

    /**
     * WriteDataRequest constructor.
     * @param string $type          Primary entity type (what resource is being updated)
     * @param string|null $id            Primary entity API ID (what is the ID at the API layer)
     * @param array $attributes     Array of attributes, keyed by the attribute field name
     * @param array $relationships  Array of entities (or entity lists), keyed by the relationship field name
     */
    public function __construct(string $type, array $attributes, array $relationships, ?string $id = null)
    {
        $this->type = $type;
        $this->id = $id;
        $this->attributes = $attributes;
        $this->relationships = $relationships;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasAttribute(string $name): bool
    {
        return array_key_exists($name, $this->attributes);
    }

    /**
     * Get attribute by name or throw exception
     * @param string $name
     * @return mixed
     */
    public function getAttribute(string $name)
    {
        if ($this->hasAttribute($name)) {
            return $this->attributes[$name];
        } else {
            throw new \RuntimeException('attribute does not exist in request: ' . $name);
        }
    }

    /**
     * @return array
     */
    public function getRelationships(): array
    {
        return $this->relationships;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasRelationship(string $name): bool
    {
        return array_key_exists($name, $this->relationships);
    }

    /**
     * Get attribute by name or throw exception
     * @param string $name
     * @return mixed
     */
    public function getRelationship(string $name)
    {
        if ($this->hasRelationship($name)) {
            return $this->relationships[$name];
        } else {
            throw new \RuntimeException('relationship does not exist in request: ' . $name);
        }
    }

    /**
     * Returns TRUE if an attribute or relationship with the given name exists in the request data
     *
     * @param string $name
     * @return bool
     */
    public function hasField(string $name): bool
    {
        return $this->hasAttribute($name) or $this->hasRelationship($name);
    }

    /**
     * Returns the attribute or relationship with the given name in the request or throws exception
     *
     * @param string $name
     * @return mixed
     */
    public function getField(string $name)
    {
        if ($this->hasAttribute($name)) {
            return $this->getAttribute($name);
        } elseif ($this->hasRelationship($name)) {
            return $this->getRelationship($name);
        } else {
            throw new \RuntimeException('field does not exist in request: ' . $name);
        }
    }

    /**
     * Return all attributes and relationships as a single array
     *
     * @return array
     */
    public function getFields(): array
    {
        return array_merge($this->attributes, $this->relationships);
    }
}
