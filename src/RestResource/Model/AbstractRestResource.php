<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Model;

use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Saluki\Access\Service\AccessChecker;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Endpoint\CreateEndpoint;
use Saluki\RestResource\Endpoint\DeleteEndpoint;
use Saluki\RestResource\Endpoint\IndexEndpoint;
use Saluki\RestResource\Endpoint\RetrieveEndpoint;
use Saluki\RestResource\Endpoint\UpdateEndpoint;
use Saluki\RestResource\QueryFilter\QueryFilterMapper;
use Saluki\RestResource\Registry\RestResourceRegistry;
use Saluki\RestResource\Service\RestResourceHelper;
use Saluki\RestResource\Field\FieldList;
use Saluki\RestResource\Field\FieldListBuilder;
use Saluki\RestResource\Field\FieldListMapper;
use Saluki\RestResource\Field\FieldListNormalizer;
use Saluki\Serialization\Contract\NormalizedItemInterface;
use Saluki\Serialization\Contract\NormalizerInterface;
use Saluki\Serialization\Model\NormalizerContext;
use Saluki\Utility\RequireConstantTrait;
use Saluki\WebInterface\Contract\EndpointInterface;
use Saluki\WebInterface\Model\RouteGroup;
use Saluki\WebInterface\Model\SalukiRequest;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Abstract REST Resource
 *
 * Provides a common framework for defining REST resources
 *
 * @package Saluki\RestResource\Model
 */
abstract class AbstractRestResource implements RestResourceInterface, NormalizerInterface
{
    use RequireConstantTrait;

    public const NAME = null;
    public const ID_PATH_SEGMENT  = 'id';
    public const ENTITY_CLASS = null;

    public const DEFAULT_INCLUDES = []; // strings, dot-notated (e.g. ["comments", "books.tags"])
    public const ID_NAME = 'id'; // Use array ['fieldName' => 'propertyName'] if field name != propertyName

    public const PAGINATION_DEFAULT_LIMIT = null;
    public const PAGINATION_MAX_LIMIT = null;
    public const DEFAULT_SORT = [];

    /**
     * @var AccessChecker
     */
    protected $accessChecker;

    /**
     * @var FieldListMapper
     */
    protected $createUpdateMapper;

    /**
     * @var EntityManagerInterface
     */
    protected $entityMgr;

    /**
     * @var FieldListNormalizer
     */
    protected $normalizer;

    /**
     * @var RestResourceAccess
     */
    private $access;

    /**
     * @var FieldListBuilder
     */
    private $fields;

    /**
     * @var QueryFilterMapper
     */
    private $indexQueryMapper;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var RestResourceRegistry
     */
    private $registry;

    /**
     * REST Resource Constructor
     *
     * Extend this constructor with additional dependencies for your resource
     *
     * @param RestResourceHelper $helper
     */
    public function __construct(RestResourceHelper $helper)
    {
        $this->access = new RestResourceAccess();
        $this->fields = $helper->createFieldListBuilder($this->getEntityClass(), $this->getIdPropertyName());

        $this->accessChecker = $helper->getAccessChecker();
        $this->createUpdateMapper = new FieldListMapper($this->fields);
        $this->entityMgr = $helper->getEntityManager();
        $this->registry = $helper->getRestResourceRegistry();

        $this->eventDispatcher = $helper->getDispatcher();
        $this->normalizer = new FieldListNormalizer($this->fields, $this->getName(), $this->getIdName());
    }

    /**
     * Initialize
     */
    public function init(): void
    {
        // Index query mapper exists after initialization is done, so use that
        // to denote whether this method has already been run
        if (! $this->indexQueryMapper) {
            $this->configureFields($this->fields);
            $this->setAccess($this->access);

            // This should happen last
            $this->indexQueryMapper = $this->buildQueryFilterMapper();
        }
    }

    // ------------------------------------------------------------------------
    // REST Resource Configuration

    /**
     * Configure fields
     *
     * @param FieldListBuilder $fields
     */
    abstract protected function configureFields(FieldListBuilder $fields): void;

    /**
     * Configure access
     *
     * @param RestResourceAccess $access
     */
    abstract protected function setAccess(RestResourceAccess $access): void;

    // ------------------------------------------------------------------------
    // Registration

    /**
     * Register the endpoints
     *
     * This uses access rules
     *
     * @param string $path
     * @param RouteGroup $routeGroup
     * @return RouteGroup
     */
    public function registerEndpoints(string $path, RouteGroup $routeGroup): RouteGroup
    {
        $this->init();

        $group = $routeGroup->newGroup($path);
        $id = $this->getIdName();

        // Setup endpoints using access rules.  So long as an access rule for an endpoint
        // is not FALSE (i.e. it is TRUE or it is a callback), we will register it.

        // If there is an index endpoint
        if ($this->access->index) {
            $group->get('/', $this->buildIndexEndpoint());
        }

        // Retrieve handler has named route so that we can reference it from endpoints/normalizers
        $group->get("/{{$id}}", $this->buildRetrieveEndpoint())->name($this->getName());

        if ($this->access->create) {
            $group->post('/', $this->buildCreateEndpoint());
        }

        if ($this->access->update) {
            $group->patch("/{{$id}}", $this->buildUpdateEndpoint());
        }

        if ($this->access->delete) {
            $group->delete("/{{$id}}", $this->buildDeleteEndpoint());
        }

        return $group;
    }

    protected function buildCreateEndpoint(): EndpointInterface
    {
        return new CreateEndpoint($this);
    }

    protected function buildUpdateEndpoint(): EndpointInterface
    {
        return new UpdateEndpoint($this);
    }

    protected function buildDeleteEndpoint(): EndpointInterface
    {
        return new DeleteEndpoint($this);
    }

    protected function buildIndexEndpoint(): EndpointInterface
    {
        return new IndexEndpoint($this);
    }

    public function buildRetrieveEndpoint(): EndpointInterface
    {
        return new RetrieveEndpoint($this);
    }

    // --------------------------------------------------------------
    // Hard-coded resource configuration

    /**
     * @return string
     */
    public function getName(): string
    {
        return (string) $this->requireConstant('NAME');
    }

    /**
     * @return null|string
     */
    public function getObjectClass(): ?string
    {
        return $this->requireConstant('ENTITY_CLASS');
    }

    /**
     * @return string
     */
    public function getIdName(): string
    {
        $idPathSegment = $this->requireConstant('ID_NAME');
        return (is_array($idPathSegment)) ? key($idPathSegment) : $idPathSegment;
    }

    /**
     * @param string $action
     * @return callable
     */
    public function getAccessCallback(string $action): callable
    {
        $rule = $this->access->$action;

        if (is_bool($rule)) {
            return function () use ($rule) {
                return $rule;
            };
        } else {
            return $rule;
        }
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->requireConstant('ENTITY_CLASS');
    }

    // --------------------------------------------------------------
    // Parameters used by every endpoint (except DELETE)

    /**
     * @return array|string[]|null
     */
    public function listDefaultFields(): ?array
    {
        return $this->fields->listDefaultFieldNames();
    }

    /**
     * @return array
     */
    public function listDefaultIncludes(): array
    {
        return static::DEFAULT_INCLUDES ?? [];
    }

    // --------------------------------------------------------------
    // Index Endpoint Parameters/methods

    /**
     * @return array
     */
    public function getDefaultSort(): array
    {
        return static::DEFAULT_SORT;
    }

    /**
     * @return int|null
     */
    public function getDefaultPaginationLimit(): ?int
    {
        return static::PAGINATION_DEFAULT_LIMIT ?: null;
    }

    public function getFieldList(): FieldList
    {
        return $this->fields;
    }

    public function getAllowedSortFields(): array
    {
        return $this->fields->listSortableFieldNames();
    }

    /**
     * @return int|null
     */
    public function getMaximumPaginationLimit(): ?int
    {
        return static::PAGINATION_MAX_LIMIT ?: null;
    }

    /**
     * @return array|QueryFilterInterface[]
     */
    public function listIndexQueryFields(): array
    {
        return $this->indexQueryMapper->listFields();
    }

    /**
     * Add any additional rules for queries here (specifically, user or role-based access)
     *
     * @param SalukiRequest $request
     * @param QueryBuilder $qb
     */
    protected function prepareIndexQuery(SalukiRequest $request, QueryBuilder $qb): void
    {
        // By default, all fields are visible
    }

    // --------------------------------------------------------------
    // Parameters/resources used by state change requests (create/update/delete)

    /**
     * @return EventDispatcherInterface|null
     */
    public function getEventDispatcher(): ?EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    // --------------------------------------------------------------
    // CRUD Actions

    /**
     * @param RestRequestContext $requestContext
     * @return object
     * @throws \ReflectionException
     */
    public function create(RestRequestContext $requestContext)
    {
        $object = $this->createUpdateMapper->mapRequest($requestContext);
        $this->entityMgr->persist($object);
        return $object;
    }

    /**
     * @param object $item
     * @param RestRequestContext $requestContext
     * @param FieldChangeTracker|null $changeTracker
     * @return object
     * @throws \ReflectionException
     */
    public function update($item, RestRequestContext $requestContext, ?FieldChangeTracker $changeTracker = null)
    {
        $object = $this->createUpdateMapper->mapRequest($requestContext, $item, $changeTracker);
        $this->entityMgr->persist($object);
        return $object;
    }

    /**
     * @param $item
     * @param RestRequestContext $requestContext
     */
    public function delete($item, RestRequestContext $requestContext): void
    {
        $this->entityMgr->remove($item);
    }

    /**
     * @param string $id
     * @param RestRequestContext $requestContext
     * @return null|object
     */
    public function retrieve(string $id, ?RestRequestContext $requestContext)
    {
        try {
            $repo = $this->entityMgr->getRepository($this->getEntityClass());
            return $repo->findOneBy([$this->getIdPropertyName() => $id]);
        } catch (ConversionException $e) {
            return null;
        }
    }

    /**
     * List records for index endpoint
     *
     * @param array $params
     * @param RestRequestContext $requestContext
     * @param IndexListOptions $listOptions
     * @return array
     */
    public function list(
        array $params,
        RestRequestContext $requestContext,
        IndexListOptions $listOptions
    ): array {
        $queryBuilder = $this->createQueryBuilder();
        $this->prepareIndexQuery($requestContext->getRequest(), $queryBuilder);

        $this->indexQueryMapper->prepareQuery(
            $params,
            $queryBuilder,
            $listOptions->getOffset(),
            $listOptions->getLimit(),
            $listOptions->getSort()
        );

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Get total count for index endpoint
     *
     * @param array $params
     * @param RestRequestContext $requestContext
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function count(array $params, RestRequestContext $requestContext): int
    {
        $queryBuilder = $this->createQueryBuilder();
        $this->prepareIndexQuery($requestContext->getRequest(), $queryBuilder);

        $this->indexQueryMapper->prepareCountQuery($params, $queryBuilder);
        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }

    // --------------------------------------------------------------
    // Normalizer Methods

    public function getTypeName(): string
    {
        return $this->requireConstant('NAME');
    }

    /**
     * @return string
     */
    public function normalizesClass(): string
    {
        return $this->normalizer->normalizesClass();
    }

    /**
     * @param mixed $item
     * @param NormalizerContext $context
     * @param array|null $fields
     * @return NormalizedItemInterface
     */
    public function normalizeItem($item, NormalizerContext $context, ?array $fields = []): NormalizedItemInterface
    {
        return $this->normalizer->normalizeItem($item, $context, $fields);
    }

    /**
     * @param string $baseAlias
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function createQueryBuilder(string $baseAlias = 't')
    {
        /** @var EntityRepository $repo */
        $repo = $this->entityMgr->getRepository($this->getEntityClass());
        return $repo->createQueryBuilder($baseAlias);
    }

    /**
     * @param object $item
     * @return string
     */
    public function getIdFromItem($item): string
    {
        return $this->normalizer->getIdFromItem($item);
    }

    // --------------------------------------------------------------
    // Helper Methods

    /**
     * Get ID Property name is used internally to this object only
     * @return string
     */
    private function getIdPropertyName(): string
    {
        $idPathSegment = $this->requireConstant('ID_NAME');
        return (is_array($idPathSegment)) ? current($idPathSegment) : $idPathSegment;
    }

    /**
     * Build the query filter mapper
     */
    private function buildQueryFilterMapper(): QueryFilterMapper
    {
        $mapper = new QueryFilterMapper(IndexEndpoint::DISALLOWED_QUERY_PARAM_NAMES);

        foreach ($this->fields->listQueryFilterMappings() as $mapping) {
            $mapper->map($mapping);
        }

        $mapper->setSortFields($this->fields->getSortCallbacks());
        if ($limit = $this->getMaximumPaginationLimit()) {
            $mapper->setMaximumLimit($this->getMaximumPaginationLimit());
        }

        return $mapper;
    }
}
