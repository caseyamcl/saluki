<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Model;

class FieldChangeTracker
{
    /**
     * @var array ['fieldName' => ['old', 'new'], etc...]
     */
    private array $changes = [];

    /**
     * Add a change
     *
     * @param string $fieldName
     * @param $oldValue
     * @param $newValue
     */
    public function addChange(string $fieldName, $oldValue, $newValue): void
    {
        $this->changes[$fieldName] = [$oldValue, $newValue];
    }

    /**
     * Return an array: ['fieldName' => ['old', 'new'], etc...]
     */
    public function listChanges(): array
    {
        return $this->changes;
    }

    /**
     * @return bool
     */
    public function hasChanges(): bool
    {
        return count($this->changes) > 0;
    }
}
