<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Validator;

use LogicException;
use RuntimeException;
use Saluki\RestResource\Registry\RestResourceRegistry;
use Saluki\Validation\AbstractValidatorRule;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\Rule\Blank;
use Saluki\Validation\Rule\OrX;
use Saluki\Validation\Rule\SubValues;
use Saluki\Validation\ValidatorContext;
use Saluki\Validation\ValidatorFieldSet;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\Validation\ValueTransformer;

/**
 * Class RelatedRecordValidator
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class RelatedRecordValidator extends AbstractValidatorRule
{
    const DEFAULT_RESOURCE_TYPE_VALIDATOR = null;

    /**
     * @var bool
     */
    private $isMultiple;

    /**
     * @var string
     */
    private $resourceName;

    /**
     * @var RestResourceRegistry
     */
    private $registry;

    /**
     * @var callable|false|null
     */
    private $resourceTypeValidator;

    /**
     * @var string
     */
    private $fieldName;

    /**
     * RelatedRecordValidator constructor.
     * @param RestResourceRegistry $registry
     * @param string $resourceName
     * @param bool $isMultiple
     * @param string $fieldName
     * @param null|callable|false $resourceTypeValidator If NULL, will validate resource type based on the resourceName
     *                                                    If FALSE, will not validate resource type at all
     *                                                    If callable, will validate resource type; signature is
     *                                                    (string $resourceType): bool
     * @param string|null $description
     */
    public function __construct(
        RestResourceRegistry $registry,
        string $resourceName,
        bool $isMultiple,
        string $fieldName,
        $resourceTypeValidator = self::DEFAULT_RESOURCE_TYPE_VALIDATOR,
        ?string $description = null
    ) {
        parent::__construct($description);
        $this->isMultiple = $isMultiple;
        $this->resourceName = $resourceName;
        $this->registry = $registry;
        $this->resourceTypeValidator = $resourceTypeValidator;
        $this->fieldName = $fieldName;
    }

    /**
     * Get a default description to use if a custom description was not provided
     * @return string
     */
    public function getDefaultDescription(): string
    {
        return ($this->isMultiple)
            ? sprintf('value must be a list of %s resource identifier', $this->resourceName)
            : sprintf('value must be a %s resource identifier', $this->resourceName);
    }

    /**
     * Run the rule
     *
     * @param mixed $value
     * @param ValidatorContext $context
     * @return object|object[]|array
     */
    public function __invoke($value, ValidatorContext $context)
    {
        // The value must contain a 'data' key
        $subValues = new ValidatorFieldSet();
        $dataField = $subValues->required('data', true);
        $items = [];

        // Each data item is either NULL or a type/id pair, and the 'id' is resolved to an object
        $dataItemRules = new ValidatorFieldSet();

        // Validate Type
        $typeField = $dataItemRules->required('type');
        switch (true) {
            case $this->resourceTypeValidator === self::DEFAULT_RESOURCE_TYPE_VALIDATOR:
                $typeField->mustEqual($this->resourceName);
                break;
            case is_callable($this->resourceTypeValidator):
                $typeField->callback($this->resourceTypeValidator);
                break;
            case $this->resourceTypeValidator === false:
                break;
            default:
                throw new LogicException('Invalid resourceTypeValidator in ' . get_called_class());
        }

        // Validate ID
        $dataItemRules->required('id')->notBlank()->custom(function (string $id, ValidatorContext $ctx) use (&$items) {
            $object = $this->registry->get($ctx->getFieldValue('type'))->retrieve($id, null);
            if (empty($object)) {
                // TODO: This should actually be converted into a 404 in the AbstractWriteEndpoint, per JSON-API spec
                throw new ValidationRuleException(sprintf('Could not find record: %s %s', $this->resourceName, $id));
            }
            $items[] = $object;
        }, ValueTransformerInterface::AFTER_VALIDATION);

        $itemRules = new ValidatorRuleSet([new OrX([new Blank(), new SubValues($dataItemRules)])]);

        // The 'data' key can be either a single 'type/id' pair (if single), or an array of such
        ($this->isMultiple)
            ? $dataField->isList()->each($itemRules)
            : $dataField->addRules($itemRules);

        $subValues->prepare(is_iterable($value) ? $value : ((array) $value), $context);

        // Return value
        if (empty($items)) { // If items is empty; no value(s) were specified
            return ($this->isMultiple) ? [] : null;
        } elseif ($this->isMultiple) { // Else, we return the items if multiple
            return $items;
        } elseif (count($items) != 1) { // else, if there are more than 1 items, we throw exception (WTF happened...?)
            throw new RuntimeException('Invalid number of values');
        } else { // else, we return the single item
            return array_shift($items);
        }
    }
}
