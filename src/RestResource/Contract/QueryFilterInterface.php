<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Contract;

use Doctrine\ORM\QueryBuilder;
use Saluki\WebInterface\Contract\RequestParamInterface;
use Doctrine\ORM\Query\Expr;

/**
 * Interface QueryFilterInterface
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
interface QueryFilterInterface extends RequestParamInterface
{
    /**
     * @return string
     */
    public function getDataType(): string;

    /**
     * Apply the parameter to a Doctrine query
     *
     * @param string $value The prepared value (prepared by the validation rule)
     * @param QueryBuilder $queryBuilder The Doctrine Query Builder
     * @return Expr\Comparison|Expr\Orx|null
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $queryBuilder);
}
