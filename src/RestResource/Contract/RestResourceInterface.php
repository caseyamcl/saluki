<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/23/18
 * Time: 12:39 PM
 */

namespace Saluki\RestResource\Contract;

use Saluki\RestResource\Field\FieldList;
use Saluki\RestResource\Model\FieldChangeTracker;
use Saluki\RestResource\Model\IndexListOptions;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\WebInterface\Model\RouteGroup;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Interface RestResourceInterface
 *
 * @package Saluki\RestResource\Contract
 */
interface RestResourceInterface
{
    // --------------------------------------------------------------
    // Basics

    /**
     * Get the route name; this does not necessarily have to match the path name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * If this resource is for a specific class, then return that
     *
     * @return null|string
     */
    public function getObjectClass(): ?string;

    /**
     * Register endpoints for this resource
     *
     * @param string $path
     * @param RouteGroup $routeGroup
     * @return RouteGroup
     */
    public function registerEndpoints(string $path, RouteGroup $routeGroup): RouteGroup;

    /**
     * @param string $action
     * @return callable  Callback signature is (RestRequestContext $requestContext): bool
     */
    public function getAccessCallback(string $action): callable;

    /**
     * @return FieldList
     */
    public function getFieldList(): FieldList;

    // --------------------------------------------------------------
    // Parameters used by all endpoints

    /**
     * Get the ID path segment name; e.g. "id"
     * @return string
     */
    public function getIdName(): string;

    /**
     * List the default field names
     *
     * @return null|array|string[]  Return default field names
     */
    public function listDefaultFields(): ?array;

    /**
     * List the default include names
     *
     * @return array  Return default include names
     */
    public function listDefaultIncludes(): array;

    // --------------------------------------------------------------
    // Parameters used by index endpoint

    /**
     * List the default sort in the form that the client should specify
     *
     * @return array  ['fieldName' => 'asc/desc', etc..]
     */
    public function getDefaultSort(): array;

    /**
     * List the fields that are allowed to be used for sorting
     * @return array|string[]
     */
    public function getAllowedSortFields(): array;

    /**
     * Get the default pagination limit
     *
     * @return int|null  NULL for no pagination by default, positive integer for default limit
     */
    public function getDefaultPaginationLimit(): ?int;

    /**
     * Get the maximum pagination limit
     *
     * @return int|null  NULL for no pagination limit
     */
    public function getMaximumPaginationLimit(): ?int;

    /**
     * @return array|QueryFilterInterface[]
     */
    public function listIndexQueryFields(): array;

    // --------------------------------------------------------------
    // Parameters used by create/update endpoints

    /*
     * @param object $item
     * @return string
     */
    public function getIdFromItem($item): string;

    /**
     * @return EventDispatcherInterface|null
     */
    public function getEventDispatcher(): ?EventDispatcherInterface;

    // --------------------------------------------------------------
    // Actions

    /**
     * Create a new object based on the request
     *
     * @param RestRequestContext $requestContext
     * @return object
     */
    public function create(RestRequestContext $requestContext);

    /**
     * Update an object based on the request
     *
     * @param object $item
     * @param RestRequestContext $requestContext
     * @param FieldChangeTracker|null $changeTracker
     * @return object  The updated object
     */
    public function update($item, RestRequestContext $requestContext, ?FieldChangeTracker $changeTracker = null);

    /**
     * Delete an item based on the request
     *
     * @param $item
     * @param RestRequestContext $requestContext
     */
    public function delete($item, RestRequestContext $requestContext): void;

    /**
     * Retrieve an object or NULL if the object was not found
     *
     * @param string $id
     * @param RestRequestContext|null $requestContext
     * @return null|object
     */
    public function retrieve(string $id, ?RestRequestContext $requestContext);

    /**
     * Retrieve a list of objects based off of the parameters
     *
     * This is used in the index endpoint
     *
     * @param array $params
     * @param RestRequestContext $requestContext
     * @param IndexListOptions $listOptions
     * @return array|object[]
     */
    public function list(array $params, RestRequestContext $requestContext, IndexListOptions $listOptions): array;

    /**
     * Count the total number of records based off a list of the parameters
     *
     * This is used in the index endpoint
     *
     * @param array $params
     * @param RestRequestContext $requestContext
     * @return int
     */
    public function count(array $params, RestRequestContext $requestContext): int;
}
