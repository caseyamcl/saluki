<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Service;

use Doctrine\ORM\EntityManagerInterface;
use Saluki\Access\Service\AccessChecker;
use Saluki\RestResource\Field\FieldListBuilder;
use Saluki\RestResource\Registry\RestResourceRegistry;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RestResourceHelper
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AccessChecker
     */
    private $accessChecker;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var RestResourceRegistry
     */
    private $restResourceRegistry;

    /**
     * FieldListBuilderFactory constructor.
     * @param RestResourceRegistry $restResourceRegistry
     * @param EntityManagerInterface $entityManager
     * @param AccessChecker $accessChecker
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        RestResourceRegistry $restResourceRegistry,
        EntityManagerInterface $entityManager,
        AccessChecker $accessChecker,
        EventDispatcherInterface $dispatcher
    ) {
        $this->restResourceRegistry = $restResourceRegistry;
        $this->entityManager = $entityManager;
        $this->accessChecker = $accessChecker;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return AccessChecker
     */
    public function getAccessChecker(): AccessChecker
    {
        return $this->accessChecker;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher(): EventDispatcherInterface
    {
        return $this->dispatcher;
    }

    /**
     * @return RestResourceRegistry
     */
    public function getRestResourceRegistry(): RestResourceRegistry
    {
        return $this->restResourceRegistry;
    }

    /**
     * @param string $className
     * @param null|string $idPropertyName  NULL to guess..
     * @return FieldListBuilder
     */
    public function createFieldListBuilder(string $className, ?string $idPropertyName = null)
    {
        return new FieldListBuilder($className, $this->restResourceRegistry, $this->entityManager, $idPropertyName);
    }
}
