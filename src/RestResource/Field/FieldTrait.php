<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 10/22/18
 * Time: 11:03 AM
 */

namespace Saluki\RestResource\Field;

use Saluki\Authentication\Contract\UserInterface;

/**
 * Class FieldTrait
 * @package Saluki\RestResource\Field
 * @property-read string $description
 * @property-read string|null $propertyName
 * @property-read callable $authCallback
 * @property-read string $dataType
 * @property-read bool $enabled
 */
trait FieldTrait
{
    /**
     * @var string|null
     */
    protected $propertyName = null;

    /**
     * @var string|null
     */
    protected $description = null;

    /**
     * @var callable|null
     */
    protected $authCallback = null;

    /**
     * @var string
     */
    protected $dataType = null;

    /**
     * @var bool
     */
    protected $enabled = false;

    /**
     * Set type (for self-documenting and auto-guess query mappers, etc)
     *
     * @param string $type
     * @return FieldTrait
     */
    public function dataType(string $type): self
    {
        $this->dataType = $type;
        return $this;
    }

    /**
     * Set description (for self-documenting purposes)
     *
     * @param string $description
     * @return static
     */
    public function description(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Role-based auth callback
     *
     * Signature is (UserInterface $user, SalukiRequest|NormalizerContext $ctx, ?$object, ?$value): bool
     *
     * @param callable $callback
     * @return static
     */
    public function onlyIf(callable $callback): self
    {
        $this->authCallback = $callback;
        return $this;
    }

    /**
     * @param string ...$role
     * @return static
     */
    public function onlyIfRole(string ...$role): self
    {
        $this->authCallback = function (UserInterface $user) use ($role) {
            return count(array_intersect($user->getRoles(), $role)) > 0;
        };

        return $this;
    }

    /**
     * @param bool $isEnabled
     * @return static
     */
    public function enabled(bool $isEnabled): self
    {
        $this->enabled = $isEnabled;
        return $this;
    }

    /**
     * @param string $propertyName
     * @return static
     */
    public function propertyName(string $propertyName): self
    {
        $this->propertyName = $propertyName;
        return $this;
    }

    /**
     * @return Field
     */
    abstract protected function __getField(): Field;

    /**
     * Getter
     *
     * @param $name
     * @return bool|\Closure|string|mixed
     */
    public function __get($name)
    {
        // If the property exists and has a non-NULL value, return it
        // If the property exists, has a NULL value, but the field definition has a value, return that

        if (property_exists($this, $name)) {
            if ($this->$name) {
                return $this->$name;
            } elseif (property_exists($this->__getField(), $name)) {
                return $this->__getField()->__get($name);
            } else {
                return null;
            }
        } elseif (property_exists($this->__getField(), $name)) {
            return $this->__getField()->__get($name);
        } else {
            throw new \RuntimeException(sprintf('Invalid property: %s::%s', get_called_class(), $name));
        }
    }
}
