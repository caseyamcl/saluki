<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Field;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\WebInterface\RequestParam\ParameterList;

/**
 * Class FieldList
 * @package Saluki\RestResource\Field
 */
class FieldList implements \IteratorAggregate, \Countable
{
    /**
     * @var Collection|Field[]
     */
    private $fields;

    /**
     * @var string
     */
    private $entityClass;

    /**
     * @var string
     */
    private $idPropertyName = 'id';

    /**
     * @var array|QueryFilterInterface[]
     */
    private $customQueryMappings = [];

    /**
     * FieldList constructor.
     * @param string $entityClass
     * @param string $idPropertyName
     */
    public function __construct(string $entityClass, string $idPropertyName = 'id')
    {
        $this->fields = new ArrayCollection();
        $this->entityClass = $entityClass;
        $this->idPropertyName = $idPropertyName;
    }

    /**
     * Add a field
     *
     * @param Field $field
     * @return Field
     */
    public function add(Field $field): Field
    {
        $this->fields->set($field->name, $field);
        return $field;
    }

    /**
     * @param string $fieldName
     * @return Field
     */
    public function getField(string $fieldName): Field
    {
        if ($this->fields->contains($fieldName)) {
            return $this->fields->get($fieldName);
        } else {
            throw new \RuntimeException('Cannot find non-existent field: ' . $fieldName);
        }
    }

    /**
     * @param string $fieldName
     * @return null|Field
     */
    public function findField(string $fieldName): ?Field
    {
        return $this->fields->get($fieldName);
    }

    /**
     * Add a custom query filter mapping
     *
     * @param QueryFilterInterface $queryFilter
     * @return QueryFilterInterface
     */
    public function addQueryFilter(QueryFilterInterface $queryFilter): QueryFilterInterface
    {
        $this->customQueryMappings[] = $queryFilter;
        return $queryFilter;
    }

    /**
     * List default field names
     *
     * @return array|string[]
     */
    public function listDefaultFieldNames(): array
    {
        return $this->fields->filter(function (Field $field) {
            return ! $field->readable->hiddenByDefault;
        })->map(function (Field $field) {
            return $field->name;
        })->toArray();
    }

    /**
     * List sortable field names
     *
     * @return array|string[]
     */
    public function listSortableFieldNames(): array
    {
        return $this->fields->filter(function (Field $field) {
            return $field->readable->enabled && (bool) $field->readable->sortCallback;
        })->map(function (Field $field) {
            return $field->name;
        })->toArray();
    }

    /**
     * @return array|callable[]  Keys are field names, values are callbacks (QueryBuilder $qb, string $ascOrDesc)
     */
    public function getSortCallbacks(): array
    {
        $coll = $this->fields->filter(function (Field $field) {
            return $field->readable->enabled && (bool) $field->readable->sortCallback;
        });

        foreach ($coll as $field) {
            $out[$field->name] = $field->readable->sortCallback;
        }

        return $out ?? [];
    }

    /**
     * List query filter mappings
     *
     * @return array|QueryFilterInterface[]
     */
    public function listQueryFilterMappings(): array
    {
        $list = $this->fields->map(function (Field $field) {
            return $field->readable->queryFilter;
        });

        return array_filter(array_merge($list->toArray(), $this->customQueryMappings));
    }

    /**
     * @param bool $isUpdate
     * @return Collection|Field[]
     */
    public function listFields(bool $isUpdate): Collection
    {
        return $this->fields->filter(function (Field $field) use ($isUpdate) {
            if ($field->getWritable()->enabled) {
                return ($isUpdate) ? $field->writable->allowedOnUpdate : $field->writable->allowedOnCreate;
            } else {
                return false;
            }
        });
    }

    /**
     * @param bool $isUpdate
     * @return ParameterList
     */
    public function listAttributeParams(bool $isUpdate): ParameterList
    {
        $fields = $this->listFields($isUpdate)->filter(function (Field $field) {
            return $field->fieldType == $field::ATTRIBUTE;
        })->map(function (Field $field) use ($isUpdate) {
            $param = clone $field->getWritable()->param;

            if ($isUpdate) {
                $param->setRequired($field->getWritable()->onUpdate == WritableField::REQUIRED);
                $param->removeDefault(); // do not process defaults for updates.
            } else {
                $param->setRequired($field->getWritable()->onCreate == WritableField::REQUIRED);
            }

            return $param;
        });

        return new ParameterList($fields);
    }

    /**
     * @param bool $isUpdate
     * @return ParameterList
     */
    public function listRelationshipParams(bool $isUpdate): ParameterList
    {
        $fields = $this->listFields($isUpdate)->filter(function (Field $field) {
            return $field->fieldType == $field::RELATED_ITEM or $field->fieldType == $field::RELATED_LIST;
        })->map(function (Field $field) use ($isUpdate) {
            $param = clone $field->getWritable()->param;

            if ($isUpdate) {
                $param->setRequired($field->getWritable()->onUpdate == WritableField::REQUIRED);
                $param->removeDefault(); // do not process defaults for updates.
            } else {
                $param->setRequired($field->getWritable()->onCreate == WritableField::REQUIRED);
            }

            return $param;
        });

        return new ParameterList($fields);
    }

    /**
     * Find the field that matches the given constructor argument name
     *
     * @param string $constructorArgName
     * @return null|Field
     */
    public function findFieldForConstructorArgName(string $constructorArgName): ?Field
    {
        return $this->fields->filter(function (Field $field) use ($constructorArgName) {
            return $field->isWritable() && $field->getWritable()->constructorArgName === $constructorArgName;
        })->first() ?: null;
    }

    /**
     * Find the field that is used for the specified property name
     *
     * @param string $propertyName
     * @return null|Field
     */
    public function findFieldForPropertyName(string $propertyName): ?Field
    {
        return $this->fields->filter(function (Field $field) use ($propertyName) {
            return ($field->readable()->propertyName == $propertyName);
        })->first() ?: null;
    }

    /**
     * Get the entity class
     *
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /**
     * @return string
     */
    public function getIdPropertyName(): string
    {
        return $this->idPropertyName;
    }

    // --------------------------------------------------------------
    // Interface methods

    /**
     * @return Collection|Field[]
     */
    public function getIterator()
    {
        return $this->fields;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->fields->count();
    }
}
