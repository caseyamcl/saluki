<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 10/31/18
 * Time: 2:41 PM
 */

namespace Saluki\RestResource\Field;

use Doctrine\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManagerInterface;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\CustomQueryFilter;
use Saluki\RestResource\Registry\RestResourceRegistry;
use Saluki\RestResource\Validator\RelatedRecordValidator;
use Saluki\WebInterface\RequestParam\RequestParam;

/**
 * Class FieldListBuilder
 * @package Saluki\RestResource\Field
 */
class FieldListBuilder extends FieldList
{
    const AUTO = null;

    private ClassMetadata $metadata;
    private RestResourceRegistry $restResourceRegistry;

    /**
     * FieldListBuilder constructor.
     * @param string $entityClass
     * @param RestResourceRegistry $restResourceRegistry
     * @param EntityManagerInterface $entityManager
     * @param string $idPropertyName
     */
    public function __construct(
        string $entityClass,
        RestResourceRegistry $restResourceRegistry,
        EntityManagerInterface $entityManager,
        string $idPropertyName = 'id'
    ) {
        parent::__construct($entityClass, $idPropertyName);
        $this->restResourceRegistry = $restResourceRegistry;
        $this->metadata = $entityManager->getMetadataFactory()->getMetadataFor($entityClass);
    }

    /**
     * Add a string field
     *
     * @param string $fieldName
     * @return Field
     */
    public function string(string $fieldName): Field
    {
        $field = $this->field($fieldName, FieldAttributeTypes::STRING);
        $field->getWritable()->transform(function ($value) {
            return (string) $value;
        });
        return $field;
    }

    /**
     * Add a boolean field
     *
     * @param string $fieldName
     * @return Field
     */
    public function boolean(string $fieldName): Field
    {
        return $this->field($fieldName, FieldAttributeTypes::BOOLEAN);
    }

    /**
     * Add a decimal field
     *
     * @param string $fieldName
     * @return Field
     */
    public function decimal(string $fieldName): Field
    {
        $field = $this->field($fieldName, FieldAttributeTypes::DECIMAL);
        $field->getWritable()->transform(function ($value) {
            return (float) $value;
        });
        return $field;
    }

    /**
     * Add an integer field
     *
     * @param string $fieldName
     * @return Field
     */
    public function integer(string $fieldName): Field
    {
        $field = $this->field($fieldName, FieldAttributeTypes::INTEGER);
        $field->getWritable()->transform(function ($value) {
            return (int) $value;
        });
        return $field;
    }

    /**
     * Add a date/time field
     *
     * @param string $fieldName
     * @return Field
     */
    public function datetime(string $fieldName): Field
    {
        return $this->field($fieldName, FieldAttributeTypes::DATETIME);
    }

    /**
     * Add a list field
     *
     * @param string $fieldName
     * @param string $itemType
     * @return Field
     */
    public function list(string $fieldName, string $itemType = ''): Field
    {
        $type = $itemType ? sprintf("%s<%s>", FieldAttributeTypes::LIST, $itemType) : FieldAttributeTypes::LIST;
        return $this->add(new Field($fieldName, $type));
    }

    /**
     * Add a 'hash' (array or key/value) field
     * @param string $fieldName
     * @param string $itemType
     * @return Field
     */
    public function hash(string $fieldName, string $itemType = 'hash'): Field
    {
        $field = $this->add(new Field($fieldName, $itemType));
        $field->writable()->isList();
        return $field;
    }

    /**
     * Add a field with a custom data type name
     *
     * @param string $fieldName
     * @param string $dataType
     * @param string $fieldType
     * @return Field
     */
    public function field(string $fieldName, string $dataType, string $fieldType = Field::ATTRIBUTE): Field
    {
        if ($field = $this->findField($fieldName)) {
            $field->dataType($dataType);
            return $field;
        } else {
            return $this->add(new Field($fieldName, $dataType, $fieldType));
        }
    }

    /**
     * @param string $name
     * @param callable $callback
     * @param string $dataType
     * @return QueryFilterInterface|CustomQueryFilter
     */
    public function queryFilter(string $name, callable $callback, string $dataType = 'field'): QueryFilterInterface
    {
        return $this->addQueryFilter(new CustomQueryFilter($name, $dataType, $callback));
    }


    /**
     * @param string $fieldName The field name
     * @param string $propertyName
     * @param null|string $resourceName
     * @param bool $isMultiple
     * @param callable|null $typeValidator  Specify a custom type validator (for heterogeneous targets)
     * @return Field
     */
    public function relationship(
        string $fieldName,
        ?string $propertyName = self::AUTO,
        ?string $resourceName = self::AUTO,
        ?bool $isMultiple = self::AUTO,
        ?callable $typeValidator = null
    ): Field {
        if ($propertyName === self::AUTO) {
            $propertyName = $fieldName;
        }

        if ($resourceName === self::AUTO) {
            $targetClass = $this->metadata->getAssociationTargetClass($propertyName);
            $resourceName = $this->restResourceRegistry->getForClass($targetClass)->getName();
        }

        if ($isMultiple === self::AUTO) {
            $isMultiple = $this->metadata->isCollectionValuedAssociation($propertyName);
        }

        // Setup parameter
        $param = new RequestParam($fieldName, $isMultiple ? "list($resourceName)" : $resourceName);
        $param->addRule(
            new RelatedRecordValidator(
                $this->restResourceRegistry,
                $resourceName,
                $isMultiple,
                $fieldName,
                $typeValidator
            )
        );

        $field = new Field(
            $fieldName,
            $param->getDataType(),
            $isMultiple ? Field::RELATED_LIST : Field::RELATED_ITEM,
            $param
        );
        $field->propertyName($propertyName);

        if ($field->isWritable()) {
            $field->writable->requiredOnCreate();
        }

        $this->add($field);
        return $field;
    }
}
