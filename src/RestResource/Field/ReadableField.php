<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Field;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\BooleanQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\CustomQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\InListQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\DateQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\IntegerQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\NumberQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\SearchQueryFilter;

/**
 * Readable Field
 *
 * @property-read bool $hiddenByDefault
 * @property-read string $propertyType
 * @property-read callable|null $getterCallback
 * @property-read callable|null $sortCallback
 * @property-read callable|null $queryFilter
 */
class ReadableField
{
    use FieldTrait {
        __get as __baseGet;
    }

    public const AUTO = true;

    /**
     * @var Field
     */
    private $field;

    /**
     * @var bool
     */
    private $hiddenByDefault = false;

    /**
     * @var callable|null
     */
    private $getterCallback = null;

    /**
     * @var string  Either 'attribute' or 'relationship'
     */
    private $propertyType;

    /**
     * @var null|QueryFilterInterface
     */
    private $queryFilter = null;

    /**
     * @var null|callback  The callback used to add the sort to the Doctrine Query
     */
    private $sortCallback = null;

    /**
     * Readable Field Constructor
     *
     * @param Field $field
     */
    public function __construct(Field $field)
    {
        $this->field = $field;
        $this->sortable(true);

        // If we can guess the filter based on the datatype, then set it; otherwise, don't.
        if ($this->buildFilter()) {
            $this->filterable(true);
        }
    }

    /**
     * Switch to writable context
     *
     * @return WritableField
     */
    public function writable(): WritableField
    {
        return $this->field->writable();
    }

    // --------------------------------------------------------------
    // Setting readable field attributes

    /**
     * Include this relationship or show this field by default
     *
     * @return static
     */
    public function showByDefault(): self
    {
        $this->hiddenByDefault = false;
        return $this;
    }

    /**
     * Hide this relationship or field by default
     *
     * @return static
     */
    public function hideByDefault(): self
    {
        $this->hiddenByDefault = true;
        return $this;
    }

    /**
     * Getter (used when getting the value)
     *
     * If you pass a string, this will set the propertyName attribute and will use Symfony PropertyAccess to retrieve
     * the value from the object.
     *
     * If you pass NULL, this will cause the getter to assume that the fieldName and the property name are identical
     * and use SymfonyAccess to retrieve the value from the object using that property name.
     *
     * If you pass a callback, this will set the propertyName to NULL and use the callback to retrieve the value.
     * The method signature is ($object, SalukiRequest $request)
     *
     * @param string|callable|null  The getter will be a property name or a callback ($object, SalukiRequest $request)
     * @return ReadableField|static
     */
    public function getter($getter): self
    {
        switch (true) {
            case is_callable($getter):
                $this->getterCallback = $getter;
                break;
            case is_string($getter):
                $this->propertyName = $getter;
                $this->getterCallback = null;
                break;
            case is_null($getter):
                $this->getterCallback = null;
                $this->propertyName = $this->__getField()->name;
                break;
            default:
                throw new \InvalidArgumentException();
        }

        return $this;
    }

    /**
     * Make this filterable
     *
     * If you pass TRUE (default; self::AUTO), then this will attempt to automatically build a query filter based on
     * the dataType
     *
     * If you pass FALSE, it will disable any previously-set query filters
     *
     * If you pass a callable function, the signature is ($value, QueryBuilder $qb)
     *
     * If you pass a QueryFilterInterface, that will be used
     *
     * @param bool|QueryFilterInterface|callable $queryFilter
     * @return static
     */
    public function filterable($queryFilter = self::AUTO): self
    {
        if (! is_bool($queryFilter) && ! is_callable($queryFilter)) {
            throw new \InvalidArgumentException(
                'Filter must be a QueryFilterInterface or boolean.  You passed ' . gettype($queryFilter)
            );
        }

        $this->queryFilter = $queryFilter;
        return $this;
    }

    /**
     * Add sortable
     *
     * If you pass TRUE (default; self::AUTO), this field will be sortable, and the propertyName will be used.  If the
     * getter is a method, then this will assume that the field name is the property name.
     *
     * If you pass FALSE, this field will not be sortable.
     *
     * If you pass a callback, you can add the sort to the query manually.  The method signature is:
     * (QueryBuilder $doctrineQueryBuilder, string $ascOrDesc)
     *
     * @param bool|callable $sortCallback
     * @return static
     */
    public function sortable($sortCallback = self::AUTO): self
    {
        if (! is_bool($sortCallback) && ! is_callable($sortCallback)) {
            throw new \InvalidArgumentException(
                'Sort callback must be a callable or boolean.  You passed ' . gettype($sortCallback)
            );
        }

        $this->sortCallback = $sortCallback;
        return $this;
    }

    /**
     * Magic Getter
     *
     * @param string $name
     * @return bool|callable|\Closure|mixed|null|QueryFilterInterface|string
     */
    public function __get(string $name)
    {
        switch ($name) {
            case 'sortCallback':
                switch (true) {
                    case is_callable($this->sortCallback):
                        $sort = $this->sortCallback;
                        break;
                    case $this->sortCallback === true:
                        $sort = function (QueryBuilder $queryBuilder, string $direction) {
                            $fieldName = sprintf(
                                "%s.%s",
                                $queryBuilder->getRootAliases()[0],
                                $this->propertyName ?: $this->__getField()->name
                            );
                            $queryBuilder->addOrderBy($fieldName, $direction);
                        };
                        break;
                    default:
                        $sort = null;
                }
                return $sort;
            case 'queryFilter':
                switch (true) {
                    case $this->queryFilter instanceof QueryFilterInterface:
                        $query = $this->queryFilter;
                        break;
                    case is_callable($this->queryFilter):
                        $query = new CustomQueryFilter(
                            $this->__getField()->name,
                            $this->__get('dataType'),
                            $this->queryFilter
                        );
                        break;
                    case $this->queryFilter === true:
                        if ($filter = $this->buildFilter()) {
                            $query = $filter;
                        } else {
                            throw new \RuntimeException('Could not determine query filter');
                        }
                        break;
                    default:
                        $query = null;
                }
                return $query;
            default:
                return $this->__baseGet($name);
        }
    }

    /**
     * Guess which query filter to build based on the field dataType, or return NULL
     *
     * @return null|QueryFilterInterface
     */
    private function buildFilter(): ?QueryFilterInterface
    {
        $fieldName = $this->__getField()->name;
        $propertyName = $this->propertyName ?: $fieldName;

        switch ($this->__get('dataType')) {
            case FieldAttributeTypes::BOOLEAN:
                return new BooleanQueryFilter($fieldName, $propertyName);
            case FieldAttributeTypes::STRING:
                return new SearchQueryFilter($fieldName, $propertyName);
            case FieldAttributeTypes::DATE:
            case FieldAttributeTypes::DATETIME:
                return new DateQueryFilter($fieldName, $propertyName);
            case FieldAttributeTypes::INTEGER:
                return new IntegerQueryFilter($fieldName, $propertyName);
            case FieldAttributeTypes::DECIMAL:
                return new NumberQueryFilter($fieldName, $propertyName);
            default:
                return (substr($this->dataType, 0, strlen('list')) == 'list')
                    ? new InListQueryFilter($fieldName, $propertyName)
                    : null;
        }
    }

    // --------------------------------------------------------------

    /**
     * @return Field
     */
    protected function __getField(): Field
    {
        return $this->field;
    }
}
