<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Field;

use Doctrine\Common\Collections\Collection;
use Icecave\Parity\Parity;
use Doctrine\Common\Collections\ArrayCollection;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use Saluki\RestResource\Behavior\ReadFieldValueTrait;
use Saluki\RestResource\Exception\FieldAccessDeniedException;
use Saluki\RestResource\Model\FieldChangeTracker;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Serialization\Model\NormalizerContext;
use SebastianBergmann\Comparator\Factory;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class FieldListMapper
 *
 * @package Saluki\RestResource\Field
 */
class FieldListMapper
{
    use ReadFieldValueTrait;

    /**
     * @var FieldList
     */
    private $fieldList;

    /**
     * @var null|PropertyAccessor
     */
    private $propertyAccess;

    /**
     * @var Factory
     */
    private $comparator;

    /**
     * FieldMapper constructor.
     * @param FieldList $fieldList
     * @param null|PropertyAccessor $propertyAccess
     */
    public function __construct(FieldList $fieldList, ?PropertyAccessor $propertyAccess = null)
    {
        $this->fieldList = $fieldList;
        $this->comparator = new Factory();
        $this->propertyAccess = $propertyAccess ?: PropertyAccess::createPropertyAccessor();
    }

    /**
     * @return FieldList
     */
    public function getFieldList(): FieldList
    {
        return $this->fieldList;
    }

    /**
     * @return string
     */
    public function getEntityClassName(): string
    {
        return $this->fieldList->getEntityClass();
    }

    // --------------------------------------------------------------
    // Mappers

    /**
     * Map a create or update request to an object using rules defined in the fields
     *
     * @param RestRequestContext $requestContext
     * @param object|null  The object to map to; if NULL, assume that we will need to create it
     * @param FieldChangeTracker $tracker  The fieldset change tracker
     * @return object   The created or updated object
     * @throws ReflectionException
     */
    public function mapRequest(RestRequestContext $requestContext, $record = null, ?FieldChangeTracker $tracker = null)
    {
        $request = $requestContext->getRequest();
        $params = $requestContext->getData()->getFields();

        // Get a list of all of the fields
        $fieldsToMap = iterator_to_array($this->fieldList->listFields((bool) $record));

        // if we are creating a new object, we'll need to build it, so we need to figure out what
        // the constructor params are (using reflection) and determine which fields map to those.
        if (! $record) {
            // Keys are field name, values are constructor argument values (in order)
            $constructorFields = $this->resolveConstructorArguments($params);
            $record = (new ReflectionClass($this->getEntityClassName()))
                ->newInstanceArgs(array_values($constructorFields));

            $setByConstructor = array_keys($constructorFields);
        } else {
            $setByConstructor = [];
        }

        // Track field changes..
        if (! $tracker) {
            $tracker = new FieldChangeTracker(get_class($record));
        }
        $normalizer = new NormalizerContext($request->getUser());

        // for both newly created and existing objects, set parameters that can be set.
        /** @var Field $field */
        foreach ($fieldsToMap as $field) {
            // If the value was passed as part of the request, then we'll attempt to update it.
            if (array_key_exists($field->name, $params)) {
                // Find the value from the request
                $value = $params[$field->name];

                // Authorize
                if ($authCallback = $field->writable()->authCallback) {
                    $authResult = call_user_func($authCallback, $request->getUser(), $request, $record);
                    if ($authResult !== true) {
                        // This user is not allowed to update this field.
                        throw new FieldAccessDeniedException($field->name, (is_string($authResult) ? $authResult : ''));
                    }
                }

                // Get the setter
                $setter = $field->getWritable()->setter;

                // Don't do anything if setter is false
                if ($setter === false) {
                    continue;
                }

                // Record the old value - if it is not readable, just record NULL
                try {
                    $oldValue = $this->readFieldValue($field, $record, $normalizer);
                    if ($oldValue instanceof Collection) {
                        $oldValue = new ArrayCollection($oldValue->toArray());
                    }
                } catch (NoSuchPropertyException $e) {
                    if (! $field->isReadable()) {
                        $oldValue = null;
                    } else {
                        throw $e;
                    }
                }

                // Figure out best way to set the property.
                if (is_callable($setter)) {
                    call_user_func($setter, $record, $value, $requestContext);
                } else {
                    // By default, we attempt to set the value using PropertyAccess.  If it fails, but was
                    // already set by the constructor, then don't worry.  Otherwise we throw an exception.
                    try {
                        $this->propertyAccess->setValue($record, $setter, $value);
                    } catch (NoSuchPropertyException $e) {
                        if (! in_array($field->name, $setByConstructor)) {
                            throw $e;
                        }
                    }
                }

                // Record the new value - if it is not readable, then always record that the field has been updated
                try {
                    $newValue = $this->readFieldValue($field, $record, $normalizer);
                    if ($this->isDifferent($oldValue, $newValue)) {
                        $tracker->addChange($field->name, $oldValue, $newValue);
                    }
                } catch (NoSuchPropertyException $e) {
                    if ((! $field->isReadable()) && $oldValue !== $value) {
                        $tracker->addChange($field->name, $oldValue, $value);
                    } else {
                        throw $e;
                    }
                }
            }
        }

        return $record;
    }

    /**
     * Resolve constructor arguments
     *
     * The keys in the returned array are the field name, NOT the constructor argument parameter name.  This is because
     * only the values are used when calling the constructor, and we need to reference the field name later.
     *
     * @param array $requestParams
     * @return array  Return an array of constructor arguments (keys are the field name, values are value to set)
     * @throws ReflectionException
     */
    private function resolveConstructorArguments(array $requestParams): array
    {
        // Use reflection to determine the name of the constructor parameters
        $reflection = new ReflectionClass($this->getEntityClassName());
        $constructorArguments = ($reflection->getConstructor())->getParameters();

        // Keys will be the constructor argument, values will be the request parameters
        $constructorArgMapping = [];

        foreach ($constructorArguments as $constructorArg) {
            // If the field exists and the value exists in the request, then set it.
            if ($field = $this->fieldList->findFieldForConstructorArgName($constructorArg->getName())) {
                if (array_key_exists($field->name, $requestParams)) {
                    $constructorArgMapping[$field->name] = $requestParams[$field->name];
                    continue;
                } elseif ($constructorArg->isDefaultValueAvailable()) {
                    $constructorArgMapping[$field->name] = $constructorArg->getDefaultValue();
                }
            }

            // If made it here: if the constructor argument is optional, skip it.  Otherwise we throw exception.
            if (! $constructorArg->isOptional()) {
                throw new RuntimeException(sprintf(
                    'Unable to resolve request field mapping for %s constructor parameter: %s',
                    $reflection->getName(),
                    $constructorArg->getName()
                ));
            }
        }

        return $constructorArgMapping;
    }

    protected function getPropertyAccess(): PropertyAccessor
    {
        return $this->propertyAccess;
    }

    /**
     * @param $oldValue
     * @param $newValue
     * @return bool
     */
    private function isDifferent($oldValue, $newValue): bool
    {
        // Speed improvement for comparing two collections
        if (
            $oldValue instanceof Collection && $newValue instanceof Collection
            && count($oldValue) !== count($newValue)
        ) {
            return true;
        }

        return Parity::isNotEqualTo(
            $oldValue instanceof Collection ? $oldValue->toArray() : $oldValue,
            $newValue instanceof Collection ? $newValue->toArray() : $newValue
        );
    }
}
