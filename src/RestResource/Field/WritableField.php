<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 10/22/18
 * Time: 10:59 AM
 */

namespace Saluki\RestResource\Field;

use Saluki\Validation\Contract\ValidatorRuleInterface;
use Saluki\Validation\Contract\ValueTransformerInterface;
use Saluki\Validation\ValidatorBuiltInRules;
use Saluki\Validation\ValidatorField;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\Contract\RequestParamInterface;
use Saluki\WebInterface\RequestParam\BooleanParam;
use Saluki\WebInterface\RequestParam\DateTimeParam;
use Saluki\WebInterface\RequestParam\DecimalParam;
use Saluki\WebInterface\RequestParam\IntegerParam;
use Saluki\WebInterface\RequestParam\RequestParam;
use Saluki\WebInterface\RequestParam\StringParam;

/**
 * Class WritableField
 *
 * @property-read callable|string|null|false $setter
 * @property-read string|null $constructorArgName
 * @property-read RequestParamInterface|ValidatorField $param
 * @property-read int $onCreate
 * @property-read int $onUpdate
 * @property-read bool $allowedOnCreate
 * @property-read bool $allowedOnUpdate
 */
class WritableField
{
    use ValidatorBuiltInRules;
    use FieldTrait {
        __get as __baseGet;
    }

    public const REQUIRED    = 1;
    public const OPTIONAL    = -1;
    public const NOT_ALLOWED = 0;

    /**
     * @var Field
     */
    private $field;

    /**
     * @var callable|string|null|bool
     */
    private $setter;

    /**
     * @var string|null
     */
    private $constructorArgName = null;

    /**
     * @var bool
     */
    private $onCreate = self::OPTIONAL;

    /**
     * @var bool
     */
    private $onUpdate = self::OPTIONAL;

    /**
     * @var RequestParamInterface|ValidatorField
     */
    private $param;

    /**
     * Writable field constructor
     *
     * @param Field $field
     * @param RequestParamInterface|ValidatorField $param  Entity fields must be setup outside this class, since they
     *                                                     require the object repository.
     */
    public function __construct(Field $field, ?RequestParamInterface $param = null)
    {
        $this->field = $field;
        $this->param = $param ?: $this->guessParamType();
    }

    /**
     * Switch to readable context
     *
     * @return ReadableField
     */
    public function readable(): ReadableField
    {
        return $this->field->readable();
    }

    public function __get(string $name)
    {
        switch ($name) {
            case 'constructorArgName':
                if ($this->constructorArgName) {
                    return $this->constructorArgName;
                } elseif (is_string($this->setter)) {
                    return $this->setter;
                } else {
                    return $this->__getField()->name;
                }
            case 'setter':
                return (is_callable($this->setter) or is_string($this->setter) or $this->setter === false)
                    ? $this->setter
                    : ($this->__get('propertyName') ?: $this->__getField()->name);
            case 'allowedOnUpdate':
                return $this->onUpdate !== self::NOT_ALLOWED;
            case 'allowedOnCreate':
                return $this->onCreate !== self::NOT_ALLOWED;
            default:
                return $this->__baseGet($name);
        }
    }

    // --------------------------------------------------------------
    // Setting writable field attributes

    /**
     * Setter (used when setting the value)
     *
     * If you pass a string, this will set the propertyName attribute and will use the Symfony PropertyAccess to
     * set the value on the object.
     *
     * If you pass NULL, this will behave the same as passing a string, except Symfony PropertyAccess will assume
     * the field name is the same as the property name.
     *
     * If you pass a callback, this will be used to set the property.  The method signature is
     * ($object, $value, RestRequestContext $requestContext)
     *
     * If you pass FALSE, this value will not be mapped to the object
     *
     * @param callable|string|null|false $setter
     * @return static
     */
    public function setter($setter): self
    {
        $this->setter = $setter;
        return $this;
    }

    /**
     * Set the constructor argument name for building the object in a CREATE context.
     *
     * If the constructor argument name is the same as the property or field name, then this value does not
     * need to be set explicitly.
     *
     * @param string $name
     * @return static
     */
    public function constructorArgName(string $name): self
    {
        $this->constructorArgName = $name;
        return $this;
    }

    /**
     * Set required on create
     *
     * @return static
     */
    public function requiredOnCreate(): self
    {
        $this->onCreate = self::REQUIRED;
        return $this;
    }

    /**
     * Set optional on create
     *
     * @return static
     */
    public function optionalOnCreate(): self
    {
        $this->onCreate = self::OPTIONAL;
        return $this;
    }

    /**
     * Set disabled for create requests
     * @return static
     */
    public function notOnCreate(): self
    {
        $this->onCreate = self::NOT_ALLOWED;
        return $this;
    }

    /**
     * Set optional on update
     * @return static
     */
    public function optionalOnUpdate(): self
    {
        $this->onUpdate = self::OPTIONAL;
        return $this;
    }

    /**
     * Set required on update
     * @return static
     */
    public function requiredOnUpdate(): self
    {
        $this->onUpdate = self::REQUIRED;
        return $this;
    }

    /**
     * Set disabled on update
     *
     * @return WritableField
     */
    public function notOnUpdate(): self
    {
        $this->onUpdate = self::NOT_ALLOWED;
        return $this;
    }

    /**
     * @param $default
     * @return WritableField
     */
    public function default($default): self
    {
        $this->param->setDefault($default);
        return $this;
    }

    // --------------------------------------------------------------

    /**
     * @return Field
     */
    protected function __getField(): Field
    {
        return $this->field;
    }

    // --------------------------------------------------------------
    // Built-in validator field methods

    /**
     * @param ValidatorRuleSet $ruleSet
     * @return WritableField
     */
    public function addRuleSet(ValidatorRuleSet $ruleSet): self
    {
        $this->param->addRuleSet($ruleSet);
        return $this;
    }

    /**
     * @param ValidatorRuleInterface $rule
     * @return self
     */
    public function addRule(ValidatorRuleInterface $rule): self
    {
        $this->param->addRule($rule);
        return $this;
    }

    /**
     * @param ValueTransformerInterface $transformer
     * @return self
     */
    public function addTransformer(ValueTransformerInterface $transformer): self
    {
        $this->param->addTransformer($transformer);
        return $this;
    }

    // --------------------------------------------------------------
    // Utility methods

    /**
     * @return RequestParamInterface
     */
    private function guessParamType(): RequestParamInterface
    {
        $fieldName = $this->__getField()->name;

        switch ($this->__get('dataType')) {
            case FieldAttributeTypes::BOOLEAN:
                return new BooleanParam($fieldName);
            case FieldAttributeTypes::DATETIME:
            case FieldAttributeTypes::DATE:
                return new DateTimeParam($fieldName);
            case FieldAttributeTypes::INTEGER:
                return new IntegerParam($fieldName);
            case FieldAttributeTypes::DECIMAL:
                return new DecimalParam($fieldName);
            case FieldAttributeTypes::STRING:
                return new StringParam($fieldName);
            default:
                return new RequestParam($fieldName, $this->__get('dataType'));
        }
    }
}
