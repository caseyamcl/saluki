<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 10/22/18
 * Time: 1:03 PM
 */

namespace Saluki\RestResource\Field;

/**
 * Class FieldTypes
 * @package Saluki\RestResource\Field
 */
final class FieldAttributeTypes
{
    const BOOLEAN = 'boolean';

    const STRING = 'string';

    const DATETIME = 'datetime';

    const DATE = 'date';

    const INTEGER = 'integer';

    const DECIMAL = 'decimal';

    const OBJECT = 'object';

    const LIST = 'list';
}
