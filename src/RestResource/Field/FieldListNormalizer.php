<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Field;

use Saluki\RestResource\Behavior\ReadFieldValueTrait;
use Saluki\Serialization\Contract\NormalizedItemInterface;
use Saluki\Serialization\Contract\NormalizerInterface;
use Saluki\Serialization\Model\NormalizedItem;
use Saluki\Serialization\Model\NormalizedRelationship;
use Saluki\Serialization\Model\NormalizerContext;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class FieldListNormalizer
 * @package Saluki\RestResource\Field
 */
class FieldListNormalizer implements NormalizerInterface
{
    use ReadFieldValueTrait;

    /**
     * @var FieldList
     */
    private $fieldList;

    /**
     * @var string
     */
    private $idPathSegmentName;

    /**
     * @var string
     */
    private $typeName;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccess;

    /**
     * FieldListNormalizer constructor.
     * @param FieldList $fieldList
     * @param string $typeName
     * @param string $idPathSegmentName
     * @param null|PropertyAccessor $propertyAccess
     */
    public function __construct(
        FieldList $fieldList,
        string $typeName,
        string $idPathSegmentName = 'id',
        ?PropertyAccessor $propertyAccess = null
    ) {
        $this->fieldList = $fieldList;
        $this->idPathSegmentName = $idPathSegmentName;
        $this->typeName = $typeName;
        $this->propertyAccess = $propertyAccess ?: PropertyAccess::createPropertyAccessor();
    }

    /**
     * @return string
     */
    public function getTypeName(): string
    {
        return $this->typeName;
    }

    /**
     * @return string
     */
    public function getIdName(): string
    {
        return $this->idPathSegmentName;
    }

    /**
     * @return array|string[]
     */
    public function listDefaultFields(): ?array
    {
        return $this->fieldList->listDefaultFieldNames();
    }

    /**
     * @return string  The type of class that is normalized
     */
    public function normalizesClass(): string
    {
        return $this->fieldList->getEntityClass();
    }

    /**
     * @param mixed $item
     * @param NormalizerContext $context
     * @param array|null $fields
     * @return NormalizedItemInterface
     */
    public function normalizeItem(
        $item,
        NormalizerContext $context,
        ?array $fields = NormalizerInterface::USE_DEFAULTS
    ): NormalizedItemInterface {
        $normalized = new NormalizedItem(
            $this->typeName,
            $this->getIdFromItem($item),
            $this->getIdName()
        );

        $normalized->addAttributes($this->getFields($item, $context, Field::ATTRIBUTE, $fields));
        $normalized->addMetaItems($this->getFields($item, $context, Field::META));

        foreach ($this->getFields($item, $context, Field::RELATED_ITEM, $fields) as $name => $rel) {
            $normalized->addRelationship($name, NormalizedRelationship::single($rel));
        }
        foreach ($this->getFields($item, $context, Field::RELATED_LIST, $fields) as $name => $relItems) {
            $normalized->addRelationship($name, NormalizedRelationship::multiple($relItems));
        }

        return $normalized;
    }

    /**
     * Get the ID from the item
     *
     * @param $item
     * @return string
     */
    public function getIdFromItem($item): string
    {
        return $this->propertyAccess->getValue($item, $this->fieldList->getIdPropertyName());
    }

    /**
     * Get fields
     *
     * @param object $item
     * @param NormalizerContext $context
     * @param string $type 'attribute', 'relationship', or 'meta'
     * @param array|null $filter  Filter which items are returned
     * @return array
     */
    private function getFields($item, NormalizerContext $context, string $type, ?array $filter = null): array
    {
        return $this->fieldList->getIterator()->filter(function (Field $field) use ($type, $filter, $item, $context) {
            return $field->isReadable()
                && $field->readable->fieldType == $type
                && ((! $filter) or in_array($field->name, $filter))
                && call_user_func($field->readable->authCallback, $context->getUser(), $context, $item) === true;
        })->map(function (Field $field) use ($item, $context) {
            return $this->readFieldValue($field, $item, $context);
        })->toArray();
    }

    /**
     * @return PropertyAccessor
     */
    protected function getPropertyAccess(): PropertyAccessor
    {
        return $this->propertyAccess;
    }
}
