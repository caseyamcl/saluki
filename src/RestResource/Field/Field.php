<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\Field;

use Saluki\WebInterface\Contract\RequestParamInterface;

/**
 * Class Field
 * @package Saluki\RestResource\Field
 * @property-read string $name
 * @property-read string $fieldType
 * @property-read ReadableField $readable
 * @property-read WritableField $writable
 */
class Field
{
    use FieldTrait;

    public const ATTRIBUTE = 'attribute';
    public const RELATED_ITEM = 'related_item';
    public const RELATED_LIST = 'related_list';
    public const META = 'meta';

    /**
     * @var ReadableField
     */
    private $readable;

    /**
     * @var WritableField
     */
    private $writable;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string  'attribute', 'relationship', or 'meta'
     */
    private $fieldType;

    /**
     * Field constructor.
     *
     * @param string $name
     * @param string $dataType
     * @param null|string $fieldType Set to 'attribute', 'relationship', or 'meta' (or NULL to disable read)
     * @param null|RequestParamInterface $writableParam
     */
    public function __construct(
        string $name,
        string $dataType = 'field',
        ?string $fieldType = self::ATTRIBUTE,
        ?RequestParamInterface $writableParam = null
    ) {
        $this->name = $name;
        $this->fieldType = $fieldType;

        // Fields are readable by default
        $this->readable = new ReadableField($this);
        $this->readable->enabled(true);

        // Fields are not writable by default
        $this->writable = new WritableField($this, $writableParam);
        $this->writable->enabled(false);

        $this->dataType($dataType);
    }

    /**
     * @param string $type
     * @return Field
     */
    public function fieldType(string $type): self
    {
        if (! in_array($type, [self::RELATED_ITEM, self::RELATED_LIST, self::ATTRIBUTE, self::META])) {
            throw new \InvalidArgumentException('Invalid property type: ' . $type);
        }
        $this->fieldType = $type;
        return $this;
    }

    public function __get(string $name)
    {
        switch ($name) {
            case 'name':
                return $this->name;
            case 'propertyName':
                return $this->propertyName ?: $this->name;
            case 'enabled':
                return $this->enabled ?: false;
            case 'fieldType':
                return $this->fieldType;
            case 'dataType':
                return $this->dataType ?: 'field';
            case 'description':
                return $this->description ?: '';
            case 'authCallback':
                return $this->authCallback ?: function () {
                    return true;
                };
            case 'readable':
                return $this->readable;
            case 'writable':
                return $this->writable;
            default:
                throw new \InvalidArgumentException(sprintf(
                    'Property not found: %s::%s',
                    get_called_class(),
                    $name
                ));
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset(string $name)
    {
        return $this->__get($name) !== null;
    }

    /**
     * @param bool $isReadable
     * @return ReadableField
     */
    public function readable(bool $isReadable = true): ReadableField
    {
        $this->readable->enabled($isReadable);
        return $this->readable;
    }

    /**
     * @param bool $isWritable
     * @return WritableField
     */
    public function writable(bool $isWritable = true): WritableField
    {
        $this->writable->enabled($isWritable);
        return $this->writable;
    }

    /**
     * Shortcut method to specify that this field is not mapped to the entity
     * @return static|Field
     */
    public function notMapped(): self
    {
        $this->readable->enabled(false);
        $this->writable->setter(function () {
            // do nothing.
        });

        return $this;
    }

    /**
     * @return Field
     */
    protected function __getField(): Field
    {
        return $this;
    }

    /**
     * @return bool
     */
    public function isReadable(): bool
    {
        return $this->readable->enabled;
    }

    /**
     * @return bool
     */
    public function isWritable(): bool
    {
        return $this->writable->enabled;
    }

    /**
     * @return ReadableField
     */
    public function getReadable(): ReadableField
    {
        return $this->readable;
    }

    /**
     * @return WritableField
     */
    public function getWritable(): WritableField
    {
        return $this->writable;
    }
}
