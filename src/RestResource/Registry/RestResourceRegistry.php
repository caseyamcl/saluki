<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/15/18
 * Time: 12:47 PM
 */

namespace Saluki\RestResource\Registry;

use Saluki\Container\Contract\SalukiRegistryInterface;
use Saluki\RestResource\Contract\RestResourceInterface;

/**
 * Class ResourceRegistry
 * @package Saluki\RestResource\ResourceRegistry
 */
class RestResourceRegistry implements \IteratorAggregate, SalukiRegistryInterface
{
    /**
     * @var \ArrayObject|RestResourceInterface[]  Key is name, value is Resource
     */
    private $items;

    /**
     * @var array|RestResourceInterface[]  Key is class name, value is Resource
     */
    private $classMap = [];

    /**
     * NormalizerRegistry constructor.
     */
    public function __construct()
    {
        $this->items = new \ArrayObject();
    }

    /**
     * @param string $name
     * @return RestResourceInterface
     */
    public function get(string $name): RestResourceInterface
    {
        if ($this->has($name)) {
            return $this->items->offsetGet($name);
        } else {
            throw new \RuntimeException('Resource not found with name: ' . $name);
        }
    }

    /**
     * @param string $className
     * @return RestResourceInterface
     */
    public function getForClass(string $className): RestResourceInterface
    {
        if (array_key_exists($className, $this->classMap)) {
            return $this->classMap[$className];
        } else {
            throw new \RuntimeException('Resource not found for class: ' . $className);
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool
    {
        return $this->items->offsetExists($name);
    }

    /**
     * @param RestResourceInterface $resource
     */
    public function add(RestResourceInterface $resource): void
    {
        $this->items[$resource->getName()] = $resource;

        if ($className = $resource->getObjectClass()) {
            $this->classMap[$className] = $resource;
        }
    }

    /**
     * Add an item to the registry
     *
     * @param RestResourceInterface $item
     * @return mixed
     */
    public function addItem($item): void
    {
        $this->add($item);
    }

    /**
     * Get iterator
     *
     * @return \ArrayObject|RestResourceInterface[]
     */
    public function getIterator()
    {
        return $this->items;
    }
}
