<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\AbstractEndpoint;

use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Serialization\Contract\NormalizerInterface;
use Saluki\Serialization\Model\PaginationInfo;
use Saluki\Serialization\Model\SerializableResponse;
use Saluki\Serialization\Model\SerializationOptions;
use Saluki\Serialization\Model\SerializationParams;
use Saluki\Utility\UnpackCSV;
use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\WebInterface\RequestParam\ParameterList;
use Saluki\WebInterface\Behavior\RequestHelper;
use Saluki\WebInterface\Model\AbstractEndpoint;

/**
 * Abstract REST Endpoint
 *
 * Incorporates common behavior for all REST Endpoints
 *
 * @package Saluki\RestResource\Model
 */
abstract class AbstractRestEndpoint extends AbstractEndpoint
{
    use RequestHelper;

    /**
     * @param RestRequestContext $context
     * @param iterable $items
     * @param PaginationInfo $pagination
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function prepareResponse(
        RestRequestContext $context,
        iterable $items,
        ?PaginationInfo $pagination = null
    ) {
        $options = new SerializationOptions(
            $this->isSingle(),
            $context->getRequest()->findQueryParam('fields'),
            $context->getRequest()->findQueryParam('include'),
            $context->getMeta(),
            'page[limit]',
            'page[offset]'
        );

        $params = new SerializationParams($items, $context->getRequest(), $options, $pagination);
        return new SerializableResponse($params);
    }

    /**
     * @return ParameterList
     */
    public function listQueryParameters(): ParameterList
    {
        $params = parent::listQueryParameters();
        $params->optional('include')
            ->setDescription('Other records to include')
            ->setDefault($this->listDefaultIncludes())
            ->transform(function ($value): array {
                return (! is_array($value)) ? UnpackCSV::un($value) : $value;
            });

        $params->optional('fields')
            ->setDescription('Fields to include')
            ->setDefault($this->listDefaultFields())
            ->custom(function ($value) {
                if (! is_array($value)) {
                    throw new ValidationRuleException('value must be a list/array keyed by type');
                }
                foreach ($value as $type => $fields) {
                    $value[$type] = is_string($fields) ? UnpackCSV::un($fields) : $fields;
                }
                return $value;
            });

        return $params;
    }

    /**
     * Does this endpoint operate on a single resource (e.g. retrieve) or multiple resources (e.g. index)
     * @return bool
     */
    abstract protected function isSingle(): bool;


    /**
     * @return array|string[]  dot-delimited includes (e.g. "books" or "books.tags")
     */
    public function listDefaultIncludes(): array
    {
        return [];
    }

    /**
     * @return null|array|array[]  keys are resource type name, values are list of attributes (string[])
     */
    public function listDefaultFields(): ?array
    {
        return NormalizerInterface::USE_DEFAULTS;
    }
}
