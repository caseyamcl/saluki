<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/11/18
 * Time: 4:23 PM
 */

namespace Saluki\RestResource\AbstractEndpoint;

use Aura\Router\Route;
use Psr\Http\Message\ResponseInterface;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\SalukiRequest;
use Laminas\Diactoros\Response\EmptyResponse;

/**
 * Abstract Delete Endpoint
 *
 * Incorporates common behavior for 'delete' REST resource endpoints
 *
 * @package Saluki\RestResource\Model
 */
abstract class AbstractDeleteEndpoint extends AbstractRestEndpoint
{
    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        $context = new RestRequestContext($request);

        // No record? give up
        if (! $record = $this->retrieveRecord($context)) {
            return JsonApiErrorResponse::notFound();
        }

        // Not allowed?  give up
        if (! $this->authorizeAccess($context, $record)) {
            return JsonApiErrorResponse::forbidden();
        }

        $this->delete($record, $context);
        return (new EmptyResponse());
    }

    public function validateRoute(Route $route): void
    {
        $this->ensureMethod($route->allows, 'DELETE');
    }

    /**
     * Does this endpoint operate on a single resource (e.g. retrieve) or multiple resources (e.g. index)
     * @return bool
     */
    protected function isSingle(): bool
    {
        return true;
    }

    /**
     * @param RestRequestContext $requestContext
     * @return object|null
     */
    abstract protected function retrieveRecord(RestRequestContext $requestContext);

    /**
     * @param RestRequestContext $requestContext
     * @param object $record
     * @return bool
     */
    abstract protected function authorizeAccess(RestRequestContext $requestContext, $record): bool;

    /**
     * @param $record
     * @param RestRequestContext $requestContext
     */
    abstract protected function delete($record, RestRequestContext $requestContext): void;
}
