<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\AbstractEndpoint;

use Aura\Router\Route;
use Psr\Http\Message\ResponseInterface;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Utility\RequireConstantTrait;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\SalukiRequest;

/**
 * Abstract REST Retrieve Endpoint
 *
 * Incorporates common behavior for 'retrieve' REST Endpoints
 *
 * @package Saluki\RestResource\Model
 */
abstract class AbstractRetrieveEndpoint extends AbstractRestEndpoint
{
    use RequireConstantTrait;

    /**
     * @param Route $route
     */
    public function validateRoute(Route $route): void
    {
        $this->ensureMethod($route->allows, 'GET');
    }

    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        $context = new RestRequestContext($request);
        if ($record = $this->retrieveRecord($context)) {
            return $this->authorizeAccess($context, $record)
                ? $this->prepareResponse($context, [$record])
                : JsonApiErrorResponse::forbidden();
        } else {
            return JsonApiErrorResponse::notFound();
        }
    }

    /**
     * @param RestRequestContext $requestContext
     * @return object|null
     */
    abstract protected function retrieveRecord(RestRequestContext $requestContext);

    /**
     * @param RestRequestContext $requestContext
     * @param object $record
     * @return bool
     */
    abstract protected function authorizeAccess(RestRequestContext $requestContext, $record): bool;

    /**
     * Does this endpoint operate on a single resource (e.g. retrieve) or multiple resources (e.g. index)
     * @return bool
     */
    protected function isSingle(): bool
    {
        return true;
    }
}
