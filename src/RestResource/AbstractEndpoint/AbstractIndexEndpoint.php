<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\AbstractEndpoint;

use Aura\Router\Route;
use Saluki\RestResource\Model\IndexListOptions;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\Serialization\Model\PaginationInfo;
use Saluki\Utility\UnpackCSV;
use Saluki\WebInterface\Behavior\PaginationParamsTrait;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\SalukiRequest;
use Saluki\WebInterface\RequestParam\ParameterList;
use Psr\Http\Message\ResponseInterface;

/**
 * Abstract Index Endpoint
 *
 * Incorporates common behavior for 'index' REST resource endpoints
 *
 * @package Saluki\RestResource\Model
 */
abstract class AbstractIndexEndpoint extends AbstractRestEndpoint
{
    use PaginationParamsTrait;

    /**
     * By default, all routes are valid
     *
     * @param Route $route
     * @return void
     */
    public function validateRoute(Route $route): void
    {
        $this->ensureMethod($route->allows, 'GET');
    }

    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        $context = new RestRequestContext($request);

        if (! $this->authorizeIndex($context)) {
            return JsonApiErrorResponse::forbidden();
        }

        $count = $this->retrieveTotalItemCount($context);
        $items = $this->retrieveItems(
            $context,
            new IndexListOptions(
                $request->getQueryParam('page')['offset'],
                $request->getQueryParam('page')['limit'],
                $request->getQueryParam('sort')
            )
        );

        return $this->prepareResponse($context, $items, new PaginationInfo(
            count($items),
            $count,
            $request->getQueryParam('page')['offset'],
            $request->getQueryParam('page')['limit']
        ));
    }

    /**
     * @return ParameterList
     */
    public function listQueryParameters(): ParameterList
    {
        // inherit from parent
        $params = parent::listQueryParameters();

        // Add pagination parameter (page[limit] and page[offset]) to the query
        $params->add($this->getPaginationParam());

        // Sort param
        $sortErrorMessage = (empty($this->getAllowedSortFields()))
            ? 'there are no sortable fields'
            : sprintf('only the following fields are sortable: %s', implode(', ', $this->getAllowedSortFields()));

        $params->optional('sort')->transform(function ($value) {
            if (is_array($value)) {
                return $value;
            }
            foreach (UnpackCSV::un($value) as $field) {
                list($field, $direction) = ((string) $field[0] == '-')
                    ? [substr($field, 1), 'DESC']
                    : [$field, 'ASC'];
                $out[$field] = $direction;
            }
            return $out ?? [];
        })->keysInList($this->getAllowedSortFields(), $sortErrorMessage)->setDefault($this->getDefaultSort());

        return $params;
    }

    /**
     * @return bool
     */
    protected function isSingle(): bool
    {
        return false;
    }

    /**
     * @param RestRequestContext $requestContext
     * @return bool
     */
    abstract protected function authorizeIndex(RestRequestContext $requestContext): bool;

    /**
     * Retrieve item count that would be returned without pagination
     *
     * @param RestRequestContext $request
     * @return int
     */
    abstract protected function retrieveTotalItemCount(RestRequestContext $request): int;


    /**
     * Retrieve items (apply pagination if applicable)
     *
     * @param RestRequestContext $request
     * @param IndexListOptions $listOptions
     * @return iterable  The records
     */
    abstract protected function retrieveItems(RestRequestContext $request, IndexListOptions $listOptions): iterable;

    /**
     * @return array
     */
    abstract public function getAllowedSortFields(): array;

    // --------------------------------------------------------------
    // Helpers

    /**
     * Get default pagination limit
     *
     * @return int|null  NULL means no pagination limit
     */
    abstract public function getDefaultPaginationLimit(): ?int;

    /**
     * Get maximum pagination limit
     *
     * @return int|null
     */
    abstract public function getMaximumPaginationLimit(): ?int;

    /**
     * Get default sort
     *
     * @return array  Values are ['field' => 'asc/desc']
     */
    abstract public function getDefaultSort(): array;
}
