<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/16/18
 * Time: 4:05 PM
 */

namespace Saluki\RestResource\AbstractEndpoint;

use Aura\Router\Route;
use Psr\Http\Message\ResponseInterface;
use Saluki\RestResource\Exception\FieldAccessDeniedException;
use Saluki\RestResource\Exception\RecordConflictException;
use Saluki\RestResource\Exception\RelatedRecordsNotFoundException;
use Saluki\RestResource\Model\RestRequestContext;
use Saluki\RestResource\Model\WriteRequestData;
use Saluki\Validation\ValidatorFieldSet;
use Saluki\WebInterface\Model\JsonApiErrorResponse;
use Saluki\WebInterface\Model\SalukiRequest;
use Saluki\WebInterface\RequestParam\ParameterList;

/**
 * Class AbstractWriteEndpoint
 * @package Saluki\RestResource\AbstractEndpoint
 */
abstract class AbstractWriteEndpoint extends AbstractRestEndpoint
{
    const NEW_RECORD = null;

    /**
     * @param Route $route
     */
    public function validateRoute(Route $route): void
    {
        $this->ensureMethod($route->allows, 'PATCH', 'PUT', 'POST');
    }

    /**
     * @return ParameterList
     */
    public function listBodyParameters(): ParameterList
    {
        $params = parent::listQueryParameters();
        $dataPropertyValues = new ValidatorFieldSet();
        $dataPropertyValues
            ->required('type')
            ->notBlank()
            ->length(1, 512)
            ->sanitize()
            ->mustEqual($this->getValidTypeName());

        if ($this->expectsId()) {
            $dataPropertyValues
                ->required('id', $this->expectsId())
                ->notBlank()
                ->length(1, 512)
                ->sanitize();
                // TODO: must equal what is in the path (must be aware of what the ID path segment is called)
        }

        $dataPropertyValues->optional('attributes')->subValues($this->listAttributeParameters());
        $dataPropertyValues->optional('relationships')->subValues($this->listRelationshipParameters());
        $params->required('data')->subValues($dataPropertyValues);

        return $params;
    }


    /**
     * @param SalukiRequest $request
     * @return ResponseInterface
     */
    public function __invoke(SalukiRequest $request): ResponseInterface
    {
        // Get the write request data from the request; this has to happen first so we can build the context
        try {
            $writeData = $this->getDataFromRequest($request);
        } catch (RelatedRecordsNotFoundException $e) {
            return new JsonApiErrorResponse($e->getJsonApiErrors(), 404);
        }

        // Setup the context
        $context = new RestRequestContext($request, $writeData);

        // No record? give up
        if ($this->expectsId()) {
            if (!$record = $this->retrieveRecord($context)) {
                return JsonApiErrorResponse::notFound();
            }
        } else {
            $record = null;
        }

        // Not allowed?  give up
        if (! $this->authorizeAccess($context, $record)) {
            return JsonApiErrorResponse::forbidden();
        }

        if ($errorResponse = $this->verifyValidRequest($writeData, $record)) {
            return $errorResponse;
        }

        try {
            $record = $this->process($context, $record);
            return $this->prepareResponse($context, [$record]);
        } catch (RecordConflictException $e) {
            return JsonApiErrorResponse::conflict($e->getMessage());
        } catch (FieldAccessDeniedException $e) {
            return JsonApiErrorResponse::forbidden($e->getMessage());
        }
    }

    // --------------------------------------------------------------

    /**
     * @param SalukiRequest $request
     * @return WriteRequestData
     * @throws RelatedRecordsNotFoundException
     */
    protected function getDataFromRequest(SalukiRequest $request): WriteRequestData
    {
        $data = $request->getParsedBody()['data'];

        // TODO: Convert relationships that were identified but the value is null into 404 errors.

        // Return prepared object
        $out = new WriteRequestData(
            $data['type'],
            $data['attributes'] ?? [],
            $data['relationships'] ?? [],
            $data['id'] ?? null
        );

        return $out;
    }

    /**
     * Verify valid request
     * @param WriteRequestData $data
     * @param object|null $record
     * @return null|JsonApiErrorResponse
     */
    protected function verifyValidRequest(WriteRequestData $data, $record = self::NEW_RECORD): ?JsonApiErrorResponse
    {
        if ($data->getType() != $this->getValidTypeName()) {
            return JsonApiErrorResponse::conflict('type name mismatch (URL must match "type" parameter in request)');
        } elseif ($record && ($data->getId() != $this->getIdFromItem($record))) {
            return JsonApiErrorResponse::conflict('ID mismatch (URL must match "id" parameter in request)');
        } else {
            return null;
        }
    }

    /**
     * Does this endpoint expect the entire resource to be transmitted ('PUT' and 'POST' do, 'PATCH' does not)
     * @return bool
     */
    abstract protected function expectsEntireResource(): bool;

    /**
     * Does this endpoint expect the ID field present ('PUT' and 'PATCH' do, 'POST' does not)
     *
     * @return bool
     */
    abstract protected function expectsId(): bool;

    /**
     * @return ParameterList
     */
    abstract protected function listAttributeParameters(): ParameterList;

    /**
     * @return ParameterList
     */
    abstract protected function listRelationshipParameters(): ParameterList;

    /**
     * @return string
     */
    abstract protected function getValidTypeName(): string;

    /**
     * @param $record
     * @return string
     */
    abstract protected function getIdFromItem($record): string;

    /**
     * Process the record
     *
     * @param RestRequestContext $requestContext
     * @param object|null $record
     * @return object|null  If NULL, then throws 404
     */
    abstract protected function process(RestRequestContext $requestContext, $record = self::NEW_RECORD);

    /**
     * @param RestRequestContext $requestContext
     * @param object|null $record
     * @return mixed
     */
    abstract protected function authorizeAccess(RestRequestContext $requestContext, $record = self::NEW_RECORD);

    /**
     * Retrieve record for PATCH requests or PUT requests that update the resource
     *
     * @param RestRequestContext $context
     * @return object|null
     */
    abstract protected function retrieveRecord(RestRequestContext $context);

    // --------------------------------------------------------------

    /**
     * Does this endpoint operate on a single resource (e.g. retrieve) or multiple resources (e.g. index)
     * @return bool
     */
    protected function isSingle(): bool
    {
        return true;
    }
}
