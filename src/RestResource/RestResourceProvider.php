<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/1/18
 * Time: 3:10 PM
 */

namespace Saluki\RestResource;

use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\RestResource\Contract\RestResourceInterface;
use Saluki\RestResource\Registry\RestResourceRegistry;

/**
 * Class RestResourceProvider
 * @package Saluki\RestResource
 */
class RestResourceProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        // Services
        $builder->addRegistry(RestResourceRegistry::class, RestResourceInterface::class, true);
        $builder->addServiceDefinition(Service\RestResourceHelper::class);
    }
}
