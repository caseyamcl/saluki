<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\QueryFilter\FilterType\BooleanQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\InListQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\DateQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\NotEmptyQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\NumberQueryFilter;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\SearchQueryFilter;
use Saluki\Validation\ValidatorField;

/**
 * Class DoctrineIndexFilterMapper
 * @package Saluki\RestResource\DoctrineRestResource
 */
class QueryFilterMapper
{
    use QueryFilterHelper;

    const NO_DEFAULT = null;

    /**
     * @var array|callable[]  Keys are API field name, value is sort callback (QueryBuilder $qb, string $ascDesc)
     */
    private $allowedSortFields = [];

    /**
     * @var int|null
     */
    private $maximumLimit = null;

    /**
     * @var array|QueryFilterInterface[]
     */
    private $fieldMappings = [];

    /**
     * @var array|string[]
     */
    private $disallowedFieldNames;

    /**
     * ListRequestDoctrineQueryMapper constructor.
     *
     * @param array|string[] $disallowedFieldNames
     */
    public function __construct(array $disallowedFieldNames = [])
    {
        $this->disallowedFieldNames = array_map('strtolower', $disallowedFieldNames);
    }

    /**
     * @param int|null $limit  Set NULL to specify no maximum limit
     */
    public function setMaximumLimit(?int $limit)
    {
        $this->maximumLimit = $limit;
    }

    /**
     * @return int|null
     */
    public function getMaximumLimit(): ?int
    {
        return $this->maximumLimit;
    }

    /**
     * @return array|QueryFilterInterface[]
     */
    public function listFields(): array
    {
        return $this->fieldMappings;
    }

    /**
     * Map allowed sort fields (use API field names, not property names)
     *
     * Keys are API field names and values are entity property name (optionally prefixed with table prefix
     * used in query builder.  If not supplied, then the default root alias is used when building Doctrine query)
     *
     * @param array $allowedSortFields  Keys are api field name, values are callback (QueryBuilder $qb, string $dir)
     */
    public function setSortFields(array $allowedSortFields)
    {
        foreach ($allowedSortFields as $fieldName => $callback) {
            $this->allowedSortFields[$fieldName] = $callback;
        }
    }

    /**
     * Get allowed sort field API names
     *
     * @return array|string[]
     */
    public function getAllowedSortFields(): array
    {
        return array_keys($this->allowedSortFields);
    }

    /**
     * Get a copy of this object with disallowed fields set
     *
     * @param array $disallowedFieldNames
     * @return QueryFilterMapper
     */
    public function withDisallowedFields(array $disallowedFieldNames): QueryFilterMapper
    {
        $that = clone $this;
        $that->disallowedFieldNames = $disallowedFieldNames;

        $diff = array_diff(array_keys($that->fieldMappings), $that->disallowedFieldNames);
        if (! empty($diff)) {
            throw new \RuntimeException('Disallowed field mapping names: ' . implode(', ', $diff));
        }

        return $that;
    }

    // --------------------------------------------------------------
    // Mappers

    /**
     * @param string $apiFieldName      The request parameter name
     * @param null|string $propertyName The entity property name, if different than API field name
     *                                  (with or without table prefix)
     * @return ValidatorField
     */
    public function mapCsv(string $apiFieldName, ?string $propertyName = null): ValidatorField
    {
        return $this->map(new InListQueryFilter($apiFieldName, $propertyName ?: $apiFieldName));
    }

    /**
     * @param string $apiFieldName      The request parameter name
     * @param null|string $propertyName The entity property name, if different than API field name
     *                                  (with or without table prefix)
     * @return ValidatorField
     */
    public function mapBoolean(string $apiFieldName, ?string $propertyName = null): ValidatorField
    {
        return $this->map(new BooleanQueryFilter($apiFieldName, $propertyName ?: $apiFieldName));
    }

    /**
     * Maps a boolean parameter that
     * @param string $apiFieldName
     * @param null|string $propertyName
     * @return ValidatorField
     */
    public function mapNotEmpty(string $apiFieldName, ?string $propertyName = null): ValidatorField
    {
        return $this->map(new NotEmptyQueryFilter($apiFieldName, $propertyName ?: $apiFieldName));
    }

    /**
     * @param string $apiFieldName          The request parameter name
     * @param array|string[] $propertyNames The entity property names to search, if different than API field name
     *                                      (with or without table prefix)
     * @return ValidatorField
     */
    public function mapSearch(string $apiFieldName, ?array $propertyNames = null): ValidatorField
    {
        $searchProperties = $propertyNames ?: [$apiFieldName];
        return $this->map(new SearchQueryFilter($apiFieldName, ...$searchProperties));
    }

    /**
     * @param string $apiFieldName
     * @param null|string $propertyName
     * @return ValidatorField
     */
    public function mapDate(string $apiFieldName, ?string $propertyName = null): ValidatorField
    {
        return $this->map(new DateQueryFilter($apiFieldName, $propertyName ?: $apiFieldName));
    }

    /**
     * @param string $apiFieldName
     * @param null|string $propertyName
     * @return ValidatorField
     */
    public function mapNumberRange(string $apiFieldName, ?string $propertyName = null): ValidatorField
    {
        return $this->map(new NumberQueryFilter($apiFieldName, $propertyName ?: $apiFieldName));
    }

    /**
     * @param string $apiFieldName
     * @param null|string $propertyName
     * @return ValidatorField
     */
    public function mapInteger(string $apiFieldName, ?string $propertyName = null): ValidatorField
    {
        return $this->map(new NumberQueryFilter($apiFieldName, $propertyName ?: $apiFieldName));
    }

    /**
     * Map an API field to a property
     *
     * @param QueryFilterInterface $queryParam
     * @return QueryFilterInterface|ValidatorField
     */
    public function map(QueryFilterInterface $queryParam): QueryFilterInterface
    {
        if (in_array(strtolower($queryParam->getName()), $this->disallowedFieldNames)) {
            throw new \InvalidArgumentException('Disallowed mapping field name: ' . $queryParam->getName());
        }

        $this->fieldMappings[$queryParam->getName()] = $queryParam;
        return $queryParam;
    }

    // --------------------------------------------------------------
    // Build Query

    /**
     * Apply mappings to the Doctrine query builder
     *
     * @param array $requestParams Request query params, validated, and transformed/prepared
     * @param QueryBuilder $queryBuilder
     * @param int $offset
     * @param int|null $limit
     * @param array $sort
     */
    public function prepareQuery(
        array $requestParams,
        QueryBuilder $queryBuilder,
        int $offset = 0,
        ?int $limit = null,
        array $sort = []
    ): void {
        // Apply filters
        foreach ($requestParams as $paramName => $paramValues) {
            if (array_key_exists($paramName, $this->fieldMappings)) {
                foreach ((array) $paramValues as $paramValue) {
                    $expr = $this->fieldMappings[$paramName]->getDoctrineQueryExpression($paramValue, $queryBuilder);
                    if ($expr) {
                        $queryBuilder->andWhere($expr);
                    }
                }
            }
        }

        // Apply offset and limit
        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }
        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        // Apply sort parameters
        foreach ($sort as $fieldName => $direction) {
            $sortCallback = $this->allowedSortFields[$fieldName];
            call_user_func($sortCallback, $queryBuilder, $direction);
        }
    }

    /**
     * @param array $requestParams
     * @param QueryBuilder $queryBuilder
     */
    public function prepareCountQuery(array $requestParams, QueryBuilder $queryBuilder)
    {
        $this->prepareQuery($requestParams, $queryBuilder);
        $queryBuilder->resetDQLParts(['select', 'orderBy']);
        $queryBuilder->setMaxResults(null);
        $queryBuilder->setFirstResult(null);

        $queryBuilder->select('COUNT(DISTINCT(' . $queryBuilder->getRootAliases()[0] . '))');
    }
}
