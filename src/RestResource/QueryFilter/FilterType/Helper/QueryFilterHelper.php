<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/4/18
 * Time: 3:54 PM
 */

namespace Saluki\RestResource\QueryFilter\FilterType\Helper;

use Doctrine\ORM\QueryBuilder;
use Ramsey\Uuid\Uuid;

trait QueryFilterHelper
{
    /**
     * Generate a random parameter name
     *
     * @param string $prefix
     * @return string
     */
    protected function randomParamName(string $prefix): string
    {
        return sprintf(
            ':query_param_%s_%s',
            $prefix ?: 'val',
            str_replace('-', '_', Uuid::uuid4()->toString())
        );
    }


    /**
     * @param QueryBuilder $queryBuilder
     * @return string
     */
    protected function getPropertyName(QueryBuilder $queryBuilder): string
    {
        return $this->prepareDoctrinePropertyName($queryBuilder, $this->propertyName);
    }

    /**
     * Get a field name
     *
     * @param QueryBuilder $queryBuilder
     * @param string $propertyName
     * @return string
     */
    protected function prepareDoctrinePropertyName(QueryBuilder $queryBuilder, string $propertyName): string
    {
        return (strpos($propertyName, '.'))
            ? $propertyName
            : sprintf('%s.%s', $queryBuilder->getRootAliases()[0], $propertyName);
    }
}
