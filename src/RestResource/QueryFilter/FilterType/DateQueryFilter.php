<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Cake\Chronos\Chronos;
use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\RequestParam\StringParam;

/**
 * Class DateRangeType
 * @package FSURCC\AccountingSystem\Core\Entity\QueryParameterType
 */
class DateQueryFilter extends StringParam implements QueryFilterInterface
{
    use QueryFilterHelper;

    const PATTERN = '/^([><])?(:)?(.+)?$/';
    const INVALID = 'Invalid date format (must be [><][:][Y-m-d][TH:m:s])';

    /**
     * @var string
     */
    private $propertyName;

    /**
     * BooleanQueryParam constructor.
     * @param string $fieldName
     * @param string $propertyName
     */
    public function __construct(string $fieldName, ?string $propertyName = null)
    {
        parent::__construct($fieldName, false);
        $this->propertyName = $propertyName ?: $fieldName;
    }

    /**
     * @return string
     */
    public function getDataType(): string
    {
        return 'date_range';
    }

    /**
     * @return callable
     */
    public function getValidator(): ValidatorRuleSet
    {
        return ValidatorRuleSet::new()->callback(function ($value) {
            if (preg_match(static::PATTERN, $value, $matches)) {
                if ($dateVale = @(new Chronos($matches[3]))) {
                    return $value;
                } else {
                    throw new ValidationRuleException('Invalid date value: ' . $matches[3]);
                }
            } else {
                throw new ValidationRuleException('Invalid date value: ' . $value);
            }
        });
    }

    /**
     * @param string $value Raw date value passed in
     * @param QueryBuilder $qb
     * @return \Doctrine\ORM\Query\Expr\Comparison
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $qb)
    {
        $propName = $this->getPropertyName($qb);

        $matches = [];
        if (preg_match(static::PATTERN, $value, $matches)) {
            $date = new Chronos($matches[3]);
        } else {
            throw new \InvalidArgumentException(static::INVALID);
        }

        $valueName = $this->randomParamName('date');

        switch ($matches[1] . $matches[2]) {
            case '<':
                $expr = $qb->expr()->lt($propName, $valueName);
                break;
            case '<:':
                $expr = $qb->expr()->lte($propName, $valueName);
                break;
            case '>':
                $expr = $qb->expr()->gt($propName, $valueName);
                break;
            case '>:':
                $expr = $qb->expr()->gte($propName, $valueName);
                break;
            default:
                $expr = $qb->expr()->eq($propName, $valueName);
        }

        $qb->setParameter($valueName, $date);
        return $expr;
    }
}
