<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\Validation\Exception\ValidationRuleException;
use Saluki\Validation\ValidatorField;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\Contract\RequestParamInterface;

/**
 * Class IntegerParamType
 * @package FSURCC\AccountingSystem\Core\Entity\ListParameterType
 */
class NumberQueryFilter extends ValidatorField implements RequestParamInterface, QueryFilterInterface
{
    const PATTERN = '/^([><])?(:)?(.+)?$/';
    const INVALID = 'Invalid number format (must be [><][:][##])';

    use QueryFilterHelper;

    /**
     * @var string
     */
    private $propertyName;

    /**
     * BooleanQueryParam constructor.
     * @param string $fieldName
     * @param string $propertyName
     */
    public function __construct(string $fieldName, string $propertyName)
    {
        parent::__construct($fieldName, false);
        $this->propertyName = $propertyName;
    }


    /**
     * @return string
     */
    public function getDataType(): string
    {
        return 'number_range';
    }

    /**
     * @return callable
     */
    public function getValidator(): ValidatorRuleSet
    {
        return ValidatorRuleSet::new()->callback(function ($value) {
            if (! preg_match(static::PATTERN, $value, $matches)) {
                throw new ValidationRuleException('value must be an integer (optional preceded by ">" or "<"');
            }
            if (! is_numeric($matches[3])) {
                throw new ValidationRuleException('value must be an integer (optional preceded by ">" or "<"');
            }

            return true;
        });
    }


    /**
     * @param float|int $value
     * @param QueryBuilder $qb
     * @return \Doctrine\ORM\Query\Expr\Comparison
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $qb)
    {
        $propName = $this->getPropertyName($qb);
        $valueName = $this->randomParamName('number');

        $matches = [];
        if (preg_match(static::PATTERN, $value, $matches)) {
            $number = (float) $matches[3];
        } else {
            throw new \InvalidArgumentException(static::INVALID);
        }

        switch ($matches[1] . $matches[2]) {
            case '<':
                $expr = $qb->expr()->lt($propName, $valueName);
                break;
            case '<:':
                $expr = $qb->expr()->lte($propName, $valueName);
                break;
            case '>':
                $expr = $qb->expr()->gt($propName, $valueName);
                break;
            case '>:':
                $expr = $qb->expr()->gte($propName, $valueName);
                break;
            default:
                $expr = $qb->expr()->eq($propName, $valueName);
        }

        $qb->setParameter($valueName, $number);
        return $expr;
    }
}
