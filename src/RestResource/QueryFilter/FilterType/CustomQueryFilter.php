<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\Validation\ValidatorField;

/**
 * Class CustomQueryFilter
 * @package Saluki\RestResource\QueryFilter\FilterType
 */
class CustomQueryFilter extends ValidatorField implements QueryFilterInterface
{
    /**
     * @var callable
     */
    private $queryBuilderCallback;

    /**
     * @var string
     */
    private $dataType;

    /**
     * CustomQueryFilter constructor.
     *
     * Callback signature is ($value, QueryBuilder $ormQueryBuilder, SalukiRequest $request): Expression|null
     * Return NULL if the query filter is not applicable.
     *
     * @param string $paramName  Filter parameter name
     * @param string $dataType   For self-documenting purposes
     * @param callable $queryBuilderCallback  Return an expression that can be added with $qb->andWhere()...
     */
    public function __construct(string $paramName, string $dataType, callable $queryBuilderCallback)
    {
        $this->queryBuilderCallback = $queryBuilderCallback;
        parent::__construct($paramName, false);
        $this->dataType = $dataType;
    }

    /**
     * @return string
     */
    public function getDataType(): string
    {
        return $this->dataType;
    }

    /**
     * Apply the parameter to a Doctrine query
     *
     * @param string $value The prepared value (prepared by the validation rule)
     * @param QueryBuilder $queryBuilder The Doctrine Query Builder
     * @return mixed
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $queryBuilder)
    {
        return call_user_func($this->queryBuilderCallback, $value, $queryBuilder);
    }
}
