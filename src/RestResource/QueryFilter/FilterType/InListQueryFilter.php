<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\Utility\UnpackCSV;
use Saluki\WebInterface\RequestParam\StringParam;

/**
 * Class CSVParamType
 * @package FSURCC\AccountingSystem\Core\Entity\QueryParameterType
 */
class InListQueryFilter extends StringParam implements QueryFilterInterface
{
    use QueryFilterHelper;

    /**
     * @var string
     */
    private $propertyName;

    /**
     * BooleanQueryParam constructor.
     * @param string $fieldName
     * @param string $propertyName
     */
    public function __construct(string $fieldName, string $propertyName)
    {
        parent::__construct($fieldName, false);
        $this->propertyName = $propertyName;
        $this->notBlank();
    }


    /**
     * Apply the parameter to a Doctrine query
     *
     * @param string $values The values
     * @param QueryBuilder $queryBuilder The Doctrine Query Builder
     * @return \Doctrine\ORM\Query\Expr\Func
     */
    public function getDoctrineQueryExpression($values, QueryBuilder $queryBuilder)
    {
        $parameterName = $this->randomParamName('csv');

        if (! is_array($values)) {
            $values = UnpackCSV::un($values);
        }

        $expr = $queryBuilder->expr()->in($this->getPropertyName($queryBuilder), $parameterName);
        $queryBuilder->setParameter($parameterName, $values);
        return $expr;
    }
}
