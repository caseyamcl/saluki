<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Doctrine\DBAL\Driver\AbstractSQLServerDriver;
use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\Utility\UnpackCSV;
use Saluki\Validation\ValidatorRuleSet;
use Saluki\WebInterface\RequestParam\StringParam;

/**
 * Class SearchParamType
 * @package FSURCC\AccountingSystem\Core\Entity\QueryParameterType
 */
class SearchQueryFilter extends StringParam implements QueryFilterInterface
{
    use QueryFilterHelper;

    /**
     * @var array
     */
    private $searchProperties;

    /**
     * SearchParamType constructor.
     * @param string $fieldName
     * @param string[] $searchProperties
     */
    public function __construct(string $fieldName, string ...$searchProperties)
    {
        parent::__construct($fieldName, false);
        $this->searchProperties = (! empty($searchProperties)) ? $searchProperties : [$fieldName];
    }

    /**
     * @return callable
     */
    public function getValidator(): ValidatorRuleSet
    {
        return ValidatorRuleSet::new()->notBlank();
    }

    /**
     * Apply the parameter to a Doctrine query
     *
     * @param string $value The raw value passed into a service request
     * @param QueryBuilder $queryBuilder The Doctrine Query Builder
     * @return \Doctrine\ORM\Query\Expr\Orx
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $queryBuilder)
    {
        $values = UnpackCSV::un($value, '|');

        $valueNames = [];
        foreach ($values as $value) {
            if (empty(trim($value))) {
                continue;
            }

            // Convert '*' to '%' if not MSSQL server
            $dbDriver = $queryBuilder->getEntityManager()->getConnection()->getDatabasePlatform();
            if (! $dbDriver instanceof AbstractSQLServerDriver) {
                $value = str_replace('*', '%', $value);// if MS-SQL, this should no
            }

            $valueName = $this->randomParamName('search');
            $valueNames[$valueName] = $value;
            $queryBuilder->setParameter($valueName, $value);
        }

        $orExpression = $queryBuilder->expr()->orX();
        foreach ($this->searchProperties as $propertyName) {
            $propName = $this->prepareDoctrinePropertyName($queryBuilder, $propertyName);

            foreach ($valueNames as $valueName => $valueValue) {
                $orExpression->add($queryBuilder->expr()->like($propName, $valueName));
            }
        }

        //$queryBuilder->setParameters($valueNames);
        return $orExpression;
    }
}
