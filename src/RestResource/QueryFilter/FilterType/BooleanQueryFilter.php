<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\WebInterface\RequestParam\BooleanParam;

/**
 * Class BooleanParamType
 * @package FSURCC\AccountingSystem\Core\Entity\QueryParameterType
 */
class BooleanQueryFilter extends BooleanParam implements QueryFilterInterface
{
    use QueryFilterHelper;

    /**
     * @var string
     */
    private $propertyName;

    /**
     * BooleanQueryParam constructor.
     * @param string $fieldName
     * @param string $propertyName
     */
    public function __construct(string $fieldName, string $propertyName)
    {
        parent::__construct($fieldName, false);
        $this->propertyName = $propertyName;
    }

    /**
     * Apply the parameter to a Doctrine query
     *
     * @param boolean $value The raw value passed into a service request
     * @param QueryBuilder $queryBuilder The Doctrine Query Builder
     * @return Expr\Comparison|Expr\Orx
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $queryBuilder)
    {
        $propName = $this->getPropertyName($queryBuilder);

        switch ((bool) $value) {
            case true:
                $queryBuilder->setParameter(':val', (bool) $value);
                return $queryBuilder->expr()->eq($propName, ':val');

            case false:
                $queryBuilder->setParameter(':val', (bool) $value);
                return $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq($propName, ':val'),
                    $queryBuilder->expr()->isNull($propName)
                );

            default:
                throw new \InvalidArgumentException("Invalid boolean value: '%s' (did you validate?)", $value);
        }
    }
}
