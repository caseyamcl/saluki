<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/24/18
 * Time: 3:06 PM
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\WebInterface\RequestParam\BooleanParam;

class NotEmptyQueryFilter extends BooleanParam implements QueryFilterInterface
{
    use QueryFilterHelper;

    /**
     * @var string
     */
    private $propertyName;

    /**
     * BooleanQueryParam constructor.
     * @param string $fieldName
     * @param string $propertyName
     */
    public function __construct(string $fieldName, string $propertyName)
    {
        parent::__construct($fieldName, false);
        $this->propertyName = $propertyName;
    }

    /**
     * Apply the parameter to a Doctrine query
     *
     * @param boolean $value The raw value passed into a service request
     * @param QueryBuilder $queryBuilder The Doctrine Query Builder
     * @return \Doctrine\ORM\Query\Expr\Andx|\Doctrine\ORM\Query\Expr\Orx
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $queryBuilder)
    {
        $propName = $this->getPropertyName($queryBuilder);

        $queryBuilder->setParameter(':empty', '');

        switch ((bool) $value) {
            case true:
                return $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->isNotNull($propName),
                    $queryBuilder->expr()->neq($propName, ':empty')
                );
            case false:
                return $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNull($propName),
                    $queryBuilder->expr()->eq($propName, ':empty')
                );
            default:
                throw new \InvalidArgumentException("Invalid boolean value: '%s' (did you validate?)", $value);
        }
    }
}
