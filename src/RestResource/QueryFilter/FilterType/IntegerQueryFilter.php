<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\RestResource\QueryFilter\FilterType;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Contract\QueryFilterInterface;
use Saluki\RestResource\QueryFilter\FilterType\Helper\QueryFilterHelper;
use Saluki\WebInterface\RequestParam\IntegerParam;

/**
 * Class IntegerParamType
 * @package FSURCC\AccountingSystem\Core\Entity\QueryParameterType
 */
class IntegerQueryFilter extends IntegerParam implements QueryFilterInterface
{
    use QueryFilterHelper;

    /**
     * @var string
     */
    private $propertyName;

    /**
     * BooleanQueryParam constructor.
     * @param string $fieldName
     * @param string $propertyName
     */
    public function __construct(string $fieldName, string $propertyName)
    {
        parent::__construct($fieldName, false);
        $this->propertyName = $propertyName;
    }

    /**
     * @param integer $value
     * @param QueryBuilder $qb
     * @return \Doctrine\ORM\Query\Expr\Comparison
     */
    public function getDoctrineQueryExpression($value, QueryBuilder $qb)
    {
        $valueName = $this->randomParamName('integer');

        $expr = $qb->expr()->eq($this->getPropertyName($qb), $valueName);
        $qb->setParameter($valueName, (int) $value);
        return $expr;
    }
}
