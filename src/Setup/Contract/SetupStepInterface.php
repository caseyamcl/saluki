<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 12:58 PM
 */

namespace Saluki\Setup\Contract;

use Saluki\Console\ConsoleIO;

/**
 * Class SetupStepInterface
 * @package Saluki\Setup\Contract
 */
interface SetupStepInterface
{
    public const NONE = [];
    public const PRE_CONTAINER = true;
    public const POST_CONTAINER = false;

    /**
     * Should this step run before the container is built?
     *
     * This should be TRUE for any step that does not explicitly rely on container services to run
     * Configuration and SalukiParams are available for dependency-injection.
     *
     * @return bool
     */
    public static function isPreContainer(): bool;

    /**
     * An array of setup classes that must run before this runs
     *
     * @return array|string[]
     */
    public static function getDependsOn(): array;

    /**
     * Check if the item that this step sets up has been setup
     *
     * @return bool
     */
    public function isSetup(): bool;

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface;
}
