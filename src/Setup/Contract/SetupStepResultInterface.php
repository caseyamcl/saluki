<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 12:59 PM
 */

namespace Saluki\Setup\Contract;

/**
 * Interface SetupStepResultInterface
 * @package Saluki\Setup\Contract
 */
interface SetupStepResultInterface
{
    /**
     * @return bool
     */
    public function succeeded(): bool;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @return bool
     */
    public function isRequired(): bool;

    /**
     * @return array
     */
    public function getExtra(): array;
}
