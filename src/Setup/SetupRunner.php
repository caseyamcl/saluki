<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 2:48 PM
 */

namespace Saluki\Setup;

use Saluki\Setup\Service\SetupService;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

class SetupRunner
{
    /**
     * @var Application
     */
    private $consoleApp;

    /**
     * SetupRunner constructor.
     * @param SetupService $setupService
     * @param string $appName
     */
    public function __construct(SetupService $setupService, string $appName)
    {
        $this->consoleApp = new Application($appName);
        $this->consoleApp->add(new ConsoleCommand\Setup($setupService));
        $this->consoleApp->setDefaultCommand(ConsoleCommand\Setup::NAME, true);
    }

    /**
     * @param null|InputInterface $input
     * @param null|OutputInterface $output
     * @return int
     */
    public function run(?InputInterface $input = null, ?OutputInterface $output = null): int
    {
        $output = $output ?: new ConsoleOutput();
        return (int) $this->consoleApp->run($input, $output);
    }
}
