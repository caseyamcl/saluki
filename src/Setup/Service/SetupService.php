<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Setup\Service;

use MJS\TopSort\Implementations\StringSort;
use Saluki\Container\Model\SalukiParams;
use Saluki\SalukiApp;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Registry\SetupStepRegistry;

/**
 * Class SetupService
 * @package Saluki\Setup\Service
 */
class SetupService
{
    /**
     * @var SalukiApp
     */
    private $app;

    /**
     * SetupService constructor.
     * @param SalukiApp $app
     */
    public function __construct(SalukiApp $app)
    {
        $this->app = $app;
    }

    /**
     * Run each step as an iterator
     *
     * @return iterable|SetupStepInterface[]
     */
    public function iterateSteps(): iterable
    {
        $params = $this->app->buildParams();

        // Pre-setup steps
        yield from $this->getPreContainerSetupSteps($params);

        // Post-setup steps (just pull them from the registry)
        $container = $this->app->getContainerBuilder()->build($params);
        yield from $container->get(SetupStepRegistry::class)->iteratePostContainerSetupSteps();
    }

    /**
     * Get pre-container setup steps
     *
     * Iterate over pre-container setup steps.
     * @param SalukiParams $params
     * @return \Generator|SetupStepInterface[]
     */
    protected function getPreContainerSetupSteps(SalukiParams $params): \Generator
    {
        // Build a copy of the DI containerBuilder
        $containerBuilder = $this->app->getContainerBuilder()->setupContainerBuilder(
            $params,
            $this->app->getContainerBuilder()->buildConfig($params)
        );

        $sorter = new StringSort();

        /** @var string $step */
        foreach ($params->getSetupSteps() as $step => $helper) {
            $isPreContainer = call_user_func([$step, 'isPreContainer']);
            if ($isPreContainer) {
                $containerBuilder->addDefinitions([$step => $helper ?: \DI\autowire($step)]);
                $sorter->add($step, call_user_func([$step, 'getDependsOn']));
            }
        }
        $container = $containerBuilder->build();
        foreach ($sorter->sort() as $stepClassName) {
            yield $container->get($stepClassName);
        }
    }
}
