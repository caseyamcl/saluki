<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 1:07 PM
 */

namespace Saluki\Setup\Step;

use Saluki\Console\ConsoleIO;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;

/**
 * Class CheckEnvironment
 * @package Saluki\Setup\Phase
 */
class SetupEnvironment implements SetupStepInterface
{
    /**
     * @return iterable|string[]
     */
    public static function getDependsOn(): array
    {
        return static::NONE;
    }


    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::PRE_CONTAINER;
    }

    /**
     * @return bool
     */
    public function isSetup(): bool
    {
        return true;
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        return SetupStepResult::note(sprintf('Timezone is <info>%s</info>', date_default_timezone_get()));
    }
}
