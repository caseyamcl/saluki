<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/20/18
 * Time: 12:32 PM
 */

namespace Saluki\Setup\Step;


use Saluki\Console\ConsoleIO;
use Saluki\Container\Model\SalukiParams;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;

class DeleteCompiledContainer implements SetupStepInterface
{
    /**
     * @var SalukiParams
     */
    private $params;

    /**
     * DeleteCompiledContainer constructor.
     * @param SalukiParams $params
     */
    public function __construct(SalukiParams $params)
    {
        $this->params = $params;
    }

    /**
     * Should this step run before the container is built?
     *
     * This should be TRUE for any step that does not explicitly rely on container services to run
     * Configuration and SalukiParams are available for dependency-injection.
     *
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::PRE_CONTAINER;
    }

    /**
     * An array of setup classes that must run before this runs
     *
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [SetupEnvironment::class];
    }

    /**
     * Check if the item that this step sets up has been setup
     *
     * @return bool
     */
    public function isSetup(): bool
    {
        return true;
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        if (file_exists($this->params->getCompilationPath())) {
            $result = @unlink($this->params->getCompilationPath());

            $message = $result
                ? 'removed previous container'
                : sprintf(
                    'failed to remove previous container file: <info>%s</info>',
                    $this->params->getCompilationPath()
                );

            return new SetupStepResult($result, $message);
        } else {
            return SetupStepResult::note('DI container not compiled; skipped removing prior instance.');
        }
    }
}
