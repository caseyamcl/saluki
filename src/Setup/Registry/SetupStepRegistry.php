<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 1:22 PM
 */

namespace Saluki\Setup\Registry;

use MJS\TopSort\Implementations\StringSort;
use Saluki\Container\Contract\SalukiRegistryInterface;
use Saluki\Setup\Contract\SetupStepInterface;

class SetupStepRegistry implements \IteratorAggregate, SalukiRegistryInterface
{
    /**
     * @var array|SetupStepInterface[]
     */
    private $items = [];

    /**
     * @var StringSort
     */
    private $sorter;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sorter = new StringSort();
    }

    /**
     * @param string $stepClassName
     * @return SetupStepInterface
     */
    public function get(string $stepClassName): SetupStepInterface
    {
        if ($this->has($stepClassName)) {
            return $this->items[$stepClassName];
        } else {
            throw new \RuntimeException("Setup step not registered: " . $stepClassName);
        }
    }

    /**
     * @param string $stepClassName
     * @return bool
     */
    public function has(string $stepClassName): bool
    {
        return array_key_exists($stepClassName, $this->items);
    }

    /**
     * @param SetupStepInterface $step
     */
    public function add(SetupStepInterface $step): void
    {
        $this->items[get_class($step)] = $step;
        $this->sorter->add(get_class($step), $step->getDependsOn());
    }

    /**
     * @return iterable|\Generator|SetupStepInterface[]
     */
    public function getIterator()
    {
        if (! empty($this->items)) {
            foreach ($this->sorter->sort() as $itemClass) {
                yield $this->items[$itemClass];
            }
        }
    }

    /**
     * Get post-container setup steps
     *
     * @return \Generator|SetupStepInterface[]
     */
    public function iteratePostContainerSetupSteps(): \Generator
    {
        foreach ($this->getIterator() as $setupStep) {
            if (! $setupStep::isPreContainer()) {
                yield $setupStep;
            }
        }
    }

    /**
     * Add an item to the registry
     *
     * @param SetupStepInterface $step
     */
    public function addItem($step): void
    {
        $this->add($step);
    }
}
