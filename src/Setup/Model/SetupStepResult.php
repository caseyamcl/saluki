<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 1:08 PM
 */

namespace Saluki\Setup\Model;

use Saluki\Setup\Contract\SetupStepResultInterface;

class SetupStepResult implements SetupStepResultInterface
{
    /**
     * @var bool
     */
    private $succeeded;

    /**
     * @var string
     */
    private $message;

    /**
     * @var bool
     */
    private $required;

    /**
     * @var array
     */
    private $extra = [];

    /**
     * @param string $message
     * @param bool $required
     * @param array $extra
     * @return SetupStepResult
     */
    public static function success(string $message, bool $required = true, array $extra = [])
    {
        return new static(true, $message, $required, $extra);
    }

    /**
     * @param string $message
     * @param bool $required
     * @param array $extra
     * @return SetupStepResult
     */
    public static function fail(string $message, bool $required = true, array $extra = [])
    {
        return new static(false, $message, $required, $extra);
    }

    /**
     * @param string $message
     * @param array $extra
     * @return SetupStepResult
     */
    public static function note(string $message, array $extra = [])
    {
        return new static(true, $message, false, $extra);
    }


    /**
     * SetupStepResult constructor.
     * @param bool $succeeded
     * @param string $message
     * @param bool $required
     * @param array $extra
     */
    public function __construct(bool $succeeded, string $message, bool $required = true, array $extra = [])
    {
        $this->succeeded = $succeeded;
        $this->message = $message;
        $this->required = $required;
        $this->extra = $extra;
    }

    /**
     * @return bool
     */
    public function succeeded(): bool
    {
        return $this->succeeded;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @return array
     */
    public function getExtra(): array
    {
        return $this->extra;
    }
}
