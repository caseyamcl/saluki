<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/21/18
 * Time: 1:56 PM
 */

namespace Saluki\Setup\ConsoleCommand;

use Saluki\Console\ConsoleIO;
use Saluki\Setup\Service\SetupService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Setup
 * @package Saluki\Setup\ConsoleCommand
 */
class Setup extends Command
{
    const NAME = 'setup';

    /**
     * @var SetupService
     */
    private $setupService;

    /**
     * Setup constructor.
     * @param SetupService $setupService
     */
    public function __construct(SetupService $setupService)
    {
        $this->setupService = $setupService;
        parent::__construct();
    }

    /**
     * Configure
     */
    protected function configure()
    {
        $this->setName(static::NAME);
        $this->setDescription('Setup the application');
        $this->addOption(
            'yes',
            'y',
            InputOption::VALUE_NONE,
            'Auto-answer "yes" to all prompts for risky actions'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new ConsoleIO($input, $output);

        $io->title($this->getApplication()->getName() . ' Setup');

        foreach ($this->setupService->iterateSteps() as $step) {
            $result = $step->__invoke($io);

            if ($result->succeeded()) {
                $io->success($result->getMessage());
            } elseif (! $result->isRequired()) {
                $io->warning($result->getMessage());
            } else {
                $io->fail($result->getMessage());
                return $io->abort();
            }
        }

        // Succeeded
        $io->writeln('');
        $io->success('Setup succeeded');
        return 0;
    }
}
