<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Logging;

use Monolog\Handler\HandlerInterface;
use Psr\Log\LoggerInterface;
use Saluki\Container\Contract\SalukiProviderInterface;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Logging\Factory\LoggerFactory;
use Saluki\Logging\Registry\Loggers;

class LoggingProvider implements SalukiProviderInterface
{
    /**
     * @param SalukiBuilder $builder
     * @return void
     */
    public function register(SalukiBuilder $builder): void
    {
        // Any handlers will be added to the error reporters
        $builder->addRegistry(Loggers::class, HandlerInterface::class);
        $builder->addServiceDefinition(LoggerInterface::class, \DI\factory(new LoggerFactory()));
    }
}
