<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Logging\Factory;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Saluki\Config\Model\Config;
use Saluki\Logging\Registry\Loggers;

/**
 * Class BaseLoggerFactory
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class LoggerFactory
{
    private string $loggerName;

    /**
     * LoggerFactory constructor.
     *
     * @param string $loggerName
     */
    public function __construct(string $loggerName = '')
    {
        $this->loggerName = $loggerName;
    }

    /**
     * @param Config $config
     * @param Loggers $errorReporters
     * @return LoggerInterface
     */
    public function __invoke(Config $config, Loggers $errorReporters): LoggerInterface
    {
        $logger = new Logger($this->loggerName);
        $logger->pushHandler($this->buildErrorLogHandler($config));
        return $logger;
    }

    /**
     * @param Config $config
     * @return ErrorLogHandler
     */
    public function buildErrorLogHandler(Config $config): ErrorLogHandler
    {
        $handler = new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, $config->getLogLevelInt());
        $formatter = new LineFormatter();
        $formatter->includeStacktraces();
        $handler->setFormatter($formatter);
        return $handler;
    }
}
