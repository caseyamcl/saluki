<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Logging\Registry;

use Monolog\Handler\GroupHandler;
use Monolog\Handler\HandlerInterface;
use Saluki\Container\Contract\SalukiRegistryInterface;

/**
 * Class ErrorReporters
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class Loggers extends GroupHandler implements SalukiRegistryInterface
{
    public function __construct(array $handlers = [], bool $bubble = true)
    {
        parent::__construct($handlers, $bubble);
    }

    /**
     * Add an item to the registry
     *
     * @param HandlerInterface $item
     * @return mixed
     */
    public function addItem($item): void
    {
        if (! $item instanceof HandlerInterface) {
            throw new \InvalidArgumentException('$item must be an instance of ' . HandlerInterface::class);
        }

        $this->handlers[] = $item;
    }
}
