<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Utility;

use Composer\Autoload\ClassLoader;
use Iterator;
use ReflectionClass;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Path;

class DirectoryClassFinder
{
    private ClassNameGuesser $classNameGuesser;
    private ?string $baseDirectory;

    /**
     * DirectoryClassFinder constructor.
     * @param string|null $baseDirectory
     * @param ClassNameGuesser|null $classNameGuesser
     */
    public function __construct(?string $baseDirectory = null, ?ClassNameGuesser $classNameGuesser = null)
    {
        if (! $classNameGuesser) {
            $composerVendorPath = dirname((new ReflectionClass(ClassLoader::class))->getFileName());
            $classNameGuesser = ClassNameGuesser::fromComposerVendorPath($composerVendorPath);
        }

        $this->classNameGuesser = $classNameGuesser;
        $this->baseDirectory = $baseDirectory;
    }

    /**
     * @param string $dirName
     * @param bool $recursive
     * @param null|string $instanceOf
     * @return Iterator|string[]  Fully-qualified class name
     */
    public function find(string $dirName, bool $recursive = true, ?string $instanceOf = null): Iterator
    {
        // Assume that relative paths are relative to the source of the application.
        if (! Path::isAbsolute($dirName)) {
            $dirName = Path::join($this->baseDirectory, $dirName);
        }

        $finder = (new Finder())->in($dirName)->name('/\.php$/i');
        if (! $recursive) {
            $finder->depth('== 0');
        }

        foreach ($finder->getIterator() as $file) {
            $className = $this->classNameGuesser->resolveClassName($file);
            if ((! $instanceOf) or is_a($className, $instanceOf, true)) {
                yield $className;
            }
        }
    }
}
