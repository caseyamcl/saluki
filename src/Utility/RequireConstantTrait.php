<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/30/18
 * Time: 4:12 PM
 */

namespace Saluki\Utility;

use Webmozart\Assert\Assert;

/**
 * Class RequireConstantTrait
 * @package Saluki\Utility
 */
trait RequireConstantTrait
{
    /**
     * @param string $name
     * @param string $message
     * @return mixed
     */
    protected function requireConstant(string $name, string $message = '')
    {
        $constantName = 'static::' . $name;

        try {
            $value = constant($constantName);

            $message = $message ?: sprintf(
                "missing required constant '%s' in class: %s",
                $name,
                get_called_class()
            );
            Assert::notEmpty($value, $message);

            return $value;
        } catch (\InvalidArgumentException $e) {
            throw new \LogicException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
