<?php

/**
 * Saluki Library
 *
 * @license http://opensource.org/licenses/MIT
 * @link https://github.com/caseyamcl/saluki
 * @package caseyamcl/saluki
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 *
 * ------------------------------------------------------------------
 */

namespace Saluki\Utility;

use RuntimeException;
use SplFileInfo;
use Symfony\Component\Filesystem\Path;

class ClassNameGuesser
{
    /**
     * @var string[]  Values are paths, keys are namespace prefix
     */
    private array $namespaceDirectoryMap = [];

    public static function fromComposerVendorPath(string $vendorDirectory): ClassNameGuesser
    {
        $map = [];

        $autoloadFiles = [
            Path::join($vendorDirectory, '/autoload_psr4.php'),
            Path::join($vendorDirectory, '/autoload_namespaces.php')
        ];

        foreach ($autoloadFiles as $file) {
            if (! is_readable($file)) {
                continue;
            }

            foreach (include($file) as $ns => $paths) {
                foreach ($paths as $path) {
                    $map[$path] = $ns;
                }
            }
        }

        ksort($map);
        return new static($map);
    }

    public function __construct(array $namespaceDirectoryMap)
    {
        $this->namespaceDirectoryMap = $namespaceDirectoryMap;
    }

    public function resolveClassName(SplFileInfo $file): string
    {
        // Get the relative path
        try {
            $info = pathinfo($file->getRealPath());
            $basePath = $this->findBasePathForPath($info['dirname']);
            $baseNs = $this->namespaceDirectoryMap[$basePath];

            $relative = Path::makeRelative(sprintf('%s/%s', $info['dirname'], $info['filename']), $basePath);

            return ltrim(sprintf(
                "\\%s\\%s",
                trim($baseNs, "\\"),
                trim(str_replace('/', "\\", $relative), "\\")
            ), "\\");
        } catch (\Throwable $e) {
            $msg = sprintf('Cannot guess namespace for path: %s (%s)' . $file->getRealPath(), $e->getMessage());
            throw new RuntimeException($msg, $e->getCode(), $e);
        }
    }

    /**
     * Find the registered base path for a given path
     *
     * @param string $path
     * @return string  Base Namespace
     * @throws RuntimeException  If no registered base path is found
     */
    public function findBasePathForPath(string $path): string
    {
        foreach (array_keys($this->namespaceDirectoryMap) as $possiblePath) {
            if (substr($path, 0, strlen($possiblePath)) === $possiblePath) {
                return $possiblePath;
            }
        }

        // If made it here..
        throw new RuntimeException("No namespace found for path: " . $path);
    }
}
