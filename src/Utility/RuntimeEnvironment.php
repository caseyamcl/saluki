<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Utility;

/**
 * Class Environment
 * @package Saluki\Utility
 */
class RuntimeEnvironment
{
    /**
     * @var string
     */
    private $appClass;

    /**
     * @var string
     */
    private $baseDirectory;

    /**
     * @var string
     */
    private $sourceDirectory;

    /**
     * Environment constructor.
     * @param string $appClass
     * @param string $baseDirectory
     * @param string $sourceDirectory
     */
    public function __construct(string $appClass, string $baseDirectory, string $sourceDirectory)
    {
        $this->appClass = $appClass;
        $this->baseDirectory = $baseDirectory;
        $this->sourceDirectory = $sourceDirectory;
    }

    /**
     * @return string
     */
    public function getAppClass(): string
    {
        return $this->appClass;
    }

    /**
     * @return string
     */
    public function getBaseDirectory(): string
    {
        return $this->baseDirectory;
    }

    /**
     * @return string
     */
    public function getSourceDirectory(): string
    {
        return $this->sourceDirectory;
    }
}
