<?php

/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace Saluki\Utility;

use Laminas\Diactoros\Stream;

use function fopen;

/**
 * Class StringStream
 * @package Saluki\Utility
 */
class StringStream extends Stream
{
    /**
     * StringStream constructor.
     * @param string $text
     */
    public function __construct(string $text)
    {
        $handle = fopen('php://memory', 'r+');
        fwrite($handle, $text);
        rewind($handle);

        parent::__construct($handle);
    }
}
