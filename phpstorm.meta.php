<?php

namespace PHPSTORM_META
{
    /** @noinspection PhpUnhandledExceptionInspection */
    /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
    /** @noinspection PhpDynamicAsStaticMethodCallInspection */
    /** @noinspection PhpUnusedLocalVariableInspection */

    $STATIC_METHOD_TYPES = [
        \Psr\Container\ContainerInterface::get('') => [
            "" == "@",
        ],
        \DI\Container::get('') => [
            "" == "@",
        ],
    ];
}