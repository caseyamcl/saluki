#!/usr/bin/env bash
#
#  Saluki Library
#
#  @license http://opensource.org/licenses/MIT
#  @link https://github.com/caseyamcl/saluki
#  @package caseyamcl/saluki
#  @author Casey McLaughlin <caseyamcl@gmail.com>
#
#  For the full copyright and license information, please view the LICENSE.md
#  file that was distributed with this source code.
#
#  ------------------------------------------------------------------
#

# Figure out path to this script
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null
cd "$SCRIPTPATH/.."

# Run
/usr/bin/php81 /usr/local/bin/composer "$@"
