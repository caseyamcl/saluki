# Saluki 

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Saluki is a simple, opinionated REST API framework using a set of established libraries.

What it provides (use any or all):

* Doctrine ORM implementation with sensible Migration defaults
* API-friendly authentication:
    * registration
    * forgot password flow
    * email/notification-based verification & login, etc.
    * Twig-based email templates (long-running-process-friendly**)
    * rate-limiting based on IP, query/body parameters, or HTTP headers
* OAuth 2.0 implementation
* Email implementation via SwiftMailer
* PSR-7 HTTP implementation
    * Endpoints are classes
* Build utilities
    * Easily generate a PHAR
* Dependency injection via PHP-DI
    * Doctrine Repository DI support (`EntityManager::getRepository()` tries container before
      falling back on default method)
* Extensible validation
* Sane logging defaults (12-factor-app friendly)

The stack:

* Doctrine ORM
* Doctrine Migrations
* Monolog
* Zend Expressive
* Symfony Console
* CakePHP Validation
* Chronos
* Configula
* Ramsey UUID
* SwiftMailer
* ShortID
* zxcvbn-php
* League OAuth 2 Server

## Structure

If any of the following are applicable to your project, then the directory structure should follow industry 
best practices by being named the following.

```
bin/        
config/
src/
tests/
vendor/
```


## Install

Via Composer

``` bash
$ composer require caseyamcl/saluki
```

## Usage

``` php
$saluki = new CaseyAMcL\Saluki();
$config = $saluki->getConfig();

```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email caseyamcl@gmail.com instead of using the issue tracker.

## Credits

- [Casey McLaughlin][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/caseyamcl/saluki.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/caseyamcl/saluki/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/caseyamcl/saluki.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/caseyamcl/saluki.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/caseyamcl/saluki.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/caseyamcl/saluki
[link-travis]: https://travis-ci.org/caseyamcl/saluki
[link-scrutinizer]: https://scrutinizer-ci.com/g/caseyamcl/saluki/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/caseyamcl/saluki
[link-downloads]: https://packagist.org/packages/caseyamcl/saluki
[link-author]: https://github.com/caseyamcl
[link-contributors]: ../../contributors
