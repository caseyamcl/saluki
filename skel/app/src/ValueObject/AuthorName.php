<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

declare(strict_types=1);

namespace SalukiExample\ValueObject;

/**
 * Class AuthorName
 *
 * @author Casey McLaughlin <caseyamcl@gmail.com>
 */
class AuthorName
{
    public function __construct(
        public readonly string $firstName,
        public readonly string $lastName
    ) {
    }

    public function getFullName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }
}
