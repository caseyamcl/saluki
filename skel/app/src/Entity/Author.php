<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 8/30/18
 * Time: 3:03 PM
 */

namespace SalukiExample\Entity;

use Cake\Chronos\Chronos;
use Cake\Chronos\ChronosInterface;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use SalukiExample\ValueObject\AuthorName;

/**
 * Class Author
 * @package SalukiExample\Entity
 */
#[ORM\Entity]
class Author
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private UuidInterface $id;

    #[ORM\Column(type: 'string')]
    private string $name;

    /**
     * @var Collection<int,Book>
     */
    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Book::class, cascade: ['ALL'])]
    private Collection $books;

    #[ORM\Column(type: 'chronos_datetime', nullable: false)]
    private DateTimeInterface $created;


    public function __construct(AuthorName $name)
    {
        $this->id = Uuid::uuid4();
        $this->name = $name->getFullName();
        $this->created = Chronos::now();
        $this->books = new ArrayCollection();
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Collection<int,Book>
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function addBook(Book $book): void
    {
        if (! $this->books->contains($book)) {
            $this->books->add($book);
        }
    }

    public function removeBook(Book $book): void
    {
        if ($this->books->contains($book)) {
            $this->books->removeElement($book);
        }
    }

    public function setAuthorName(AuthorName $authorName): void
    {
        $this->name = $authorName->getFullName();
    }
}
