<?php
/*
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace SalukiExample\Entity;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;

class BookEventListener implements EventSubscriber
{
    public function prePersist(LifecycleEventArgs $args)
    {
        if (! $args->getObject() instanceof Book) {
            return;
        }

        /** @var Book $book */
        $book = $args->getObject();
        $book->setName(strtoupper($book->getName()));

        error_log('--> Test -> prePersist event triggered for book');
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        if (! $args->getObject() instanceof Book) {
            return;
        }

        error_log('--> Test -> preUpdate event triggered for book');

        /** @var Book $book */
        $book = $args->getObject();
        $book->setName(strtoupper($book->getName()));

        // error_log('--> Test -> Change set: ' . var_export($args->getEntityChangeSet(), true));
        // $args->setNewValue('name', strtoupper($book->getName()));
    }

    public function getSubscribedEvents(): array
    {
        return [Events::prePersist, Events::preUpdate];
    }
}
