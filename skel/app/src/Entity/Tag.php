<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/14/18
 * Time: 4:19 PM
 */

namespace SalukiExample\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Tag
 * @package SalukiExample\Entity
 */
#[ORM\Table(name: 'book_tag')]
#[ORM\Entity]
class Tag
{
    #[ORM\Column(type: 'uuid')]
    #[ORM\Id]
    private UuidInterface $id;

    #[ORM\Column(type: 'string', nullable: false)]
    private string $name;

    /**
     * @var Collection<int,Book>
     */
    #[ORM\ManyToMany(targetEntity: 'SalukiExample\Entity\Book', mappedBy: 'tags', cascade: ['ALL'])]
    private Collection $books;

    /**
     * Tag constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->books = new ArrayCollection();
        $this->name = $name;
        $this->id = Uuid::uuid4();
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Collection<int,Book>
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }
}
