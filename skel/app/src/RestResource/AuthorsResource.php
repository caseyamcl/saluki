<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 9/7/18
 * Time: 1:57 PM
 */

namespace SalukiExample\RestResource;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Field\FieldListBuilder;
use Saluki\RestResource\Model\AbstractRestResource;
use Saluki\RestResource\Model\RestResourceAccess;
use Saluki\WebInterface\Model\SalukiRequest;
use SalukiExample\Entity\Author;
use SalukiExample\ValueObject\AuthorName;

/**
 * Class AuthorsResource
 * @package SalukiExample\RestResource
 */
class AuthorsResource extends AbstractRestResource
{
    public const ENTITY_CLASS = Author::class;
    public const NAME = 'authors';
    public const PAGINATION_DEFAULT_LIMIT = 5;

    /**
     * Configure access
     *
     * @param RestResourceAccess $access
     */
    protected function setAccess(RestResourceAccess $access): void
    {
        // Everybody has access.
        $access->default = true;
    }

    /**
     * Add any additional rules for queries here (specifically, user or role-based access)
     *
     * @param SalukiRequest $request
     * @param QueryBuilder $qb
     */
    protected function prepareIndexQuery(SalukiRequest $request, QueryBuilder $qb): void
    {
        // Pass... No special rules here.
    }

    protected function configureFields(FieldListBuilder $fields): void
    {
        $fields->string('name')
            ->description('Author name')
            ->writable()
                ->requiredOnCreate()
                ->transform(function ($authorName) {
                    if ($authorName instanceof AuthorName) {
                        return $authorName;
                    } else {
                        [$firstName, $lastName] = explode(' ', (string) $authorName, 2);
                        return new AuthorName($firstName, $lastName);
                    }
                })
            ->readable()->sortable()->filterable();

        $fields->datetime('created')
            ->readable()->filterable();

        $fields->relationship('books')
            ->writable()->optionalOnCreate();
    }
}
