<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/14/18
 * Time: 4:50 PM
 */

namespace SalukiExample\RestResource;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Field\FieldListBuilder;
use Saluki\RestResource\Model\AbstractRestResource;
use Saluki\RestResource\Model\RestResourceAccess;
use Saluki\WebInterface\Model\SalukiRequest;
use SalukiExample\Entity\Tag;

class TagsResource extends AbstractRestResource
{
    public const ENTITY_CLASS = Tag::class;
    public const NAME = 'tags';
    public const ID_NAME = 'name';

    /**
     * Configure fields
     *
     * @param FieldListBuilder $fields
     */
    protected function configureFields(FieldListBuilder $fields): void
    {
        $fields->string('name')
            ->readable()
            ->writable()
                ->requiredOnCreate()
                ->length(2, 255)
                ->sanitize();
    }

    /**
     * Configure access
     *
     * @param RestResourceAccess $access
     */
    protected function setAccess(RestResourceAccess $access): void
    {
        $access->allowAll();
    }

    /**
     * Add any additional rules for queries here (specifically, user or role-based access)
     *
     * @param SalukiRequest $request
     * @param QueryBuilder $qb
     */
    protected function prepareIndexQuery(SalukiRequest $request, QueryBuilder $qb): void
    {
        // no restrictions
    }
}
