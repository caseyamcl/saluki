<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

namespace SalukiExample\RestResource;

use Doctrine\ORM\QueryBuilder;
use Saluki\RestResource\Field\FieldListBuilder;
use Saluki\RestResource\Model\AbstractRestResource;
use Saluki\RestResource\Model\RestResourceAccess;
use Saluki\Validation\ValidatorContext;
use Saluki\WebInterface\Model\SalukiRequest;
use SalukiExample\Entity\Book;

class BooksResource extends AbstractRestResource
{
    public const ENTITY_CLASS = Book::class;
    public const NAME = 'books';

    /**
     * Configure fields
     *
     * @param FieldListBuilder $fields
     */
    protected function configureFields(FieldListBuilder $fields): void
    {
        $fields->string('name')
            ->readable()->filterable()
            ->writable()->requiredOnCreate()->callback(function ($value, ValidatorContext $context) {
                if (! $context->hasOtherDataItem('request')) {
                    throw new \Exception("Something went wrong...");
                }
            });

        $fields->relationship('author')
            ->readable()
            ->writable()->requiredOnCreate()->notBlank();

        $fields->relationship('tags')
            ->readable()
            ->writable()->optionalOnCreate();
    }

    /**
     * Configure access
     *
     * @param RestResourceAccess $access
     */
    protected function setAccess(RestResourceAccess $access): void
    {
        $access->allowAll();
    }

    /**
     * Add any additional rules for queries here (specifically, user or role-based access)
     *
     * @param SalukiRequest $request
     * @param QueryBuilder $qb
     */
    protected function prepareIndexQuery(SalukiRequest $request, QueryBuilder $qb): void
    {
        // TODO: Implement prepareIndexQuery() method.
    }
}
