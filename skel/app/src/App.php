<?php

namespace SalukiExample;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Saluki\ApiDocumenter\RestResource\IndexRestResource;
use Saluki\Authentication\Endpoint\AuthEndpoint;
use Saluki\Authentication\Endpoint\UserRegisterEndpoint;
use Saluki\Authentication\RestResource\UserResource;
use Saluki\Container\Model\SalukiBuilder;
use Saluki\Emailer\Setting\EmailDefaultLocalPart;
use Saluki\Emailer\Setting\EmailDomain;
use Saluki\Emailer\Setting\SmtpAuthMode;
use Saluki\Emailer\Setting\SmtpHost;
use Saluki\Emailer\Setting\SmtpPassword;
use Saluki\Emailer\Setting\SmtpPort;
use Saluki\Emailer\Setting\SmtpUsername;
use Saluki\SalukiApp;
use Saluki\Settings\Endpoint\SettingsIndexEndpoint;
use Saluki\Settings\Endpoint\SettingsPutEndpoint;
use Saluki\Settings\Endpoint\SettingsRetrieveEndpoint;
use Saluki\WebInterface\Model\RouteGroup;
use Saluki\WebInterface\Service\BackOffMiddlewareFactory;
use SalukiExample\Entity\BookEventListener;
use SalukiExample\RestResource\AuthorsResource;
use SalukiExample\RestResource\BooksResource;
use SalukiExample\RestResource\TagsResource;
use SalukiExample\Setup\SetupDummyData;

/**
 * Class App
 * @package SalukiExample
 */
class App extends SalukiApp
{
    /**
     * @param SalukiBuilder $builder
     */
    protected function configure(SalukiBuilder $builder): void
    {
        $this->registerBuiltInServices($builder);

        $builder->setConfigDefaults([
            'db'                 => ['driver' => 'pdo_sqlite', 'path' => '/tmp/saluki.db'],
            'encryption_enabled' => true,
            'encryption_key'     => 'def00000d30a520eac17ef48284ec3ca7f951d7d6ef6c41f3f8e1feb65b8c28048d508b0465f4e103a5dc8a455dd62a691d3e86b42f3ca67249cabdbea900acad7c9903b',
            'log_level'          => 'info',
            'dev_mode'           => true,
            'settings' => [
                SmtpHost::NAME     => 'smtp.mailtrap.io',
                SmtpPort::NAME     => 2525,
                SmtpUsername::NAME => '5d5055bb701688',
                SmtpPassword::NAME => '20c0f412d5334d',
                SmtpAuthMode::NAME => 'cram-md5',
                EmailDefaultLocalPart::NAME => 'system',
                EmailDomain::NAME => 'rcc.fsu.edu'
            ]
        ]);

        $builder->setMigrationsLocation(
            __DIR__ . '/DatabaseMigration',
            __NAMESPACE__ . '\DatabaseMigration'
        );

        $builder->addSetupStep(SetupDummyData::class);
        $builder->addFromDirectory(__DIR__ . '/RestResource');

        // Testing event listener...
        $builder->addFromDirectory(__DIR__ . '/EventListener');
        $builder->addBuildCallback(function (ContainerInterface $c) {
            $em = $c->get(EntityManagerInterface::class);
            $em->getEventManager()->addEventSubscriber(new BookEventListener());
        });

        // Specify entity classes directory
        $builder->addEntityDirectory($this->getSourcePath('/Entity'));
    }

    // --------------------------------------------------------------

    /**
     * @param RouteGroup $routes
     * @param ContainerInterface $container
     */
    public function routes(RouteGroup $routes, ContainerInterface $container): void
    {
        $backoff = $container->get(BackOffMiddlewareFactory::class)->all();

        $routes->resource('/users', $container->get(UserResource::class));

        $routes->post('/auth', $container->get(AuthEndpoint::class))->add($backoff);
        $routes->post('/register', $container->get(UserRegisterEndpoint::class))->add($backoff);

        $routes->get('/settings/{name}', $container->get(SettingsRetrieveEndpoint::class));
        $routes->get('/settings', $container->get(SettingsIndexEndpoint::class));
        $routes->put('/settings/{name}', $container->get(SettingsPutEndpoint::class));

        $routes->resource('/authors', $container->get(AuthorsResource::class));
        $routes->resource('/books', $container->get(BooksResource::class));
        $routes->resource('/tags', $container->get(TagsResource::class));

        $routes->resource('', $container->get(IndexRestResource::class));
    }
    /**
     * @return string
     */
    public function appName(): string
    {
        return 'Skeleton App';
    }
}
