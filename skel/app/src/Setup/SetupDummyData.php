<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */


namespace SalukiExample\Setup;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Faker\Factory;
use Faker\ORM\Doctrine\Populator;
use Ramsey\Uuid\Uuid;
use Saluki\Console\ConsoleIO;
use Saluki\Persistence\Service\TransactionManager;
use Saluki\Persistence\Setup\DbUpToDateSetupStep;
use Saluki\Setup\Contract\SetupStepInterface;
use Saluki\Setup\Contract\SetupStepResultInterface;
use Saluki\Setup\Model\SetupStepResult;
use SalukiExample\Entity\Author;
use SalukiExample\Entity\Book;
use SalukiExample\Entity\Tag;

class SetupDummyData implements SetupStepInterface
{
    private EntityManagerInterface $entityManager;
    private TransactionManager $transactionManager;

    /**
     * SetupDummyData constructor.
     * @param EntityManagerInterface $entityManager
     * @param TransactionManager $transactionManager
     */
    public function __construct(EntityManagerInterface $entityManager, TransactionManager $transactionManager)
    {
        $this->entityManager = $entityManager;
        $this->transactionManager = $transactionManager;
    }

    /**
     * @return bool
     */
    public static function isPreContainer(): bool
    {
        return self::POST_CONTAINER;
    }

    /**
     * @return array|string[]
     */
    public static function getDependsOn(): array
    {
        return [DbUpToDateSetupStep::class];
    }

    /**
     * @return bool
     */
    public function isSetup(): bool
    {
        $existing = $this->entityManager->getRepository(Author::class)->findAll();
        return count($existing) > 0;
    }

    /**
     * @param ConsoleIO $io
     * @return SetupStepResultInterface
     */
    public function __invoke(ConsoleIO $io): SetupStepResultInterface
    {
        if ($this->isSetup()) {
            return SetupStepResult::success('Dummy data is already setup');
        }

        // Create fake data
        $this->transactionManager->execute(function () {

            $tagModifier = function (Tag $tag, array $inserted) {
                /** @var array|Book[] $books */
                $books = $inserted[Book::class];
                shuffle($books);
                $books = array_values($books);

                for ($i = 0; $i < rand(1, count($books)); $i++) {
                    $books[$i]->addTag($tag);
                }
            };

            $generator = Factory::create();
            $populator = new Populator($generator, $this->entityManager);
            $populator->addEntity(Author::class, 30, [
                'id'   => fn() => Uuid::uuid4(),
                'name' => fn() => $generator->name,
                'created' => fn() => $generator->dateTimeBetween()
            ]);

            $populator->addEntity(Book::class, 40, [
                'tags' => fn() => new ArrayCollection(),
                'id'   => fn() => Uuid::uuid4(),
                'name' => fn() => $generator->text(50)
            ]);

            $populator->addEntity(Tag::class, 100, [
                'id'   => fn() => Uuid::uuid4(),
                'name' => fn() => $generator->word
            ], [$tagModifier]);

            $populator->execute();
        });

        // Now


        return SetupStepResult::success('Dummy data setup');
    }
}
