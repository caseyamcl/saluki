<?php
/**
 *  Saluki Library
 *
 *  @license http://opensource.org/licenses/MIT
 *  @link https://github.com/caseyamcl/saluki
 *  @package caseyamcl/saluki
 *  @author Casey McLaughlin <caseyamcl@gmail.com>
 *
 *  For the full copyright and license information, please view the LICENSE.md
 *  file that was distributed with this source code.
 *
 *  ------------------------------------------------------------------
 */

/**
 * Created by PhpStorm.
 * User: casey
 * Date: 11/13/18
 * Time: 1:42 PM
 */

namespace SalukiExample\EventListener;

use Doctrine\Common\Collections\Collection;
use Saluki\Emailer\Model\EmailMessage;
use Saluki\Emailer\Service\EmailService;
use Saluki\RestResource\Event\RestFieldUpdateEvent;
use Saluki\RestResource\Event\RestObjectEvent;
use Saluki\RestResource\Event\RestRequestEvents;
use SalukiExample\Entity\Book;
use SalukiExample\Entity\Tag;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TestBookEventListener implements EventSubscriberInterface
{
    private EmailService $emailService;

    public function __construct(EmailService $emailService)
    {
        $this->emailService = $emailService;
    }

    public function propertyEvent(RestFieldUpdateEvent $event, string $eventName)
    {
        error_log(sprintf(
            '--> TEST -> Detected property change for %s.%s (%s) (%s)',
            get_class($event->getObject()),
            $event->getPropertyName(),
            $event->getFieldName(),
            $eventName
        ));
    }

    public function objectEvent(RestObjectEvent $event, string $eventName)
    {
        $email = new EmailMessage(
            'test@example.org',
            'Test Event Listener',
            'Test Body'
        );

        $this->emailService->sendMessage($email);

        error_log(sprintf(
            "--> TEST -> Detected object event for %s (%s)",
            get_class($event->getObject()),
            $eventName
        ));
    }

    public function relationshipEvent(RestFieldUpdateEvent $event)
    {
        /** @var Collection|Tag[] $old */
        $old = $event->getOldValue();
        /** @var Collection|Tag[] $new */
        $new = $event->getNewValue();

        error_log(sprintf(
            '--> TEST -> Detected relationship change for %s: %s <===> %s',
            get_class($event->getObject()),
            implode(', ', $old->map(function(Tag $tag) { return $tag->getName(); })->toArray()),
            implode(', ', $new->map(function(Tag $tag) { return $tag->getName(); })->toArray())
        ));
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            RestRequestEvents::fieldChange(Book::class, 'name') => 'propertyEvent',
            RestRequestEvents::fieldChange(Book::class, 'tags') => 'relationshipEvent',
            RestRequestEvents::anyAction(Book::class) => 'objectEvent'
        ];
    }
}
