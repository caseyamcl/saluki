<?php

declare(strict_types=1);

namespace SalukiExample\DatabaseMigration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181114213528 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE book_tag_join (book_id CHAR(36) NOT NULL --(DC2Type:uuid)
        , tag_id CHAR(36) NOT NULL --(DC2Type:uuid)
        , PRIMARY KEY(book_id, tag_id))');
        $this->addSql('CREATE INDEX IDX_DF91713816A2B381 ON book_tag_join (book_id)');
        $this->addSql('CREATE INDEX IDX_DF917138BAD26311 ON book_tag_join (tag_id)');
        $this->addSql('CREATE TABLE book_tag (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user_token AS SELECT id, created_timestamp, revoked_timestamp, expires_timestamp, user_id, type FROM user_token');
        $this->addSql('DROP TABLE user_token');
        $this->addSql('CREATE TABLE user_token (id CHAR(36) NOT NULL COLLATE BINARY --(DC2Type:uuid)
        , user_id VARCHAR(255) NOT NULL COLLATE BINARY, type VARCHAR(255) NOT NULL COLLATE BINARY, created_timestamp DATETIME NOT NULL, revoked_timestamp DATETIME DEFAULT NULL, expires_timestamp DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO user_token (id, created_timestamp, revoked_timestamp, expires_timestamp, user_id, type) SELECT id, created_timestamp, revoked_timestamp, expires_timestamp, user_id, type FROM __temp__user_token');
        $this->addSql('DROP TABLE __temp__user_token');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, username, display_name, email, password, password_timestamp, created_timestamp, verified_timestamp, approved_timestamp, roles FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL COLLATE BINARY --(DC2Type:uuid)
        , username VARCHAR(255) NOT NULL COLLATE BINARY, display_name VARCHAR(512) NOT NULL COLLATE BINARY, email VARCHAR(512) NOT NULL COLLATE BINARY, password VARCHAR(1024) DEFAULT NULL COLLATE BINARY, roles CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        , password_timestamp DATETIME DEFAULT NULL, created_timestamp DATETIME NOT NULL, verified_timestamp DATETIME DEFAULT NULL, approved_timestamp DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO user (id, username, display_name, email, password, password_timestamp, created_timestamp, verified_timestamp, approved_timestamp, roles) SELECT id, username, display_name, email, password, password_timestamp, created_timestamp, verified_timestamp, approved_timestamp, roles FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__author AS SELECT id, name, created FROM author');
        $this->addSql('DROP TABLE author');
        $this->addSql('CREATE TABLE author (id CHAR(36) NOT NULL COLLATE BINARY --(DC2Type:uuid)
        , name VARCHAR(255) NOT NULL COLLATE BINARY, created DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO author (id, name, created) SELECT id, name, created FROM __temp__author');
        $this->addSql('DROP TABLE __temp__author');
        $this->addSql('DROP INDEX IDX_CBE5A331F675F31B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__book AS SELECT id, author_id, name FROM book');
        $this->addSql('DROP TABLE book');
        $this->addSql('CREATE TABLE book (id CHAR(36) NOT NULL COLLATE BINARY --(DC2Type:uuid)
        , author_id CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , name VARCHAR(255) NOT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_CBE5A331F675F31B FOREIGN KEY (author_id) REFERENCES author (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO book (id, author_id, name) SELECT id, author_id, name FROM __temp__book');
        $this->addSql('DROP TABLE __temp__book');
        $this->addSql('CREATE INDEX IDX_CBE5A331F675F31B ON book (author_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_client AS SELECT id, secret, description, created_timestamp, allowed_ips, owner_id, system_key, enabled, domain_names FROM oauth_client');
        $this->addSql('DROP TABLE oauth_client');
        $this->addSql('CREATE TABLE oauth_client (id VARCHAR(64) NOT NULL COLLATE BINARY, secret VARCHAR(512) NOT NULL COLLATE BINARY, description CLOB DEFAULT NULL COLLATE BINARY, allowed_ips CLOB NOT NULL COLLATE BINARY --(DC2Type:json_array)
        , owner_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, system_key VARCHAR(512) DEFAULT NULL COLLATE BINARY, enabled BOOLEAN NOT NULL, domain_names CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        , created_timestamp DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO oauth_client (id, secret, description, created_timestamp, allowed_ips, owner_id, system_key, enabled, domain_names) SELECT id, secret, description, created_timestamp, allowed_ips, owner_id, system_key, enabled, domain_names FROM __temp__oauth_client');
        $this->addSql('DROP TABLE __temp__oauth_client');
        $this->addSql('DROP INDEX IDX_55DCF7552CCB2688');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_refresh_token AS SELECT id, access_token_id, expires, created_date_time, revoked FROM oauth_refresh_token');
        $this->addSql('DROP TABLE oauth_refresh_token');
        $this->addSql('CREATE TABLE oauth_refresh_token (id VARCHAR(100) NOT NULL COLLATE BINARY, access_token_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, expires DATETIME NOT NULL, created_date_time DATETIME NOT NULL, revoked DATETIME DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_55DCF7552CCB2688 FOREIGN KEY (access_token_id) REFERENCES oauth_access_token (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO oauth_refresh_token (id, access_token_id, expires, created_date_time, revoked) SELECT id, access_token_id, expires, created_date_time, revoked FROM __temp__oauth_refresh_token');
        $this->addSql('DROP TABLE __temp__oauth_refresh_token');
        $this->addSql('CREATE INDEX IDX_55DCF7552CCB2688 ON oauth_refresh_token (access_token_id)');
        $this->addSql('DROP INDEX IDX_F7FA86A419EB6921');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_access_token AS SELECT id, client_id, user_id, created_timestamp, expires_timestamp FROM oauth_access_token');
        $this->addSql('DROP TABLE oauth_access_token');
        $this->addSql('CREATE TABLE oauth_access_token (id VARCHAR(255) NOT NULL COLLATE BINARY, client_id VARCHAR(64) DEFAULT NULL COLLATE BINARY, user_id VARCHAR(255) NOT NULL COLLATE BINARY, created_timestamp DATETIME NOT NULL, expires_timestamp DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT FK_F7FA86A419EB6921 FOREIGN KEY (client_id) REFERENCES oauth_client (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO oauth_access_token (id, client_id, user_id, created_timestamp, expires_timestamp) SELECT id, client_id, user_id, created_timestamp, expires_timestamp FROM __temp__oauth_access_token');
        $this->addSql('DROP TABLE __temp__oauth_access_token');
        $this->addSql('CREATE INDEX IDX_F7FA86A419EB6921 ON oauth_access_token (client_id)');
        $this->addSql('DROP INDEX IDX_C84CCF84E3A4EE59');
        $this->addSql('DROP INDEX IDX_C84CCF84888114B4');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_access_token_oauth_scope AS SELECT oauth_access_token_id, scope_name FROM oauth_access_token_oauth_scope');
        $this->addSql('DROP TABLE oauth_access_token_oauth_scope');
        $this->addSql('CREATE TABLE oauth_access_token_oauth_scope (oauth_access_token_id VARCHAR(255) NOT NULL COLLATE BINARY, scope_name VARCHAR(100) NOT NULL COLLATE BINARY, PRIMARY KEY(oauth_access_token_id, scope_name), CONSTRAINT FK_C84CCF84888114B4 FOREIGN KEY (oauth_access_token_id) REFERENCES oauth_access_token (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_C84CCF84E3A4EE59 FOREIGN KEY (scope_name) REFERENCES oauth_scope (name) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO oauth_access_token_oauth_scope (oauth_access_token_id, scope_name) SELECT oauth_access_token_id, scope_name FROM __temp__oauth_access_token_oauth_scope');
        $this->addSql('DROP TABLE __temp__oauth_access_token_oauth_scope');
        $this->addSql('CREATE INDEX IDX_C84CCF84E3A4EE59 ON oauth_access_token_oauth_scope (scope_name)');
        $this->addSql('CREATE INDEX IDX_C84CCF84888114B4 ON oauth_access_token_oauth_scope (oauth_access_token_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE book_tag_join');
        $this->addSql('DROP TABLE book_tag');
        $this->addSql('CREATE TEMPORARY TABLE __temp__author AS SELECT id, name, created FROM author');
        $this->addSql('DROP TABLE author');
        $this->addSql('CREATE TABLE author (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , name VARCHAR(255) NOT NULL, created DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO author (id, name, created) SELECT id, name, created FROM __temp__author');
        $this->addSql('DROP TABLE __temp__author');
        $this->addSql('DROP INDEX IDX_CBE5A331F675F31B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__book AS SELECT id, author_id, name FROM book');
        $this->addSql('DROP TABLE book');
        $this->addSql('CREATE TABLE book (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , name VARCHAR(255) NOT NULL, author_id CHAR(36) DEFAULT \'NULL --(DC2Type:uuid)\' COLLATE BINARY --(DC2Type:uuid)
        , PRIMARY KEY(id))');
        $this->addSql('INSERT INTO book (id, author_id, name) SELECT id, author_id, name FROM __temp__book');
        $this->addSql('DROP TABLE __temp__book');
        $this->addSql('CREATE INDEX IDX_CBE5A331F675F31B ON book (author_id)');
        $this->addSql('DROP INDEX IDX_F7FA86A419EB6921');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_access_token AS SELECT id, client_id, user_id, created_timestamp, expires_timestamp FROM oauth_access_token');
        $this->addSql('DROP TABLE oauth_access_token');
        $this->addSql('CREATE TABLE oauth_access_token (id VARCHAR(255) NOT NULL, client_id VARCHAR(64) DEFAULT NULL, user_id VARCHAR(255) NOT NULL, created_timestamp DATETIME NOT NULL, expires_timestamp DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO oauth_access_token (id, client_id, user_id, created_timestamp, expires_timestamp) SELECT id, client_id, user_id, created_timestamp, expires_timestamp FROM __temp__oauth_access_token');
        $this->addSql('DROP TABLE __temp__oauth_access_token');
        $this->addSql('CREATE INDEX IDX_F7FA86A419EB6921 ON oauth_access_token (client_id)');
        $this->addSql('DROP INDEX IDX_C84CCF84888114B4');
        $this->addSql('DROP INDEX IDX_C84CCF84E3A4EE59');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_access_token_oauth_scope AS SELECT oauth_access_token_id, scope_name FROM oauth_access_token_oauth_scope');
        $this->addSql('DROP TABLE oauth_access_token_oauth_scope');
        $this->addSql('CREATE TABLE oauth_access_token_oauth_scope (oauth_access_token_id VARCHAR(255) NOT NULL, scope_name VARCHAR(100) NOT NULL, PRIMARY KEY(oauth_access_token_id, scope_name))');
        $this->addSql('INSERT INTO oauth_access_token_oauth_scope (oauth_access_token_id, scope_name) SELECT oauth_access_token_id, scope_name FROM __temp__oauth_access_token_oauth_scope');
        $this->addSql('DROP TABLE __temp__oauth_access_token_oauth_scope');
        $this->addSql('CREATE INDEX IDX_C84CCF84888114B4 ON oauth_access_token_oauth_scope (oauth_access_token_id)');
        $this->addSql('CREATE INDEX IDX_C84CCF84E3A4EE59 ON oauth_access_token_oauth_scope (scope_name)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_client AS SELECT id, secret, description, created_timestamp, allowed_ips, owner_id, system_key, enabled, domain_names FROM oauth_client');
        $this->addSql('DROP TABLE oauth_client');
        $this->addSql('CREATE TABLE oauth_client (id VARCHAR(64) NOT NULL, secret VARCHAR(512) NOT NULL, description CLOB DEFAULT NULL, allowed_ips CLOB NOT NULL --(DC2Type:json_array)
        , owner_id VARCHAR(255) DEFAULT NULL, system_key VARCHAR(512) DEFAULT NULL, enabled BOOLEAN NOT NULL, domain_names CLOB NOT NULL --(DC2Type:json)
        , created_timestamp DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO oauth_client (id, secret, description, created_timestamp, allowed_ips, owner_id, system_key, enabled, domain_names) SELECT id, secret, description, created_timestamp, allowed_ips, owner_id, system_key, enabled, domain_names FROM __temp__oauth_client');
        $this->addSql('DROP TABLE __temp__oauth_client');
        $this->addSql('DROP INDEX IDX_55DCF7552CCB2688');
        $this->addSql('CREATE TEMPORARY TABLE __temp__oauth_refresh_token AS SELECT id, access_token_id, expires, created_date_time, revoked FROM oauth_refresh_token');
        $this->addSql('DROP TABLE oauth_refresh_token');
        $this->addSql('CREATE TABLE oauth_refresh_token (id VARCHAR(100) NOT NULL, access_token_id VARCHAR(255) DEFAULT NULL, expires DATETIME NOT NULL, created_date_time DATETIME NOT NULL, revoked DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO oauth_refresh_token (id, access_token_id, expires, created_date_time, revoked) SELECT id, access_token_id, expires, created_date_time, revoked FROM __temp__oauth_refresh_token');
        $this->addSql('DROP TABLE __temp__oauth_refresh_token');
        $this->addSql('CREATE INDEX IDX_55DCF7552CCB2688 ON oauth_refresh_token (access_token_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, username, display_name, email, password, password_timestamp, created_timestamp, verified_timestamp, approved_timestamp, roles FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , username VARCHAR(255) NOT NULL, display_name VARCHAR(512) NOT NULL, email VARCHAR(512) NOT NULL, password VARCHAR(1024) DEFAULT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password_timestamp DATETIME DEFAULT NULL, created_timestamp DATETIME NOT NULL, verified_timestamp DATETIME DEFAULT NULL, approved_timestamp DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO user (id, username, display_name, email, password, password_timestamp, created_timestamp, verified_timestamp, approved_timestamp, roles) SELECT id, username, display_name, email, password, password_timestamp, created_timestamp, verified_timestamp, approved_timestamp, roles FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user_token AS SELECT id, created_timestamp, revoked_timestamp, expires_timestamp, user_id, type FROM user_token');
        $this->addSql('DROP TABLE user_token');
        $this->addSql('CREATE TABLE user_token (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , user_id VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, created_timestamp DATETIME NOT NULL, revoked_timestamp DATETIME DEFAULT NULL, expires_timestamp DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO user_token (id, created_timestamp, revoked_timestamp, expires_timestamp, user_id, type) SELECT id, created_timestamp, revoked_timestamp, expires_timestamp, user_id, type FROM __temp__user_token');
        $this->addSql('DROP TABLE __temp__user_token');
    }
}
