<?php

declare(strict_types=1);

namespace SalukiExample\DatabaseMigration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201211145100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'create notification log table';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE notification_log (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , timestamp DATETIME NOT NULL --(DC2Type:chronos_datetime)
        , channel VARCHAR(100) NOT NULL, "recipient" VARCHAR(1024) NOT NULL, reason VARCHAR(100) NOT NULL, subject VARCHAR(2048) DEFAULT NULL, details CLOB DEFAULT NULL --(DC2Type:json)
        , PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');
        $this->addSql('DROP TABLE notification_log');
    }
}
