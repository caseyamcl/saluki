<?php

declare(strict_types=1);

namespace SalukiExample\DatabaseMigration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181019190149 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE user_token (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , created_timestamp DATETIME NOT NULL, revoked_timestamp DATETIME DEFAULT NULL, expires_timestamp DATETIME DEFAULT NULL, user_id VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE setting (name VARCHAR(255) NOT NULL, value CLOB NOT NULL --(DC2Type:json)
        , PRIMARY KEY(name))');
        $this->addSql('CREATE TABLE email_template (name VARCHAR(255) NOT NULL, is_html BOOLEAN NOT NULL, from_address VARCHAR(255) DEFAULT NULL, reply_to VARCHAR(255) DEFAULT NULL, subject VARCHAR(1024) NOT NULL, body CLOB NOT NULL, PRIMARY KEY(name))');
        $this->addSql('CREATE TABLE user (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , username VARCHAR(255) NOT NULL, display_name VARCHAR(512) NOT NULL, email VARCHAR(512) NOT NULL, password VARCHAR(1024) DEFAULT NULL, password_timestamp DATETIME DEFAULT NULL, created_timestamp DATETIME NOT NULL, verified_timestamp DATETIME DEFAULT NULL, approved_timestamp DATETIME DEFAULT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE TABLE author (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , name VARCHAR(255) NOT NULL, created DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE book (id CHAR(36) NOT NULL --(DC2Type:uuid)
        , author_id CHAR(36) DEFAULT NULL --(DC2Type:uuid)
        , name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CBE5A331F675F31B ON book (author_id)');
        $this->addSql('CREATE TABLE oauth_client (id VARCHAR(64) NOT NULL, secret VARCHAR(512) NOT NULL, description CLOB DEFAULT NULL, created_timestamp DATETIME NOT NULL, allowed_ips CLOB NOT NULL --(DC2Type:json_array)
        , owner_id VARCHAR(255) DEFAULT NULL, system_key VARCHAR(512) DEFAULT NULL, enabled BOOLEAN NOT NULL, domain_names CLOB NOT NULL --(DC2Type:json)
        , PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE oauth_refresh_token (id VARCHAR(100) NOT NULL, access_token_id VARCHAR(255) DEFAULT NULL, expires DATETIME NOT NULL, created_date_time DATETIME NOT NULL, revoked DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_55DCF7552CCB2688 ON oauth_refresh_token (access_token_id)');
        $this->addSql('CREATE TABLE oauth_scope (name VARCHAR(100) NOT NULL, PRIMARY KEY(name))');
        $this->addSql('CREATE TABLE oauth_access_token (id VARCHAR(255) NOT NULL, client_id VARCHAR(64) DEFAULT NULL, user_id VARCHAR(255) NOT NULL, created_timestamp DATETIME NOT NULL, expires_timestamp DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F7FA86A419EB6921 ON oauth_access_token (client_id)');
        $this->addSql('CREATE TABLE oauth_access_token_oauth_scope (oauth_access_token_id VARCHAR(255) NOT NULL, scope_name VARCHAR(100) NOT NULL, PRIMARY KEY(oauth_access_token_id, scope_name))');
        $this->addSql('CREATE INDEX IDX_C84CCF84888114B4 ON oauth_access_token_oauth_scope (oauth_access_token_id)');
        $this->addSql('CREATE INDEX IDX_C84CCF84E3A4EE59 ON oauth_access_token_oauth_scope (scope_name)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE user_token');
        $this->addSql('DROP TABLE setting');
        $this->addSql('DROP TABLE email_template');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE author');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE oauth_client');
        $this->addSql('DROP TABLE oauth_refresh_token');
        $this->addSql('DROP TABLE oauth_scope');
        $this->addSql('DROP TABLE oauth_access_token');
        $this->addSql('DROP TABLE oauth_access_token_oauth_scope');
    }
}
