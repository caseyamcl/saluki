# Changelog

All notable changes to `saluki` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## v0.6.4 - 2022-12-20
### Changed
- Removed code deprecations

## v0.6.3 - 2022-12-14
### Fixed
- Bugfix to `JsonApiError` class that caused invalid response documents when errors were returned

## v0.6.2 - 2022-12-13
### Fixed
- Loosened up type constraints in Setting entity class

## V0.6.1 - 2022-12-13
### Fixed
- REVERT: `composer.json` allows either Monolog v2 or v3 

## v0.6 - 2022-12-13
### Fixed
- Added `webmozart/assert` as explicit dependency in `composer.json`
### Changed
- BREAKING: Now require minimum PHP 8.1
- BREAKING: Switched Doctrine config to use Attributes instead of Annotations
- BREAKING: Monolog upgraded to v3

## v0.5 - 2022-03-17
### Fixed
- Lots of upgrades and bugfixes...
- `console --dev` mode fix in `SalukiApp.php`

## v0.4 - 2022-03-08
### Fixed
- Clarify optional `basePath` parameter in `SalukiApp.php`
- Ensure `array_key_exists()` is run against actual array in `SalukiServiceDefinitionList.php`
- Clarify optional `resolverClassName` parameter in `UserResolverFactory.php`

### Changed
- BREAKING: Now require minimum PHP 8.0
- BREAKING: Updated `settings-manager` to v1 with updated API
- BREAKING: Too many to list (fortunately, we're only using it internally :)

## v0.3.7 - 2021-11-09
### Fixed
- Bugfix in `DbDebugLogger.php`

## v0.3.6 - 2021-06-14
### Changed
- Updated dependencies
- Replaced `mkopinsky/zxcvbn-php` with `bjeavons/zxcvbn-php` (since the former package was abandoned)
- Replaced `fzaninotto/faker` with `phpfaker/faker` (since the former was abandoned)
- Clarified that this library depends on Symfony ~v4.4 (LTS) in the `composer.json` file

### Removed
- `sensio/security-checker` development dependency (since it has been abandoned)
- Swoole library suggestion in `composer.json`

## v0.3.5 - 2021-03-02
### Fixed
- Error in `AuthEndpoint`: replace `RuntimeException` with `OAuthServerException` when authentication fails

## v0.3.4 - 2021-02-11
### Fixed
- Re-merge OAuthClientRepository (somehow it got out of sync with the copy in GitLab)

## v0.3.3 - 2021-02-11
### Fixed
- Allow NULL for relationship values

## v0.3.2 - 2021-01-19
### Fixed
- Refactor OAuthClientRepository to match workflow in new OAuth server library version

## v0.3.1 -2021-01-19
### Fixed
- Account for changed JWT library DX in auth endpoint

## v0.3 - 2021-01-19
### Changed
- Updated league/oauth-server to v8, as well as some other dependencies

## v0.2.2 - 2020-12-17
### Fixed
- correct column name in query builder (`NotificationLogEntryRepository::findNotificationsSentTo()`)

## v0.2.1 - 2020-12-17
## Fixed
- expose `$reason` parameter in EmailService and EmailTemplateBuilder classes
- actually return results from `NotificationLogEntryRepository::findNotificationsSentTo()`

## v0.2 - 2020-12-10
### Added
- Notification log (automatically populated by Emailer)

### Fixed
- Locked in version of `doctrine-uuid` 

## v0.1.32 - 2020-07-08
### Fixed
- Allow empty arrays in to-many relationship fields

## v0.1.31 - 2020-06-30
- Revert minimum PHP version requirement of v7.3 in `composer.json`

## v0.1.30 - 2020-06-30
### Fixed
- Bugfixes where the field name doesn't match the property name

## v0.1.29 - 2020-04-16
### Fixed
- Made fix to set constructor arguments with default values to their default value in FieldListMapper

## v0.1.28 - 2020-03-31
### Fixed
- Made Alphanumeric validation rule work with empty strings

## v0.1.27 - 2020-03-24
### Changed
- Remove `ext-swoole` for requirements in `composer.json` and instead move it to suggested packages

## v0.1.26 - 2019-04-25
### Fixed
- Auth endpoint now allows 'refresh_token' parameter

## v0.1.25 - 2019-04-23
### Changed
- Use `mkopinsky/zxcvbn-php` instead of `bjeavons/zxcvbn-php` to match Javascript algorithm more closely (should change
  back to `bjeavons` library when [PR #32](https://github.com/bjeavons/zxcvbn-php/pull/32) is merged and released.

## v0.1.24 - 2019-04-22
### Fixed
- Re-added erroneously removed ORM namespace import in EmailTemplateEntity

## v0.1.23 - 2019-04-22
### Fixed
- Fixed bugs with new Configula interface class
- Fixed up a number of other small items

## v0.1.22 - 2019-04-11
### Changed
- Upgrade to Configula v3 stable (and a few other minor updates)
- Upgrade to PHPUnit v8 (and ignore PHPUnit cache file in repo)

## v0.1.21 - 2019-04-01
### Changed
- BC BREAK: Updated interface `ErrorReporterInterface` to now accept optional second `$extra` parameter
- Improved error reporting in web requests to include some information about the request.

## v0.1.20 - 2019-03-29
### Changed
- Reverted EntityEventMapper to use prePersist, preRemove, preUpdate

## v0.1.19 - 2019-03-03
### Changed
- Signature change to RelatedRecordValidator

## v0.1.18 - 2019-03-03
### Fixed
- Fixed references to Twig classes using new PSR-syntax

## v0.1.17 - 2019-03-13
### Changed
- Improvements to related record validator to allow for heterogeneous targets when creating/updating relationships

## v0.1.16 - 2019-02-27
### Fixed
- FieldList now unsets defaults for all parameters on update (PATCH) so that defaults do not clobber actual values.

## v0.1.15 - 2019-02-26
### Fixed
- FieldListMapper now checks for presence of constructor parameter in the request before it attempts to look it up. 

## v0.1.14 - 2019-02-20
### Fixed
- Log errors during bootstrap even if container not built yet.

## v0.1.13 - 2019-02-12
### Fixed
- CORS middleware expose all headers by default
## v0.1.12 - 2019-02-12
### Fixed
- Allow common headers for CORS by default
- Add logger to CORS middleware constructor

## v0.1.11 - 2019-02-07
### Removed
-  Ill-thought-out `AbstactEntityRepository::iterateAll()` to iterate over large collections of entities
### Added
-  `AbstactEntityRepository::listIds()` to iterate over IDs in a repository 

## v0.1.10 - 2019-02-07
### Added
- `TransactionManager` now passes entity manager to callable

## v0.1.9 - 2019-02-07
### Added
- `AbstactEntityRepository::iterateAll()` to iterate over large collections of entities
### Changed
- `AbstractEntityRepository::countAll()` accepts optional callback to modify query before running it

## v0.1.8 - 2019-02-04
### Changed
- OAuthUserRepository now throws a custom exception (`InvalidAccessTokenException`), which is caught in OAuthMiddleware
  when a valid access token fails to resolve to a user. 

## v0.1.7 - 2019-02-01
### Fixed
- Bugfix to Validator email local (don't append @example.org to actual value)

## v0.1.6 - 2019-02-01
### Fixed
- Bugfix to Validator email local

## v0.1.5 - 2019-01-31
### Fixed
- Small bugfix to ErrorReporterRegistry

## v0.1.4 - 2019-01-31
### Added
- `ErrorReporter` module
### Changed
- `HttpErrorMiddleware` changed to `ErrorHandlerMiddleware`, and it handles
   and reports all errors correctly
- Various changes to the Console runner to include better error reporting
### Removed
- `SalukiApp::boot()` no longer needs the `setup` parameter.
- `SalukiApp::logWebRequestHandler()`; instead add an error handler

## v0.1.3 - 2019-01-30
### Fixed
- Swoole handles exceptions better

## v0.1.2 - 2019-01-30
### Added
- Request/Response logging in the Swoole `http:serve` endpoint
### Fixed
- Doctrine bugfix: generate proxy files only if the do not exist in production (vs never)

## v0.1.1 - 2019-01-29 
### Added
- New interface `FindUserByIdentifer` to make overriding default user sub-system easier
- New method `DoctrineResourceFields::setIdGetter()` to allow per-resource custom ID fields
- New query filter: `NotEmptyQueryFilter`
- Json API Serializer now always includes pagination information for non-single record responses
- New `ValidatorContext` and three new rules `CustomRule`, `OtherFieldExists` and `OtherFieldAbsent`
- Callback and Custom validators now have access to the ValidatorContext
- All endpoints (including auth endpoint) now add the user roles to the response object as `X-User-Roles`
- Added `hasQueryParam` and `hasBodyParm` to `SalukiRequest`
- Added `validUuid` validator
- Added `ValidatorRuleSet::prependRule()`
- Check valid Uuid for most relationships
- Added `inputIsPiped` method to `ConsoleIO` class
- New setup step: `DbGenerateProxyClasses`
- Added `ParameterList::allowUndefinedFields`
- New validator: `UnixPath`
- Added ability to specify ID_PROPERTY_NAME in AbstractRestResource.
- Added FieldAccessDeniedException to return appropriate responses when a user tries to write to a 
  field they don't have access to.
### Changed
- Updated `addYamlFile()` method signature in SalukiBuilder to allow optional configuration files
- Renamed `RequireAuthMiddleware` to `RequireUserMiddleware`
- Simplified logic in `OAuthUserRepository::resolveAccessTokenToUser()` and added exception 
- Moved guessing of `User::getPassword()` method to the UserEntityResolver and simplified the PasswordService so that
  it can be used in a broader context.
- Updated the way that `DoctrineResourceFields` gets the ID from the entity in order to make it more flexible.
- Made `getAllowedSortFields` an abstract method in `AbstractIndexEndpoint` so that child classes must implement it
- Added `IfOtherField` and `IfNotOtherField` validators
- Validator no longer runs transformations on default values
- Validator requires passing by reference when calling `getErrors` so that built-in transformations correctly mutate
  value
- Made the primary RouteGroup a service in the container so we can use it in other services.
- Refactored setup and rest resource fields
- Made DbUpToDateSetupStep run before container is built
### Fixed
- Entering password for admin user setup step is now masked/hidden correctly
- `DbConfig` automatic prefix of `pdo_` for certain drivers now works
- Added `RequireOAuthMiddleware` to check for any valid (even anonymous) OAuth session
- Bugfix to `QueryFilterMapper::mapSearch()` argument unpacking
- `JsonApiSerializer` handles NULL relationships a little better
- Made `$propertyName` parameter optional for `QueryFieldMapper::mapRelatedRecordCount()`
- Fixed bug with number of parameters in `SearchQueryFilter`
- Non-valid type conversions will be caught in the `Entity` validator type.
- Modulo by zero bug in Pagination info squashed
- Use transaction manager in `SetupAdminUserStep` to ensure changes are persisted
- Bugfix to DoctrineResourceFields exception condition.
- Bugfix to BooleanQueryFilter for `false` values
- `UserResourceFields` passing values to writable works correctly
- `UserResourceFields` hashes passwords when they are submitted
- After transformers run on fields
- Small bugfix to regex used in index endpoint `filter` parameter
- Cascade rules for OAuth entities
- Fixed encryption factory
- Fixed `OauthClientRepository::getClientEntity()` when encryption is enabled
- Fixed bug where FieldListNormalizer wasn't getting the ID property name correctly
### Removed
- RelatedRecordCount Query filter (for now).  This is a bit more complicated, and probably needs to be handled on
  a case-by-case basis.

## v0.1 - 2018-09-18
### Added
- Everything - initial development release
